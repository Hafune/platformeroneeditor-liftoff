package mygdx.game.light

import box2dLight.PositionalLight
import box2dLight.RayHandler
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter
import mygdx.game.TileMapRender
import mygdx.game.editor.UndoRedo.addAction
import mygdx.game.lib.MyMath
import org.w3c.dom.Document
import org.w3c.dom.Element

class LightMap(private val camera: OrthographicCamera) {
    private val LightParam = "LightParam"
    private val type = "type"
    private val mapX = "mapX"
    private val mapY = "mapY"
    private val rayHandler = RayHandler(World(Vector2(0f, 0f), false))
    private var targetLight = .5f
    private var currentLight = targetLight
    private val characterLight: LampLight
    private val dayLight: DayLight
    private val lampRays = 10
    var map: Array<Array<PositionalLight?>>? = null
        private set
    private val width = TileMapRender.TILE_WIDTH
    private val height = TileMapRender.TILE_HEIGHT

    fun update() {
        currentLight = MyMath.tendsToValue(currentLight, targetLight, .015f, 0f)
        rayHandler.setAmbientLight(currentLight)
        rayHandler.setCombinedMatrix(camera)
        dayLight.update()
        rayHandler.updateAndRender()
    }

    fun useCustomViewport(x: Int, y: Int, width: Int, height: Int) {
        rayHandler.useCustomViewport(x, y, width, height)
    }

    fun refreshLocationLight(name: String) {
        when (name) {
            "PrisonCave" -> {
                targetLight = .5f
                dayLight.setTargetAlpha(0f)
            }
            "Mountain" -> {
                targetLight = 1f
                dayLight.setTargetAlpha(.92f)
            }
            "FirstTown" -> {
                targetLight = 1f
                dayLight.setTargetAlpha(.9f)
            }
            else -> {
                targetLight = 1f
                dayLight.setTargetAlpha(.92f)
            }
        }
    }

    private fun buildLight(type: Lights, x: Int, y: Int) {
        if (type == Lights.Lamp) {
            buildLampLight(x, y)
        }
    }

    fun buildLampLight(x: Int, y: Int) {
        val lamp = LampLight(rayHandler, lampRays)
        lamp.setPosition(x * width + width / 2f, y * height + height / 2f)
        map!![x][y] = lamp
    }

    val lampLight: LampLight
        get() = LampLight(rayHandler, lampRays)

    fun removeLamps() {
        if (map != null) {
            for (positionalLights in map!!) {
                for (y in 0 until map!![0].size) {
                    if (positionalLights[y] != null) positionalLights[y]!!.remove()
                }
            }
        }
    }

    fun createNewMap(width: Int, height: Int) {
        map = Array(width) { arrayOfNulls(height) }
    }

    fun editLightMap(type: Lights, x: Int, y: Int) {
        val backObject = getLightsType(x, y)
        if (backObject != type) {
            if (map!![x][y] != null) map!![x][y]!!.remove()
            map!![x][y] = null
            buildLight(type, x, y)
            val forwardData = EditLightData(type, x, y)
            val backData = EditLightData(backObject, x, y)
            addAction({ lightUndoRedoCallBack(forwardData) }, {
                lightUndoRedoCallBack(
                    backData
                )
            })
        }
    }

    private fun lightUndoRedoCallBack(`object`: Any) {
        val data = `object` as EditLightData
        val x = data.x
        val y = data.y
        if (map!![x][y] != null) map!![x][y]!!.remove()
        map!![x][y] = null
        buildLight(data.type, x, y)
    }

    fun saveParam(document: Document): Element {
        val param = document.createElement(LightParam)
        for (x in map!!.indices) {
            for (y in 0 until map!![0].size) {
                if (map!![x][y] != null) {
                    val light = document.createElement("itemModel")
                    light.setAttribute(mapX, x.toString())
                    light.setAttribute(mapY, y.toString())
                    light.setAttribute(type, getLightsType(x, y).toString())
                    param.appendChild(light)
                }
            }
        }
        return param
    }

    fun exportGdxXml(document: XmlWriter) {
        val param = document.element(LightParam)
        for (x in map!!.indices) {
            for (y in 0 until map!![0].size) {
                if (map!![x][y] != null) {
                    val light = document.element("itemModel")
                    light.attribute(mapX, x.toString())
                    light.attribute(mapY, y.toString())
                    light.attribute(type, getLightsType(x, y).toString())
                    light.pop()
                }
            }
        }
        param.pop()
    }

    private fun getLightsType(x: Int, y: Int): Lights {
        var l = Lights.NoLight
        if (map!![x][y] != null) {
            if (map!![x][y] is LampLight) l = Lights.Lamp
        }
        return l
    }

    fun load(root: XmlReader.Element) {
        if (root.hasChild(LightParam)) {
            val maps = root.getChildByName(LightParam)
            for (i in 0 until maps.childCount) {
                val light = maps.getChild(i)
                val mapX = light.getAttribute(mapX).toInt()
                val mapY = light.getAttribute(mapY).toInt()
                val type = Lights.valueOf(light.getAttribute(type))
                buildLight(type, mapX, mapY)
            }
        }
    }

    init {
        rayHandler.setAmbientLight(targetLight)
        characterLight = LampLight(rayHandler, lampRays)
        dayLight = DayLight(rayHandler, camera)
    }
}