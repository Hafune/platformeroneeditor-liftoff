package mygdx.game.light

import box2dLight.ConeLight
import box2dLight.RayHandler
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import mygdx.game.lib.MyMath.tendsToValue
import java.util.function.Consumer

class DayLight(rayHandler: RayHandler,private val camera: OrthographicCamera) {

    private val coneLights = ArrayList<ConeLight>()
    private val len = ArrayList<Int>()
    private val direct = ArrayList<Int>()
    private var targetAlpha = .85f
    private val currentColor = Color(1f, 1f, 1f, .85f)
    fun update() {
        val x: Float = camera.position.x - camera.viewportWidth * camera.zoom / 2 - 700
        val y: Float = camera.position.y - camera.viewportHeight * camera.zoom / 2 + 700
        currentColor.a = tendsToValue(currentColor.a, targetAlpha, .015f, 0f)
        coneLights.forEach(Consumer { c: ConeLight -> c.color = currentColor })
        for (i in len.indices) {
            if (len[i] + direct[i] > 3060 ||
                len[i] + direct[i] < 1840
            ) {
                direct[i] = -direct[i]
            }
            len[i] = len[i] + direct[i]
            coneLights[i].distance = len[i].toFloat()
        }
        coneLights.forEach(Consumer { c: ConeLight -> c.setPosition(x, y) })
    }

    fun setTargetAlpha(targetLight: Float) {
        targetAlpha = targetLight
    }

    init {
        val count = 40 / 20
        var start = 300
        for (i in 0..19) {
            val coneLight = ConeLight(rayHandler, 3, currentColor, 2450f, 0f, 0f, 320f, 1f)
            coneLight.direction = (start + Math.random() * 2).toFloat()
            start += count
            coneLights.add(coneLight)
            len.add((2450 - 600 + Math.random() * 1200).toInt())
            direct.add(6)
        }
    }
}