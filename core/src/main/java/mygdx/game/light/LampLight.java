package mygdx.game.light;

import box2dLight.PointLight;
import box2dLight.RayHandler;

public class LampLight extends PointLight
{
    private float fluctuation = .05f;
    private float currentRadius;
    private float radius = 350;
    private float time = 1.5f;
    private int speedDirection = 1;

    public LampLight(RayHandler rayHandler, int rays)
    {
        super(rayHandler, rays);
        setColor(0, 0, 0, 0.7f);
        setDistance(radius);
    }

    public void setFluctuation(float fluctuation)
    {
        this.fluctuation = fluctuation;
    }

    public void setTime(float time)
    {
        this.time = time;
    }

    @Override
    public void update()
    {
        super.update();
        if (Math.abs(currentRadius - radius) > radius * fluctuation) {
            if (currentRadius > radius) speedDirection = -1;
            else speedDirection = 1;
        }
        float speed = radius * fluctuation / (60 / time) * 2 * speedDirection;
        currentRadius += speed;
        super.setDistance(currentRadius);
    }

    @Override
    public void setDistance(float dist)
    {
        super.setDistance(dist);
        radius = dist;
        currentRadius = dist;
        speedDirection = 1;
    }
}
