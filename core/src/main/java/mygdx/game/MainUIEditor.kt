package mygdx.game

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.kotcrab.vis.ui.VisUI
import mygdx.game.editor.widgetEditor.ActorBuilder
import mygdx.game.editor.widgetEditor.WidgetEditorWin
import mygdx.game.lib.EditorPositionsUiFileHandler
import mygdx.game.lib.actorToCenter
import mygdx.game.uiSkins.UiSkins


class MainUIEditor : ApplicationAdapter() {

    private val interfaceRoot = Group()
    private lateinit var stage: Stage
    private lateinit var camera: OrthographicCamera

    override fun create() {

        VisUI.load()
        stage = Stage(ScreenViewport())
        Gdx.input.inputProcessor = stage
        camera = stage.camera as OrthographicCamera

        val data = EditorPositionsUiFileHandler.findSettingOrCreate(ScreenContainer.windowSettingName)

        if (data.width == 0f) {
            data.width = ScreenContainer.appScreenWidth
            data.height = ScreenContainer.appScreenHeight
        }
        Gdx.graphics.setWindowedMode(data.width.toInt(), data.height.toInt())

        interfaceRoot.width = ScreenContainer.appScreenWidth
        interfaceRoot.height = ScreenContainer.appScreenHeight
        stage.addActor(interfaceRoot)

        val path = "UiMoneyTimeHint.json"
        val editor = WidgetEditorWin("ui/$path")
        editor.setupGroup(interfaceRoot)
        ActorBuilder(UiSkins.skin)
    }

    override fun render() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT)

        camera.actorToCenter(interfaceRoot)

        stage.act()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun pause() {}

    override fun resume() {}
    override fun dispose() {}
}