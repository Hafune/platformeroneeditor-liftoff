package mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.TimeUtils

class Print : Disposable {

    private var lastTimeCounted: Long
    private val font: BitmapFont
    private val batch: SpriteBatch
    private var cam: OrthographicCamera

    private val messages = ArrayList<String>()
    private val times = ArrayList<Float>()

    private fun resize(screenWidth: Int, screenHeight: Int) {
        cam = OrthographicCamera(screenWidth.toFloat(), screenHeight.toFloat())
        cam.translate((screenWidth / 2).toFloat(), (screenHeight / 2).toFloat())
        cam.update()
        batch.projectionMatrix = cam.combined
    }

    private fun update() {
        if (cam.viewportWidth.toInt() != Gdx.graphics.width || cam.viewportWidth.toInt() != Gdx.graphics.height) {
            resize(Gdx.graphics.width, Gdx.graphics.height)
        }

        val delta = TimeUtils.timeSinceMillis(lastTimeCounted)
        lastTimeCounted = TimeUtils.millis()

        for (i in times.size - 1 downTo 0) {
            times[i] = times[i] - delta.toFloat() / 1000f
            if (times[i] <= 0) {
                times.removeAt(i)
                messages.removeAt(i)
            }
        }
    }

    fun print(s: Any?) {
        println(s)
        times.add(3f)
        messages.add(s.toString())

        if (times.size > 30) {
            times.removeFirst()
            messages.removeFirst()
        }
    }

    fun render() {
        update()
        batch.begin()
        var y = 100f

        messages.forEach { s ->
            font.draw(batch, s, 3f, (Gdx.graphics.height - y))
            y += font.lineHeight
        }
        batch.end()
    }

    override fun dispose() {
        font.dispose()
        batch.dispose()
    }

    init {
        lastTimeCounted = TimeUtils.millis()
        font = BitmapFont()
        batch = SpriteBatch()
        cam = OrthographicCamera(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
    }
}