package mygdx.game.dataBase

import mygdx.game.dataBase.services.usersService.Users
import org.jetbrains.exposed.sql.Table

object UserSwitches : Table("user_switches") {

    val user_id = integer("user_id").references(Users.id)
    val switch_id = integer("switch_id")
    val type_key = varchar("type_key",50)
}