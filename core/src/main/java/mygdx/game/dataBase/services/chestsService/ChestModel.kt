package mygdx.game.dataBase.services.chestsService

import mygdx.game.dataBase.IntModel
import mygdx.game.dataBase.services.itemsService.ItemModel

class ChestModel(override val map: HashMap<String, Any?>) : IntModel(map) {

    override var id: Int by map

    var itemModels = ArrayList<ItemModel>()
}
