package mygdx.game.dataBase.services.doorsService

import mygdx.game.dataBase.UserSwitches
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class DoorsService {

    fun getByUserId(user_id: Int): HashSet<Int> {
        return transaction {
            val list = UserSwitches.select {
                UserSwitches.type_key eq "door" and (UserSwitches.user_id eq user_id)
            }.toList()
            hashSetOf(*list.map { it[UserSwitches.switch_id] }.toTypedArray())
        }
    }

    fun saveForUser(user_id: Int, doors: HashSet<Int>) {
        transaction {
            UserSwitches.deleteWhere {
                UserSwitches.user_id eq user_id and (UserSwitches.type_key eq "door")
            }
            doors.forEach { id ->
                UserSwitches.insert {
                    it[UserSwitches.user_id] = user_id
                    it[switch_id] = id
                    it[type_key] = "door"
                }
            }
        }
    }
}
