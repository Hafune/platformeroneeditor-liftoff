package mygdx.game.dataBase.services.characterService

import mygdx.game.dataBase.IntModel

class CharacterModel(override val map: HashMap<String, Any?>) : IntModel(map) {
    override var id: Int by map
    var user_id: Int? by map
    val level: Int by map
    val name: String by map
    val card_background: String by map
    val control: String by map
    val player_id: Int by map
}
