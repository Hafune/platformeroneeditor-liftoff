package mygdx.game.dataBase.services.storesService

import mygdx.game.dataBase.StringModel
import mygdx.game.dataBase.services.itemsService.ItemModel

class StoreModel(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map

    var itemModels = ArrayList<ItemModel>()
}
