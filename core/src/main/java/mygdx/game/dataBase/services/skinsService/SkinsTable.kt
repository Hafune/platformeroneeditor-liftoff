package mygdx.game.dataBase.services.skinsService

import org.jetbrains.exposed.dao.id.IntIdTable

object SkinsTable : IntIdTable("skins") {
    val name = varchar("name")
    val idx = integer("idx")
    val type_key = varchar("type_key").references(SkinTypes.key)
    val editor_group = varchar("editor_group")
    val src = varchar("src")
    val src_body = varchar("src_body").nullable()
    val main_skin_id = integer("main_skin_id").references(SkinsTable.id).nullable()

    private fun varchar(name: String) = varchar(name, 200)
}