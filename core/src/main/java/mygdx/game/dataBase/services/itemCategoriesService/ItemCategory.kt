package mygdx.game.dataBase.services.itemCategoriesService

import mygdx.game.dataBase.StringModel

class ItemCategory(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map
    val show_in_stores: Boolean by map
    val icon_id: Int by map
}