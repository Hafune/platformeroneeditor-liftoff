package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.StringModel

class StateModel(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map
}