package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.IntModel

class SkinModel(override val map: HashMap<String, Any?>) : IntModel(map) {

    override var id: Int by map
    var name: String by map
    var idx: Int by map
    var type_key: String by map
    var editor_group: String by map
    var src: String by map
    var src_body: String? by map
    var main_skin_id: Int? by map

    lateinit var type: SkinTypeModel
}