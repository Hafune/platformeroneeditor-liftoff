package mygdx.game.dataBase.services.chestsService

import mygdx.game.dataBase.services.itemsService.Items
import org.jetbrains.exposed.sql.Table

object ChestItems : Table("chest_items") {

    val chest_id = integer("chest_id").references(Chests.id)
    val item_id = integer("item_id").references(Items.id)
    val capacity = integer("capacity")
}