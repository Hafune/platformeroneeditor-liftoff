package mygdx.game.dataBase.services.itemsService

import mygdx.game.dataBase.IntModel

class ItemModel(override val map: HashMap<String, Any?>) : IntModel(map) {

    override var id: Int by map
    val name: String by map
    val icon_id: Int by map
    val price: Int by map
    val use_mechanic: String by map
    val max_capacity: Int by map
    val category_key: String by map

    var capacity: Int = 0
}