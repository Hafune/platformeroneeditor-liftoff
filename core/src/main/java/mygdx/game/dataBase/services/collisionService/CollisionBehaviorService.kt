package mygdx.game.dataBase.services.collisionService

import mygdx.game.dataBase.ServiceStringKey
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class CollisionBehaviorService : ServiceStringKey<CollisionBehaviorModel>(CollisionBehavior) {

    fun getAll(): Array<CollisionBehaviorModel> {
        return transaction {
            CollisionBehavior.selectAll().toList().map {
                createOneModel(CollisionBehaviorModel::class, it)
            }.toTypedArray()
        }
    }

    fun getList(): List<Pair<String, String>> {
        return transaction {
            CollisionCollide.selectAll().toList()
                .map { it[CollisionCollide.collide_key] to it[CollisionCollide.owner_key] }
        }
    }

    fun getGroups(): List<String> {
        return transaction {
            CollisionBehavior.selectAll().toList()
                .map { it[CollisionBehavior.key] }
        }
    }
}
