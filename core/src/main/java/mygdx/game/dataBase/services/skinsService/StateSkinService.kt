package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.Service
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class StateSkinService : Service<StateSkinModel>(StateSkinLink) {

    fun getSkinIdForState(state: String, main_skin_id: Int): Int {
        return transaction {
            val data = StateSkinLink.select {
                StateSkinLink.state_key eq state and (StateSkinLink.main_skin_id eq main_skin_id)
            }.execute(this)!!

            data.getInt(StateSkinLink.skin_id.name)
        }
    }
}
