package mygdx.game.dataBase.services.iconsService

import mygdx.game.dataBase.ServiceStringKey
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class IconTypesService : ServiceStringKey<IconType>(IconTypes) {

    fun getTypes(): ArrayList<IconType> {
        return transaction {
            val list = IconTypes.selectAll().toList()
            val models = createManyModels(IconType::class, list)

            arrayListOf(*models.toTypedArray())
        }
    }
}
