package mygdx.game.dataBase.services.storesService

import mygdx.game.dataBase.services.itemsService.Items
import org.jetbrains.exposed.sql.Table

object StoreItems : Table("store_items") {

    val store_key = varchar("store_key", 50).references(Stores.key)
    val item_id = integer("item_id").references(Items.id)
}