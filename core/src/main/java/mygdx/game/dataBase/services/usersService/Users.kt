@file:Suppress("unused")

package mygdx.game.dataBase.services.usersService

import mygdx.game.dataBase.services.iconsService.Icons
import mygdx.game.dataBase.services.skinsService.SkinsTable
import org.jetbrains.exposed.dao.id.IntIdTable

object Users : IntIdTable("users") {

    val on_load_action = varchar("on_load_action")
    val icon_id = integer("icon_id").references(Icons.id)
    val skin_id = integer("skin_id").references(SkinsTable.id)

    val x = float("x")
    val y = float("y")
    val name = varchar("name")
    val location = varchar("location")
    val variables_json = varchar("variables_json",100000)
    val game_time = integer("game_time")

    private fun varchar(name: String) = varchar(name, 50)
}