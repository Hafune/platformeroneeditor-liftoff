package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.StringModel

class SkinTypeModel(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map
    var skin_class: String by map
    val width: Int by map
    val height: Int by map
    val origin_x: Float by map
    val origin_y: Float by map
    val radius: Float by map
}