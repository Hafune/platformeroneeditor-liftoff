package mygdx.game.dataBase.services.audioPropertiesService

import mygdx.game.dataBase.services.usersService.Users
import org.jetbrains.exposed.dao.id.IntIdTable

object AudioProperties : IntIdTable("audio_properties") {
    val user_id = integer("user_id").references(Users.id)
    val volume = float("volume")
}