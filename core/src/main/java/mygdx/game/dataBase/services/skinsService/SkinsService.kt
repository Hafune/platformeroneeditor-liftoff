package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.ServiceIntId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class SkinsService : ServiceIntId<SkinModel>(SkinsTable) {

    private val skinTypesService = SkinTypesService()
    val stateSkinService = StateSkinService()

    fun getSkins(): ArrayList<SkinModel> {
        return transaction {
            val list = SkinsTable.selectAll().toList()
            val models = createManyModels(SkinModel::class, list)

            models.forEach {
                val typeRow = SkinTypes.select { SkinTypes.key eq it.type_key }.execute(this)!!
                val model = skinTypesService.createOneModel(SkinTypeModel::class, typeRow)
                it.type = model
            }

            arrayListOf(*models.toTypedArray())
        }
    }

    fun getFirstByName(name: String): SkinModel {
        return transaction {
            val list = SkinsTable.select { SkinsTable.name eq name }.limit(1).execute(this)!!
            val model = createOneModel(SkinModel::class, list)

            val typeRow = SkinTypes.select { SkinTypes.key eq model.type_key }.execute(this)!!
            model.type = skinTypesService.createOneModel(SkinTypeModel::class, typeRow)
            model
        }
    }

    public override fun insert(model: SkinModel): SkinModel {
        return super.insert(model)
    }
}
