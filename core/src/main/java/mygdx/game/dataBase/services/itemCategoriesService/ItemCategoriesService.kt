package mygdx.game.dataBase.services.itemCategoriesService

import mygdx.game.dataBase.ServiceStringKey
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class ItemCategoriesService : ServiceStringKey<ItemCategory>(ItemCategories) {

    fun getCategoriesForStores(): ArrayList<ItemCategory> {
        return transaction {
            val list = ItemCategories.selectAll().toList()
            val models = createManyModels(ItemCategory::class, list)
            arrayListOf(*models.toTypedArray())
        }
    }
}
