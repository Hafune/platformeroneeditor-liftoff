@file:Suppress("unused")
package mygdx.game.dataBase.services.iconsService

import org.jetbrains.exposed.dao.id.IntIdTable

object Icons : IntIdTable("icons") {
    val name = varchar("name")
    val idx = integer("idx")
    val type_key = varchar("type_key").references(IconTypes.key)
    val src = varchar("src")

    private fun varchar(name: String) = varchar(name, 200)
}