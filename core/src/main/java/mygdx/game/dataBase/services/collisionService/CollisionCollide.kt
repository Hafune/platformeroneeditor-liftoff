package mygdx.game.dataBase.services.collisionService

import org.jetbrains.exposed.sql.Table

object CollisionCollide : Table("collision_collide") {
    val owner_key = varchar("owner_key", 50).references(CollisionBehavior.key)
    val collide_key = varchar("collide_key", 50).references(CollisionBehavior.key)
}