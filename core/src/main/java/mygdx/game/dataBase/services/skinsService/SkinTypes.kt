package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.StringKeyTable

object SkinTypes : StringKeyTable("skin_types") {

    val skin_class = varchar("skin_class")
    val width = integer("width")
    val height = integer("height")
    val origin_x = float("origin_x")
    val origin_y = float("origin_y")
    val radius = float("radius")

    private fun varchar(name: String) = varchar(name, 50)
}