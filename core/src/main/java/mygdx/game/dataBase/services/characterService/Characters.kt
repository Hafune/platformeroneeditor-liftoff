package mygdx.game.dataBase.services.characterService

import mygdx.game.dataBase.services.usersService.Users
import org.jetbrains.exposed.dao.id.IntIdTable

object Characters : IntIdTable("characters") {
    val user_id = integer("user_id").references(Users.id).nullable()
    val level = integer("level")
    val name = varchar("name",32)
    val card_background = varchar("card_background",250)
    val control = varchar("control",20)
    val player_id = integer("player_id")
}