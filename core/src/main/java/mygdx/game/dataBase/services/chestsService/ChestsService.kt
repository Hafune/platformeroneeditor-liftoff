package mygdx.game.dataBase.services.chestsService

import mygdx.game.dataBase.ServiceIntId
import mygdx.game.dataBase.UserSwitches
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class ChestsService : ServiceIntId<ChestModel>(Chests) {

    fun getByUserId(user_id: Int): ArrayList<ChestModel> {
        return transaction {
            val list = Chests.join(UserSwitches, JoinType.INNER, additionalConstraint = {
                UserSwitches.user_id eq user_id and (UserSwitches.switch_id eq Chests.id) and (UserSwitches.type_key eq "chest")
            }).selectAll().toList()

            arrayListOf(*createManyModels(ChestModel::class, list).toTypedArray())
        }
    }

    fun saveForUser(user_id: Int, chestModels: List<ChestModel>) {
        transaction {
            val ids = chestModels.map { it.id }
            UserSwitches.deleteWhere {
                UserSwitches.user_id eq user_id and (UserSwitches.type_key eq "chest")
            }
            ids.forEach { chest_id ->
                UserSwitches.insert {
                    it[UserSwitches.user_id] = user_id
                    it[switch_id] = chest_id
                    it[type_key] = "chest"
                }
            }
        }
    }

    fun getByLocation(location: String): ArrayList<ChestModel> {
        return transaction {
            val list = Chests.select { Chests.location eq location }.toList()
            arrayListOf(*createManyModels(ChestModel::class, list).toTypedArray())
        }
    }
}
