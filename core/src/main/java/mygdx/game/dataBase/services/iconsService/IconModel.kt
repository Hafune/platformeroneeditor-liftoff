package mygdx.game.dataBase.services.iconsService

import mygdx.game.dataBase.IntModel

class IconModel(override val map: HashMap<String, Any?>) : IntModel(map) {

    override var id: Int by map
    val name: String by map
    val idx: Int by map
    val type_key: String by map
    val src: String by map

    lateinit var type: IconType
}