package mygdx.game.dataBase.services.itemsService

import mygdx.game.dataBase.services.usersService.Users
import org.jetbrains.exposed.sql.Table

object UserItems : Table("user_items") {

    val user_id = integer("user_id").references(Users.id).nullable()
    val item_id = integer("item_id").references(Items.id)
    val capacity = integer("capacity")
}