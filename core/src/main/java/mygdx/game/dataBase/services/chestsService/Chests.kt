package mygdx.game.dataBase.services.chestsService

import org.jetbrains.exposed.dao.id.IntIdTable

object Chests : IntIdTable("chests") {
    val name = varchar("name")
    val location = varchar("location")

    private fun varchar(name: String) = varchar(name, 50)
}