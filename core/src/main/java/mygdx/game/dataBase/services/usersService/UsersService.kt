package mygdx.game.dataBase.services.usersService

import mygdx.game.dataBase.ServiceIntId
import mygdx.game.lib.MyObjectMapper
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class UsersService : ServiceIntId<UserModel>(Users) {

    @Suppress("UNCHECKED_CAST")
    fun getUsers(): List<UserModel> {
        return transaction {
            val list = Users.select { Users.name notInList listOf("NewGame", "Tmp") }.toList()

            val models = createManyModels(UserModel::class, list)
            models.forEach {
                it.variables =
                    MyObjectMapper.readValue(it.variables_json, HashMap::class.java) as HashMap<String, Any>
            }
            models
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun getUserRawByName(name: String): UserModel {
        return transaction {
            val data = Users.select { Users.name eq name }.limit(1).execute(this)!!

            val model = createOneModel(UserModel::class, data)
            model.variables =
                MyObjectMapper.readValue(model.variables_json, HashMap::class.java) as HashMap<String, Any>
            model
        }
    }

    fun newRaw(): UserModel {
        return newRaw(UserModel::class)
    }
}
