package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.Model

class StateSkinModel(override val map: HashMap<String, Any?>) : Model {

    var state_key: String by map
    val owner_skin_id: Int by map
    val skin_id: Int by map
}