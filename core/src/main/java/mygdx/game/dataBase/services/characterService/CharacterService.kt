package mygdx.game.dataBase.services.characterService

import mygdx.game.dataBase.ServiceIntId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class CharacterService : ServiceIntId<CharacterModel>(Characters) {

    fun getByUserId(id: Int): List<CharacterModel> {
        return transaction {
            val data = Characters.select { Characters.user_id eq id }.toList()
            if (data.isEmpty()) {
                val model = newRaw(CharacterModel::class)
                model.user_id = id
                update(model)
                return@transaction listOf(model)
            }
            createManyModels(CharacterModel::class, data)
        }
    }
}
