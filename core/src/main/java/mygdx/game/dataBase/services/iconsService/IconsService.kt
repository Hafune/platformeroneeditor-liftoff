package mygdx.game.dataBase.services.iconsService

import mygdx.game.dataBase.ServiceIntId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class IconsService : ServiceIntId<IconModel>(Icons) {

    val iconTypesService = IconTypesService()

    fun getIcons(): ArrayList<IconModel> {
        return transaction {
            val list = Icons.selectAll().toList()
            val models = createManyModels(IconModel::class, list)

            models.forEach {
                val type = IconTypes.select { IconTypes.key eq it.type_key }.execute(this)!!
                val model = iconTypesService.createOneModel(IconType::class, type)
                it.type = model
            }

            arrayListOf(*models.toTypedArray())
        }
    }
}
