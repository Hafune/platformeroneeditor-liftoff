package mygdx.game.dataBase.services.usersService

import mygdx.game.dataBase.IntModel
import java.util.Date

class UserModel(override val map: HashMap<String, Any?>) : IntModel(map) {

    override var id: Int by map
    var on_load_action: String by map
    var icon_id: Int by map
    var skin_id: Int by map
    var x: Float by map
    var y: Float by map
    var name: String by map
    var location: String by map
    var variables_json: String by map
    var game_time: Long by map

    val startTime = Date().time
    lateinit var variables: HashMap<String, Any>
}