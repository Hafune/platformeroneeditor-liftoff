package mygdx.game.dataBase.services.skinsService

import org.jetbrains.exposed.sql.Table

object StateSkinLink : Table("state_skin_link") {

    val state_key = varchar("state_key",50)
    val skin_id = integer("skin_id").references(SkinsTable.id)
    val main_skin_id = integer("main_skin_id").references(SkinsTable.id)

    override val primaryKey = PrimaryKey(state_key, skin_id, name = "state_skin_link_pk")
}