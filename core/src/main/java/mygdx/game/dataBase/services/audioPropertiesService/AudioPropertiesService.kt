package mygdx.game.dataBase.services.audioPropertiesService

import mygdx.game.dataBase.ServiceIntId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class AudioPropertiesService : ServiceIntId<AudioProperty>(AudioProperties) {

    fun getByUserId(id: Int): AudioProperty {
        return transaction {
            val data = AudioProperties.select { AudioProperties.user_id eq id }.limit(1).execute(this)!!
            if (data.isClosed) {
                val model = newRaw(AudioProperty::class)
                model.user_id = id
                update(model)
                return@transaction model
            }
            createOneModel(AudioProperty::class, data)
        }
    }
}
