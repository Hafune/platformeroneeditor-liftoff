package mygdx.game.dataBase.services.audioPropertiesService

import mygdx.game.dataBase.IntModel

class AudioProperty(override val map: HashMap<String, Any?>) : IntModel(map) {
    override var id: Int by map
    var user_id: Int by map
    var volume: Float by map
}
