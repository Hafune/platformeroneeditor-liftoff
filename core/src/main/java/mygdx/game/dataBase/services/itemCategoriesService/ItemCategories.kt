package mygdx.game.dataBase.services.itemCategoriesService

import mygdx.game.dataBase.StringKeyTable

object ItemCategories : StringKeyTable("item_categories") {
    val show_in_stores = bool("show_in_stores")
    val icon_id = integer("icon_id")
}