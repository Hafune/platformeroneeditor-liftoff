package mygdx.game.dataBase.services.skinsService

import mygdx.game.dataBase.ServiceStringKey
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class SkinTypesService : ServiceStringKey<SkinTypeModel>(SkinTypes) {

    fun getTypes(): ArrayList<SkinTypeModel> {
        return transaction {
            val list = SkinTypes.selectAll().toList()
            val models = createManyModels(SkinTypeModel::class, list)

            arrayListOf(*models.toTypedArray())
        }
    }
}
