@file:Suppress("unused")
package mygdx.game.dataBase.services.itemsService

import mygdx.game.dataBase.services.itemCategoriesService.ItemCategories
import org.jetbrains.exposed.dao.id.IntIdTable

object Items : IntIdTable("items") {
    val name = varchar("name")
    val icon_id = integer("icon_id")
    val price = integer("price")
    val use_mechanic = varchar("use_mechanic")
    val max_capacity = integer("max_capacity")
    val category_key = varchar("category_key").references(ItemCategories.key)

    private fun varchar(name: String) = varchar(name, 50)
}