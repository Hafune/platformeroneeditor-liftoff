@file:Suppress("unused")
package mygdx.game.dataBase.services.iconsService

import mygdx.game.dataBase.StringKeyTable

object IconTypes : StringKeyTable("icon_types") {

    val width = integer("width")
    val height = integer("height")
    val origin_x = float("origin_x")
    val origin_y = float("origin_y")
    val flip_x = bool("flip_x")
    val flip_y = bool("flip_y")
}