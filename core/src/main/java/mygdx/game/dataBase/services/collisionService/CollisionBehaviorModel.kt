package mygdx.game.dataBase.services.collisionService

import mygdx.game.dataBase.StringModel

class CollisionBehaviorModel(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map
}
