package mygdx.game.dataBase.services.iconsService

import mygdx.game.dataBase.StringModel

class IconType(override val map: HashMap<String, Any?>) : StringModel(map) {

    override var key: String by map
    val width: Int by map
    val height: Int by map
    val origin_x: Float by map
    val origin_y: Float by map
    val flip_x: Boolean
        get() = map["flip_x"] == 1
    val flip_y: Boolean
        get() = map["flip_y"] == 1
}