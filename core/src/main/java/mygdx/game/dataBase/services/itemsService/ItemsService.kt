package mygdx.game.dataBase.services.itemsService

import mygdx.game.dataBase.ServiceIntId
import mygdx.game.dataBase.services.chestsService.ChestItems
import mygdx.game.dataBase.services.storesService.StoreItems
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class ItemsService : ServiceIntId<ItemModel>(Items) {

    fun getNewUserItems(): ArrayList<ItemModel> {
        return transaction {
            val list = UserItems.select { UserItems.user_id eq null }.map {
                val data = Items.select { Items.id eq it[UserItems.item_id] }.limit(1).execute(this)!!
                val model = createOneModel(ItemModel::class, data)
                model.capacity = it[UserItems.capacity]
                model
            }
            arrayListOf(*list.toTypedArray())
        }
    }

    fun getUserItems(user_id: Int): ArrayList<ItemModel> {
        return transaction {
            val list = UserItems.select { UserItems.user_id eq user_id }.map {
                val data = Items.select { Items.id eq it[UserItems.item_id] }.limit(1).execute(this)!!
                val model = createOneModel(ItemModel::class, data)
                model.capacity = it[UserItems.capacity]
                model
            }
            arrayListOf(*list.toTypedArray())
        }
    }

    fun getChestItems(chest_id: Int): ArrayList<ItemModel> {
        return transaction {
            val list = ChestItems.select { ChestItems.chest_id eq chest_id }.map {
                val data = Items.select { Items.id eq it[ChestItems.item_id] }.limit(1).execute(this)!!
                val model = createOneModel(ItemModel::class, data)
                model.capacity = it[ChestItems.capacity]
                model
            }
            arrayListOf(*list.toTypedArray())
        }
    }

    fun getStoreItems(store_key: String): ArrayList<ItemModel> {
        return transaction {
            val list = StoreItems.select { StoreItems.store_key eq store_key }.map {
                val data = Items.select { Items.id eq it[StoreItems.item_id] }.limit(1).execute(this)!!
                createOneModel(ItemModel::class, data)
            }
            arrayListOf(*list.toTypedArray())
        }
    }

    fun saveUserItems(user_id: Int, itemModels: List<ItemModel>) {
        transaction {
            UserItems.deleteWhere { UserItems.user_id eq user_id }
            itemModels.forEach { item ->
                UserItems.insert {
                    it[UserItems.user_id] = user_id
                    it[item_id] = item.id
                    it[capacity] = item.capacity
                }
            }
        }
    }
}
