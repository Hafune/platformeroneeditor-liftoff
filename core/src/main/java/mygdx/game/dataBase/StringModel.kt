package mygdx.game.dataBase

abstract class StringModel(open val map: HashMap<String, Any?>) {
    abstract var key: String
}
