package mygdx.game.dataBase

import mygdx.game.dataBase.services.audioPropertiesService.AudioPropertiesService
import mygdx.game.dataBase.services.characterService.CharacterService
import mygdx.game.dataBase.services.chestsService.ChestsService
import mygdx.game.dataBase.services.collisionService.CollisionBehaviorService
import mygdx.game.dataBase.services.doorsService.DoorsService
import mygdx.game.dataBase.services.iconsService.IconsService
import mygdx.game.dataBase.services.itemCategoriesService.ItemCategoriesService
import mygdx.game.dataBase.services.itemsService.ItemsService
import mygdx.game.dataBase.services.skinsService.SkinsService
import mygdx.game.dataBase.services.storesService.StoresService
import mygdx.game.dataBase.services.usersService.UsersService
import org.jetbrains.exposed.sql.Database

class DatabaseServices(val database: Database) {

    val audioPropertiesService = AudioPropertiesService()
    val characterService = CharacterService()
    val chestService = ChestsService()
    val collisionBehaviorService = CollisionBehaviorService()
    val doorService = DoorsService()
    val iconService = IconsService()
    val itemCategoriesService = ItemCategoriesService()
    val itemService = ItemsService()
    val skinService = SkinsService()
    val stateService = StatesService()
    val storeService = StoresService()
    val userService = UsersService()
}
