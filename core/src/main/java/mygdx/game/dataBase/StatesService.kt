package mygdx.game.dataBase

import mygdx.game.dataBase.services.skinsService.StateModel
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class StatesService : ServiceStringKey<StateModel>(States) {

    fun getAll(): ArrayList<StateModel> {
        return transaction {
            val data = States.selectAll().toList()

            arrayListOf(*createManyModels(StateModel::class, data).toTypedArray())
        }
    }
}
