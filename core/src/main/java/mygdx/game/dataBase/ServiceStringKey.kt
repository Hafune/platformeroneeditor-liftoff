package mygdx.game.dataBase

import mygdx.game.lib.MyClassForName
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.sql.ResultSet
import kotlin.reflect.KClass

abstract class ServiceStringKey<M : StringModel>(val table: StringKeyTable) {

    @Suppress("UNCHECKED_CAST")
    fun update(model: M) {
        transaction {
            table.update({ table.key eq model.key }) { u ->
                table.columns.forEach {
                    it as Column<Any?>
                    u[it] = model.map[it.name]
                }
            }
        }
    }

    fun createOneModel(c: KClass<M>, data: ResultSet): M {
        return transaction {
            val map = HashMap<String, Any?>().also { map ->
                table.columns.forEach {
                    map[it.name] = data.getObject(it.name)
                }
            }
            MyClassForName.get(c, map)
        }
    }

    protected fun createOneModel(c: KClass<M>, data: ResultRow): M {
        return transaction {
            val map = HashMap<String, Any?>().also { map ->
                table.columns.forEach {
                    if (data[it] is EntityID<*>) {
                        map[it.name] = (data[it] as EntityID<*>).value
                    } else {
                        map[it.name] = data[it]
                    }
                }
            }
            MyClassForName.get(c, map)
        }
    }

    protected fun createManyModels(c: KClass<M>, list: List<ResultRow>): List<M> {
        return transaction {
            list.map { data ->
                val map = HashMap<String, Any?>().also { map ->
                    table.columns.forEach {
                        if (data[it] is EntityID<*>) {
                            map[it.name] = (data[it] as EntityID<*>).value
                        } else {
                            map[it.name] = data[it]
                        }
                    }
                }
                MyClassForName.get(c, map)
            }
        }
    }
}
