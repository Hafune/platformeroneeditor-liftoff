package mygdx.game.dataBase

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

open class StringKeyTable(name: String = "", columnName: String = "key") : Table(name) {
    val key: Column<String> = varchar(columnName, 256)
    override val primaryKey by lazy { super.primaryKey ?: PrimaryKey(key) }
}