package mygdx.game.dataBase

abstract class IntModel(open val map: HashMap<String, Any?>) {
    abstract var id: Int
}
