package mygdx.game.dataBase

import mygdx.game.lib.MyClassForName
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.ResultSet
import kotlin.reflect.KClass

abstract class ServiceIntId<M : IntModel>(val table: IntIdTable) {

    @Suppress("UNCHECKED_CAST")
    fun update(model: M) {
        transaction {
            table.update({ table.id eq model.id }) { u ->
                table.columns.forEach {
                    it as Column<Any?>
                    u[it] = model.map[it.name]
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    protected open fun insert(model: M): M {
        val map = model.map
        return transaction {
            val id = table.insertAndGetId { t ->
                table.columns.forEach {
                    if (map.containsKey(it.name)) {
                        t[it as Column<Any>] = map[it.name]!!
                    }
                }
            }
            model.id = id.value
            return@transaction model
        }
    }

    @Suppress("UNCHECKED_CAST")
    protected fun newRaw(c: KClass<M>): M {
        return transaction {
            val id = table.insertAndGetId {}

            val data = table.select { table.id eq id }.limit(1).execute(this)!!

            createOneModel(c, data)
        }
    }

    protected fun createOneModel(c: KClass<M>, data: ResultSet): M {
        return transaction {
            val map = HashMap<String, Any?>().also { map ->
                table.columns.forEach {
                    map[it.name] = data.getObject(it.name)
                }
            }
            MyClassForName.get(c, map)
        }
    }

    protected fun createManyModels(c: KClass<M>, list: List<ResultRow>): List<M> {
        return transaction {
            list.map { data ->
                val map = HashMap<String, Any?>().also { map ->
                    table.columns.forEach {
                        if (data[it] is EntityID<*>) {
                            map[it.name] = (data[it] as EntityID<*>).value
                        } else {
                            map[it.name] = data[it]
                        }
                    }
                }
                MyClassForName.get(c, map)
            }
        }
    }
}
