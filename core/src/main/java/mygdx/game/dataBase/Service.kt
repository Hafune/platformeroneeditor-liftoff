package mygdx.game.dataBase

import mygdx.game.lib.MyClassForName
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.ResultSet
import kotlin.reflect.KClass

abstract class Service<M : Model>(val table: Table) {

    fun createOneModel(c: KClass<M>, data: ResultSet): M {
        return transaction {
            val map = HashMap<String, Any?>().also { map ->
                table.columns.forEach {
                    map[it.name] = data.getObject(it.name)
                }
            }
            MyClassForName.get(c, map)
        }
    }

    protected fun createManyModels(c: KClass<M>, list: List<ResultRow>): List<M> {
        return transaction {
            list.map { data ->
                val map = HashMap<String, Any?>().also { map ->
                    table.columns.forEach {
                        if (data[it] is EntityID<*>) {
                            map[it.name] = (data[it] as EntityID<*>).value
                        } else {
                            map[it.name] = data[it]
                        }
                    }
                }
                MyClassForName.get(c, map)
            }
        }
    }
}
