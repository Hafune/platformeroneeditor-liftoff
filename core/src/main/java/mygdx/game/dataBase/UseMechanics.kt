package mygdx.game.dataBase

import mygdx.game.dataBase.services.skinsService.SkinTypes
import org.jetbrains.exposed.sql.Table

object UseMechanics  : Table("use_mechanics") {
    val key = SkinTypes.varchar("key", 20)

    override val primaryKey = PrimaryKey(key, name = "key")
}