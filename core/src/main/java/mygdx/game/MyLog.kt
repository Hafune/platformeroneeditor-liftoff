package mygdx.game

object MyLog {
    private val p = Print()

    fun print(vararg s: Any) = s.forEach { p.print(it) }

    operator fun get(vararg s: Any?) = p.print(s.fold("") { acc, v -> return@fold "$acc $v" })

    fun render() = p.render()
}