package mygdx.game.editor

import mygdx.game.actions.ActionContext
import mygdx.game.actions.AwaitParallel
import mygdx.game.actions.AwaitSequence
import mygdx.game.actions.IAwait
import mygdx.game.editor.FormItemFactory.Companion.K_CLASS_NAME
import mygdx.game.lib.MergeObjectWithMap
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper
import mygdx.game.systems.MyEngine

class ActionEditorBuilder(private val myEngine: MyEngine) {

    companion object {
        private val hash = HashMap<String, ArrayList<*>>()
    }

    @Suppress("UNCHECKED_CAST")
    private fun readAction(list: ArrayList<*>): IAwait {
        var curNode: IAwait? = null
        val arr = ArrayList<IAwait>()
        var map: HashMap<String, Any>? = null
        try {
            list.forEach {
                if (it is Map<*, *>) {

                    map = it as HashMap<String, Any>
                    curNode = MyClassForName.get(map!![K_CLASS_NAME] as String)
                    MergeObjectWithMap.merge(curNode!!, map!!)
                    curNode!!.myEngine = myEngine

                } else if (it is ArrayList<*>) {
                    arr.add(readAction(it))
                }
            }
        } catch (e: RuntimeException) {
            throw Error("ActionBuilder error $list $e")
        }
        if (curNode is AwaitSequence) (curNode as AwaitSequence).arr = arr.toTypedArray()
        if (curNode is AwaitParallel) (curNode as AwaitParallel).arr = arr.toTypedArray()
        MergeObjectWithMap.merge(curNode!!, map!!)
        return curNode!!
    }

    private fun getLaunchAction(scriptFileName: String, list: ArrayList<*>): IAwait {
        val action = AwaitSequence()
        action.scriptFileName = scriptFileName
        action.actionContext = ActionContext()
        val arr = ArrayList<IAwait>()
        list.forEach {
            arr.add(readAction(it as ArrayList<*>))
        }
        if (arr.size > 0) {
            action.arr = arr.toTypedArray()
        }
        return action
    }

    fun buildAction(scriptFileName: String, command: String): IAwait {
        if (!hash.containsKey(command)) {
            hash[command] = MyObjectMapper.readValue(command, ArrayList::class.java)
        }
        val list = hash[command]!!
        return getLaunchAction(scriptFileName, list)
    }
}