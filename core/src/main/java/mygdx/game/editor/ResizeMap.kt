package mygdx.game.editor

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import ktx.scene2d.KCheckBox
import ktx.scene2d.buttonGroup
import ktx.scene2d.checkBox
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visTextButton
import mygdx.game.editor.formItems.FormItemSpinnerInt
import mygdx.game.language.GameText

class ResizeMap(editorRoot: EditorRoot) : EditorWindow("Resize map menu") {

    private val topLeft: KCheckBox
    private val topRight: KCheckBox
    private val botLeft: KCheckBox
    private val botRight: KCheckBox

    private val checkBoxes = scene2d.buttonGroup(1, 1) {
        topLeft = checkBox(GameText["resizeMapWin.topLeftButton"]).apply { userObject = Align.topLeft }
        topRight = checkBox(GameText["resizeMapWin.topRightButton"]).apply { userObject = Align.topRight }
        row()
        botLeft = checkBox(GameText["resizeMapWin.botLeftButton"]).apply { userObject = Align.bottomLeft }
        botRight = checkBox(GameText["resizeMapWin.botRightButton"]).apply { userObject = Align.bottomRight }
    }

    private val inputWidth = FormItemSpinnerInt().apply { valName = "width" }
    private val inputHeight = FormItemSpinnerInt().apply { valName = "height" }

    init {
        addCloseButton()
        add(checkBoxes)
        row()

        val group = HorizontalGroup()
        group.addActor(inputWidth)
        group.addActor(inputHeight)

        add(group)
        row()

        val applyButton = scene2d.visTextButton(GameText["resizeMapWin.applyButton"])

        add(applyButton)

        applyButton.onClick {
            editorRoot.tileMapEditor.resizeMap(
                inputWidth.value + 1,
                inputHeight.value + 1,
                checkBoxes.buttonGroup.checked.userObject as Int
            )
        }
    }
}