package mygdx.game.editor

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import mygdx.game.icons.Icon
import mygdx.game.lib.MyDragListener

class DragButton(
    val parent: Group,
    val scaleX: Float,
    val scaleY: Float,
    val icon: Icon,
    var callback: ((x: Float, y: Float) -> Unit)? = null
) {

    init {
        icon.addListener(
            object : MyDragListener() {
                override fun drag(event: InputEvent, x: Float, y: Float, pointer: Int) {
                    icon.x += x
                    icon.y += y

                    callback?.invoke(x / scaleX, y / scaleY)
                }
            }
        )
    }

    fun show(cx: Float = icon.x / scaleX, cy: Float = icon.y / scaleY) {
        parent.addActor(icon.also { it.x = cx * scaleX - it.width / 2; it.y = cy * scaleY - it.height / 2 })
    }

    fun hide() = icon.remove()
}
