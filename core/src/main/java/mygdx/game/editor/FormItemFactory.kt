package mygdx.game.editor

import com.fasterxml.jackson.databind.node.ObjectNode
import mygdx.game.editor.formItems.FormItem
import mygdx.game.editor.formItems.FormItemLabelKClass
import mygdx.game.lib.MyObjectMapper
import kotlin.reflect.KClass

class FormItemFactory(private val editorRoot: EditorRoot) {
    companion object {
        const val K_CLASS_NAME = "KClass"
        const val IGNORE_SAVE_FORM = "ignore_save_form"
    }

    private fun sort(arr: ArrayList<FormItem<*>>): ArrayList<FormItem<*>> {
        val indexList = ArrayList<Int>()
        val weightList = ArrayList<FormItem<*>>()

        val sortPropertyNames =
            listOf(
                K_CLASS_NAME,
                IGNORE_SAVE_FORM,
                "entity",
                "fromX",
                "fromY",
                "toX",
                "toY",
                "x",
                "y",
                "z",
                "defPositionX",
                "defPositionY",
                "defPositionZ",
                "offsetX",
                "offsetY",
                "positionX",
                "positionY",
                "positionZ",
                "cellX",
                "cellY",
                "centerX",
                "centerY",
                "smoothStart",
                "smoothEnd",
                "centerY",
                "width",
                "height",
                "radius"
            )
        arr.forEach {
            var index = sortPropertyNames.indexOf(it.valName)
            if (index < 0) index = 999
            indexList.add(index)
            weightList.add(it)
        }
        var sorted: Boolean
        do {
            sorted = true
            for (i in 0 until indexList.size - 1) {
                if (indexList[i + 1] < indexList[i]) {
                    sorted = false
                    val t = indexList[i]
                    val a = weightList[i]
                    indexList[i] = indexList[i + 1]
                    weightList[i] = weightList[i + 1]
                    indexList[i + 1] = t
                    weightList[i + 1] = a
                }
            }
        } while (!sorted)

        return weightList
    }

    fun buildMapFromFormItems(actorList: Iterable<FormItem<Any>>): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        actorList.forEach {
            map[it.valName] = it.value
        }
        return map
    }

    @Suppress("UNCHECKED_CAST")
    fun buildFormItemsFromType(kClass: KClass<*>): ArrayList<FormItem<*>> {

        val relatedForms = HashMap<String, FormItem<Any>>()

        val arr = FormAnnotationHandler().buildForms(kClass)
        arr.forEach {
            relatedForms[it.valName] = it as FormItem<Any>
            it.editorRoot = editorRoot
            it.relatedForms = relatedForms
            it.handleInitialize()
        }
        return sort(arr)
    }

    @Suppress("UNCHECKED_CAST")
    fun buildItemsFromModel(model: Any, attachFormsToModel: Boolean): ArrayList<FormItem<*>> {
        val forms = buildFormItemsFromType(model::class)

        if (attachFormsToModel)
            FormAnnotationHandler().attachFormsToModel(model, forms)
        else
            FormAnnotationHandler().fillFormsFromModel(model, forms)


        return sort(forms)
    }

    fun buildJsonObjectFromItems(actorList: Iterable<FormItem<*>>): ObjectNode {
        val obj = MyObjectMapper.createObjectNode()
        actorList.forEach {
            if (!it.valName.startsWith(IGNORE_SAVE_FORM)) {
                obj.putPOJO(it.valName, it.value)
            }
        }
        return obj
    }

    @Suppress("UNCHECKED_CAST")
    fun buildItemsFromClassAndFillValues(kClass: KClass<*>, values: Map<String, Any>): ArrayList<FormItem<*>> {
        val forms = buildFormItemsFromType(kClass)
        if (values.containsKey(K_CLASS_NAME)) {
            forms.add(FormItemLabelKClass().apply {
                valName = K_CLASS_NAME
                value = values[K_CLASS_NAME] as String
            })
        }
        forms.forEach {
            it as FormItem<Any>
            if (values.containsKey(it.valName)) {
                it.value = values[it.valName]!!
            }
        }
        return sort(forms)
    }
}