package mygdx.game.editor.characterMenu

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onChange
import mygdx.game.characterSkins.Skin
import mygdx.game.characterSkins.SkinStorage

class CharacterSkinSelect(
    skinStorage: SkinStorage,
    editorsMenuRoot: Group,
    buttonText: String,
    onSelect: (Skin?) -> Unit
) :
    VisTextButton(buttonText) {

    private val characterSkinsMenu: CharacterSkinsMenu

    init {
        val iconMap: MutableMap<TextureRegion, Skin?> =
            skinStorage.storageIdMap.map { it.value.iconTexture to it.value }.toMap()
                .toMutableMap()
        iconMap[skinStorage.getById(15).iconTexture] = null

        characterSkinsMenu = CharacterSkinsMenu(skinStorage, title = "Character skin") {
            onSelect(it)
        }

        onChange {
            when (characterSkinsMenu.parent) {
                null -> {
                    editorsMenuRoot.addActor(characterSkinsMenu)
                    characterSkinsMenu.addAction(Actions.fadeIn(.2f))
                }
                else -> {
                    characterSkinsMenu.addAction(Actions.sequence(Actions.fadeOut(.2f), Actions.removeActor()))
                }
            }
        }
    }
}