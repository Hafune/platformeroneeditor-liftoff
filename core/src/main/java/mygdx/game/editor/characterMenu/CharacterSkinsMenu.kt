package mygdx.game.editor.characterMenu

import com.badlogic.gdx.scenes.scene2d.ui.Container
import ktx.actors.onClick
import ktx.scene2d.container
import ktx.scene2d.scene2d
import ktx.scene2d.vis.addTabContentsTo
import ktx.scene2d.vis.tab
import ktx.scene2d.vis.tabbedPane
import ktx.scene2d.vis.visTable
import mygdx.game.MyLog
import mygdx.game.characterSkins.Skin
import mygdx.game.characterSkins.SkinStorage
import mygdx.game.editor.EditorWindow
import mygdx.game.myWidgets.VisImageButtonWrap
import mygdx.game.myWidgets.VerticalTableContainer


class CharacterSkinsMenu(
    private val skinStorage: SkinStorage,
    title: String = "Character skins",
    var onSelect: ((skin: Skin) -> Unit)? = null
) : EditorWindow(title) {

    var curSkin = skinStorage.getById(1)
        private set

    init {
        addCloseButton()
        val table = scene2d.container {
            visTable {
                val pane = tabbedPane("vertical") { cell ->
                    cell.growY()
                    skinStorage.storageMap.forEach { (key, skins) ->
                        tab(key, closeableByUser = false) {
                            container(
                                VerticalTableContainer(
                                    6,
                                    *skins.map {
                                        Container(VisImageButtonWrap(it.value.iconTexture).apply {
                                            onClick {
                                                MyLog.print("skin ID:${it.value.id}")
                                                curSkin = it.value
                                                onSelect?.invoke(curSkin)
                                            }
                                        }).apply { size(60f) }
                                    }.toTypedArray()
                                )
                            )
                        }
                    }
                }

                val container = visTable()
                pane.addTabContentsTo(container)
                pane.switchTab(0)

                setFillParent(true)
            }
        }

        add(table)
    }
}