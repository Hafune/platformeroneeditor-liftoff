package mygdx.game.editor.characterMenu

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Image
import ktx.actors.onClick
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.MyLog
import mygdx.game.TileMapRender
import mygdx.game.characterSkins.Skin
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.*
import mygdx.game.icons.SystemIconStorage
import mygdx.game.lib.ReflectionSetPropertyValue
import mygdx.game.lib.TextureStorage
import mygdx.game.myWidgets.VisImageButtonWrap
import mygdx.game.systems.MyEngine
import kotlin.math.floor
import kotlin.reflect.KClass


class CharacterMapEditor(editorRoot: EditorRoot, private val myEngine: MyEngine) {

    private val editorSelect =
        Sprite(TextureStorage[Gdx.files.localStoragePath + "system/selectCircle.png"]).also {
            it.setOriginCenter()
            it.setAlpha(.8f)
        }
    private val clickInteractiveSystem = myEngine.worldProcessingSystem.clickInteractiveSystem

    private val characterSkinsMenu: CharacterSkinsMenu
    private val options = EditorWindow("character options")

    private var curEntity: Entity? = null

    private val place: TouchPlace

    private var selectedEntities = ArrayList<Entity>()

    val characterMenuRoot = Group()
    val characterMapEditorRoot = object : Group() {
        override fun draw(batch: Batch?, parentAlpha: Float) {
            super.draw(batch, parentAlpha)
            cont@ for (entity in selectedEntities) {
                val position = entity.get<PositionComponent>()!!
                with(editorSelect)
                {
                    setPosition(
                        floor(position.x * TileMapRender.TILE_WIDTH - originX),
                        floor(position.y * TileMapRender.TILE_HEIGHT - originY - 15)
                    )
                    draw(batch)
                }
            }
        }
    }

    private val dragButton = DragButton(
        parent = characterMapEditorRoot,
        scaleX = TileMapRender.TILE_WIDTH,
        scaleY = TileMapRender.TILE_HEIGHT,
        icon = SystemIconStorage.buildByName("steppers_plus_hover")
    )

    private val characterForm = CharacterForm(editorRoot)

    private val selectedEntitiesComponentWin = SelectedCharactersForm(editorRoot)

    init {
        characterForm.onApply = {
            curEntity?.let { characterForm.decorateEntity(it) }
        }

        selectedEntitiesComponentWin.classListWidget.setupOnChangeValues { target, valName, value ->
            handleMassEditValue(selectedEntities, target, valName, value)
        }

        selectedEntitiesComponentWin.onAddComponent = {
            selectedEntities.forEach { entity ->
                entity.add(it)
            }
        }

        selectedEntitiesComponentWin.onRemoveComponent = {
            selectedEntities.forEach { entity ->
                entity.remove(it::class.java)
            }
        }

        place = TouchPlace(
            1f,
            1f,
            .1f,
            .1f,
            myEngine.iconStorage.buildByName("5"),
            onRightUp = { x, y -> onRightClick(x, y) }
        )

        place.onLeftUp = { x, y ->
            if (place.lcmBounds.width > 1 || place.lcmBounds.height > 1) {
                selectCharacterInPlace(place.lcmBounds)
            } else {
                if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    val entity = clickInteractiveSystem.getAnyCharacterInPoint(x, y)
                    if (entity != null) {
                        if (selectedEntities.contains(entity)) {
                            selectedEntities.remove(entity)
                        } else selectedEntities.add(entity)
                        refreshSelectedEntityComponents()
                    }
                } else createNewCharacter(x, y)
            }
        }

        val removeButton = VisImageButtonWrap(myEngine.iconStorage.buildById(20).region).apply {
            onClick {
                if (curEntity != null) {
                    removeCharacter(curEntity!!)
                } else {
                    selectedEntities.forEach {
                        removeCharacter(it)
                    }
                    selectedEntities.clear()
                    dragButton.hide()
                }
            }
        }

        characterSkinsMenu = CharacterSkinsMenu(myEngine.skinStorage) { skin ->
            placeChangeIcon(skin)
            curEntity?.let {
                characterForm.decorateEntity(curEntity!!)
                characterForm.setupEntityValues(curEntity!!, false)
            }
            characterForm.classListWidget.setComponentValue(
                AnimationSkinComponent::class,
                AnimationSkinComponent::skin_id.name,
                skin.id
            )
            selectedEntities.forEach {
                it.get<AnimationSkinComponent>()?.run {
                    UndoRedo.saveStateBack(this)
                    this.setSkin(skin)
                    UndoRedo.saveStateForward(this)
                }
            }
            UndoRedo.writeStep()
        }

        dragButton.callback = { x, y ->
            selectedEntities.forEach {
                val component = it.get<PositionComponent>()!!

                UndoRedo.saveStateBack(component)

                component.x += x
                component.y += y
                component.defX += x
                component.defY += y

                UndoRedo.saveStateForward(component)

                val dx = dragButton.icon.x
                val dy = dragButton.icon.y
                UndoRedo.addAction({
                    dragButton.icon.x = dx
                    dragButton.icon.y = dy
                }, {
                    dragButton.icon.x = dx - x * dragButton.scaleX
                    dragButton.icon.y = dy - y * dragButton.scaleY
                })
            }
            UndoRedo.writeStep()
        }

        editorRoot.editorsMenuRoot.addActor(characterMenuRoot)
        editorRoot.mapEditorsRoot.addActor(characterMapEditorRoot)
        characterMenuRoot.isVisible = false
        characterMapEditorRoot.isVisible = false

        options.add(removeButton)
        characterMapEditorRoot.addActor(place)
        characterMenuRoot.addActor(characterForm)
        characterMenuRoot.addActor(selectedEntitiesComponentWin)
        characterMenuRoot.addActor(characterSkinsMenu)
        characterMenuRoot.addActor(options)
    }

    private fun placeChangeIcon(skin: Skin) {
        place.changeIcon(Group().apply {
            addActor(Image(skin.iconTexture).apply {
                x = -skin.motions.originX
                y = -skin.motions.originY
            })
        })
    }

    private fun onRightClick(x: Float, y: Float) {
        selectedEntities.clear()
        dragButton.hide()
        val entity = clickInteractiveSystem.getAnyCharacterInPoint(x, y)
        entity?.run { readCharacter(entity) }
    }

    private fun selectCharacterInPlace(rect: Rectangle) {
        selectedEntities.clear()
        selectedEntities.addAll(
            myEngine.engine.getEntitiesFor(allOf(PositionComponent::class, BodyComponent::class).get()).filter {
                val position = it.get<PositionComponent>()!!
                position.x >= rect.x &&
                        position.x <= rect.x + rect.width &&
                        position.y >= rect.y &&
                        position.y <= rect.y + rect.height
            })
        if (selectedEntities.isNotEmpty()) {
            val sumX = selectedEntities.sumOf { it.get<PositionComponent>()!!.x.toDouble() }.toFloat()
            val sumY = selectedEntities.sumOf { it.get<PositionComponent>()!!.y.toDouble() }.toFloat()
            val cx = sumX / selectedEntities.size
            val cy = sumY / selectedEntities.size
            MyLog[selectedEntities.size]
            dragButton.show(cx, cy)
        } else dragButton.hide()

        refreshSelectedEntityComponents()
    }

    private fun refreshSelectedEntityComponents() {
        val acc = Entity()
        selectedEntities.forEach { entity ->
            entity.components.forEach { component ->
                if (selectedEntities.all { it.getComponent(component::class.java) != null }) {
                    acc.add(component)
                }
            }
        }
        selectedEntitiesComponentWin.setupEntityValues(acc, false)
    }

    private fun removeCharacter(entity: Entity) {
        myEngine.engine.removeEntity(entity)
        UndoRedo.addAction(
            {
                myEngine.engine.removeEntity(entity)
            },
            {
                myEngine.engine.addEntity(entity)
            })
        UndoRedo.writeStep()
    }

    private fun readCharacter(entity: Entity) {
        curEntity = entity
        characterForm.setupEntityValues(entity, true)
        entity.get<AnimationSkinComponent>()?.let { placeChangeIcon(it.skin) }
    }

    private fun createNewCharacter(x: Float, y: Float) {
        val animation = characterForm.classListWidget.buildComponent(AnimationSkinComponent::class)
        if (animation.skin_id <= 0) return

        val entity = Entity()
        curEntity = entity

        characterForm.decorateEntity(entity)
        entity.get<PositionComponent>()!!.also {
            it.defX = x
            it.defY = y
            it.x = x
            it.y = y
        }

        myEngine.engine.addEntity(entity)
        characterForm.setupEntityValues(entity, true)

        UndoRedo.addAction(
            {
                myEngine.engine.addEntity(entity)
            },
            {
                myEngine.engine.removeEntity(entity)
            })
        UndoRedo.writeStep()
    }

    fun updateBounds() {
        characterMapEditorRoot.setSize(characterMapEditorRoot.parent.width, characterMapEditorRoot.parent.height)
    }

    private fun handleMassEditValue(
        entities: Iterable<Entity>,
        target: KClass<MyAbstractComponent>,
        valName: String,
        value: Any
    ) {
        if (hasInChangeComponentList(target, valName)) {
            entities.forEach {
                val component = it.components.find { c -> c::class == target }
                if (component != null) {
                    UndoRedo.saveStateBack(component)
                    ReflectionSetPropertyValue().setupValue(component, valName, value)
                    UndoRedo.saveStateForward(component)
                }
            }
        }
        UndoRedo.writeStep()
    }

    private fun hasInChangeComponentList(target: KClass<MyAbstractComponent>, valName: String): Boolean {
        return target != PositionComponent::class ||
                (target == PositionComponent::class && (valName == "angle" || valName == "defAngle"))
    }
}