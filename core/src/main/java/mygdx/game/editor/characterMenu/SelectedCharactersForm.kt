package mygdx.game.editor.characterMenu

import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.PlatformComponent
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.BuildActionComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.InteractRadiusComponent
import mygdx.game.components.gameWorld.characterComponents.ShadowComponent
import mygdx.game.editor.ComponentFormWin
import mygdx.game.editor.EditorRoot
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
class SelectedCharactersForm(editorRoot: EditorRoot) :
    ComponentFormWin<MyAbstractComponent>(
        title = "Selected characters form",
        editorRoot = editorRoot,

        addedComponents = arrayOf(
            BuildActionComponent::class,
            InteractComponent::class,
            InteractRadiusComponent::class,
            PlatformComponent::class,
            ShadowComponent::class,
            UserControllerComponent::class,
        ) as Array<KClass<MyAbstractComponent>>
    )
