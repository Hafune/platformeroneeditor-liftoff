package mygdx.game.editor.characterMenu

import com.badlogic.ashley.core.Entity
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.PlatformComponent
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.BuildActionComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.InteractRadiusComponent
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.*
import mygdx.game.editor.ComponentFormWin
import mygdx.game.editor.EditorRoot
import java.util.stream.Stream
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
class CharacterForm(editorRoot: EditorRoot) :
    ComponentFormWin<MyAbstractComponent>(
        title = "Character form",
        editorRoot = editorRoot,
        default = Entity().also { entity ->
            Stream.of(
                AbilityComponent(),
                AnimationSkinComponent(),
                BodyComponent(),
                IconComponent(),
                NameComponent(),
                PositionComponent(),
                RepulsiveForceComponent(),
                ShadowComponent(),
            ).forEach {
                entity.add(it)
            }
        },

        addedComponents = arrayOf(
            BuildActionComponent::class,
            InteractComponent::class,
            InteractRadiusComponent::class,
            PlatformComponent::class,
            UserControllerComponent::class,
            ShadowComponent::class,
        ) as Array<KClass<MyAbstractComponent>>
    )
