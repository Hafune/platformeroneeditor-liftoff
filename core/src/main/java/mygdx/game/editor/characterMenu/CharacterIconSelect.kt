package mygdx.game.editor.characterMenu

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onChange
import mygdx.game.editor.EditorWindowImageButtonMenu
import mygdx.game.icons.Icon
import mygdx.game.icons.IconStorage

class CharacterIconSelect(
    iconStorage: IconStorage,
    editorsMenuRoot: Group,
    buttonText: String,
    onSelect: (Icon?) -> Unit
) :
    VisTextButton(buttonText) {

    private val iconMenu: EditorWindowImageButtonMenu<Icon>

    init {
        val iconMap: MutableMap<TextureRegion, Icon?> =
            iconStorage.getCharacterIcons().toSortedMap().map { it.value.region to it.value }.toMap()
                .toMutableMap()
        iconMap[iconStorage.buildById(15).region] = null

        iconMenu = EditorWindowImageButtonMenu("Character icon", iconMap) {
            onSelect(it)
        }

        onChange {
            when (iconMenu.parent) {
                null -> {
                    editorsMenuRoot.addActor(iconMenu)
                    iconMenu.addAction(Actions.fadeIn(.2f))
                }
                else -> {
                    iconMenu.addAction(Actions.sequence(Actions.fadeOut(.2f), Actions.removeActor()))
                }
            }
        }
    }
}