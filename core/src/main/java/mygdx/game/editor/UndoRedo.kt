package mygdx.game.editor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.TimeUtils
import ktx.collections.isNotEmpty
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

object UndoRedo {
    private val forwardActions = Array<() -> Unit>()
    private val backActions = Array<() -> Unit>()
    private val step = Array<Int>()
    private var index = 0
    private var stepIndex = 0
    private var recorded = 0
    private var addActionTime = 100000L
    private var elapsedTime = 0L

    fun writeStep() {
        if (recorded == 0) return

        if (step.size - 1 > stepIndex) {
            step.removeRange(stepIndex + 1, step.size - 1)
        }

        elapsedTime = TimeUtils.timeSinceMillis(addActionTime)
        addActionTime = TimeUtils.millis()

        //записывает действие в один стек если прошел маленький промежуток времени
        if (elapsedTime < 500 && step.isNotEmpty()) {
            step[step.size - 1] = step.last() + recorded
        } else {
            step.add(recorded)
        }
        stepIndex = step.size - 1
        recorded = 0
    }

    fun addAction(inForward: () -> Unit, inBack: () -> Unit) {
        addBack(inBack)
        addForward(inForward)
    }

    private fun addForward(inForward: () -> Unit) {
        if (forwardActions.size - 1 > index) {
            forwardActions.removeRange(index + 1, forwardActions.size - 1)
        }
        forwardActions.add(inForward)
        index = forwardActions.size - 1
        recorded++
    }

    private fun addBack(inBack: () -> Unit) {
        if (backActions.size - 1 > index) {
            backActions.removeRange(index + 1, backActions.size - 1)
        }
        backActions.add(inBack)
    }

    fun saveStateBack(obj: Any) {
        val map = getState(obj)
        addBack { applyState(obj, map) }
    }

    fun saveStateForward(obj: Any) {
        val map = getState(obj)
        addForward { applyState(obj, map) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun getState(obj: Any): HashMap<KMutableProperty<Any>, Any?> {
        val map = HashMap<KMutableProperty<Any>, Any?>()
        obj.javaClass.kotlin.memberProperties.forEach {
            if (it !is KMutableProperty<*>) return@forEach
            it as KMutableProperty<Any>
            it.isAccessible = true
            map[it] = it.get(obj)
        }
        return map
    }

    private fun applyState(obj: Any, map: HashMap<KMutableProperty<Any>, Any?>) {
        map.forEach { (prop, value) ->
            prop.isAccessible = true
            prop.setter.call(obj, value)
        }
    }

    private fun back() {
        if (stepIndex < 0 || stepIndex >= step.size) return
        for (i in 0 until step[stepIndex]) {
            if (index < 0) return
            backActions[index]()
            index--
        }
        if (stepIndex >= 0) stepIndex--
    }

    private fun forward() {
        if (stepIndex < -1 || stepIndex >= step.size - 1) return
        stepIndex++
        for (i in 0 until step[stepIndex]) {
            if (forwardActions.size - 1 <= index) return
            index++
            forwardActions[index]()
        }
    }

    fun clear() {
        forwardActions.clear()
        backActions.clear()
        step.clear()
        index = 0
        stepIndex = 0
        recorded = 0
    }

    fun checkInput() {
        Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) && Gdx.input.isKeyJustPressed(Input.Keys.Z)) {
            back()
        }
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) && Gdx.input.isKeyJustPressed(Input.Keys.X)) {
            forward()
        }
    }
}