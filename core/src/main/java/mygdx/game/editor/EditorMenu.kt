package mygdx.game.editor

import com.badlogic.gdx.scenes.scene2d.Group
import ktx.actors.onClick
import ktx.scene2d.scene2d
import ktx.scene2d.vis.menu
import ktx.scene2d.vis.menuBar
import ktx.scene2d.vis.menuItem
import ktx.scene2d.vis.visTable
import mygdx.game.editor.actionMenu.ActionMenu
import mygdx.game.editor.actionMenu.ActionTreeWin
import mygdx.game.editor.actionMenu.NewActionMenu
import mygdx.game.editor.actionMenu.NewActionTreeWin
import mygdx.game.editor.tileMapMenu.EditorSceneLoader
import mygdx.game.editor.tileMapMenu.TeleportForm
import mygdx.game.systems.MyEngine

class EditorMenu(editorRoot: EditorRoot, myEngine: MyEngine) : Group() {

    private val editorSceneLoader = EditorSceneLoader(myEngine)
    private val actionTreeWin = ActionTreeWin(editorRoot, "Action Editor")
    private val newActionTreeWin = NewActionTreeWin(editorRoot, "Action Editor")
    private val actionMenu = ActionMenu(editorRoot, actionTreeWin.actionTree)
    private val newActionMenu = NewActionMenu(editorRoot, newActionTreeWin.actionTree)
    private val resizeMap = ResizeMap(editorRoot)
    private val teleportForm = TeleportForm(editorRoot, myEngine)

    init {
        actionTreeWin.actionMenu = actionMenu
        newActionTreeWin.actionMenu = newActionMenu

        val menu = scene2d.visTable {
            menuBar { cell ->
                cell.top().growX().expandY().row()
                menu("Load map") {
                    menuItem("win") {
                        onClick {
                            editorRoot.editorsMenuRoot.addActor(editorSceneLoader.fadeIn())
                        }
                    }
                    menuItem("restart") {
                        onClick {
                            myEngine.restart()
                        }
                    }
                }
                menu("Resize") {
                    menuItem("click") {
                        onClick {
                            editorRoot.editorsMenuRoot.addActor(resizeMap.fadeIn())
                        }
                    }
                }
                menu("Teleport") {
                    menuItem("click") {
                        onClick {
                            editorRoot.editorsMenuRoot.addActor(teleportForm.fadeIn())
                        }
                    }
                }
                menu("Tree`s") {
                    menuItem("Action tree") {
                        onClick {
                            editorRoot.editorsMenuRoot.addActor(actionTreeWin.fadeIn())
                            editorRoot.editorsMenuRoot.addActor(actionMenu)
                        }
                    }
                    menuItem("New Action tree") {
                        onClick {
                            val actionTreeWin = ActionTreeWin(editorRoot, "New Action Editor")
                            val actionMenu = ActionMenu(editorRoot, actionTreeWin.actionTree)
                            actionTreeWin.actionMenu = actionMenu

                            editorRoot.editorsMenuRoot.addActor(actionTreeWin.fadeIn())
                            editorRoot.editorsMenuRoot.addActor(actionMenu)
                        }
                    }
                    menuItem("test Action tree") {
                        onClick {
                            editorRoot.editorsMenuRoot.addActor(newActionTreeWin.fadeIn())
                            editorRoot.editorsMenuRoot.addActor(newActionMenu)
                        }
                    }
                }
                menu("Views") {
                    menuItem("Tile form") {
                        onClick {
                            editorRoot.tileMenuRoot.addActor(editorRoot.tileMapEditor.tileForm.fadeIn())
                        }
                    }
                    menuItem("Tiles menu") {
                        onClick {
                            editorRoot.tileMenuRoot.addActor(editorRoot.tileMapEditor.tileDrawItemsMenu.fadeIn())
                        }
                    }
                    menuItem("Tool menu") {
                        onClick {
                            editorRoot.tileMenuRoot.addActor(editorRoot.tileMapEditor.toolEditorMenu.fadeIn())
                        }
                    }
                }
            }
            setFillParent(true)
        }
        height = menu.minHeight

        addActor(menu)
    }
}