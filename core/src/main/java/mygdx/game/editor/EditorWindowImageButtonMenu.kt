package mygdx.game.editor

import com.badlogic.gdx.graphics.g2d.TextureRegion
import ktx.actors.onClick
import mygdx.game.myWidgets.VisImageButtonWrap
import mygdx.game.myWidgets.WindowVerticalFlowContainer

class EditorWindowImageButtonMenu<T>(
    winName: String,
    map: Map<TextureRegion, T?>, onSelect: (T?) -> Unit
) : WindowVerticalFlowContainer(winName, *map.map {
    VisImageButtonWrap(it.key).apply {
        onClick {
            onSelect(it.value)
        }
    }
}.toTypedArray()) {

    init {
        addCloseButton()
    }
}