package mygdx.game.editor

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.kotcrab.vis.ui.widget.VisWindow
import mygdx.game.editor.formItems.FormItem
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
class ComponentWin<T : Any>(
    editorRoot: EditorRoot,
    val component: T,
    attachFormsToModel: Boolean,
    private val onRemove: ((T) -> Unit)? = null
) :
    VisWindow(component::class.simpleName!!) {

    private var form: ArrayList<FormItem<Any>>

    init {
        addCloseButton()
        setKeepWithinStage(false)

        userObject = component::class.qualifiedName

        form =
            FormItemFactory(editorRoot).buildItemsFromModel(component, attachFormsToModel) as ArrayList<FormItem<Any>>
        form.forEach {
            add(it as Actor).growX()
            row()
        }
    }

    fun setupOnChangeValues(onChangeValue: (target: KClass<T>, valName: String, value: Any) -> Unit) {
        form.forEach {
            it.onChangeValue = { value ->
                onChangeValue.invoke(component::class as KClass<T>, it.valName, value)
            }
        }
    }

    override fun close() {
        remove()
        onRemove?.invoke(component)
    }

    operator fun get(propertyKey: String): Any? {
        return children.items.forEach { form ->
            if (form is FormItem<*> && form.valName == propertyKey) {
                return form.value
            }
        }
    }

    override fun drawChildren(batch: Batch?, parentAlpha: Float) {
        super.drawChildren(batch, parentAlpha)
    }
}