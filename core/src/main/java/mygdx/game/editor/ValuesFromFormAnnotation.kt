package mygdx.game.editor

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

@Suppress("UNCHECKED_CAST")
class ValuesFromFormAnnotation {

    fun get(item: Any): HashMap<String, Any> {
        val values = HashMap<String, Any>()
        item.javaClass.kotlin.memberProperties.forEach {
            if (it !is KMutableProperty<*>) return@forEach
            it as KMutableProperty<Any>

            if (it.hasAnnotation<FormDefault>() || it.hasAnnotation<Form>()) {
                it.isAccessible = true
                values[it.name] = it.getValue(item, it) as Any
            }
        }
        return values
    }

    fun setupValues(item: Any, values: HashMap<String, Any>) {
        item.javaClass.kotlin.memberProperties.forEach {
            if (it !is KMutableProperty<*>) return@forEach
            it as KMutableProperty<Any>

            if (it.hasAnnotation<FormDefault>() || it.hasAnnotation<Form>()) {
                it.isAccessible = true
                it.setter.call(item, values[it.name])
            }
        }
    }
}