package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.Actor
import ktx.actors.onClick
import mygdx.game.editor.TouchPlace
import mygdx.game.icons.Icon

@Suppress("UNCHECKED_CAST")
open class FormItemTouchCell : AbstractFormItem<String>() {

    private lateinit var icon: Icon

    lateinit var place: TouchPlace

    override var valName: String = ""
    override var value: String = ""

    open var cursorIcon: Actor? = null
    open val resizableIcon = false
    open val lcmStepWidth = 1f
    open val lcmStepHeight = 1f

    var nameX = "x"
    var namey = "y"
    var nameWidth = "width"
    var nameHeight = "height"


    override fun handleInitialize() {
        icon = editorRoot.myEngine.iconStorage.buildByName("plus32x32")
        add(icon)
        onClick {
            stage.addActor(place)
        }

        cursorIcon = cursorIcon ?: editorRoot.myEngine.iconStorage.buildByName("cursor")

        place = TouchPlace(
            lcmStepWidth = lcmStepWidth,
            lcmStepHeight = lcmStepHeight,
            icon = cursorIcon!!,
            resizableIcon = resizableIcon
        )
        place.onLeftUp = { _, _ ->
            relatedForms[nameX]?.value = place.lcmBounds.x
            relatedForms[namey]?.value = place.lcmBounds.y
            relatedForms[nameWidth]?.value = place.lcmBounds.width
            relatedForms[nameHeight]?.value = place.lcmBounds.height
        }
        place.onRightUp = { _, _ ->
            place.remove()
        }
    }
}