package mygdx.game.editor.formItems

import com.kotcrab.vis.ui.widget.VisLabel
import ktx.actors.onClick

class FormItemLabelKClass : AbstractFormItem<String>() {

    private val valueLabel = VisLabel()
    
    override var valName: String = ""

    override var value: String
        get() = key
        set(value) {
            key = value
            valueLabel.setText(getText())
        }

    init {
        add(valueLabel)
        onClick {
            println(key)
        }
    }
}