package mygdx.game.editor.formItems

import ktx.collections.gdxArrayOf
import mygdx.game.myWidgets.Option

class FormItemSelectBoxCollisionBehavior : FormItemHandleInitializeSelectBox<String>() {

    override fun handleInitialize() {
        values =
            gdxArrayOf(
                *Option.buildFrom(
                    *editorRoot.myEngine.db.collisionBehaviorService.getAll().map { it.key }.toTypedArray()
                )
            )
        super.handleInitialize()
    }
}