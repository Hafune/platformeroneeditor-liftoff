package mygdx.game.editor.formItems

import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner
import ktx.actors.onChange
import ktx.actors.onKeyboardFocusEvent

class FormItemSpinnerInt : AbstractFormItem<Int>() {

    private val label = VisLabel()
    private val model = IntSpinnerModel(0, -500000, 500000, 1)
    private val spinner = Spinner(name, model)

    override var valName: String
        get() = label.text.toString()
        set(value) {
            label.setText(value)
        }
    override var value: Int
        get() = model.value
        set(value) {
            model.value = value
        }
    init {
        add(label)
        add(spinner)
        spinner.onChange {
            updateOwnerValue()
        }
        spinner.onKeyboardFocusEvent { e ->
            watchChanges = !e.isFocused
        }
    }
}