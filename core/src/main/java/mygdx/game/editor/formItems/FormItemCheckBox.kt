package mygdx.game.editor.formItems

import com.badlogic.gdx.graphics.Color
import com.kotcrab.vis.ui.widget.VisCheckBox
import ktx.actors.onChange

class FormItemCheckBox : AbstractFormItem<Boolean>() {

    private val checkBox = VisCheckBox("", false)

    init {
        val s = checkBox.style
        s.fontColor = Color(80f, 255f, 120f, 1f)
        s.overFontColor = Color(80f, 255f, 120f, 1f)
        checkBox.style = s
        add(checkBox)
        onChange {
            updateOwnerValue()
        }
    }

    override var valName: String
        get() = checkBox.label.text.toString()
        set(value) {
            checkBox.setText(value)
        }
    override var value: Boolean
        get() = checkBox.isChecked
        set(value) {
            checkBox.isChecked = value
        }
}