package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import ktx.scene2d.container
import ktx.scene2d.scene2d
import mygdx.game.characterSkins.Skin
import mygdx.game.editor.characterMenu.CharacterSkinSelect
import kotlin.math.max

class FormItemCharacterSkinSelector : AbstractFormItem<Int>() {

    private var skin: Skin? = null
    private val imgContainer = Container<Image>()
    private var image = Image()
    private lateinit var select: CharacterSkinSelect

    private var t = ""
    private var v = 0

    override var valName: String
        get() = t
        set(value) {
            t = value
        }
    override var value: Int
        get() = v
        set(value) {
            setImageTexture(if (value == 0) null else editorRoot.myEngine.skinStorage.getById(value))
        }

    override fun handleInitialize() {
        select = CharacterSkinSelect(editorRoot.myEngine.skinStorage, editorRoot.editorsMenuRoot, "Character skin") {
            skin = it
            setImageTexture(it)
        }

        imgContainer.actor = image
        add(select)
        add(scene2d.container {
            actor = imgContainer
            size(max(48f, max(image.width, image.height)))
        })
    }

    private fun setImageTexture(s: Skin?) {
        when (s) {
            null -> {
                image.drawable = null
                v = 0
            }
            else -> {
                image.drawable = TextureRegionDrawable(s.iconTexture)
                v = s.id
            }
        }
        updateOwnerValue()
    }
}