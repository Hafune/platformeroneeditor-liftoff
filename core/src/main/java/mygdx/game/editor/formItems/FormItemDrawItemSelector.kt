package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import ktx.actors.onClick
import ktx.scene2d.container
import ktx.scene2d.scene2d
import mygdx.game.editor.tileMapMenu.TileDrawItemsMenu
import mygdx.game.icons.Icon
import mygdx.game.tileSets.DrawItemEraser
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

@Suppress("UNCHECKED_CAST")
class FormItemDrawItemSelector : AbstractFormItem<String>() {

    private var drawItem: IDrawItem = DrawItemEraser
    private val imgContainer = Container<Image>()
    private var image = Image()

    private lateinit var select:TileDrawItemsMenu

    private lateinit var plus:Icon

    private var title = ""

    override var valName: String
        get() = title
        set(value) {
            title = value
        }
    override var value: String
        get() = drawItem.value
        set(value) {
            if (value == "") {
                drawItem = DrawItemEraser
                return
            }
            drawItem = editorRoot.myEngine.graphicResources.getDrawItem(value)
            image.drawable = TextureRegionDrawable(drawItem.icon)
            updateOwnerValue()
        }

    override fun handleInitialize() {
        plus = editorRoot.myEngine.iconStorage.buildByName("plus32x32")
        select = TileDrawItemsMenu(editorRoot.myEngine.graphicResources.tileSetPages, {
            drawItem = it
            image.drawable = TextureRegionDrawable(drawItem.icon)
            updateOwnerValue()
        }, title = "DrawItem selector")

        imgContainer.actor = image
        add(plus)
        add(scene2d.container {
            actor = imgContainer
            size(48f)
        })
        plus.onClick {
            editorRoot.tileMenuRoot.addActor(select)
        }
    }
}