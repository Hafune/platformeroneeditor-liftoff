package mygdx.game.editor.formItems

import mygdx.game.characterSkins.MotionNames
import mygdx.game.myWidgets.Option

class FormItemSelectBoxMotionNames : FormItemSelectBox<String>(*Option.buildFromObject(MotionNames)){

    init {
        value = MotionNames.STAND
    }
}