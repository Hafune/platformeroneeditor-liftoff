package mygdx.game.editor.formItems

import ktx.collections.gdxArrayOf
import mygdx.game.myWidgets.Option

class FormItemSelectBoxSkinStates : FormItemHandleInitializeSelectBox<String>() {

    override fun handleInitialize() {
        values =
            gdxArrayOf(
                *Option.buildFrom(
                    *editorRoot.myEngine.db.stateService.getAll().map { it.key }.toTypedArray()
                )
            )
        super.handleInitialize()
    }
}