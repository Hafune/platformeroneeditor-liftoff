package mygdx.game.editor.formItems

import mygdx.game.components.gameWorld.tileComponents.TileFrame
import mygdx.game.myWidgets.Option

class FormItemSelectBoxTileFrame : FormItemSelectBox<String>(*Option.buildFromObject(TileFrame)){

    init {
        value = TileFrame.FIRST_FRAME
    }
}