package mygdx.game.editor.formItems

import com.badlogic.gdx.physics.box2d.BodyDef
import mygdx.game.myWidgets.Option

class FormItemSelectBoxBodyType : FormItemSelectBox<String>(*Option.buildFromJavaEnum(BodyDef.BodyType.values())){

    init {
        value = BodyDef.BodyType.DynamicBody.name
    }
}