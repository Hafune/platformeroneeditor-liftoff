package mygdx.game.editor.formItems

import mygdx.game.editor.EditorRoot
import kotlin.reflect.KMutableProperty

interface FormItem<T> {

    var editorRoot: EditorRoot
    var relatedForms: HashMap<String, FormItem<Any>>
    var valName: String
    var value: T
    var valueOwner: Any?
    var valueLink: KMutableProperty<Any>?
    var onChangeValue: ((T) -> Unit)?

    fun handleInitialize()
}