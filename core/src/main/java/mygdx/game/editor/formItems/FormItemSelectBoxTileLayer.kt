package mygdx.game.editor.formItems

import mygdx.game.editor.tileMapMenu.TileLayer
import mygdx.game.myWidgets.Option

class FormItemSelectBoxTileLayer : FormItemSelectBox<String>(*Option.buildFromObject(TileLayer)){

    init {
        value = TileLayer.BACKGROUND
    }
}