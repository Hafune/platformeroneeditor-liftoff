package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import ktx.scene2d.container
import ktx.scene2d.scene2d
import mygdx.game.editor.characterMenu.CharacterIconSelect
import mygdx.game.icons.Icon

class FormItemCharacterIconSelector : AbstractFormItem<Int>() {

    private var icon: Icon? = null
    private var image = Image()
    private lateinit var select: CharacterIconSelect

    private var t = ""
    private var v = 0

    override var valName: String
        get() = t
        set(value) {
            t = value
        }
    override var value: Int
        get() = v
        set(value) {
            if (value == 0) {
                setImageTexture(null)
            } else {
                setImageTexture(editorRoot.myEngine.iconStorage.buildById(value))
            }
        }

    override fun handleInitialize() {
        select = CharacterIconSelect(editorRoot.myEngine.iconStorage, editorRoot.editorsMenuRoot, "Character icon") {
            icon = it
            setImageTexture(it)
        }

        add(select).left()
        add(scene2d.container {
            actor = image
            size(77f)
        }).left()
    }

    private fun setImageTexture(icon: Icon?) {
        when (icon) {
            null -> {
                image.drawable = null
                v = 0
            }
            else -> {
                image.drawable = TextureRegionDrawable(icon.region)
                v = icon.id
            }
        }
        updateOwnerValue()
    }
}