package mygdx.game.editor.formItems

import com.badlogic.gdx.graphics.g2d.Animation
import mygdx.game.myWidgets.Option

class FormItemSelectBoxAnimationMode : FormItemSelectBox<String>(*Option.buildFromJavaEnum(Animation.PlayMode.values())){

    init {
        value = Animation.PlayMode.NORMAL.name
    }
}