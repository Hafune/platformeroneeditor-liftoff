package mygdx.game.editor.formItems

import mygdx.game.actions.ActionContext
import mygdx.game.lib.ReflectionFindDelegatePropertyNames
import mygdx.game.myWidgets.Option

class FormItemSelectBoxActionContextVariables :
    FormItemSelectBox<String>(*Option.buildFrom(*list)) {

    private companion object {
        val list = ReflectionFindDelegatePropertyNames(ActionContext()).toTypedArray()
    }
}