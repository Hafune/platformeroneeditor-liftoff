package mygdx.game.editor.formItems

import mygdx.game.actions.PrimitiveTypes
import mygdx.game.myWidgets.Option

class FormItemSelectBoxPrimitiveTypes : FormItemSelectBox<String>(*Option.buildFromObject(PrimitiveTypes)){

    init {
        value = PrimitiveTypes.FLOAT
    }
}