package mygdx.game.editor.formItems

import ktx.collections.gdxArrayOf
import mygdx.game.myWidgets.Option
import mygdx.game.systems.gameWorld.abilities.FindAbilitiesAnnotation

class FormItemSelectBoxAbilities : FormItemHandleInitializeSelectBox<String>() {

    private companion object {
        var list = arrayListOf<String>()
    }

    override fun handleInitialize() {
        if (list.size == 0) {
            list = FindAbilitiesAnnotation().find(editorRoot.myEngine)
        }

        values = gdxArrayOf(*Option.buildFrom(*list.toTypedArray()))
        super.handleInitialize()
    }
}