package mygdx.game.editor.formItems

import mygdx.game.actions.OperatorTypes
import mygdx.game.myWidgets.Option

class FormItemSelectOperatorTypes : FormItemSelectBox<String>(*Option.buildFromObject(OperatorTypes)){

    init {
        value = OperatorTypes.ASSIGN
    }
}