package mygdx.game.editor.formItems

import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.VisTextField
import ktx.actors.onChange
import ktx.actors.onKeyboardFocusEvent

class FormItemTextField : AbstractFormItem<String>() {

    val label = VisLabel()
    val textField = VisTextField()

    override var valName: String
        get() = label.text.toString()
        set(value) {
            label.setText(value)
        }

    override var value: String
        get() = textField.text
        set(value) {
            textField.text = value
        }

    init {
        add(label)
        add(textField)
        textField.onChange {
            updateOwnerValue()
        }
        textField.onKeyboardFocusEvent { e ->
            watchChanges = !e.isFocused
        }
    }
}