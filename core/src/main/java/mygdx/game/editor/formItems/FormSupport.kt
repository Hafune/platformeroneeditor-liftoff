package mygdx.game.editor.formItems

import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
annotation class FormSupport(vararg val values: KClass<*>)
