package mygdx.game.editor.formItems

import com.kotcrab.vis.ui.widget.VisLabel
import ktx.actors.onClick
import ktx.scene2d.KTextButton
import ktx.scene2d.scene2d
import ktx.scene2d.textButton
import mygdx.game.lib.MyFileChooserOpen

class FormItemFilePathPicker : AbstractFormItem<String>() {

    private val fileChooserOpen: MyFileChooserOpen = MyFileChooserOpen {
        val path = it.path()
        value = path.replaceBefore("assets/", "").replace("assets/", "").replaceAfter(it.name(), "")
    }

    override var valName: String
        get() = label.text.toString()
        set(value) {
            label.setText(value)
        }

    private var v = ""
    override var value: String
        get() = v
        set(value) {
            v = value
            val split = v.split("/")
            if (split.size > 2) {
                button.setText(".../${split[split.size - 2]}/${split.last()}")
            } else button.setText(value)
            updateOwnerValue()
        }
    val label = VisLabel()
    lateinit var button: KTextButton

    override fun handleInitialize() {
        button = scene2d.textButton("undefined") {
            onClick {
                editorRoot.editorsMenuRoot.addActor(fileChooserOpen.fadeIn())
            }
        }
        add(label)
        add(button)
    }
}