package mygdx.game.editor.formItems

import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.spinner.FloatSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner
import ktx.actors.onChange
import ktx.actors.onKeyboardFocusEvent

class FormItemSpinnerFloat : AbstractFormItem<Float>() {

    private val label = VisLabel()
    private val model = FloatSpinnerModel("0", "-500000", "500000", "0.5", 10)
    private val spinner = Spinner(name, model)

    override var valName: String
        get() = label.text.toString()
        set(value) {
            label.setText(value)
        }
    override var value: Float
        get() = model.value.toFloat()
        set(value) {
            model.value = value.toBigDecimal()
            updateOwnerValue()
        }

    init {
        add(label)
        add(spinner)
        spinner.onChange {
            updateOwnerValue()
        }
        spinner.onKeyboardFocusEvent { e ->
            watchChanges = !e.isFocused
        }
    }
}