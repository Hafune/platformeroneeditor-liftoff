package mygdx.game.editor.formItems

import mygdx.game.components.gameWorld.tileComponents.TileAnimation
import mygdx.game.myWidgets.Option

class FormItemSelectBoxTileAnimation : FormItemSelectBox<String>(*Option.buildFromObject(TileAnimation)){

    init {
        value = TileAnimation.NOT_ANIMATED
    }
}