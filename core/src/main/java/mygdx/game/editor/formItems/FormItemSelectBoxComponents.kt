package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import ktx.actors.onClick
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visTextButton
import mygdx.game.components.PlatformComponent
import mygdx.game.components.PlatformIgnoreComponent
import mygdx.game.components.gameWorld.AttachOffsetComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.FormItemFactory
import mygdx.game.language.GameText
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper
import mygdx.game.myWidgets.Option

@Suppress("UNCHECKED_CAST")
class FormItemSelectBoxComponents : AbstractFormItem<String>() {

    private val container = HorizontalGroup()
    private val select = FormItemSelectBox(
        *Option.buildFromKClasses(PositionComponent::class, PlatformComponent::class, PlatformIgnoreComponent::class, AttachOffsetComponent::class)
    )

    override var valName: String
        get() = select.valName
        set(value) {
            select.valName = value
        }

    override var value: String
        set(v) {
            val map = MyObjectMapper.readValue(v, HashMap::class.java) as HashMap<String, Any>
            val type = MyClassForName.getType(map[FormItemFactory.K_CLASS_NAME] as String)
            select.value = type

            container.clear()
            FormItemFactory(editorRoot).buildItemsFromClassAndFillValues(type, map).forEach {
                container.addActor(it as Actor)
            }
        }
        get() {
            val map =
                FormItemFactory(editorRoot).buildMapFromFormItems(container.children.map { it as FormItem<Any> })
            return MyObjectMapper.writeValueAsString(map)
        }

    init {
        add(select).left()
        add(scene2d {
            visTextButton(GameText["add"]) {
                onClick {
                    container.clear()
                    container.addActor(FormItemLabelKClass().also {
                        it.value = select.value.qualifiedName!!
                        it.valName = FormItemFactory.K_CLASS_NAME
                    })
                    FormItemFactory(editorRoot).buildFormItemsFromType(select.value).forEach {
                        container.addActor(it as Actor)
                    }
                }
            }
        }).left()
        row()
        add(container)
    }
}