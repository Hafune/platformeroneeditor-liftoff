package mygdx.game.editor.formItems

import com.badlogic.gdx.scenes.scene2d.Actor
import mygdx.game.uiSkins.UiSkins

@Suppress("UNCHECKED_CAST")
class FormItemTouchRegion : FormItemTouchCell() {

    //TODO создать скин отдельно для редактора
    override var cursorIcon: Actor? = UiSkins.Labels.touch_region_background()
    override val resizableIcon = true
}