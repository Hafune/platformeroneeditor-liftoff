@file:Suppress("LeakingThis")

package mygdx.game.editor.formItems

import com.badlogic.gdx.utils.Array
import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.VisSelectBox
import ktx.actors.onChange
import mygdx.game.myWidgets.Option

open class FormItemSelectBox<T>(vararg var values: Option<T>, var onSelect: ((value: T) -> Unit)? = null) : AbstractFormItem<T>() {

    private val label = VisLabel()
    private val selectBox = VisSelectBox<Option<T>>()
    private var callbackIsOpen = true

    override var valName: String
        get() = key
        set(value) {
            key = value
            label.setText(getText())
        }

    override var value: T
        get() = selectBox.selected.value!!
        set(value) {
            callbackIsOpen = false
            selectBox.selected = selectBox.items.find { it.value == value }
            callbackIsOpen = true
        }

    init{
        add(label)
        add(selectBox)

        selectBox.items = Array<Option<T>>().apply { addAll(*values) }
        selectBox.onChange {
            updateOwnerValue()
        }
        onChange {
            if (callbackIsOpen) {
                onSelect?.invoke(selectBox.selected.value!!)
            }
        }
    }
}