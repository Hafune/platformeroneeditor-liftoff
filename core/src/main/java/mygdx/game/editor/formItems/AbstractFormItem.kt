package mygdx.game.editor.formItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.ui.Table
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.UndoRedo
import mygdx.game.language.GameText
import kotlin.reflect.KMutableProperty
import kotlin.reflect.jvm.isAccessible

@Suppress("LeakingThis")
abstract class AbstractFormItem<T> : Table(), FormItem<T> {
    override lateinit var editorRoot: EditorRoot
    override lateinit var relatedForms: HashMap<String, FormItem<Any>>

    private var lastValue: T? = null
    override var onChangeValue: ((T) -> Unit)? = null

    override var valueOwner: Any? = null
    override var valueLink: KMutableProperty<Any>? = null
    var watchChanges = true
    private var blockChanges = false

    protected var key = ""
    protected fun getText(textKey: String = key): String {
        try {
            return GameText[textKey]
        } catch (_: RuntimeException) {
        }
        return textKey
    }

    init {
        left()
    }

    override fun handleInitialize() {
        lastValue = value
    }

    @Suppress("UNCHECKED_CAST")
    protected fun updateOwnerValue() {
        if (blockChanges) return

        if (lastValue != value) {
            lastValue = value
            onChangeValue?.invoke(value)
        }

        valueLink ?: return

        valueLink!!.isAccessible = true
        if (valueLink!!.getter.call(valueOwner) == value) return

        val redoValue = value
        val undoValue = valueLink!!.getter.call(valueOwner)

        valueLink!!.setter.call(valueOwner, value)

        UndoRedo.addAction({
            valueLink!!.setter.call(valueOwner, redoValue)
        }, {
            valueLink!!.setter.call(valueOwner, undoValue)
        })
        UndoRedo.writeStep()

    }

    @Suppress("UNCHECKED_CAST")
    override fun draw(batch: Batch?, parentAlpha: Float) {
        super.draw(batch, parentAlpha)

        if (!watchChanges) return
        valueLink ?: return

        blockChanges = true

        valueLink!!.isAccessible = true

        val v = valueLink!!.getter.call(valueOwner) as T
        if (v != value) {
            value = v
        }
        blockChanges = false
    }
}