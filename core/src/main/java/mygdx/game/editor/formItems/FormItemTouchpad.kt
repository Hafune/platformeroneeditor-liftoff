package mygdx.game.editor.formItems

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad
import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.spinner.FloatSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner
import ktx.actors.onChange
import ktx.actors.onKeyboardFocusEvent
import mygdx.game.ProjectConstants
import java.math.BigDecimal
import kotlin.math.round

class FormItemTouchpad(onSelect: ((value: Float) -> Unit)? = null) : AbstractFormItem<Float>() {

    private val touchpad = Touchpad(10f, ProjectConstants.editorSkin).apply {
        val vector = Vector2()
        onChange {
            if (knobPercentX != 0f || knobPercentY != 0f) {
                vector.x = knobPercentX
                vector.y = knobPercentY
                v = vector.angleDeg()
                model.value = BigDecimal.valueOf(round(v).toDouble())
                onSelect?.invoke(v)

                updateOwnerValue()
            }
        }
    }

    private val model = FloatSpinnerModel("0", "0", "360", "45")
    private val spinner = Spinner(name, model).also {
        it.onChange {
            v = model.text.toFloat()
            onSelect?.invoke(v)

            updateOwnerValue()
        }
        onKeyboardFocusEvent { e ->
            watchChanges = !e.isFocused
        }
    }

    override var valName: String
        get() = label.text.toString()
        set(value) {
            label.setText(value)
        }
    override var value: Float
        get() = v
        set(value) {
            v = value
            model.value = BigDecimal.valueOf(value.toDouble())
        }
    val label = VisLabel()
    var v = 0f

    init {
        add(label).row()
        add(touchpad).row()
        add(spinner).row()
    }
}