package mygdx.game.editor.formItems

import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
annotation class Form(val kClass: KClass<*>)
