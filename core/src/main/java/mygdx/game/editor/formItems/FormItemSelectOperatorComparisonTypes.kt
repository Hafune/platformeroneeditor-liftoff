package mygdx.game.editor.formItems

import mygdx.game.actions.OperatorComparisonTypes
import mygdx.game.myWidgets.Option

class FormItemSelectOperatorComparisonTypes : FormItemSelectBox<String>(*Option.buildFromObject(OperatorComparisonTypes)){

    init {
        value = OperatorComparisonTypes.EQUALS
    }
}