package mygdx.game.editor

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Event
import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.VisTextButton
import com.kotcrab.vis.ui.widget.VisWindow
import ktx.actors.onChange
import ktx.actors.onClick
import ktx.actors.txt
import mygdx.game.lib.ActionCallbackEachAct
import mygdx.game.lib.EditorPositionsUi
import mygdx.game.myWidgets.IntSpinner
import mygdx.game.systems.MyEngine

object ActorSetting : VisWindow("Ui Element setting") {

    private val nameLabel = VisLabel("none")
    private val xLabel = IntSpinner(0, 0, 10000, 1, "x")
    private val yLabel = IntSpinner(0, 0, 10000, 1, "y")
    private val widthLabel = IntSpinner(0, 0, 10000, 1, "width")
    private val heightLabel = IntSpinner(0, 0, 10000, 1, "height")
    private val exportAllLabel = VisTextButton("export all")

    private var actor: Actor? = null
    private var needDisableDebug = false

    private var action = ActionCallbackEachAct {
        if (!xLabel.textField.hasKeyboardFocus()) xLabel.value = actor!!.x.toInt()
        if (!yLabel.textField.hasKeyboardFocus()) yLabel.value = actor!!.y.toInt()
        if (!widthLabel.textField.hasKeyboardFocus()) widthLabel.value = actor!!.width.toInt()
        if (!heightLabel.textField.hasKeyboardFocus()) heightLabel.value = actor!!.height.toInt()
        actor!!.fire(Event())
    }

    lateinit var myEngine: MyEngine

    init {
        setKeepWithinStage(false)
        addCloseButton()

        isResizable = true

        add(nameLabel)
        row()
        add(widthLabel)
        add(heightLabel)
        row()
        add(xLabel)
        add(yLabel)
        row()
        add(exportAllLabel)

        xLabel.onChange {
            actor?.x = xLabel.value.toFloat()
        }
        yLabel.onChange {
            actor?.y = yLabel.value.toFloat()
        }
        widthLabel.onChange {
            actor?.width = widthLabel.value.toFloat()
        }
        heightLabel.onChange {
            actor?.height = heightLabel.value.toFloat()
        }
        exportAllLabel.onClick {
            myEngine.positionsUiStorage.exportAll()
        }

        EditorPositionsUi.setup(this, "Ui Element setting")
        pack()
    }

    fun setupElement(a: Actor) {
        if (a === actor) return

        actor?.removeAction(action)
        if (needDisableDebug) actor?.debug = false
        actor = a
        if (a.debug) needDisableDebug = false
        a.debug = true

        a.addAction(action)
        a.parent.addActor(this)
        nameLabel.txt = a.name
    }
}