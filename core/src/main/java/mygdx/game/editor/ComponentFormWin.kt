package mygdx.game.editor

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onClick
import mygdx.game.ScreenContainer
import mygdx.game.myWidgets.ClassListWidget
import mygdx.game.myWidgets.ClassSelectForm
import mygdx.game.myWidgets.ScrollPaneVertical
import kotlin.reflect.KClass

@Suppress("LeakingThis")
open class ComponentFormWin<T : Component>(
    title: String,
    default: Entity = Entity(),
    addedComponents: Array<KClass<T>>,
    val editorRoot: EditorRoot,
    var onAddComponent: ((T) -> Unit)? = null,
    var onRemoveComponent: ((T) -> Unit)? = null,
    var onApply: (() -> Unit)? = null,
    widgets: Array<Actor>? = null
) :
    EditorWindow(title) {

    private val sWidth = ScreenContainer.appScreenWidth
    private val sHeight = ScreenContainer.appScreenHeight

    private val content = Table().apply { left() }

    val classListWidget = ClassListWidget<T>(editorRoot = editorRoot, onRemove = {
        onRemoveComponent?.invoke(it)
    })

    private val classSelectForm =
        ClassSelectForm(
            container = classListWidget,
            list = addedComponents,
            onAdd = { onAddComponent?.invoke(it) }
        )

    private val scroll: ScrollPaneVertical


    private val applyButton = VisTextButton("Apply").apply {
        onClick {
            onApply?.invoke()
        }
    }

    init {
        addCloseButton()
        widgets?.forEach {
            content.add(it).row()
        }
        content.add(HorizontalGroup().also {
            it.addActor(classSelectForm)
            it.addActor(applyButton)
        }).row()
        content.add(classListWidget).row()

        scroll = ScrollPaneVertical(content)
        scroll.listeners.removeIndex(0)
        add(scroll).top().left().expand().fill().row()
        setupEntityValues(default, false)
    }

    @Suppress("UNCHECKED_CAST")
    fun setupEntityValues(entity: Entity, attachFormToComponents: Boolean) {
        classListWidget.setComponents(
            attachFormToComponents,
            entity.components.map { it as T }
        )
        val h = height
        pack()
        pack()
        if (height > sHeight * .8f) height = sHeight * .8f
        y += (h - height) / 2
        if (y + height + 50 > sHeight) y = sHeight - height - 50
    }

    @Suppress("UNCHECKED_CAST")
    fun decorateEntity(entity: Entity) {
        val componentNameList = ArrayList<String>()
        classListWidget.buildComponents().forEach { incoming ->
            val component =
                entity.components.map { it as T }
                    .firstOrNull { it::class.qualifiedName == incoming::class.qualifiedName }
                    ?: incoming
            ValuesFromFormAnnotation().apply {
                setupValues(component, get(incoming))
            }
            componentNameList.add(incoming::class.qualifiedName!!)
            entity.add(component)
        }

        for (c in entity.components) {
            if (!componentNameList.contains(c::class.qualifiedName)) {
                entity.remove(c::class.java)
            }
        }

        if (editorRoot.myEngine.engine.entities.contains(entity, true)) {
            editorRoot.myEngine.engine.removeEntity(entity)
            editorRoot.myEngine.engine.addEntity(entity)
        }
    }
}