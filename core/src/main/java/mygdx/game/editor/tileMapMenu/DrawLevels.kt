package mygdx.game.editor.tileMapMenu

enum class DrawLevels {
    FLOOR, WITH_EDGE, DECORATION
}