package mygdx.game.editor.tileMapMenu

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.utils.Align
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onClick
import ktx.ashley.get
import mygdx.game.MyLog
import mygdx.game.TileMapRender
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.EditorWindow
import mygdx.game.editor.TouchPlace
import mygdx.game.editor.UndoRedo
import mygdx.game.editor.formItems.FormItemSpinnerInt
import mygdx.game.systems.MyEngine
import mygdx.game.tileSets.DrawItemEraser
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import java.awt.Point
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


class TileMapEditor(editorRoot: EditorRoot, private val myEngine: MyEngine) : Group() {

    var switchMode = false
    var fillMode = false
    var drawBlock = false
    val tileMapRender: TileMapRender = myEngine.screenContainer.tileMapRender
    private var layerOffsetWin =
        EditorWindow("CopyArea layerOffset").apply {
            add(FormItemSpinnerInt().also {
                it.valName = "character layer ${TileMapRender.charDrawLvl} , min 0, max ${TileMapRender.WORLD_LAYERS} "
            })
        }
    private var copyArea: Array<Array<Array<Entity>>>? = null
    private var pasteColumn = 0
    private var pasteLine = 0
    private var pasteX = 0
    private var pasteY = 0
    private var leftClickPressed = false
    private var rightClickPressed = false
    private val leftClickCellDown = Point()
    private val leftClickCellUp = Point()
    private val rightClickCellDown = Point()
    private val rightClickCellUp = Point()
    private val mouseMovedPoint = Point()

    private var selectedTile: Entity? = null

    val tileForm: TileForm = TileForm(editorRoot)

    val tileMapWallEditor = TileMapWallEditor(editorRoot, myEngine.screenContainer.tileMapRender)
    var tileDrawItemsMenu: TileDrawItemsMenu
    val toolEditorMenu = ToolEditorMenu(editorRoot, myEngine)

    val touchPlace: TouchPlace
    private val pressAreaButton = VisTextButton("sort eraser to top").also {
        it.onClick {
            if (copyArea != null) {
                for (x in 0 until copyArea!!.size) {
                    for (y in 0 until copyArea!![0].size) {
                         copyArea!![x][y].sortBy { entity ->
                            if (entity.get<TileComponent>()!!.drawItem == DrawItemEraser) {
                                1
                            } else 0
                        }
                    }
                }
            }
        }
    }

    init {
        tileForm.onApply = {
            selectedTile?.let { tileForm.decorateEntity(selectedTile!!) }
        }

        touchPlace = TouchPlace(
            1f,
            1f,
            icon = myEngine.iconStorage.buildByName("5"),
            onMoved = { x, y ->
                mouseMovedPoint.x = x.toInt()
                mouseMovedPoint.y = y.toInt()
            },
            onDragged = { x, y ->
                mouseMovedPoint.x = x.toInt()
                mouseMovedPoint.y = y.toInt()
            },
            onLeftUp = { x, y ->
                touchUpLeft(x.toInt(), y.toInt())
            },
            onRightUp = { x, y -> touchUpRight(x.toInt(), y.toInt()) },
            onLeftDown = { x, y -> touchDownLeft(x.toInt(), y.toInt()) },
            onRightDown = { x, y -> touchDownRight(x.toInt(), y.toInt()) })

        tileDrawItemsMenu = TileDrawItemsMenu(myEngine.graphicResources.tileSetPages, {
            setDrawItem(it)
        }, {
            singleSet = it
        })

        editorRoot.tileMapEditorRoot.addActor(this)
        editorRoot.tileMenuRoot.addActor(tileForm)
        editorRoot.tileMapEditorRoot.addActor(touchPlace)
        editorRoot.tileMenuRoot.addActor(tileDrawItemsMenu)
        editorRoot.tileMenuRoot.addActor(toolEditorMenu)
        editorRoot.tileMenuRoot.addActor(editorRoot.myEngine.screenContainer.tileMapRender.layersForm)
        editorRoot.tileMenuRoot.addActor(layerOffsetWin)
        editorRoot.tileMenuRoot.addActor(pressAreaButton)
    }

    private class TileSet {
        var set: ArrayList<ArrayList<IDrawItem>>? = null
    }

    private var singleSet: ArrayList<ArrayList<IDrawItem>>? = null
    private val savedSingleSet = Array(9) { TileSet() }

    private fun createSingleTile(): Entity {
        val entity = Entity()
        tileForm.decorateEntity(entity)
        val tile = entity.get<TileComponent>()!!
        tile.setDrawItem(myEngine.graphicResources.getDrawItem(tile.drawItemValue))
        return entity
    }

    var insertTileZIndexOffset = 0

    fun setDrawItem(drawItem: IDrawItem) {
        tileForm.classListWidget.setComponentValue(
            TileComponent::class,
            TileComponent::drawItemValue.name,
            drawItem.value
        )
        copyArea = null
        singleSet = null
    }

    fun resizeMap(width: Int, height: Int, alight: Int) {
        var offsetX = 0
        var offsetY = 0
        when (alight) {
            Align.bottomLeft -> {
                offsetX = 0
                offsetY = 0
            }
            Align.topLeft -> {
                offsetX = 0
                offsetY = tileMapRender.worldHeight - height
            }
            Align.bottomRight -> {
                offsetX = tileMapRender.worldWidth - width
                offsetY = 0
            }
            Align.topRight -> {
                offsetX = tileMapRender.worldWidth - width
                offsetY = tileMapRender.worldHeight - height
            }
        }

        val tiles = ArrayList<Entity>()
        for (x0 in 0 until width) {
            for (y0 in 0 until height) {
                for (z in 0 until TileMapRender.WORLD_LAYERS) {
                    val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(x0 + offsetX, y0 + offsetY, z)
                    if (entity != null) {
                        tiles.add(entity)
                        val position = entity.get<MapPositionComponent>()!!
                        position.defWorldX = x0
                        position.defWorldY = y0
                        position.defWorldZ = z
                    }
                }
            }
        }

        tileMapRender.createCleanMap(width, height)
        tiles.forEach {
            it.get<MapPositionComponent>()!!.resetPosition()
            myEngine.engine.addEntity(it)
        }
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        super.draw(batch, parentAlpha)
        width = tileMapRender.width
        height = tileMapRender.height
        for (width in 0 until tileMapRender.worldWidth) {
            for (height in 0 until tileMapRender.worldHeight) {
                if (tileMapRender.cellOnScreen(width, height, 0)) {
                    val w = width * TileMapRender.TILE_WIDTH
                    val h = height * TileMapRender.TILE_HEIGHT
                    if (drawBlock && tileMapRender.mapBlockState.isBlocked(width, height)) {
                        batch.draw(
                            myEngine.iconStorage.buildByName("5").region,
                            w.toFloat(),
                            h.toFloat()
                        )
                    }
                    if (drawBlock && tileMapRender.lightMap.map!![width][height] != null) {
                        batch.draw(
                            myEngine.iconStorage.buildByName("1").region,
                            w.toFloat(),
                            h.toFloat()
                        )
                    }
                }
            }
        }

        if (drawBlock) {
            myEngine.locationsTeleports.list.forEach {
                val w = it.fromX * TileMapRender.TILE_WIDTH
                val h = it.fromY * TileMapRender.TILE_HEIGHT
                batch.draw(
                    myEngine.iconStorage.buildByName("teleport").region,
                    w.toFloat(),
                    h.toFloat()
                )
            }
        }

        var width = 0
        var height = 0
        if (!switchMode) {
            if (copyArea != null) {
                width = copyArea!!.size
                height = copyArea!![0].size
            } else if (singleSet != null) {
                width = singleSet!![0].size
                height = singleSet!!.size
            }
        }
        if (rightClickPressed) {
            drawRect(
                batch,
                rightClickCellDown.x,
                rightClickCellDown.y,
                mouseMovedPoint.x,
                mouseMovedPoint.y,
            )
        } else if (leftClickPressed) {
            if (width > 0 || height > 0) {
                pasteColumn = if (leftClickCellDown.x - mouseMovedPoint.x > 0) {
                    abs(leftClickCellDown.x - mouseMovedPoint.x) / width + 1
                } else (abs(leftClickCellDown.x - mouseMovedPoint.x) + 1) / width
                pasteLine = if (leftClickCellDown.y - mouseMovedPoint.y > 0) {
                    abs(leftClickCellDown.y - mouseMovedPoint.y) / height + 1
                } else (abs(leftClickCellDown.y - mouseMovedPoint.y) + 1) / height
                pasteColumn = max(pasteColumn, 1)
                pasteLine = max(pasteLine, 1)
                pasteX =
                    if (leftClickCellDown.x - mouseMovedPoint.x <= 0) leftClickCellDown.x else leftClickCellDown.x - (pasteColumn - 1) * width
                pasteY =
                    if (leftClickCellDown.y - mouseMovedPoint.y <= 0) leftClickCellDown.y else leftClickCellDown.y - (pasteLine - 1) * height

                drawRect(
                    batch,
                    pasteX,
                    pasteY,
                    pasteX + pasteColumn * width - 1,
                    pasteY + pasteLine * height - 1,
                )
            } else drawRect(
                batch,
                leftClickCellDown.x,
                leftClickCellDown.y,
                mouseMovedPoint.x,
                mouseMovedPoint.y,
            )
        } else if (width != 0 || height != 0) {
            drawRect(
                batch,
                mouseMovedPoint.x,
                mouseMovedPoint.y,
                mouseMovedPoint.x + width - 1,
                mouseMovedPoint.y + height - 1,
            )
        }
    }

    private fun drawRect(batch: Batch, x0: Int, y0: Int, x1: Int, y1: Int) {
        val width = TileMapRender.TILE_WIDTH
        val height = TileMapRender.TILE_HEIGHT
        for (x in min(x0, x1)..max(x0, x1)) {
            batch.draw(
                myEngine.iconStorage.buildByName("10").region,
                (x * width).toFloat(),
                (min(y0, y1) * height).toFloat()
            )
            batch.draw(
                myEngine.iconStorage.buildByName("10").region,
                (x * width).toFloat(),
                (max(y0, y1) * height).toFloat()
            )
        }
        for (y in min(y0, y1)..max(y0, y1)) {
            batch.draw(
                myEngine.iconStorage.buildByName("10").region,
                (min(x0, x1) * width).toFloat(),
                (y * height).toFloat()
            )
            batch.draw(
                myEngine.iconStorage.buildByName("10").region,
                (max(x0, x1) * width).toFloat(),
                (y * height).toFloat()
            )
        }
    }

    private fun callBackForTile(entity: Entity) {
        val tile = entity.get<TileComponent>()!!
        val position = entity.get<MapPositionComponent>()!!
        if (tile.drawItem === DrawItemEraser) {
            with(position) {
                tileMapRender.clearWorldCell(worldX, worldY, worldZ)
            }
        } else {
            tileMapRender.setWorldCells(entity)
            refreshNeighbor(position.worldX, position.worldY, position.worldZ)
        }
    }

    private fun touchDownLeft(cx: Int, cy: Int) {
        leftClickPressed = true
        leftClickCellDown.setLocation(cx, cy)
    }

    private fun touchDownRight(cx: Int, cy: Int) {
        rightClickPressed = true
        rightClickCellDown.setLocation(cx, cy)
    }

    private fun touchUpLeft(cx: Int, cy: Int) {
        leftClickPressed = false
        leftClickCellUp.setLocation(cx, cy)
        val x = min(leftClickCellDown.x, leftClickCellUp.x)
        val y = min(leftClickCellDown.y, leftClickCellUp.y)
        val width = max(leftClickCellDown.x, leftClickCellUp.x) - x
        val height = max(leftClickCellDown.y, leftClickCellUp.y) - y

        if (switchMode) {
            tileMapRender.tileMapContainer.switchTopPlace(x, y, width, height)
        } else if (copyArea != null) {
            pastePlace(pasteX, pasteY, pasteColumn, pasteLine)
        } else {
            if (singleSet != null) {
                pasteCellSet(pasteX, pasteY, pasteColumn, pasteLine)
            } else {
                pasteCells(x, y, width, height, createSingleTile())
            }
        }
        UndoRedo.writeStep()
    }

    private fun touchUpRight(cx: Int, cy: Int) {
        rightClickPressed = false
        rightClickCellUp.setLocation(cx, cy)
        copyPlace(rightClickCellDown.x, rightClickCellDown.y, rightClickCellUp.x, rightClickCellUp.y)
    }

    private fun pastePlace(startPositionX: Int, startPositionY: Int, column: Int, line: Int) {
        for (stepX in 0 until column) {
            for (stepY in 0 until line) {
                for (x in copyArea!!.indices) {
                    for (y in 0 until copyArea!![x].size) {
                        val sendX = startPositionX + x + stepX * copyArea!!.size
                        val sendY: Int = startPositionY + y + stepY * copyArea!![0].size
                        for (z in 0 until copyArea!![x][y].size) {
                            val oz = z + (layerOffsetWin.getChild(1) as FormItemSpinnerInt).value
                            if (oz < TileMapRender.WORLD_LAYERS) {
                                val entity = copyArea!![x][y][z]
                                pullOutDrawItem(sendX, sendY, entity)
                                insertCopyTileTo(entity, sendX, sendY, oz)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun pasteCellSet(startPositionX: Int, startPositionY: Int, column: Int, line: Int) {
        for (stepX in 0 until column) {
            for (stepY in 0 until line) {
                for (y in singleSet!!.indices) {
                    for (x in 0 until singleSet!![x.toInt()].size) {
                        val entity = createSingleTile()
                        val tile = entity.get<TileComponent>()!!
                        tile.applyParam(tile.exportParam())
                        tile.setDrawItem(singleSet!![y][x])

                        val downX = startPositionX + x + stepX * singleSet!![0].size
                        val downY = startPositionY + y + stepY * singleSet!!.size
                        pasteCell(downX, downY, entity)
                    }
                }
            }
        }
    }

    private fun pasteCells(x: Int, y: Int, width: Int, height: Int, entity: Entity) {
        if (fillMode && width == 0 && height == 0)
            for (cx in 0 until tileMapRender.worldWidth) {
                for (cy in 0 until tileMapRender.worldHeight) {
                    pasteCell(cx, cy, entity)
                }
            }
        else
            for (cx in x..x + width) {
                for (cy in y..y + height) {
                    pasteCell(cx, cy, entity)
                }
            }
    }

    private fun pasteCell(x: Int, y: Int, entity: Entity) {
        val drawItem = entity.get<TileComponent>()!!.drawItem
        if (drawItem === DrawItemEraser) {
            for (z in TileMapRender.WORLD_LAYERS - 1 downTo 0) {
                if (eraseCell(x, y, z)) {
                    insertCopyTileTo(entity, x, y, z)
                    break
                }
            }
        } else if (drawItem.drawLevel == DrawLevels.DECORATION) {
            setTileToTop(x = x, y = y, minLayer = insertTileZIndexOffset, entity)
        } else {
            val z = drawItem.drawLevel.ordinal + insertTileZIndexOffset
            insertCopyTileTo(entity, x, y, z)
            eraseUpLevels(x, y, z)
        }
    }

    private fun setTileToTop(x: Int, y: Int, minLayer: Int, entity: Entity) {
        pullOutDrawItem(x, y, entity)
        val z = max(tileMapRender.tileMapContainer.getFreeTopLvl(x, y), minLayer)
        insertCopyTileTo(entity, x, y, z)
    }

    private fun pullOutDrawItem(x: Int, y: Int, entity: Entity): Entity? {
        val drawItem = entity.get<TileComponent>()!!.drawItem
        val sameEntity = tileMapRender.tileMapContainer.findDrawItem(x, y, drawItem)
        if (sameEntity != null) {
            val position = sameEntity.get<MapPositionComponent>()!!
            eraseCell(x, y, position.worldZ)
            tileMapRender.tileMapContainer.forEachFloorToTopLvl(x, y, position.worldZ + 1) { it, z ->
                if (z != TileMapRender.charDrawLvl && tileMapRender.getWorldCell(
                        x,
                        y,
                        z - 1
                    ) == null
                ) {
                    insertCopyTileTo(it, x, y, z - 1)
                    eraseCell(x, y, z)
                }
            }
            return sameEntity
        }
        return null
    }

    private fun eraseCell(x: Int, y: Int, z: Int): Boolean {
        if (tileMapRender.tileMapContainer.getWorldCellIfExist(x, y, z) != null) {
            val entity = Entity()
            entity.add(TileComponent().also { it.setDrawItem(DrawItemEraser) })
                .add(MapPositionComponent().setupWorldPosition(x, y, z))

            insertCopyTileTo(entity, x, y, z)
            return true
        }
        return false
    }

    private fun copyPlace(x0: Int, y0: Int, x1: Int, y1: Int) {
        copyArea = null
        val placeX = min(x0, x1)
        val placeY = min(y0, y1)
        val placeWidth = abs(x0 - x1)
        val placeHeight = abs(y0 - y1)
        if (placeWidth == 0 && placeHeight == 0) {
            for (z in TileMapRender.WORLD_LAYERS - 1 downTo 0) {
                val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(placeX, placeY, z)
                if (entity != null) {
                    copyArea = null
                    singleSet = null
                    selectedTile = entity
                    tileForm.setupEntityValues(selectedTile!!, false)

                    val item = entity.get<TileComponent>()!!.drawItem
                    val position = entity.get<MapPositionComponent>()!!

                    println("path ${item.imagePath} ${item.indexFromSet} x=${position.worldX} y=${position.worldY} z=${position.worldZ}")
                    break
                }
            }
        } else {
            copyArea = buildCopyArea(placeWidth + 1, placeHeight + 1, tileMapRender.layersForm.totalEnabled())
            var layer = 0
            cont@ for (z in 0 until TileMapRender.WORLD_LAYERS) {
                if (tileMapRender.layersForm.isLayerDisabled(z)) continue@cont
                for (x in 0 until copyArea!!.size) {
                    for (y in 0 until copyArea!![0].size) {
                        val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(placeX + x, placeY + y, z)
                        if (entity != null) {
                            copyArea!![x][y][layer] = entity
                        }
                    }
                }
                layer++
            }
        }
    }

    private fun buildCopyArea(width: Int, height: Int, layers: Int): Array<Array<Array<Entity>>>? {
        val copyArea: Array<Array<Array<Entity>>>
        if (width != 0 || height != 0) {
            copyArea = Array(width) {
                Array(height) {
                    Array(
                        layers
                    ) {
                        Entity().add(TileComponent().also { z -> z.setDrawItem(DrawItemEraser) })
                            .add(MapPositionComponent())
                    }
                }
            }
        } else {
            return null
        }
        return copyArea
    }

    private fun eraseUpLevels(x: Int, y: Int, curZ: Int) {
        if (!tileMapRender.checkWorldEdges(x, y, curZ)) {
            return
        }
        for (z in curZ + 1 until TileMapRender.WORLD_LAYERS) {
            val entity = Entity()
            entity.add(TileComponent().also { it.setDrawItem(DrawItemEraser) })
                .add(MapPositionComponent().setupWorldPosition(x, y, z))
            insertCopyTileTo(entity, x, y, z)
        }
    }

    private fun insertCopyTileTo(sample: Entity, x: Int, y: Int, z: Int) {
        if (tileMapRender.checkWorldEdges(x, y, z)) {
            val entity = Entity()
            sample.components.forEach { entity.add((it as MyAbstractComponent).copy()) }
            val position = entity.get<MapPositionComponent>()!!
            position.setupWorldPosition(x, y, z)
            var back = tileMapRender.getWorldCell(x, y, z)
            if (back == null) {
                back = Entity()
                back.add(TileComponent().also { it.setDrawItem(DrawItemEraser) })
                    .add(MapPositionComponent().setupWorldPosition(x, y, z))
            }
            val container = tileMapRender.tileMapContainer
            val arrBefore = container.getTileFriendlyDataArray(x - 1, x + 1, y - 1, y + 1)

            tileMapRender.setWorldCells(entity)
            refreshNeighbor(x, y, z)

            val arrAfter = container.getTileFriendlyDataArray(x - 1, x + 1, y - 1, y + 1)

            UndoRedo.addAction(
                {
                    callBackForTile(entity)
                    container.applyTileFriendlyDataArray(x - 1, y - 1, arrAfter)
                },
                {
                    callBackForTile(back)
                    container.applyTileFriendlyDataArray(x - 1, y - 1, arrBefore)
                }
            )
        }
    }

    private fun refreshNeighbor(x: Int, y: Int, z: Int) {
        if (Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT)) {
            return
        }
        val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(x, y, z)
        if (entity != null) {
            refreshTileInfo(entity)
            refreshNeighborTileInfo(
                entity,
                Gdx.input.isKeyPressed(
                    Input.Keys.SHIFT_LEFT
                )
            )
        }
    }

    private fun refreshNeighborTileInfo(entity: Entity, onlyFriends: Boolean) {
        val tile = entity.get<TileComponent>()!!
        val position = entity.get<MapPositionComponent>()!!
        with(position) {
            refreshNeighborTile(worldX + 1, worldY, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX, worldY + 1, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX - 1, worldY, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX, worldY - 1, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX + 1, worldY + 1, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX - 1, worldY + 1, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX + 1, worldY - 1, worldZ, tile.drawItem, onlyFriends)
            refreshNeighborTile(worldX - 1, worldY - 1, worldZ, tile.drawItem, onlyFriends)
        }
    }

    private fun refreshTileInfo(entity: Entity) {
        val tile = entity.get<TileComponent>()!!
        val position = entity.get<MapPositionComponent>()!!
        with(tile) {
            tileFriendly.right =
                neighborDrawItemIsIdentically(position.worldX + 1, position.worldY, position.worldZ, drawItem)
            tileFriendly.top =
                neighborDrawItemIsIdentically(position.worldX, position.worldY + 1, position.worldZ, drawItem)
            tileFriendly.left =
                neighborDrawItemIsIdentically(position.worldX - 1, position.worldY, position.worldZ, drawItem)
            tileFriendly.bot =
                neighborDrawItemIsIdentically(position.worldX, position.worldY - 1, position.worldZ, drawItem)
            tileFriendly.topRight =
                neighborDrawItemIsIdentically(position.worldX + 1, position.worldY + 1, position.worldZ, drawItem)
            tileFriendly.topLeft =
                neighborDrawItemIsIdentically(position.worldX - 1, position.worldY + 1, position.worldZ, drawItem)
            tileFriendly.botRight =
                neighborDrawItemIsIdentically(position.worldX + 1, position.worldY - 1, position.worldZ, drawItem)
            tileFriendly.botLeft =
                neighborDrawItemIsIdentically(position.worldX - 1, position.worldY - 1, position.worldZ, drawItem)
            tileFriendly.topRight = tileFriendly.top && tileFriendly.right && tileFriendly.topRight
            tileFriendly.topLeft = tileFriendly.top && tileFriendly.left && tileFriendly.topLeft
            tileFriendly.botRight = tileFriendly.bot && tileFriendly.right && tileFriendly.botRight
            tileFriendly.botLeft = tileFriendly.bot && tileFriendly.left && tileFriendly.botLeft
            setTileFriendly(tileFriendly)
        }
    }

    private fun neighborDrawItemIsIdentically(x: Int, y: Int, z: Int, drawItem: IDrawItem): Boolean {
        if (!tileMapRender.checkWorldEdges(x, y, z)) return true
        for (z0 in 0 until TileMapRender.WORLD_LAYERS) {
            if (!Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || x >= min(
                    leftClickCellDown.x,
                    leftClickCellUp.x
                ) && x <= max(leftClickCellDown.x, leftClickCellUp.x) && y >= min(
                    leftClickCellDown.y,
                    leftClickCellUp.y
                ) && y <= max(leftClickCellDown.y, leftClickCellUp.y)
            ) {
                val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(x, y, z0)
                if (entity != null && entity.get<TileComponent>()!!.drawItem === drawItem) {
                    return true
                }
            }
        }
        return false
    }

    private fun refreshNeighborTile(x: Int, y: Int, z: Int, drawItem: IDrawItem, onlyFriend: Boolean) {
        if (!Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || x >= min(
                leftClickCellDown.x,
                leftClickCellUp.x
            ) && x <= max(leftClickCellDown.x, leftClickCellUp.x) && y >= min(
                leftClickCellDown.y,
                leftClickCellUp.y
            ) && y <= max(leftClickCellDown.y, leftClickCellUp.y)
        ) {
            val entity = tileMapRender.tileMapContainer.getWorldCellIfExist(x, y, z)
            if (entity != null) {
                if (!onlyFriend) {
                    refreshTileInfo(entity)
                } else if (entity.get<TileComponent>()!!.drawItem === drawItem
                ) {
                    refreshTileInfo(entity)
                }
            }
        }
    }

    fun saveTilesetSlot(slot: Int) {
        if (!(savedSingleSet.indices).contains(slot)) return

        savedSingleSet[slot].set = singleSet
        MyLog.print("save tileSet $slot")
    }

    fun loadTilesetSlot(slot: Int) {
        if (!(savedSingleSet.indices).contains(slot)) return

        copyArea = null
        singleSet = savedSingleSet[slot].set
        MyLog.print("load tileSet $slot")
    }
}