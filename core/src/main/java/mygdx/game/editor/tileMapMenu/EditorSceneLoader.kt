package mygdx.game.editor.tileMapMenu

import com.badlogic.gdx.files.FileHandle
import ktx.actors.onClick
import ktx.scene2d.scene2d
import ktx.scene2d.table
import ktx.scene2d.textButton
import mygdx.game.editor.EditorWindow
import mygdx.game.loadSave.MyFileHandler
import mygdx.game.loadSave.SceneLoader
import mygdx.game.myWidgets.ScrollPaneVertical
import mygdx.game.systems.MyEngine

class EditorSceneLoader(private val myEngine: MyEngine) : EditorWindow("Scene loader") {

    private var list: ScrollPaneVertical

    init {
        addCloseButton()

        val pair = MyFileHandler.getFilesFromFolder(SceneLoader.locationsPath)
        val names = pair.second
        val file = pair.first
        val table = scene2d.table()

        val pairs = arrayListOf<Pair<String, FileHandle>>()
        names.forEachIndexed { index, s -> pairs.add(s to file[index]) }
        pairs.sortBy { it.first }
        pairs.forEach { p ->
            table.add(scene2d.textButton(p.first) {
                onClick {
                    osSelect(p.second)
                }
            }).left()
            table.row()
        }

        list = ScrollPaneVertical(table)
        add(list)
    }

    private fun osSelect(file: FileHandle) {
        myEngine.editorMode = true
        myEngine.beginDisposeScene {
            SceneLoader().load(myEngine, file.nameWithoutExtension()) {
                myEngine.screenContainer.interfaceRoot.clear()
//                myEngine.loginScreenMenu.remove()
            }
        }
    }
}