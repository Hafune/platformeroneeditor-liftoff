package mygdx.game.editor.tileMapMenu

import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.TileMapRender
import mygdx.game.components.gameWorld.tileComponents.WallComponent
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.TouchPlace
import mygdx.game.editor.UndoRedo
import java.awt.Point
import kotlin.math.max
import kotlin.math.min

class TileMapWallEditor(private val editorRoot: EditorRoot, private val tileMapRender: TileMapRender) {

    private var leftClickPressed = false
    private var rightClickPressed = false

    private val mouseMovedPoint = Point()
    private val leftClickCellDown = Point()
    private val leftClickCellUp = Point()
    private val rightClickDown = Point()
    private val rightClickUp = Point()

    private val place: TouchPlace

    private fun touchDownLeft(cx: Int, cy: Int) {
        leftClickPressed = true
        leftClickCellDown.setLocation(cx, cy)
    }

    private fun touchDownRight(cx: Int, cy: Int) {
        rightClickPressed = true
        rightClickDown.setLocation(cx, cy)
    }

    private fun touchUpLeft(cx: Int, cy: Int) {
        leftClickPressed = false
        leftClickCellUp.setLocation(cx, cy)

        val x0 = min(leftClickCellDown.x, leftClickCellUp.x)
        val x1 = max(leftClickCellDown.x, leftClickCellUp.x)
        val y0 = min(leftClickCellDown.y, leftClickCellUp.y)
        val y1 = max(leftClickCellDown.y, leftClickCellUp.y)
        for (x in x0..x1) {
            for (y in y0..y1) {
                addWall(x, y)
            }
        }
        UndoRedo.writeStep()
    }

    private fun touchUpRight(cx: Int, cy: Int) {
        rightClickPressed = false
        rightClickUp.setLocation(cx, cy)

        val x0 = min(rightClickDown.x, rightClickUp.x)
        val x1 = max(rightClickDown.x, rightClickUp.x)
        val y0 = min(rightClickDown.y, rightClickUp.y)
        val y1 = max(rightClickDown.y, rightClickUp.y)
        for (x in x0..x1) {
            for (y in y0..y1) {
                removeWall(x, y)
            }
        }
        UndoRedo.writeStep()
    }

    private fun removeWall(x: Int, y: Int) {
        const@ for (z in TileMapRender.WORLD_LAYERS - 1 downTo 0) {
            if (!tileMapRender.checkWorldEdges(x, y, z)) continue@const
            val entity = tileMapRender.getWorldCell(x, y, z)
            entity ?: continue@const

            if (entity.get<WallComponent>() != null) {
                entity.remove<WallComponent>()
                UndoRedo.addAction(
                    {
                        entity.remove<WallComponent>()
                    }, {
                        entity.add(WallComponent())
                    })
                return
            }
        }
    }

    private fun addWall(x: Int, y: Int) {
        const@ for (z in TileMapRender.WORLD_LAYERS - 1 downTo 0) {
            if (!tileMapRender.checkWorldEdges(x, y, z)) continue@const
            val entity = tileMapRender.getWorldCell(x, y, z)
            entity ?: continue@const

            if (entity.get<WallComponent>() == null) {
                entity.add(WallComponent())
                UndoRedo.addAction(
                    {
                        entity.add(WallComponent())
                    }, {
                        entity.remove<WallComponent>()
                    })
            }
            return
        }
    }

    fun setEnable(enable: Boolean) {
        if (enable) editorRoot.tileMapEditorRoot.addActor(place)
        else place.remove()
    }

    init {
        place = TouchPlace(
            1f,
            1f,
            icon = editorRoot.myEngine.iconStorage.buildByName("5"),
            onMoved = { x, y ->
                mouseMovedPoint.x = x.toInt()
                mouseMovedPoint.y = y.toInt()
            },
            onDragged = { x, y ->
                mouseMovedPoint.x = x.toInt()
                mouseMovedPoint.y = y.toInt()
            },
            onLeftUp = { x, y -> touchUpLeft(x.toInt(), y.toInt()) },
            onRightUp = { x, y -> touchUpRight(x.toInt(), y.toInt()) },
            onLeftDown = { x, y -> touchDownLeft(x.toInt(), y.toInt()) },
            onRightDown = { x, y -> touchDownRight(x.toInt(), y.toInt()) })
    }
}