package mygdx.game.editor.tileMapMenu

import com.kotcrab.vis.ui.widget.VisCheckBox
import mygdx.game.editor.EditorWindow

class LayersForm(size: Int) : EditorWindow("Layers form") {

    private val list = ArrayList<VisCheckBox>()

    init {
        repeat(size) {
            add(VisCheckBox("Layers ${size - list.size}").also {
                list.add(it)
                it.isChecked = true
            }).row()
        }
        list.reverse()
    }

    fun isLayerDisabled(layer: Int) = !list[layer].isChecked

    fun totalEnabled() = list.fold(0) { acc, cb -> if (cb.isChecked) acc + 1 else acc }
}