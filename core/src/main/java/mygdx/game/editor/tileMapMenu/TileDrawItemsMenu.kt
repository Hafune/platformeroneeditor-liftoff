package mygdx.game.editor.tileMapMenu

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Container
import ktx.scene2d.container
import ktx.scene2d.scene2d
import ktx.scene2d.vis.*
import mygdx.game.myWidgets.Button
import mygdx.game.editor.EditorWindow
import mygdx.game.myWidgets.VerticalTableMultiSelectContainer
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import kotlin.math.max
import kotlin.math.min

class TileDrawItemsMenu(
    val pages:  ArrayList<ArrayList<IDrawItem>>,
    val onSelect: (IDrawItem) -> Unit,
    val onMultiSelect: ((ArrayList<ArrayList<IDrawItem>>) -> Unit)? = null,
    title: String = "Tiles menu"
) : EditorWindow(title) {

    private val pane: KTabbedPane
    private lateinit var box: VerticalTableMultiSelectContainer

    init {

        val table = scene2d.container {
            visTable {
                pane = tabbedPane("vertical") { cell ->
                    cell.growY()
                    pages.forEachIndexed { index, page ->
                        tab("page $index", closeableByUser = false) {
                            container(
                                VerticalTableMultiSelectContainer(
                                    8,
                                    *page.map {
                                        Container(Button(it.icon) { select(it) }).also { c ->
                                            c.size(36f)
                                            c.userObject = it
                                        }
                                    }.toTypedArray()
                                ) { onMultiSelectHandler(it) }.also { box = it }
                            )
                        }
                    }
                }

                val container = visTable().cell(grow = true)
                pane.addTabContentsTo(container)
                pane.switchTab(0)

                setFillParent(true)
            }
        }
        add(table)
        addCloseButton()
    }

    fun switchTab(direction: Int) {
        if (pane.tabs.size == 0) return
        val index = pane.tabs.indexOf(pane.activeTab)
        when (direction) {
            1 -> {
                pane.switchTab(min(index + 1, pane.tabs.size - 1))
            }
            -1 -> {
                pane.switchTab(max(index - 1, 0))
            }
        }
    }

    private fun select(iDrawItem: IDrawItem) {
        onSelect(iDrawItem)
    }

    private fun onMultiSelectHandler(list: ArrayList<Actor>) {
        val singleSet = ArrayList<ArrayList<IDrawItem>>()
        var row = ArrayList<IDrawItem>()
        singleSet.add(row)
        var rowLine = list.first().y
        list.forEach {
            if (it.y != rowLine) {
                row = ArrayList()
                singleSet.add(row)
                rowLine = it.y
            }
            row.add(it.userObject as IDrawItem)
        }
        singleSet.reverse()
        onMultiSelect?.invoke(singleSet)
    }
}