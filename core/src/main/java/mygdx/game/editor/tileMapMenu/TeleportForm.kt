package mygdx.game.editor.tileMapMenu

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.XmlReader
import com.kotcrab.vis.ui.widget.VisScrollPane
import com.kotcrab.vis.ui.widget.VisWindow
import ktx.actors.onClick
import ktx.actors.onKeyDown
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.scene2d.container
import ktx.scene2d.scene2d
import ktx.scene2d.textButton
import mygdx.game.MapData
import mygdx.game.MyLog
import mygdx.game.TileContainer
import mygdx.game.TileMapRender
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.EditorRoot
import mygdx.game.lib.MyFileChooserOpen
import mygdx.game.editor.TouchPlace
import mygdx.game.editor.formItems.FormItemSpinnerInt
import mygdx.game.editor.formItems.FormItemTextField
import mygdx.game.language.GameText
import mygdx.game.lib.MyObjectMapper
import mygdx.game.loadSave.EntityLoader
import mygdx.game.loadSave.MapLoader.Companion.TILE_XML_CONTAINER_NAME
import mygdx.game.editor.EditorWindow
import mygdx.game.systems.MyEngine
import mygdx.game.systems.gameWorld.ComponentInitializer
import kotlin.math.max
import kotlin.math.sign

class TeleportForm(private val editor: EditorRoot, private val myEngine: MyEngine) : EditorWindow("Teleport form") {

    private val locationTo = FormItemTextField()
    private val toX = FormItemSpinnerInt().apply { valName = "toX" }
    private val toY = FormItemSpinnerInt().apply { valName = "toY" }
    private val angle = FormItemSpinnerInt().apply { valName = "angle" }

    private lateinit var touchPlace: TouchPlace
    private val locationButton = scene2d.textButton(GameText["Editor.TeleportForm.selectLocationButton"]) {
        onClick {
            selectLocation()
        }
    }

    private var engine: Engine? = null
    private var tileMapContainer: TileContainer? = null

    private var zoom = 1f

    private val render = object : Actor() {
        override fun draw(batch: Batch, parentAlpha: Float) {
            for (z in 0 until tileMapContainer!!.worldLayers) {
                for (my in 0 until tileMapContainer!!.worldHeight) {
                    for (mx in 0 until tileMapContainer!!.worldWidth) {
                        val tile = tileMapContainer!!.worldCells[mx][my][z]?.get<TileComponent>()
                        if (tile != null) {
//                            tile.container.draw(batch, parentAlpha)
                            tile.container.act(Gdx.graphics.deltaTime)
                            tile.animationUpdate(myEngine.worldProcessingSystem.time, Gdx.graphics.deltaTime)
                            val drawX = x + mx * TileMapRender.TILE_WIDTH
                            val drawY = y + my * TileMapRender.TILE_HEIGHT
                            tile.draw(batch, drawX, drawY)
                        }
                    }
                }
            }
        }
    }

    private val back = Actor().also {
        it.onClick {
            hide()
        }
        it.onKeyDown { keyCode ->
            if (keyCode == Input.Keys.ESCAPE) {
                hide()
            }
        }
    }
    private val container = Container(render).also { it.isTransform = true }
    private val scroll = VisScrollPane(container).also { it.setFillParent(true) }
    private val mapWindow = EditorWindow("Select teleport point").apply {
        scroll.listeners.removeIndex(1)
        add(scene2d.container(scroll)).expand()
    }

    @Suppress("UNCHECKED_CAST")
    private val filePicker = MyFileChooserOpen {
        val xmlReader = XmlReader()
        val root = xmlReader.parse(it)

        val xml = root.getChildByName(TILE_XML_CONTAINER_NAME)
        val mapData =
            MapData(MyObjectMapper.readValue(xml.text, HashMap::class.java) as HashMap<String, Any?>)

        engine = Engine()
        engine!!.addEntityListener(
            allOf(TileComponent::class).get(),
            object : ComponentInitializer.MyListener() {
                override fun entityAdded(entity: Entity) {
                    with(entity.get<TileComponent>()!!) {
                        tileFriendly.setValue(getTileFriendlyValue())
                        setFrameValue(frameState)
                        setDrawItem(myEngine.graphicResources.getDrawItem(drawItemValue))
                    }
                }
            })
        tileMapContainer = TileContainer(mapData.width!!, mapData.height!!, TileMapRender.WORLD_LAYERS, engine!!)
        EntityLoader().load(xml).forEach { entity ->
            engine!!.addEntity(entity)
        }
        render.width = (tileMapContainer!!.worldWidth * TileMapRender.TILE_WIDTH).toFloat()
        render.height = (tileMapContainer!!.worldHeight * TileMapRender.TILE_HEIGHT).toFloat()
        locationTo.value = it.nameWithoutExtension()
        showMap()
    }.also {
        it.setDirectory(Gdx.files.localStoragePath + "/locations")
    }

    private fun showMap() {
        back.width = Gdx.app.graphics.width.toFloat()
        back.height = Gdx.app.graphics.height.toFloat()
        editor.editorsMenuRoot.addActor(back)
        editor.editorsMenuRoot.addActor(mapWindow)
        back.stage.keyboardFocus = back
    }

    private fun hide() {
        back.stage.keyboardFocus = null
        back.remove()
        mapWindow.fadeOut()
    }

    override fun fadeIn(): VisWindow {
        editor.tileMapEditorRoot.addActor(touchPlace)
        return super.fadeIn()
    }

    override fun close() {
        touchPlace.remove()
        super.close()
    }

    private fun selectLocation() {
        editor.editorsMenuRoot.addActor(filePicker.fadeIn())
    }

    init {
        addCloseButton()

        add(locationButton)
        row()
        add(locationTo)
        row()
        add(toX)
        row()
        add(toY)
        row()
        add(angle)

        render.addListener(object : ClickListener() {
            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (button == 1) {
                    toX.value = (x / TileMapRender.TILE_WIDTH).toInt()
                    toY.value = (y / TileMapRender.TILE_HEIGHT).toInt()
                }
                render.stage.scrollFocus = render
                return false
            }

            override fun scrolled(event: InputEvent, x: Float, y: Float, amountX: Float, amountY: Float): Boolean {
                zoom += sign(amountY) * .2f
                zoom = max(zoom, 1f)
                container.setScale(1 / zoom)
                return true
            }
        })


        touchPlace = TouchPlace(
            1f,
            1f,
            icon = myEngine.iconStorage.buildByName("teleport"),
            onLeftUp = { _, _ ->
                myEngine.locationsTeleports.buildTeleportArea(
                    locationTo.value,
                    toX.value,
                    toY.value,
                    angle.value.toFloat(),
                    touchPlace.lcmTouchDown,
                    touchPlace.lcmTouchUp
                )
            },
            onRightUp = { x, y ->
                MyLog.print("$x $y")
                if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    myEngine.locationsTeleports.list.firstOrNull { it.fromX == x.toInt() && it.fromY == y.toInt() }
                        ?.let {
                            locationTo.value = it.locationTo
                            toX.value = it.toX
                            toY.value = it.toY
                            angle.value = it.direction.toInt()
                        }
                } else {
                    myEngine.locationsTeleports.removeTeleportArea(
                        touchPlace.rcmTouchDown,
                        touchPlace.rcmTouchUp
                    )
                }
            }
        )
    }
}

