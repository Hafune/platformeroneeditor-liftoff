package mygdx.game.editor.tileMapMenu

import com.kotcrab.vis.ui.layout.FlowGroup
import ktx.actors.onChange
import ktx.scene2d.image
import ktx.scene2d.imageButton
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visTextButton
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.TouchPlace
import mygdx.game.light.Lights
import mygdx.game.myWidgets.Button
import mygdx.game.editor.EditorWindow
import mygdx.game.systems.MyEngine
import mygdx.game.tileSets.DrawItemEraser

class ToolEditorMenu(editorRoot: EditorRoot, myEngine: MyEngine) :
    EditorWindow("Tool menu") {

    init {
        addCloseButton()
        val place = TouchPlace(
            1f,
            1f,
            icon = myEngine.iconStorage.buildByName("5"),
            onLeftUp = { x, y ->
                myEngine.screenContainer.tileMapRender.lightMap.editLightMap(Lights.Lamp, x.toInt(), y.toInt())
            },
            onRightUp = { x, y ->
                myEngine.screenContainer.tileMapRender.lightMap.editLightMap(Lights.NoLight, x.toInt(), y.toInt())
            })


        add(scene2d.imageButton("toggle") {
            val iconOff = myEngine.iconStorage.buildByName("5")
            val iconOn = myEngine.iconStorage.buildByName("switch")
            image(iconOn.region)
            onChange {
                editorRoot.tileMapEditor.switchMode = isChecked
                if (isChecked) editorRoot.tileMapEditor.touchPlace.changeIcon(iconOn)
                else editorRoot.tileMapEditor.touchPlace.changeIcon(iconOff)
            }
        })

        with(FlowGroup(false)) {
            addActor(
                scene2d.visTextButton("edit walls", "toggle") {
                    onChange {
                        editorRoot.tileMapEditor.tileMapWallEditor.setEnable(isChecked)
                    }
                })

            addActor(scene2d.visTextButton("grid", "toggle") {
                onChange {
                    editorRoot.tileMapEditor.drawBlock = isChecked
                    myEngine.myBox2dHandler.drawDebug = isChecked
                }
            })

            addActor(scene2d.visTextButton("fill map mode", "toggle") {
                onChange {
                    editorRoot.tileMapEditor.fillMode = isChecked
                }
            })
            this@ToolEditorMenu.add(this)
        }

        add(Button(myEngine.iconStorage.buildByName("eraser").region) {
            editorRoot.tileMapEditor.setDrawItem(
                DrawItemEraser
            )
        })
        add(Button(myEngine.iconStorage.buildByName("1").region) { editorRoot.tileMapEditorRoot.addActor(place) })
        add(Button(myEngine.iconStorage.buildByName("2").region) { place.remove() })
        add(Button(myEngine.iconStorage.buildByName("3").region) {
            if (editorRoot.tileMenuRoot.isVisible) {
                editorRoot.tileMenuRoot.isVisible = false
                editorRoot.tileMapEditorRoot.isVisible = false
                editorRoot.characterMapEditor.characterMenuRoot.isVisible = true
                editorRoot.characterMapEditor.characterMapEditorRoot.isVisible = true
                editorRoot.characterMapEditor.characterMenuRoot.addActor(this)
            } else {
                editorRoot.tileMenuRoot.isVisible = true
                editorRoot.tileMapEditorRoot.isVisible = true
                editorRoot.characterMapEditor.characterMenuRoot.isVisible = false
                editorRoot.characterMapEditor.characterMapEditorRoot.isVisible = false
                editorRoot.tileMenuRoot.addActor(this)
            }
        })
    }
}