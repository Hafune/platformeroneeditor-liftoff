package mygdx.game.editor.tileMapMenu

import com.badlogic.ashley.core.Entity
import mygdx.game.TileMapRender
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.gameWorld.BuildActionComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.InteractRadiusComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.SlowDownComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.ComponentFormWin
import mygdx.game.editor.formItems.FormItemSelectBox
import mygdx.game.editor.tileMapMenu.TileLayer.BACKGROUND
import mygdx.game.lib.Object.getObjectKey
import mygdx.game.myWidgets.Option
import java.util.stream.Stream
import kotlin.reflect.KClass

object TileLayer {
    const val BACKGROUND = "Background"
    const val FOREGROUND = "Foreground"
}

@Suppress("UNCHECKED_CAST")
class TileForm(editorRoot: EditorRoot) :
    ComponentFormWin<MyAbstractComponent>(
        title = "Tile form",
        editorRoot = editorRoot,
        default = Entity().also { entity ->
            Stream.of(
                TileComponent(),
                MapPositionComponent()
            ).forEach {
                entity.add(it)
            }
        },

        addedComponents = arrayOf(
            InteractComponent::class,
            InteractRadiusComponent::class,
            BuildActionComponent::class,
            SlowDownComponent::class
        ) as Array<KClass<MyAbstractComponent>>,

        widgets = arrayOf(
            FormItemSelectBox<String>(*Option.buildFromObject(TileLayer)) {
                editorRoot.tileMapEditor.insertTileZIndexOffset =
                    if (it == BACKGROUND) 0 else TileMapRender.charDrawLvl
            }.apply { valName = getObjectKey(TileLayer) }
        )
    )