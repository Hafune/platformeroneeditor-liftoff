package mygdx.game.editor

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.VisTextField
import ktx.actors.onKeyDown
import ktx.actors.onKeyUp
import ktx.scene2d.Scene2DSkin
import mygdx.game.TileMapRender
import mygdx.game.components.CameraTargetComponent
import mygdx.game.components.IgnoreDisposeSceneComponent
import mygdx.game.components.gameWorld.IgnoreSaveComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.characterMenu.CharacterMapEditor
import mygdx.game.editor.tileMapMenu.TileMapEditor
import mygdx.game.lib.bottom
import mygdx.game.lib.left
import mygdx.game.loadSave.SceneExporter
import mygdx.game.systems.MyEngine

class EditorRoot(val myEngine: MyEngine) {

    val mapEditorsRoot = Group()
    val editorsMenuRoot = object : Group() {
        override fun act(delta: Float) {
            update()
            super.act(delta)
        }
    }
    val tileMenuRoot = Group()
    val tileMapEditorRoot = Group()
    val tileMapEditor: TileMapEditor
    private val editorMenu: EditorMenu

    val characterMapEditor: CharacterMapEditor
    private val sceneExporter = SceneExporter(myEngine)

    private val position = PositionComponent()
    private val cameraEntity = Entity().also { entity ->
        entity.add(position)
        entity.add(CameraTargetComponent().also {
            it.weight = 1000
        })
        entity.add(IgnoreSaveComponent())
        entity.add(IgnoreDisposeSceneComponent())
    }

    @Suppress("UNCHECKED_CAST")
    private fun update() {
        mapEditorsRoot.setSize(mapEditorsRoot.parent.width, mapEditorsRoot.parent.height)
        tileMapEditorRoot.setSize(tileMapEditorRoot.parent.width, tileMapEditorRoot.parent.height)
        characterMapEditor.updateBounds()

        val camera = myEngine.screenContainer.camera
        editorsMenuRoot.x = camera.left()
        editorsMenuRoot.y = camera.bottom()

        val cameraWidth = if (myEngine.editorMode) 0f else camera.viewportWidth * camera.zoom
        val cameraHeight = if (myEngine.editorMode) 0f else camera.viewportHeight * camera.zoom

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            if (!mapEditorsRoot.isVisible) {
                editorsMenuRoot.isVisible = true
                mapEditorsRoot.isVisible = true

                position.x = camera.position.x / TileMapRender.TILE_WIDTH
                position.y = camera.position.y / TileMapRender.TILE_HEIGHT

                try {
                    myEngine.engine.addEntity(cameraEntity)
                } catch (_: Exception) {
                }
            } else {
                editorsMenuRoot.isVisible = false
                mapEditorsRoot.isVisible = false

                myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
                myEngine.worldProcessingSystem.setProcessing(true)

                myEngine.screenContainer.interfaceRoot.isVisible = true

                if (!myEngine.editorMode) myEngine.engine.removeEntity(cameraEntity)
            }
        }

        val tileMapRender = myEngine.screenContainer.tileMapRender

        val vector =
            myEngine.cameraUpdateSystem.calcInnerCameraPosition(
                Vector2(
                    position.x * TileMapRender.TILE_WIDTH,
                    position.y * TileMapRender.TILE_HEIGHT
                ),
                cameraWidth,
                cameraHeight,
                tileMapRender.width,
                tileMapRender.height
            )
        position.x = vector.x / TileMapRender.TILE_WIDTH
        position.y = vector.y / TileMapRender.TILE_HEIGHT

        val cameraMovementSpeed = .2f
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            position.x -= cameraMovementSpeed
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            position.x += cameraMovementSpeed
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            position.y -= cameraMovementSpeed
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            position.y += cameraMovementSpeed
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_MULTIPLY)) {
            if (myEngine.cameraUpdateSystem.zoom == 1f) {
                //настройки камеры
                myEngine.cameraUpdateSystem.zoom = .89f
            } else myEngine.cameraUpdateSystem.zoom = 1f
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F5)) {

            sceneExporter.exportMap()
            sceneExporter.exportCharacters()
            sceneExporter.exportTeleport()
        }
//        if (Gdx.input.isKeyJustPressed(Input.Keys.MINUS)) {
//            val entity = Entity().apply {
//                val component = InterpolationComponent()
//                component.range = 528f
//                component.totalTime = 10f
//                component.interpolations =
////                    arrayListOf(
////                        MyInterpolation().also {
////                            it.interpolation = CircleIn
////                            it.scale = .5f
////                        },
////                        MyInterpolation().also {
////                            it.interpolation = CircleOut
////                            it.valueOffset = .5f
////                            it.scale = .5f
////                        },
////                        MyInterpolation().also {
////                            it.interpolation = CircleOut
////                            it.reverse = true
////                            it.valueOffset = .5f
////                            it.scale = .5f
////                        },
////                        MyInterpolation().also {
////                            it.interpolation = CircleIn
////                            it.reverse = true
////                            it.scale = .5f
////                        })
//                arrayListOf(
//                    MyInterpolation().also {
//                        it.interpolation = Linear
//                    }
//                )
//                val a = MyObjectMapper.writeValueAsString(component.exportParam())
//                val b = InterpolationComponent()
//                val param = MyObjectMapper.readValue(a, HashMap::class.java) as HashMap<String, Any>
//                b.applyParam(param)
//                add(b)
//                PlayAnimationFlipbook()
//            }
//            myEngine.engine.addEntity(entity)
//        }
    }

    init {
        Scene2DSkin.defaultSkin = VisUI.getSkin()

        myEngine.screenContainer.tileMapRender.addActor(mapEditorsRoot)
        mapEditorsRoot.isVisible = false
        mapEditorsRoot.touchable = Touchable.childrenOnly
        mapEditorsRoot.addActor(tileMapEditorRoot)

        myEngine.screenContainer.stage.addActor(editorsMenuRoot)
        editorsMenuRoot.isVisible = false

        editorsMenuRoot.addActor(tileMenuRoot)
        characterMapEditor = CharacterMapEditor(this, myEngine)

        tileMapEditor = TileMapEditor(this, myEngine)

        val keySet = HashSet<Int>()
        editorsMenuRoot.onKeyDown(catchEvent = true) {
            if (stage.keyboardFocus is VisTextField) return@onKeyDown

            keySet.add(it)

            val drawItemsMenuTabSwitch: (direction: Int) -> Unit = { d -> tileMapEditor.tileDrawItemsMenu.switchTab(d) }
            val saveSlot: (slot: Int) -> Unit = { s -> tileMapEditor.saveTilesetSlot(s) }
            val loadSlot: (slot: Int) -> Unit = { s -> tileMapEditor.loadTilesetSlot(s) }

            when (it) {
                Input.Keys.DOWN -> {
                    drawItemsMenuTabSwitch(1)
                }
                Input.Keys.UP -> {
                    drawItemsMenuTabSwitch(-1)
                }
                Input.Keys.E -> {
                    drawItemsMenuTabSwitch(1)
                }
                Input.Keys.Q -> {
                    drawItemsMenuTabSwitch(-1)
                }
            }

            if (keySet.contains(Input.Keys.ALT_LEFT) && (7..16).contains(it)) {
                saveSlot(it - 7)
            } else if ((7..16).contains(it)) {
                loadSlot(it - 7)
            }

            UndoRedo.checkInput()
        }
        editorsMenuRoot.onKeyUp {
            keySet.remove(it)
        }

        editorMenu = EditorMenu(this, myEngine)
        editorMenu.width = myEngine.screenContainer.interfaceRoot.width
        editorMenu.y = myEngine.screenContainer.interfaceRoot.height - editorMenu.height
        editorsMenuRoot.addActor(editorMenu)
    }
}