package mygdx.game.editor

import mygdx.game.editor.formItems.*
import mygdx.game.lib.MyClassForName
import java.math.BigDecimal
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.starProjectedType
import kotlin.reflect.jvm.isAccessible

@Suppress("UNCHECKED_CAST")
class FormAnnotationHandler {

    fun buildForms(modelClass: KClass<*>): ArrayList<FormItem<*>> {
        val arr = ArrayList<FormItem<*>>()
        val values = modelClass.findAnnotation<FormSupport>()?.values
        if (values != null) {
            for (i in values.indices) {
                MyClassForName.get<FormItem<*>>(values[i])
                    .apply {
                        valName = FormItemFactory.IGNORE_SAVE_FORM
                        arr.add(this)
                    }
            }
        }
        modelClass.memberProperties.forEach {
            if (it !is KMutableProperty<*>) return@forEach
            it as KMutableProperty<Any>

            if (it.hasAnnotation<Form>()) {
                MyClassForName.get<FormItem<*>>(it.findAnnotation<Form>()!!.kClass)
                    .apply {
                        valName = it.name
                        arr.add(this)
                    }
            } else if (it.hasAnnotation<FormDefault>()) {
                buildItemForValue(it.returnType)
                    .apply {
                        valName = it.name
                        arr.add(this)
                    }
            }
        }
        return arr
    }

    private fun buildItemForValue(value: Any?): FormItem<*> {
        return when (value) {
            Boolean::class.starProjectedType -> FormItemCheckBox()
            Int::class.starProjectedType -> FormItemSpinnerInt()
            Float::class.starProjectedType -> FormItemSpinnerFloat()
            Double::class.starProjectedType -> FormItemSpinnerFloat()
            BigDecimal::class.starProjectedType -> FormItemSpinnerFloat()
            String::class.starProjectedType -> FormItemTextField()
            StringBuilder::class.starProjectedType -> FormItemTextField()
            else -> FormItemTextField()
        }
    }

    fun attachFormsToModel(model: Any, forms: ArrayList<FormItem<*>>) {
        model.javaClass.kotlin.memberProperties.forEach {
            val form = findForm(forms, it.name) ?: return@forEach

            fillFormFromProperty(model, it, form)
            form.valueOwner = model
            form.valueLink = it as KMutableProperty<Any>
        }
    }

    fun fillFormsFromModel(model: Any, forms: ArrayList<FormItem<*>>) {
        model.javaClass.kotlin.memberProperties.forEach {
            val form = findForm(forms, it.name) ?: return@forEach

            fillFormFromProperty(model, it, form)
        }
    }

    private fun fillFormFromProperty(model: Any, prop: KProperty1<Any, *>, form: FormItem<Any>) {
        if (!prop.hasAnnotation<FormDefault>() && !prop.hasAnnotation<Form>()) return
        if (prop !is KMutableProperty<*>) return
        prop.isAccessible = true

        prop as KMutableProperty<Any>

        form.value = prop.getValue(model, prop) as Any
    }

    private fun findForm(forms: ArrayList<FormItem<*>>, valName: String): FormItem<Any>? {
        val form = forms.find { f -> f.valName == valName }
        form ?: return null
        return form as FormItem<Any>
    }

    fun fillModelFromForms(model: Any, forms: ArrayList<FormItem<*>>) {
        model.javaClass.kotlin.memberProperties.forEach {
            if (!it.hasAnnotation<FormDefault>() && !it.hasAnnotation<Form>()) return@forEach
            if (it !is KMutableProperty<*>) return@forEach
            it.isAccessible = true

            it as KMutableProperty<Any>

            val form = forms.find { f -> f.valName == it.name }
            form ?: return@forEach
            form as FormItem<Any>

            it.setter.call(model, form.value)
        }
    }
}