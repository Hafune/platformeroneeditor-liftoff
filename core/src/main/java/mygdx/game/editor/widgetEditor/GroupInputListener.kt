package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.ui.*
import ktx.actors.onTouchDown
import ktx.actors.setKeyboardFocus
import ktx.actors.setScrollFocus
import mygdx.game.editor.widgetEditor.factory.GroupFactory
import mygdx.game.editor.widgetEditor.factory.ImageButtonFactory
import mygdx.game.editor.widgetEditor.factory.ImageFactory
import mygdx.game.editor.widgetEditor.factory.LabelFactory
import mygdx.game.myWidgets.Option
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
internal class GroupInputListener(
    private val skin: Skin,
    private val widgetOptions: List<Option<KClass<out Actor>>>,
    private val tenDrawableOptions: List<Option<String>>,
    private val textureOptions: List<Option<Texture>>,
    private val styleMap: HashMap<KClass<*>, List<Option<String>>>,
    private val buildGroupListener: () -> GroupInputListener
) : InputListener() {

    private val tooltipManager = TooltipManager().also { it.instant() }

    private fun onClassSelect(clazz: KClass<*>, group: Group, x: Float, y: Float) {
        fun addChild(actor: Actor) {
            actor.x = x
            actor.y = y
            group.addActor(actor)
        }
        when (clazz) {
            Group::class -> {
                val actor = GroupFactory().buildGroup(buildGroupListener())
                addChild(actor)
            }
            Label::class -> {
                addOptions(
                    x,
                    y,
                    group,
                    styleMap[Label.LabelStyle::class]!!,
                    buildTooltip = {
                        Tooltip(Label("Label", skin, it), tooltipManager)
                    })
                { style ->
                    addChild(LabelFactory().buildLabel(skin = skin, style = style))
                }
            }
            ImageButton::class -> {
                addOptions(
                    x,
                    y,
                    group,
                    styleMap[ImageButton.ImageButtonStyle::class]!!,
                    buildTooltip = {
                        Tooltip(ImageButton(skin, it), tooltipManager)
                    }
                ) { style ->
                    addChild(ImageButtonFactory().buildImageButton(skin, style))
                }
            }
            Image::class -> {
                val drawables = "drawables"
                val textures = "textures"
                val imageOptions = listOf(drawables, textures).map { Option(it, it) }
                addOptions(
                    x,
                    y,
                    group,
                    imageOptions
                ) { option ->
                    when (option) {
                        drawables ->
                            addOptions(
                                x,
                                y,
                                group,
                                tenDrawableOptions,
                                buildTooltip = {
                                    Tooltip(Image(skin, it), tooltipManager)
                                }
                            ) { drawableName ->
                                addChild(ImageFactory().buildImage(skin, drawableName))
                            }
                        textures ->
                            addOptions(
                                x,
                                y,
                                group,
                                textureOptions,
                                buildTooltip = {
                                    Tooltip(Image(it), tooltipManager)
                                }
                            ) { texture ->
                                addChild(ImageFactory().buildImage(texture))
                            }
                    }
                }
            }
        }
    }

    override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
        //rcm
        if (button == 1) {
            val group = event.listenerActor as Group
            addOptions(x, y, group, widgetOptions) { onClassSelect(it, group, x, y) }
        }
        event.stop()
        return true
    }

    private fun <T> addOptions(
        x: Float,
        y: Float,
        group: Group,
        list: List<Option<T>>,
        buildTooltip: ((value: T) -> InputListener)? = null,
        onSelect: (value: T) -> Unit
    ) {
        val verticalGroup = OptionsVerticalGroup(*list.toTypedArray(), buildTooltip = buildTooltip)

        val back = OutsideClickBackground()
        back.onTouchDown {
            back.remove()
            verticalGroup.remove()
        }

        verticalGroup.onSelect = {
            back.remove()
            verticalGroup.remove()
            onSelect(it)
        }

        group.addActor(back)
        group.addActor(verticalGroup)

        verticalGroup.x = x - verticalGroup.width / 2
        verticalGroup.y = y - verticalGroup.height

        val screenPos = verticalGroup.localToStageCoordinates(Vector2())
        if (screenPos.y < 0) verticalGroup.y -= screenPos.y

        verticalGroup.setKeyboardFocus()
        verticalGroup.setScrollFocus()
    }
}