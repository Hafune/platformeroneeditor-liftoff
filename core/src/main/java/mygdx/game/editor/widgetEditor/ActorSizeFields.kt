package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Table

internal class ActorSizeFields(actor: Actor) : Table() {
    private val xLabel = FloatPropertyField()
    private val yLabel = FloatPropertyField()
    private val widthLabel = FloatPropertyField()
    private val heightLabel = FloatPropertyField()

    init {
        xLabel.attachProperty(actor, "getX", "setX")
        yLabel.attachProperty(actor, "getY", "setY")
        widthLabel.attachProperty(actor, "getWidth", "setWidth")
        heightLabel.attachProperty(actor, "getHeight", "setHeight")

        left()
        add(MyValueName("x", xLabel)).growX()
        add(MyValueName("y", yLabel)).growX()
        row()
        add(MyValueName("width", widthLabel)).growX()
        add(MyValueName("height", heightLabel)).growX()
    }
}