@file:Suppress("UNCHECKED_CAST")

package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.fasterxml.jackson.core.type.TypeReference
import mygdx.game.lib.MyObjectMapper
import mygdx.game.lib.TextureStorage

class ActorBuilder(private val skin: Skin) {

    private val hash = HashMap<Any, List<SceneWidgetData>>()
    private val typeRef = object : TypeReference<List<SceneWidgetData>>() {}

    fun build(file: FileHandle): List<Actor> {
        if (!hash.containsKey(file)){
            hash[file] = MyObjectMapper.readValue(file.file(), typeRef)
        }
        val data = hash[file]!!
        return data.map { buildActors(it) }
    }

    fun build(json: String): List<Actor> {
        val data = MyObjectMapper.readValue(json, typeRef)
        return data.map { buildActors(it) }
    }

    private fun buildActors(data: SceneWidgetData): Actor {
        val actor: Actor
        when (data.clazz) {
            Label::class.qualifiedName -> {
                actor = Label(data.text, skin, data.style)
            }
            ImageButton::class.qualifiedName -> {
                actor = ImageButton(skin, data.style)
            }
            Image::class.qualifiedName -> {
                actor = if (data.drawableName != null) {
                    Image(skin, data.drawableName)
                } else {
                    Image(TextureStorage[data.texturePath!!])
                }
            }
            Group::class.qualifiedName -> {
                actor = Group()
                data.children.forEach {
                    actor.addActor(buildActors(it))
                }
            }
            else -> {
                actor = Label("UNKNOWN actor", skin, data.style)
            }
        }
        actor.x = data.x
        actor.y = data.y
        actor.width = data.width
        actor.height = data.height
        actor.name = data.name

        return actor
    }
}
