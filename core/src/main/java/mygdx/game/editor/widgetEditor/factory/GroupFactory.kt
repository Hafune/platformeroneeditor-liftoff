package mygdx.game.editor.widgetEditor.factory

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Group
import mygdx.game.editor.widgetEditor.EDITOR_MARK
import mygdx.game.editor.widgetEditor.GroupInputListener
import mygdx.game.lib.MyDragHandler
import mygdx.game.lib.onTouchDownCloseEvent

internal class GroupFactory {
    fun buildGroup(groupInputListener: GroupInputListener): Group {
        val actor = Group()
        actor.userObject = EDITOR_MARK
        actor.debug = true
        actor.width = 100f
        actor.height = 100f
        actor.onTouchDownCloseEvent { e, ex, ey ->
            if (e.button == 0) {
                val vec = actor.localToActorCoordinates(actor.parent, Vector2(ex, ey))
                actor.listeners.firstOrNull { it is MyDragHandler } ?: addMenu(
                    x = vec.x,
                    y = vec.y,
                    actor = actor
                )
            }
        }
        actor.addListener(groupInputListener)

        return actor
    }

    private fun addMenu(actor: Group, x: Float, y: Float) {
        SettingWindowFactory().build(
            "Group options",
            actor = actor,
            x = x,
            y = y,
            isDraggable = true,
            isResizable = true,
        )
        actor.debug = true
    }
}