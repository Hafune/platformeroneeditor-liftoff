package mygdx.game.editor.widgetEditor.factory

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import mygdx.game.editor.widgetEditor.EDITOR_MARK
import mygdx.game.lib.MyDragHandler
import mygdx.game.lib.onTouchDownCloseEvent

internal class ImageFactory {

    fun buildImage(skin: Skin, drawableName: String): Image {
        return markForEditor(Image(skin, drawableName))
    }

    fun buildImage(texture: Texture): Image {
        return markForEditor(Image(texture))
    }

    private fun markForEditor(actor: Image): Image {
        actor.userObject = EDITOR_MARK
        actor.onTouchDownCloseEvent { e, ex, ey ->
            val vec = actor.localToActorCoordinates(actor.parent, Vector2(ex, ey))
            actor.listeners.firstOrNull { it is MyDragHandler } ?: addMenu(
                x = vec.x,
                y = vec.y,
                actor = actor
            )
        }

        actor.onTouchDownCloseEvent { e, ex, ey ->
            if (e.button == 0) {
                val vec = actor.localToActorCoordinates(actor.parent, Vector2(ex, ey))
                actor.listeners.firstOrNull { it is MyDragHandler } ?: addMenu(
                    x = vec.x,
                    y = vec.y,
                    actor = actor
                )
            }
        }
        return actor
    }

    private fun addMenu(actor: Image, x: Float, y: Float) {
        SettingWindowFactory().build(
            "Image options",
            actor = actor,
            x = x,
            y = y,
            isDraggable = true,
            isResizable = true,
            debugOnFocus = true
        )
    }
}
