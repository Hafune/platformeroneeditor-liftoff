package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.Actor

internal class OutsideClickBackground : Actor() {
    init {
        x = -50000f
        y = -50000f
        width = 100000f
        height = 100000f
    }
}