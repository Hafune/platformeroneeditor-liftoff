package mygdx.game.editor.widgetEditor

internal class SceneWidgetData {
    var clazz = ""
    var style = "default"
    var name: String? = null
    var text: String? = null
    var texturePath:String? = null
    var drawableName:String? = null
    var alight: Int? = null
    var x = 0f
    var y = 0f
    var width = 0f
    var height = 0f
    var children = ArrayList<SceneWidgetData>()
}