@file:Suppress("UNCHECKED_CAST")

package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.kotcrab.vis.ui.widget.VisTextButton
import com.kotcrab.vis.ui.widget.VisWindow
import ktx.actors.onClick
import mygdx.game.lib.FileStorage
import mygdx.game.lib.MyObjectMapper
import mygdx.game.uiSkins.UiSkins

class WidgetEditorWin(val jsonPath: String) : VisWindow("Widget editor") {

    private lateinit var workingGroup: Group
    private val mySkin = Skin(Gdx.files.internal(UiSkins.skinPath))

    private val inputListener: GroupInputListener

    private val root = "system/menu-assets/XBOX BUTTONS - Premium Assets"

    private val paths = listOf(
        "$root/Digital Buttons/ABXY-standart",
        "$root/D-Pad-standart",
        "system/menu-assets"
    )

    private val widgetBuilder = WidgetBuilder(mySkin, paths)

    private val dragListener = widgetBuilder.buildGroupInputListener()
    private val actorBuilder = ActorBuilder(mySkin)


    init {
        setCenterOnAdd(true)
        setKeepWithinStage(false)

        inputListener = widgetBuilder.buildGroupInputListener()

        val export = VisTextButton("export")
        export.onClick {
            val stack = arrayListOf<SceneWidgetData>()
            workingGroup.children.forEach { a ->
                a.userObject?.let {
                    if (it == EDITOR_MARK) {
                        stack.add(widgetBuilder.writeData(a))
                    }
                }
            }
            val srt = MyObjectMapper.writeValueAsString(stack)
            Gdx.files.local(jsonPath).writeString(srt, false)
//            val typeRef = object : TypeReference<List<SceneWidgetData>>() {}
//            val l = MyObjectMapper.readValue(srt, typeRef)
//            val n = l.map { widgetBuilder.buildEditableActors(it) }
//            n.firstOrNull()?.let {
//                it.y += 100
//                workingGroup.addActor(it)
//            }
            val actors = actorBuilder.build(srt)
            actors
        }
        add(export)
    }

    fun setupGroup(group: Group) {
        if (::workingGroup.isInitialized) {
            workingGroup.removeListener(inputListener)
        }
        workingGroup = group
        workingGroup.debug()

        workingGroup.addListener(inputListener)
        val file = FileStorage[jsonPath]
        if (file.exists()) {
            widgetBuilder.buildActors(file).forEach {
                workingGroup.addActor(it)
            }
        }
        group.stage.addActor(this)
    }
}
