package mygdx.game.editor.widgetEditor

import com.kotcrab.vis.ui.widget.spinner.FloatSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner
import ktx.actors.onChange
import ktx.actors.onKeyboardFocusEvent
import mygdx.game.lib.ActionCallbackEachAct

internal class FloatPropertyField(
    default: String = "0",
    min: String = "-500000",
    max: String = "500000",
    step: String = "1",
    scale: Int = 1,
    val model: FloatSpinnerModel = FloatSpinnerModel(
        default, min, max, step, scale
    )
) : Spinner(
    null, model
) {
    private var setFun: (() -> Unit)? = null
    private var getFun: (() -> Unit)? = null

    var value: Float
        set(v) {
            model.value = v.toBigDecimal()
        }
        get() = model.value.toFloat()

    fun attachProperty(obj: Any, propName: String) {}

    fun attachProperty(obj: Any, getName: String, setName: String) {
        obj.javaClass.kotlin.members.firstOrNull {
            it.name == getName &&
                    it.parameters.size == 1
        }?.let {
            getFun = { value = it.call(obj) as Float }
        }
        obj.javaClass.kotlin.members.firstOrNull {
            it.name == setName &&
                    it.parameters.size == 2 }?.let {
            setFun = { it.call(obj, value) }
        }
    }

    init {
        var unlock = true
        onKeyboardFocusEvent { e ->
            unlock = !e.isFocused
        }

        addAction(ActionCallbackEachAct {
            if (unlock) getFun?.invoke()
        })

        onChange {
            setFun?.invoke()
        }
    }
}