package mygdx.game.editor.widgetEditor.factory

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Align
import com.kotcrab.vis.ui.widget.VisTextField
import ktx.actors.onKeyDown
import ktx.actors.setKeyboardFocus
import ktx.actors.txt
import mygdx.game.editor.formItems.FormItemSelectBox
import mygdx.game.editor.widgetEditor.EDITOR_MARK
import mygdx.game.editor.widgetEditor.styleName
import mygdx.game.lib.MyDragHandler
import mygdx.game.lib.onTouchDownClose
import mygdx.game.myWidgets.Option

internal class LabelFactory {

    fun buildLabel(text: String? = "Label", skin: Skin, style: String): Label {
        val actor = Label(text, skin, style)
        actor.styleName = style
        actor.userObject = EDITOR_MARK
        actor.onTouchDownClose {
            actor.listeners.firstOrNull { it is MyDragHandler } ?: addMenu(actor)
        }
        return actor
    }

    private fun addMenu(actor: Label) {
        val win = SettingWindowFactory().build(
            "Label options",
            actor = actor,
            x = actor.x,
            y = actor.y,
            isDraggable = true,
            isResizable = true,
            debugOnFocus = true,

            )
        val textField = VisTextField(actor.txt)
        val alignField = FormItemSelectBox(
//            Option("bottom", Align.bottom),
//            Option("bottomLeft", Align.bottomLeft),
//            Option("bottomRight", Align.bottomRight),
//            Option("center", Align.center),
            Option("left", Align.left),
            Option("right", Align.right),
//            Option("top", Align.top),
//            Option("topLeft", Align.topLeft),
//            Option("topRight", Align.topRight),
        )
        win.add(textField)
        win.row()
        win.add(alignField)
        win.row()
        win.pack()
        win.x = actor.x
        win.y = actor.y - win.height

        textField.setKeyboardFocus()
        textField.onKeyDown(true) {
            actor.txt = text
        }
        alignField.value = actor.labelAlign
        alignField.onSelect = {
            actor.setAlignment(it)
        }
    }
}