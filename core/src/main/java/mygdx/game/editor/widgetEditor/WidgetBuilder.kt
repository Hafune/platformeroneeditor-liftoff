@file:Suppress("UNCHECKED_CAST")

package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.fasterxml.jackson.core.type.TypeReference
import com.ray3k.tenpatch.TenPatchDrawable
import ktx.actors.txt
import ktx.style.getAll
import mygdx.game.editor.widgetEditor.factory.GroupFactory
import mygdx.game.editor.widgetEditor.factory.ImageButtonFactory
import mygdx.game.editor.widgetEditor.factory.ImageFactory
import mygdx.game.editor.widgetEditor.factory.LabelFactory
import mygdx.game.lib.MyObjectMapper
import mygdx.game.myWidgets.Option
import kotlin.reflect.KClass

internal const val EDITOR_MARK = "EDITOR_MARK"

private val styleHash = HashMap<Any, String?>()
internal var Actor.styleName: String
    get() = styleHash[this] ?: "default"
    set(value) {
        styleHash[this] = value
    }
private var Drawable.path: String?
    get() = styleHash[this]
    set(value) {
        styleHash[this] = value
    }
private var Texture.path: String?
    get() = styleHash[this]
    set(value) {
        styleHash[this] = value
    }

class WidgetBuilder(val skin: Skin, textureFolderPaths: Iterable<String>) {

    private val typeRef = object : TypeReference<List<SceneWidgetData>>() {}

    private val styleOptionsMap = HashMap<KClass<*>, List<Option<String>>>()
    private val widgetOptions =
        listOf(Group::class, Label::class, ImageButton::class, Image::class)
            .map { Option(it.simpleName!!, it) }

    private var tenDrawableOptions = emptyList<Option<String>>()

    private val inputListener: GroupInputListener

    private val texturePaths = textureFolderPaths.fold(
        HashMap<String, Texture>()
    ) { acc, s ->
        Gdx.files.local(s).list(".png").forEach {
            val texture = Texture(it)
            texture.path = it.path()
            acc[it.path()] = texture
        }
        acc
    }

    init {
        listOf(LabelStyle::class, ImageButtonStyle::class).forEach {
            val data = skin.getAll(it.java)
            if (data != null) {
                styleOptionsMap[it] = data.keys().map { k -> Option(k, k) }
            }
        }
        val tenPatchDrawable = skin.getAll<TenPatchDrawable>()
        if (tenPatchDrawable != null) tenDrawableOptions = tenPatchDrawable.map {
            (it.value as Drawable).path = it.key
            Option(it.key, it.key)
        }

        inputListener = buildGroupInputListener()
    }

    internal fun buildGroupInputListener(): GroupInputListener {
        return GroupInputListener(
            skin = skin,
            widgetOptions = widgetOptions,
            tenDrawableOptions = tenDrawableOptions,
            textureOptions = texturePaths.map { Option(it.key.split("/").last(), it.value) },
            styleMap = styleOptionsMap
        ) { buildGroupInputListener() }
    }

    internal fun writeData(actor: Actor): SceneWidgetData {
        val data = SceneWidgetData()
        data.clazz = actor::class.qualifiedName!!
        data.x = actor.x
        data.y = actor.y
        data.width = actor.width
        data.height = actor.height
        data.name = actor.name

        when (actor) {
            is Label -> {
                data.text = actor.txt
                data.name = actor.txt
                data.style = actor.styleName
                data.alight = actor.labelAlign
            }
            is ImageButton -> {
                data.style = actor.styleName
            }
            is Image -> {
                val drawable = actor.drawable
                if (drawable is TextureRegionDrawable) data.texturePath = drawable.region.texture.path
                else data.drawableName = drawable.path
            }
            is Group -> {
                actor.children.forEach { a ->
                    a.userObject?.let {
                        if (it == EDITOR_MARK) {
                            data.children.add(writeData(a))
                        }
                    }
                }
            }
        }

        return data
    }

    internal fun buildActors(file: FileHandle): List<Actor> {
        val data = MyObjectMapper.readValue(file.file(), typeRef)
        return data.map { buildEditableActors(it) }
    }

    internal fun buildEditableActors(data: SceneWidgetData): Actor {
        val actor: Actor
        when (data.clazz) {
            Label::class.qualifiedName -> {
                actor = LabelFactory().buildLabel(data.text, skin, data.style)
                data.alight?.let { actor.setAlignment(it) }
            }
            ImageButton::class.qualifiedName -> {
                actor = ImageButtonFactory().buildImageButton(skin, data.style)
            }
            Image::class.qualifiedName -> {
                actor = if (data.drawableName != null) {
                    ImageFactory().buildImage(skin, data.drawableName!!)
                } else {
                    ImageFactory().buildImage(texturePaths[data.texturePath]!!)
                }
            }
            Group::class.qualifiedName -> {
                actor = GroupFactory().buildGroup(buildGroupInputListener())
                data.children.forEach {
                    actor.addActor(buildEditableActors(it))
                }
            }
            else -> {
                actor = Label("UNKNOWN actor", skin, data.style)
            }
        }
        actor.x = data.x
        actor.y = data.y
        actor.width = data.width
        actor.height = data.height
        actor.name = data.name

        return actor
    }
}
