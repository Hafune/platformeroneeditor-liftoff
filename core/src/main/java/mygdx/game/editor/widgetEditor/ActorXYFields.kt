package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Table

internal class ActorXYFields(actor: Actor) : Table() {
    private val xLabel = FloatPropertyField()
    private val yLabel = FloatPropertyField()

    init {
        xLabel.attachProperty(actor, "getX", "setX")
        yLabel.attachProperty(actor, "getY", "setY")

        left()
        add(MyValueName("x", xLabel)).growX()
        add(MyValueName("y", yLabel)).growX()
    }
}