package mygdx.game.editor.widgetEditor.factory

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import mygdx.game.editor.widgetEditor.EDITOR_MARK
import mygdx.game.editor.widgetEditor.styleName
import mygdx.game.lib.MyDragHandler
import mygdx.game.lib.onTouchDownClose

internal class ImageButtonFactory {

    fun buildImageButton(skin: Skin, style: String): ImageButton {
        val actor = ImageButton(skin, style)
        actor.styleName = style
        actor.userObject = EDITOR_MARK
        actor.onTouchDownClose {
            actor.listeners.firstOrNull { it is MyDragHandler } ?: addMenu(actor)
        }
        return actor
    }

    private fun addMenu(actor: ImageButton) {
        SettingWindowFactory().build(
            "ImageButton options",
            actor = actor,
            x = actor.x,
            y = actor.y,
            isDraggable = true,
            isResizable = false,
            debugOnFocus = true
        )
    }
}
