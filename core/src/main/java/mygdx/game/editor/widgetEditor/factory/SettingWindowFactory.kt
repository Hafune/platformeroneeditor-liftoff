package mygdx.game.editor.widgetEditor.factory

import com.badlogic.gdx.scenes.scene2d.Actor
import com.kotcrab.vis.ui.widget.VisWindow
import ktx.actors.onTouchDown
import mygdx.game.editor.widgetEditor.ActorSizeFields
import mygdx.game.editor.widgetEditor.ActorXYFields
import mygdx.game.editor.widgetEditor.OutsideClickBackground
import mygdx.game.editor.widgetEditor.RemoveActorButton
import mygdx.game.lib.MyDragHandler
import mygdx.game.lib.MyResizeHandler
import mygdx.game.lib.onTouchDownClose

internal class SettingWindowFactory {
    fun build(
        title: String,
        actor: Actor,
        x: Float,
        y: Float,
        isDraggable: Boolean = false,
        isResizable: Boolean = false,
        debugOnFocus: Boolean = false
    ): VisWindow {
        val win = VisWindow(title)
        val back = OutsideClickBackground()
        val group = actor.parent
        val myDragHandler = MyDragHandler()
        val myResizeHandler = MyResizeHandler()

        if (isResizable) actor.addListener(myResizeHandler)
        if (isDraggable) actor.addListener(myDragHandler)

        fun drop() {
            back.remove()
            win.remove()
            if (debugOnFocus) actor.debug = false
            if (isResizable) actor.removeListener(myResizeHandler)
            if (isDraggable) actor.removeListener(myDragHandler)
        }
        if (debugOnFocus) actor.debug = true

        win.setKeepWithinStage(false)

        if (isResizable) win.add(ActorSizeFields(actor))
        else win.add(ActorXYFields(actor))
        win.row()
        win.add(RemoveActorButton(actor) { drop() })
        win.row()
        win.pack()
        win.x = x
        win.y = y - win.height

        group.addActorBefore(actor, back)
        group.addActorAfter(actor, win)

        win.onTouchDownClose { }

        back.onTouchDown {
            drop()
        }
        return win
    }
}