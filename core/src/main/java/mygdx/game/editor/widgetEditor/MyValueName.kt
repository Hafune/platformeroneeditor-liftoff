package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.kotcrab.vis.ui.widget.VisLabel

internal class MyValueName(name: String, actor: Actor) : Table() {
    init {
        add(VisLabel(name)).left()
        add(actor).right().growX()
        pack()
    }
}