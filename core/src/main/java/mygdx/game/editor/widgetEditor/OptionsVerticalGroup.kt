package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.kotcrab.vis.ui.widget.VisTextButton
import mygdx.game.lib.onTouchDownClose
import mygdx.game.myWidgets.Option
import java.lang.Float.min

internal class OptionsVerticalGroup<T>(vararg list: Option<T>, buildTooltip: ((value: T) -> InputListener)? = null) :
    ScrollPane(null) {

    var onSelect: ((value: T) -> Unit)? = null

    init {
        val group = VerticalGroup()
        list.sortedBy { it.label }.forEach { o ->
            val label = VisTextButton(o.label)

            label.onTouchDownClose {
                onSelect?.invoke(o.value!!)
            }

            buildTooltip?.let {
                label.addListener(buildTooltip(o.value!!))
            }

            group.addActor(label)
        }

        actor = group
        width = min(group.prefWidth,600f)
        height = min(group.prefHeight,400f)
    }
}