package mygdx.game.editor.widgetEditor

import com.badlogic.gdx.scenes.scene2d.Actor
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onClick

internal class RemoveActorButton(actor: Actor, afterClick: (() -> Unit)? = null) : VisTextButton("remove") {
    init {
        onClick {
            actor.remove()
            afterClick?.invoke()
        }
    }
}