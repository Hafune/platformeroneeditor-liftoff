package mygdx.game.editor.actionMenu

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.scenes.scene2d.Actor
import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.VisTextField
import ktx.actors.onClick
import ktx.actors.onKeyDown
import ktx.actors.onKeyUp
import ktx.scene2d.horizontalGroup
import ktx.scene2d.scene2d
import ktx.scene2d.textButton
import ktx.scene2d.verticalGroup
import ktx.scene2d.vis.visTextButton
import mygdx.game.editor.*
import mygdx.game.lib.MyFileChooserOpen
import mygdx.game.lib.MyFileChooserSave

class NewActionTreeWin(editorRoot: EditorRoot, name: String) : EditorWindow(name) {

    val actionTree = NewActionTree(editorRoot)
    lateinit var actionMenu: NewActionMenu

    private val fileChooserOpen: MyFileChooserOpen = MyFileChooserOpen {
        actionTree.applyCommand(it.readString())
        currentFileName.setText(it.name())
    }

    private val fileChooserSave: MyFileChooserSave = MyFileChooserSave {
        fileChooserOpen.file = it
        currentFileName.setText(it.name())
    }

    private val currentFileName = VisLabel("")

    init {
        addCloseButton()

        add(currentFileName).left().colspan(100).row()
        add(scene2d.verticalGroup {
            horizontalGroup {
                visTextButton("New") {
                    onClick {
                        actionTree.applyCommand("[]")
                    }
                }
                visTextButton("Open") {
                    onClick {
                        editorRoot.editorsMenuRoot.addActor(fileChooserOpen.fadeIn())
                    }
                }
                visTextButton("Copy") {
                    onClick {
                        actionTree.copyNode()
                    }
                }
                visTextButton("Paste") {
                    onClick {
                        actionTree.pasteNode()
                    }
                }
                visTextButton("Save") {
                    onClick {
                        fileChooserOpen.file?.writeString(actionTree.getCommand(), false)
                    }
                }
                visTextButton("Save as...") {
                    onClick {
                        fileChooserSave.data = actionTree.getCommand()
                        editorRoot.editorsMenuRoot.addActor(fileChooserSave.fadeIn())
                    }
                }
                verticalGroup {
                    textButton("build action") {
                        onClick {
                            ActionEditorBuilder(editorRoot.myEngine).buildAction(
                                scriptFileName = "ActionTree",
                                command = actionTree.getCommand()
                            ).start()
                        }
                    }
                }
            }
        }).top().left()
        row()

        add(Actor().apply { width = 50f })

        onClick {
            stage
            if (stage.keyboardFocus is VisTextField) return@onClick
            stage.keyboardFocus = this

        }
        val set = HashSet<Int>()
        onKeyDown {
            if (set.add(it)) {
                if (stage.keyboardFocus is VisTextField) return@onKeyDown
                if (set.contains(Keys.CONTROL_LEFT) && set.indexOf(Keys.CONTROL_LEFT) < set.indexOf(Keys.C)) {
                    NewActionTree.copyCommand = actionTree.copyNode()
                }
                if (set.contains(Keys.CONTROL_LEFT) && set.indexOf(Keys.CONTROL_LEFT) < set.indexOf(Keys.V)) {
                    actionTree.pasteNode()
                }
                if (set.indexOf(Keys.FORWARD_DEL) == 0) {
                    actionTree.removeSelected()
                }
            }
        }
        onKeyUp {
            set.remove(it)
        }

        row()

        add(actionTree)
        titleTable.children.items[1].onClick {
            actionMenu.remove()
        }

        onClick {
            actionMenu.toFront()
        }
    }
}