package mygdx.game.editor.actionMenu

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.FormItemFactory
import mygdx.game.editor.formItems.FormItem
import mygdx.game.icons.SystemIconStorage
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyBackgroundDrawable
import mygdx.game.lib.TextureStorage


@Suppress("UNCHECKED_CAST")
class NodeItem(
    private val editorRoot: EditorRoot,
    private val registry: (n: NodeItem) -> Unit,
    private val unRegistry: (n: NodeItem) -> Unit,
    private val onClick: (n: NodeItem) -> Unit,
    var parentNode: NodeItem? = null
) : HorizontalGroup() {

    var isContainer = true

    private val icon = SystemIconStorage.buildByName("steppers_minus_hover")
    private val plus = TextureRegionDrawable(TextureStorage["system/steppers_plus_hover.png"])
    private val minus = TextureRegionDrawable(TextureStorage["system/steppers_minus_hover.png"])

    val items = ArrayList<FormItem<*>>()
    var subNodes = ArrayList<NodeItem>()
    private val verticalGroup = VerticalGroup()
    private val subGroup = VerticalGroup()
    private val subOffset = HorizontalGroup()
    private val table = Table()


    val blue = MyBackgroundDrawable("system/white_color_texture.png")
    val gray = MyBackgroundDrawable("system/white_color_texture.png")

    init {
        registry(this)
        x = 10f
        rowAlign(Align.top)
        addActor(verticalGroup)
        verticalGroup.addActor(table)
        verticalGroup.left().fill()
        subOffset.addActor(Actor().also { it.width = 10f })
        subOffset.addActor(subGroup)
        subGroup.left().fill()

        blue.setColor(27, 161, 226, 255) // r, g, b, a
        gray.setColor(62, 62, 66, 255) // r, g, b, a

        icon.onClick {
            if (image.drawable == plus) image.drawable = minus else image.drawable = plus

            if (image.drawable != plus) verticalGroup.addActor(subOffset)
            else subOffset.remove()
        }

        table.touchable = Touchable.enabled
        table.addListener(object : InputListener() {
            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                onClick(this@NodeItem)
                return true
            }

            override fun enter(event: InputEvent, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                if (table.background == null) {
                    table.background = gray
                }
            }

            override fun exit(event: InputEvent, x: Float, y: Float, pointer: Int, toActor: Actor?) {
                if (table.background == gray && toActor != table) {
                    table.background = null
                }
            }
        })
    }

    fun unSelect() {
        table.background = null
    }

    fun select() {
        table.background = blue
    }

    fun fillNode(list: ArrayList<*>) {
        list.forEach { item ->
            if (item is Map<*, *>) {

                val obj = MyClassForName.getType(item[FormItemFactory.K_CLASS_NAME] as String)

                var last: Cell<*>? = null
                FormItemFactory(editorRoot).buildItemsFromClassAndFillValues(obj, item as Map<String, Any>)
                    .forEach {
                        items.add(it)
                        it as Actor
                        last = table.add(it).left()
                    }
                last?.expand()
            } else if (item is ArrayList<*>) {
                addSubNode(item)
            }
        }
    }

    fun addSubNode(list: ArrayList<*>) {
        addActorAt(0, icon)

        val node = NodeItem(editorRoot, registry, unRegistry, onClick, this)
        subNodes.add(node)
        node.fillNode(list)
        verticalGroup.addActor(subOffset)
        subGroup.addActor(node)
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        if (parent.width != table.width) {
            table.width = parent.width
        }
        super.draw(batch, parentAlpha)
    }

    fun getNodeCommand(): ArrayList<Any> {
        val param = FormItemFactory(editorRoot).buildJsonObjectFromItems(items)

        val list = ArrayList<Any>()

        if (param.size() > 0) list.add(param)

        subNodes.forEach {
            list.add(it.getNodeCommand())
        }

        return list
    }
}