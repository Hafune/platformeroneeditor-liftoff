package mygdx.game.editor.actionMenu

import com.badlogic.gdx.scenes.scene2d.Group
import mygdx.game.actions.AwaitParallel
import mygdx.game.actions.AwaitSequence
import mygdx.game.actions.VisibleInActionMenu
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.EditorWindow
import mygdx.game.editor.FormItemFactory
import mygdx.game.editor.FormItemFactory.Companion.K_CLASS_NAME
import mygdx.game.editor.formItems.FormItem
import mygdx.game.editor.formItems.FormItemLabelKClass
import mygdx.game.language.GameText
import mygdx.game.lib.MyClassForName
import mygdx.game.myWidgets.Option
import mygdx.game.myWidgets.SearchableList
import org.reflections.Reflections
import kotlin.reflect.KClass
import kotlin.streams.toList


class ActionMenu(editorRoot: EditorRoot, private val actionTree: ActionTree) :
    EditorWindow("Actions menu") {

    init {
        val reflections = Reflections("mygdx.game.actions")

        val v = reflections.store["TypesAnnotated"]!![VisibleInActionMenu::class.qualifiedName!!]!!

        val actionClassTypes = v.stream().map {
            MyClassForName.getType(it)
        }.toList()

        val list = actionClassTypes.map {
            Option(GameText[it.qualifiedName!!], it)
        }.sortedBy { it.label }.toTypedArray()

        val onSelect = { it: KClass<*> ->
            println(it.toString())
            val node = actionTree.tree.selection?.items()?.firstOrNull()
            if (node != null) {
                val fItem = (node.actor as Group).children.first() as FormItem<*>
                val formItem = FormItemLabelKClass()
                formItem.valName = K_CLASS_NAME
                formItem.value = it.qualifiedName!!

                if (fItem.value == AwaitParallel::class.qualifiedName ||
                    fItem.value == AwaitSequence::class.qualifiedName ||
                    actionTree.tree.rootNodes.contains(node)
                ) {
                    actionTree.addIntoSelectedNode(
                        formItem, *FormItemFactory(editorRoot).buildFormItemsFromType(it).toTypedArray(),
                    )
                } else {
                    actionTree.addNode(
                        formItem, *FormItemFactory(editorRoot).buildFormItemsFromType(it).toTypedArray(),
                    )
                }
            }
        }
        add(SearchableList(list, onSelect))
    }
}