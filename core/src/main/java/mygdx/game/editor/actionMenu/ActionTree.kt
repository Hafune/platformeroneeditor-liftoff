package mygdx.game.editor.actionMenu

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Tree
import com.badlogic.gdx.utils.SnapshotArray
import com.kotcrab.vis.ui.widget.VisTable
import ktx.scene2d.KNode
import ktx.scene2d.horizontalGroup
import ktx.scene2d.scene2d
import ktx.scene2d.vis.KVisTree
import ktx.scene2d.vis.visScrollPane
import ktx.scene2d.vis.visTable
import ktx.scene2d.vis.visTree
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.FormItemFactory
import mygdx.game.editor.formItems.FormItem
import mygdx.game.editor.formItems.FormItemLabelKClass
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper

class ActionTree(private val editorRoot: EditorRoot) : VisTable() {

    companion object {
        private const val ROOT = "ROOT"
        var copyCommand = arrayListOf("")
    }

    val tree: KVisTree

    init {
        add(scene2d.visScrollPane {
            setOverscroll(false, false)
            visTable {
                add(Actor().apply { height = 50f })
                row()
                add(Actor().apply { width = 50f })

                visTree {
                    tree = this
                    horizontalGroup {
                        addActor(FormItemLabelKClass().apply {
                            valName = ROOT
                        })
                    }
                    expandAll()
                }

                add(Actor().apply { width = 50f })
                row()
                add(Actor().apply { height = 50f })
            }
        }).left().top()
    }

    fun copyNode(): ArrayList<String> {
        tree.selection?.let {
            return tree.selection.map { MyObjectMapper.writeValueAsString(getNodeCommand(it))!! }.toTypedArray()
                .toCollection(ArrayList())
        }
        return ArrayList()
    }

    fun pasteNode() {
        if (tree.selection != null && copyCommand.isNotEmpty()) {
            copyCommand.forEach {
                val list = MyObjectMapper.readValue(it, ArrayList::class.java)
                insertNode(
                    tree.selection.first().parent as KNode<*>,
                    list,
                    index = tree.selection.first().parent.children.indexOf(tree.selection.first())
                )
            }
        }
    }

    fun addIntoSelectedNode(vararg params: FormItem<*>) {
        fun add(context: KNode<*>) {
            with(context) {
                horizontalGroup {
                    params.forEach {
                        addActor(it as Actor)
                    }
                }
                expandAll()
            }
        }

        val node = tree.selection?.items()?.firstOrNull()
        if (node != null) add(node) else add(tree.rootNodes.items.first() as KNode)
    }

    fun addNode(vararg params: FormItem<*>) {
        fun add(context: KNode<*>) {
            with(context) {
                val index = tree.selection.first().parent.children.indexOf(tree.selection.first()) + 1
                val n: KNode<*>
                horizontalGroup { kNode ->
                    n = kNode
                    if (index >= 0) {
                        zIndex = index
                    }
                    params.forEach {
                        addActor(it as Actor)
                    }
                }
                if (index >= 0) {
                    n.remove()
                    context.insert(index, n)
                }
                expandAll()
            }
        }

        val node = tree.selection.firstOrNull()?.parent
        if (node != null) add(node) else add(tree.rootNodes.items.first() as KNode)
    }

    @Suppress("UNCHECKED_CAST")
    private fun insertNode(node: KNode<*>, list: ArrayList<*>, index: Int = -1) {
        var curNode: KNode<*>? = null
        list.forEach { item ->
            if (item is Map<*, *>) {
                with(node) {
                    val n: KNode<*>
                    horizontalGroup { kNode ->
                        expand()
                        n = kNode
                        if (index >= 0) {
                            zIndex = index
                        }
                        curNode = kNode

                        val obj = MyClassForName.getType(item[FormItemFactory.K_CLASS_NAME] as String)
                        FormItemFactory(editorRoot).buildItemsFromClassAndFillValues(obj, item as Map<String, Any>)
                            .forEach {
                                addActor(it as Actor)
                            }
                    }
                    if (index >= 0) {
                        n.remove()
                        node.insert(index, n)
                    }
                    expandAll()
                }
            } else if (curNode != null && item is ArrayList<*>) {
                insertNode(curNode!!, item)
            }
        }
    }

    fun getCommand(): String {
        val rootList = ArrayList<Any>()
        tree.rootNodes.items.first().children.forEach {
            if (it != null) {
                rootList.add(getNodeCommand(it))
            }
        }
        return MyObjectMapper.writeValueAsString(rootList)!!
    }

    @Suppress("UNCHECKED_CAST")
    private fun getNodeCommand(node: Tree.Node<*, *, *>): ArrayList<Any> {
        val group = node.actor as HorizontalGroup

        val param = FormItemFactory(editorRoot).buildJsonObjectFromItems(group.children as SnapshotArray<FormItem<*>>)

        val list = ArrayList<Any>()
        list.add(param)
        node.children.items.forEach {
            if (it != null) {
                list.add(getNodeCommand(it))
            }
        }
        return list
    }

    fun applyCommand(command: String) {
        val list = MyObjectMapper.readValue(command, ArrayList::class.java)
        with(tree)
        {
            rootNodes.forEach { it.remove() }
            horizontalGroup { node ->
                addActor(FormItemLabelKClass().apply {
                    valName = ROOT
                })
                list.forEach {
                    insertNode(node, it as ArrayList<*>)
                }
            }
            expandAll()
        }
    }

    fun removeSelected() {
        val node = tree.selection?.items()?.firstOrNull() as KNode?
        if (tree.rootNodes.contains(node)) return
        tree.selection?.items()?.forEach { it.remove() }
    }
}