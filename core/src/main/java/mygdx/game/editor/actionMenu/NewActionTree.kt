package mygdx.game.editor.actionMenu

import com.badlogic.gdx.scenes.scene2d.Actor
import com.kotcrab.vis.ui.widget.VisTable
import ktx.scene2d.KNode
import ktx.scene2d.horizontalGroup
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visScrollPane
import ktx.scene2d.vis.visTable
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.FormItemFactory
import mygdx.game.editor.formItems.FormItem
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper

class NewActionTree(private val editorRoot: EditorRoot) : VisTable() {

    companion object {
        private const val ROOT = "ROOT"
        var copyCommand = arrayListOf("")
    }

    private val rootItem: NodeItem
    private val nodes = ArrayList<NodeItem>()
    private val selected = ArrayList<NodeItem>()

    init {
        add(scene2d.visScrollPane {
            setOverscroll(false, false)
            visTable {
                add(Actor().apply { height = 50f })
                row()
                add(Actor().apply { width = 50f })

                rootItem = NodeItem(editorRoot,
                    registry = {
                        nodes.add(it)
                    },
                    unRegistry = {
                        nodes.remove(it)
                    },
                    onClick = { node ->
                        nodes.forEach { it.unSelect() }
                        node.select()
                        selected.clear()
                        selected.add(node)
                    })
                add(rootItem)
//                visTree {
//                    tree = this
//                    horizontalGroup {
//                        addActor(FormItemLabelKClass().apply {
//                            valName = ROOT
//                        })
//                    }
//                    expandAll()
//                }

                add(Actor().apply { width = 50f })
                row()
                add(Actor().apply { height = 50f })
            }
        }).left().top()
    }

    fun copyNode(): ArrayList<String> {
        return selected.map { MyObjectMapper.writeValueAsString(it.getNodeCommand())!! }.toTypedArray()
            .toCollection(ArrayList())
    }

    fun pasteNode() {
        val first = selected.firstOrNull()
        if (first != null && copyCommand.isNotEmpty()) {
            copyCommand.forEach {
                val list = MyObjectMapper.readValue(it, ArrayList::class.java)

                if (first.isContainer) {
                    first.addSubNode(list)
                }
                else {
                    first.parentNode?.addSubNode(list)
                }
            }
        }
    }

    fun addIntoSelectedNode(vararg params: FormItem<*>) {
        fun add(context: KNode<*>) {
            with(context) {
                horizontalGroup {
                    params.forEach {
                        addActor(it as Actor)
                    }
                }
                expandAll()
            }
        }

//        val node = tree.selection?.itemModels()?.firstOrNull() as KNode?
//        if (node != null) add(node) else add(tree.rootNodes.itemModels.first() as KNode)
    }

    fun addNode(vararg params: FormItem<*>) {
        fun add(context: KNode<*>) {
            with(context) {
                val index = tree.selection.first().parent.children.indexOf(tree.selection.first()) + 1
                val n: KNode<*>
                horizontalGroup { kNode ->
                    n = kNode
                    if (index >= 0) {
                        zIndex = index
                    }
                    params.forEach {
                        addActor(it as Actor)
                    }
                }
                if (index >= 0) {
                    n.remove()
                    context.insert(index, n)
                }
                expandAll()
            }
        }

//        val node = tree.selection.firstOrNull()?.parent as KNode<*>?
//        if (node != null) add(node) else add(tree.rootNodes.itemModels.first() as KNode)
    }

    @Suppress("UNCHECKED_CAST")
    private fun insertNode(node: KNode<*>, list: ArrayList<*>, index: Int = -1) {
        var curNode: KNode<*>? = null
        list.forEach { item ->
            if (item is Map<*, *>) {
                with(node) {
                    val n: KNode<*>
                    horizontalGroup { kNode ->
                        expand()
                        n = kNode
                        if (index >= 0) {
                            zIndex = index
                        }
                        curNode = kNode

                        val obj = MyClassForName.getType(item[FormItemFactory.K_CLASS_NAME] as String)
                        FormItemFactory(editorRoot).buildItemsFromClassAndFillValues(obj, item as Map<String, Any>)
                            .forEach {
                                addActor(it as Actor)
                            }
                    }
                    if (index >= 0) {
                        n.remove()
                        node.insert(index, n)
                    }
                    expandAll()
                }
            } else if (curNode != null && item is ArrayList<*>) {
                insertNode(curNode!!, item)
            }
        }
    }

    fun getCommand(): String {
        val rootList = ArrayList<Any>()
//        tree.rootNodes.itemModels.first().children.forEach {
//            if (it != null) {
//                rootList.add(getNodeCommand(it))
//            }
//        }
        return MyObjectMapper.writeValueAsString(rootList)!!
    }

    fun applyCommand(command: String) {
        val list = MyObjectMapper.readValue(command, ArrayList::class.java)
        rootItem.fillNode(list)
        val a = MyObjectMapper.writeValueAsString(rootItem.getNodeCommand())

//        with(tree)
//        {
//            rootNodes.forEach { it.remove() }
//            horizontalGroup { node ->
//                addActor(FormItemLabelKClass().apply {
//                    valName = ROOT
//                })
//                list.forEach {
//                    insertNode(node, it as ArrayList<*>)
//                }
//            }
//            expandAll()
//        }
    }

    fun removeSelected() {
//        val node = tree.selection?.itemModels()?.firstOrNull() as KNode?
//        if (tree.rootNodes.contains(node)) return
//        tree.selection?.itemModels()?.forEach { it.remove() }
    }
}