package mygdx.game.editor.actionMenu

import mygdx.game.editor.EditorRoot
import mygdx.game.editor.EditorWindow


class NewActionMenu(editorRoot: EditorRoot, private val actionTree: NewActionTree) :
    EditorWindow("Actions menu") {

//    init {
//        val reflections = Reflections("mygdx.game.actions")
//
//        val v = reflections.store["TypesAnnotated"]!![VisibleInActionMenu::class.qualifiedName!!]!!
//
//        val actionClassTypes = v.stream().map {
//            MyClassForName.getType(it)
//        }.toList()
//
//        val list = actionClassTypes.map {
//            Option(GameText[it.qualifiedName!!], it)
//        }.sortedBy { it.label }.toTypedArray()
//
//        val onSelect = { it: KClass<*> ->
//            println(it.toString())
//            val node = actionTree.tree.selection?.itemModels()?.firstOrNull()
//            if (node != null) {
//                val fItem = (node.actor as Group).children.first() as FormItem<*>
//                val formItem = FormItemLabelKClass()
//                formItem.valName = K_CLASS_NAME
//                formItem.value = it.qualifiedName!!
//
//                if (fItem.value == AwaitParallel::class.qualifiedName ||
//                    fItem.value == AwaitSequence::class.qualifiedName ||
//                    actionTree.tree.rootNodes.contains(node)
//                ) {
//                    actionTree.addIntoSelectedNode(
//                        formItem, *FormItemFactory(editorRoot).buildFormItemsFromType(it).toTypedArray(),
//                    )
//                } else {
//                    actionTree.addNode(
//                        formItem, *FormItemFactory(editorRoot).buildFormItemsFromType(it).toTypedArray(),
//                    )
//                }
//            }
//        }
//        add(SearchableList(list, onSelect))
//    }
}