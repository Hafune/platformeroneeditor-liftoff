package mygdx.game.editor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import mygdx.game.TileMapRender
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min

class TouchPlace(
    var lcmStepWidth: Float,
    var lcmStepHeight: Float,
    rcmStepWidth: Float = lcmStepWidth,
    rcmStepHeight: Float = lcmStepHeight,
    private var icon: Actor,
    val onMoved: ((x: Float, y: Float) -> Unit)? = null,
    val onDragged: ((x: Float, y: Float) -> Unit)? = null,
    var onLeftDown: ((x: Float, y: Float) -> Unit)? = null,
    var onRightDown: ((x: Float, y: Float) -> Unit)? = null,
    var onLeftUp: ((x: Float, y: Float) -> Unit)? = null,
    var onRightUp: ((x: Float, y: Float) -> Unit)? = null,
    var resizableIcon: Boolean = false,
    private val unitToPixelScale: Float = TileMapRender.TILE_WIDTH
) : Group() {

    private val lcmX = AxisHandler(lcmStepWidth)
    private val lcmY = AxisHandler(lcmStepHeight)
    private val rcmX = AxisHandler(rcmStepWidth)
    private val rcmY = AxisHandler(rcmStepHeight)

    private val move = Vector2()
    private var drag = Vector2()

    val lcmTouchDown = Vector2()
    val lcmTouchUp = Vector2()

    val rcmTouchDown = Vector2()
    val rcmTouchUp = Vector2()

    val lcmBounds = Rectangle()
    val rcmBounds = Rectangle()

    init {
        width = 1000f * unitToPixelScale
        height = 1000f * unitToPixelScale
        addActor(icon)
        addListener(object : ClickListener() {
            override fun mouseMoved(event: InputEvent, x: Float, y: Float): Boolean {
                move.x = lcmX.click(x)
                move.y = lcmY.click(y)

                lcmBounds.x = move.x
                lcmBounds.y = move.y
                lcmBounds.width = lcmStepWidth
                lcmBounds.height = lcmStepHeight

                rcmBounds.x = move.x
                rcmBounds.y = move.y
                rcmBounds.width = lcmStepWidth
                rcmBounds.height = lcmStepHeight

                setupIconPosition()
                onMoved?.invoke(move.x, move.y)
                return true
            }

            override fun touchDragged(event: InputEvent, x: Float, y: Float, pointer: Int) {
                drag.x = lcmX.click(x)
                drag.y = lcmY.click(y)

                updateBounds(lcmBounds, lcmTouchDown, drag)
                setupIconPosition()
                onDragged?.invoke(drag.x, drag.y)
            }

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                when (button) {
                    0 -> {
                        lcmTouchDown.x = lcmX.click(x)
                        lcmTouchDown.y = lcmY.click(y)
                        onLeftDown?.invoke(lcmTouchDown.x, lcmTouchDown.y)
                    }
                    1 -> {
                        rcmTouchDown.x = rcmX.click(x)
                        rcmTouchDown.y = rcmY.click(y)
                        onRightDown?.invoke(rcmTouchDown.x, rcmTouchDown.y)
                    }
                }
                return true
            }

            override fun touchUp(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int) {
                when (button) {
                    0 -> {
                        lcmTouchUp.x = lcmX.click(x)
                        lcmTouchUp.y = lcmY.click(y)
                        updateBounds(lcmBounds, lcmTouchDown, lcmTouchUp)
                        onLeftUp?.invoke(lcmTouchUp.x, lcmTouchUp.y)
                    }
                    1 -> {
                        rcmTouchUp.x = rcmX.click(x)
                        rcmTouchUp.y = rcmY.click(y)
                        updateBounds(rcmBounds, rcmTouchDown, rcmTouchUp)
                        onRightUp?.invoke(rcmTouchUp.x, rcmTouchUp.y)
                    }
                }
            }
        })
    }

    private fun setupIconPosition() {
        var stepX = lcmStepWidth
        var stepY = lcmStepHeight
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            stepX = lcmStepWidth / unitToPixelScale
            stepY = lcmStepHeight / unitToPixelScale
        }
        icon.x = (move.x + stepX / 2) * unitToPixelScale - icon.width/ 2
        icon.y = (move.y + stepY / 2) * unitToPixelScale - icon.height/ 2
        if (resizableIcon) {
            icon.x = lcmBounds.x * unitToPixelScale
            icon.y = lcmBounds.y * unitToPixelScale
            icon.width = lcmBounds.width * unitToPixelScale
            icon.height = lcmBounds.height * unitToPixelScale
        }
    }

    private fun updateBounds(bounds: Rectangle, touchDown: Vector2, touchUp: Vector2) {
        bounds.x = min(touchDown.x, touchUp.x)
        bounds.y = min(touchDown.y, touchUp.y)
        bounds.width = abs(touchDown.x - touchUp.x) + lcmStepWidth
        bounds.height = abs(touchDown.y - touchUp.y) + lcmStepHeight
    }

    fun changeIcon(newIcon: Actor) {
        icon.remove()
        icon = newIcon
        addActor(icon)
    }

    private class AxisHandler(
        var stepWidth: Float,
        private val unitToPixelScale: Float = TileMapRender.TILE_WIDTH
    ) {
        fun click(value: Float): Float {
            return calcPosition(value, stepWidth)
        }

        private fun calcPosition(position: Float, step: Float): Float {
            return applyStep(convertPosition(position), step)
        }

        private fun applyStep(value: Float, step: Float): Float {
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                return value
            }
            return floor(value / step) * step
        }

        private fun convertPosition(value: Float): Float {
            return value / unitToPixelScale
        }
    }
}

