package mygdx.game.editor

import com.kotcrab.vis.ui.widget.VisWindow
import mygdx.game.lib.EditorPositionsUi

@Suppress("LeakingThis")
open class EditorWindow(winName: String) : VisWindow(winName) {

    init {
        isResizable = true
        setKeepWithinStage(false)
        EditorPositionsUi.setup(this, winName)
    }
}