package mygdx.game.components

import com.badlogic.gdx.graphics.g2d.SpriteBatch

interface MyAnimation {

    fun draw(batch: SpriteBatch, x: Float, y: Float, time: Float)
}