package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class AnimationImageComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<AnimationImageComponent>()
    }

    @JsonIgnore
    var callback: (() -> Unit)? = null

    @JsonIgnore
    var time = 0f

    @JsonIgnore
    lateinit var animation: Animation<TextureRegion>
}