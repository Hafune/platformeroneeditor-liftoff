package mygdx.game.components.gameWorld

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.actions.ActionContext
import mygdx.game.actions.BuildAction
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemFilePathPicker
import mygdx.game.systems.MyEngine

class InteractComponent : MyAbstractComponent() {

    @JsonIgnore
    override val serializable = true

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    fun interact(entity: Entity, myEngine: MyEngine) {
        if (myEngine.editorMode) return

        BuildAction().also {
            it.fileName = fileName
            it.myEngine = myEngine
            it.actionContext = ActionContext()
            it.interactEntity = entity
        }.start()
    }

    companion object {
        val mapper = mapperFor<InteractComponent>()
    }
}