package mygdx.game.components.gameWorld

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.actions.ActionContext
import mygdx.game.actions.BuildAction
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemFilePathPicker
import mygdx.game.systems.MyEngine

class InteractRadiusComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<InteractRadiusComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    @FormDefault
    var radius = 2f

    @FormDefault
    val reloadTime = 1f

    @JsonIgnore
    var currentReloadTime: Float = 0f

    fun interact(entity: Entity, myEngine: MyEngine) {
        if (myEngine.editorMode) return

        currentReloadTime = reloadTime
        BuildAction().also {
            it.fileName = fileName
            it.myEngine = myEngine
            it.actionContext = ActionContext()
            it.interactEntity = entity
        }.start()
    }
}