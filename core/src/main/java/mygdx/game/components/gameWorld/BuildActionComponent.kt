package mygdx.game.components.gameWorld

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemFilePathPicker

class BuildActionComponent : MyAbstractComponent() {

    @JsonIgnore
    override val serializable = true

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    companion object {
        val mapper = mapperFor<BuildActionComponent>()
    }
}