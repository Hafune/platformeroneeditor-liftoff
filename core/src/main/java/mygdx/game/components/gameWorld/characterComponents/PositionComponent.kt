package mygdx.game.components.gameWorld.characterComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.*
import mygdx.game.lib.MyMath

@FormSupport(FormItemTouchCell::class)
class PositionComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<PositionComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @FormDefault
    var defX = 0f

    @FormDefault
    var defY = 0f

    @FormDefault
    var defZ = 1f

    @Form(FormItemTouchpad::class)
    var defAngle = 0f

    @JsonIgnore
    @Form(FormItemTouchpad::class)
    var angle = 0f

    @JsonIgnore
    @FormDefault
    var x = 0f

    @JsonIgnore
    @FormDefault
    var y = 0f

    @JsonIgnore
    @FormDefault
    var z = 1f

    fun getWorldX() = x.toInt()
    fun getWorldY() = y.toInt()
    fun getWorldZ() = z.toInt()

    fun setPosition(x: Float = this.x, y: Float = this.y, z: Float = this.z) {
        this.x = x
        this.y = y
        this.z = z
    }

    fun setDefaultPosition(x: Float, y: Float, z: Float) {
        defX = x
        defY = y
        defZ = z
    }

    fun setAngleToward(x: Float, y: Float) {
        angle = MyMath.getAngleDeg(
            this.x,
            this.y,
            x,
            y
        )
    }

    override fun applyParam(map: HashMap<String, Any>) {
        super.applyParam(map)
        angle = defAngle
        x = defX
        y = defY
        z = defZ
    }
}