package mygdx.game.components.gameWorld.tileComponents

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import ktx.scene2d.container
import ktx.scene2d.scene2d
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemDrawItemSelector
import mygdx.game.editor.formItems.FormItemSelectBoxTileAnimation
import mygdx.game.editor.formItems.FormItemSelectBoxTileFrame
import mygdx.game.tileSets.DrawItemEraser
import mygdx.game.tileSets.TileFriendlyData
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

object TileAnimation {
    const val NOT_ANIMATED = "NOT_ANIMATED"
    const val PLAIN = "PLAIN"
    const val WHEEL = "WHEEL"
    const val SWITCH = "SWITCH"
}

object TileFrame {
    const val FIRST_FRAME = "FIRST_FRAME"
    const val LAST_FRAME = "LAST_FRAME"
}

open class TileComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<TileComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    val container = scene2d.container {
        isTransform = true
        actor = object : Actor() {
            override fun draw(batch: Batch, parentAlpha: Float) {
                drawItem.draw(batch, 0f, 0f, tileFriendly, localTime)
            }
        }
    }

    @JsonIgnore
    var drawItem: IDrawItem = DrawItemEraser
        private set

    @Form(FormItemDrawItemSelector::class)
    var drawItemValue = ""
        private set

    @JsonIgnore
    var callback: (() -> Unit)? = null

    @Form(FormItemSelectBoxTileAnimation::class)
    var animationType = TileAnimation.PLAIN

    @Form(FormItemSelectBoxTileFrame::class)
    var frameState = TileFrame.FIRST_FRAME
        private set

    @JsonIgnore
    private var timeDirection = 0

    @JsonIgnore
    private var localTime = 0f

    @JsonIgnore
    val tileFriendly = TileFriendlyData()

    init {
        setTileFriendlyValue(tileFriendly.getValue())
    }

    fun setDrawItem(dItem: IDrawItem) {
        drawItem = dItem
        drawItemValue = drawItem.value
    }

    fun getTileFriendlyValue(): String {
        return tileFriendly.getValue()
    }

    fun setTileFriendly(friendlyData: TileFriendlyData) {
        tileFriendly.setValue(friendlyData.getValue())
        setTileFriendlyValue(friendlyData.getValue())
    }

    private fun setTileFriendlyValue(value: String) {
        tileFriendly.setValue(value)
    }

    fun setFrameValue(value: String) {
        frameState = value
        if (frameState == TileFrame.FIRST_FRAME) {
            localTime = 0f
        } else if (frameState == TileFrame.LAST_FRAME) {
            localTime = drawItem.animationDuration - drawItem.frameDuration
        }
    }

    override fun exportParam(): HashMap<String, Any> {
        val m = super.exportParam()
        m[::tileFriendly.name] = tileFriendly.getValue()
        return m
    }

    override fun applyParam(map: HashMap<String, Any>) {
        setTileFriendlyValue(map[::tileFriendly.name] as String)
        return super.applyParam(map)
    }

    fun animationUpdate(worldTime: Float, deltaTime: Float) {
        val timeOffset = (deltaTime * timeDirection.toFloat())
        if (animationType == TileAnimation.NOT_ANIMATED) {
            localTime = 0f
        } else if (animationType == TileAnimation.PLAIN) {
            localTime = worldTime
        } else if (animationType == TileAnimation.WHEEL) {
            if (localTime + timeOffset < drawItem.animationDuration && localTime + timeOffset >= 0) {
                localTime += timeOffset
            } else timeDirection = 0
        } else if (animationType == TileAnimation.SWITCH) {
            localTime += timeOffset
            if (timeDirection == 1 && localTime >= drawItem.animationDuration - drawItem.frameDuration) {
                frameState = TileFrame.LAST_FRAME
                sendSignal()
            }
            if (timeDirection == -1 && localTime <= 0) {
                frameState = TileFrame.FIRST_FRAME
                sendSignal()
            }
            if (localTime < 0f) {
                localTime = 0f
                timeDirection = 0
            } else if (localTime > drawItem.animationDuration - drawItem.frameDuration) {
                localTime = drawItem.animationDuration - drawItem.frameDuration
                timeDirection = 0
            }
        }
    }

    fun draw(batch: Batch, x: Float, y: Float) {
        drawItem.draw(batch, x, y, tileFriendly, localTime)
    }

    fun startAnimation(callback: (() -> Unit)? = null) {
        this.callback = callback
        if (animationType == TileAnimation.WHEEL) {
            if (timeDirection != 0) return
            localTime = 0f
            timeDirection = 1
//            SoundPlayer.play(drawItem.soundPathForForward)
        }
        if (animationType == TileAnimation.SWITCH) {
            if (localTime > 0) {
                timeDirection = -1
//                SoundPlayer.play(drawItem.soundPathForBack)
            } else if (localTime == 0f) {
                timeDirection = 1
//                SoundPlayer.play(drawItem.soundPathForForward)
            }
        }
    }

    private fun sendSignal() {
        callback?.invoke()
        callback = null
    }

    override fun toString(): String {
        return "${this::class.simpleName} drawItem:$drawItem"
    }
}