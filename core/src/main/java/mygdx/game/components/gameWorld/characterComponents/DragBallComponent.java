package mygdx.game.components.gameWorld.characterComponents;

import com.badlogic.ashley.core.ComponentMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import mygdx.game.components.MyAbstractComponent;

public class DragBallComponent extends MyAbstractComponent
{
    public static final ComponentMapper<DragBallComponent> map = ComponentMapper.getFor(DragBallComponent.class);

    @JsonIgnore
    public boolean canCatchBall = true;
    @JsonIgnore
    public boolean ballIsCaught = false;
}
