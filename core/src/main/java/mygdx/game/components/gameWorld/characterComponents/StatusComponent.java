package mygdx.game.components.gameWorld.characterComponents;

import com.badlogic.ashley.core.ComponentMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import mygdx.game.components.MyAbstractComponent;

public class StatusComponent extends MyAbstractComponent
{
    public static final ComponentMapper<StatusComponent> map = ComponentMapper.getFor(StatusComponent.class);

    @JsonIgnore
    public int maxHealthPoint = 76;
    @JsonIgnore
    public int curHealthPoint = maxHealthPoint;

    public void addCurHealthPoint(float addHealthPoint)
    {
        this.curHealthPoint += (int) addHealthPoint;
        if (this.curHealthPoint > maxHealthPoint) this.curHealthPoint = maxHealthPoint;
    }
}
