package mygdx.game.components.gameWorld

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.lib.interpolations.MyInterpolation

class InterpolationComponent : MyAbstractComponent() {

    @JsonIgnore
    lateinit var interpolations: ArrayList<MyInterpolation>

    @FormDefault
    var time = 0f
    @FormDefault
    var range = 0f
    @FormDefault
    var totalTime = 10f


    companion object {
        val mapper = mapperFor<InterpolationComponent>()
    }

    override fun exportParam(): HashMap<String, Any> {
        val map = super.exportParam()
        map[::interpolations.name] = interpolations.map { it.export() }
        return map
    }

    @Suppress("UNCHECKED_CAST")
    override fun applyParam(map: HashMap<String, Any>) {
        super.applyParam(map)
        interpolations = (map[::interpolations.name] as List<String>).map {
            val i = MyInterpolation()
            i.import(it)
            i
        }.toMutableList() as ArrayList<MyInterpolation>
    }
}