package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.gdx.graphics.g2d.Sprite
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class ShadowComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<ShadowComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    lateinit var shadow: Sprite
}