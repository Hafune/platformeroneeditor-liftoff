package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Rectangle
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor

class TriggerPositionComponent(val rect: Rectangle) : Component {

    companion object {
        val mapper = mapperFor<TriggerPositionComponent>()
    }

    @JsonIgnore
    var callback: (() -> Unit)? = null

    @JsonIgnore
    var detectorName = ""

    @JsonIgnore
    var targetName: String? = null

    @JsonIgnore
    var locationName: String = ""
}