package mygdx.game.components.gameWorld

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class MapPointComponent : MyAbstractComponent() {
    companion object {
        val mapper = mapperFor<MapPointComponent>()
    }

    @JsonIgnore
    var cellX = 0

    @JsonIgnore
    var cellY = 0

    @JsonIgnore
    var lastX = 0
        private set

    @JsonIgnore
    var lastY = 0
        private set

    @JsonIgnore
    var route = arrayOf<IntArray>()
        set(value) {
            field = value
            index = 0
            val last = value.last()
            lastX = last[0]
            lastY = last[1]
        }

    @JsonIgnore
    var index = 0
}