package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.ashley.core.Component
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor

class AwaitUserVariableComponent : Component {

    companion object {
        val mapper = mapperFor<AwaitUserVariableComponent>()
    }

    @JsonIgnore
    lateinit var value: String

    @JsonIgnore
    var callback: (() -> Unit)? = null

    @JsonIgnore
    var name = ""

    @JsonIgnore
    var operator: String = ""
}