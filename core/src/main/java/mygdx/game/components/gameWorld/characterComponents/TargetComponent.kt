package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.ashley.core.Entity
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class TargetComponent(val entity: Entity) : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<TargetComponent>()
    }
}