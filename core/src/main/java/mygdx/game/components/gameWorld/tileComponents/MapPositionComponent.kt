package mygdx.game.components.gameWorld.tileComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.FormDefault

class MapPositionComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<MapPositionComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @FormDefault
    var defWorldX = 0

    @FormDefault
    var defWorldY = 0

    @FormDefault
    var defWorldZ = 0

    @JsonIgnore
    var worldX = 0

    @JsonIgnore
    var worldY = 0

    @JsonIgnore
    var worldZ = 0

    private var defAngle = 0f

    @JsonIgnore
    var angle: Float = 0f

    val x: Float
        get() {
            return worldX + .5f
        }

    val y: Float
        get() {
            return worldY + .5f
        }

    fun setupWorldPosition(x: Int, y: Int, z: Int): MapPositionComponent {
        defWorldX = x
        defWorldY = y
        defWorldZ = z
        worldX = x
        worldY = y
        worldZ = z
        return this
    }

    fun resetPosition() {
        worldX = defWorldX
        worldY = defWorldY
        worldZ = defWorldZ
    }

    override fun applyParam(map: HashMap<String, Any>) {
        super.applyParam(map)
        worldX = defWorldX
        worldY = defWorldY
        worldZ = defWorldZ
        angle = defAngle
    }
}