package mygdx.game.components.gameWorld

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.FormDefault

class NameComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<NameComponent>()
    }

    @JsonIgnore
    override val serializable: Boolean = true

    @FormDefault
    var name = ""
}