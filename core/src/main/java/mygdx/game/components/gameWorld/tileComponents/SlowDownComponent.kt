package mygdx.game.components.gameWorld.tileComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class SlowDownComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<SlowDownComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var slowPercent = .5f
}