package mygdx.game.components.gameWorld

import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class IgnoreSaveComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<IgnoreSaveComponent>()
    }
}