package mygdx.game.components.gameWorld.tileComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class WallComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<WallComponent>()
    }
    
    @JsonIgnore
    override var serializable = true
}