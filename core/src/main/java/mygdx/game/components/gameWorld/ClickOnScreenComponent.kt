package mygdx.game.components.gameWorld

import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class ClickOnScreenComponent(val x: Float, val y: Float) : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<ClickOnScreenComponent>()
    }
}