package mygdx.game.components.gameWorld.characterComponents

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemSelectBoxBodyType
import mygdx.game.editor.formItems.FormItemSelectBoxCollisionBehavior
import mygdx.game.lib.MyVector

class BodyComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<BodyComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var myBody: Body? = null

    @FormDefault
    var defAcceleration = 70f

    @FormDefault
    var defLinearDamping = 20f

    @Form(FormItemSelectBoxCollisionBehavior::class)
    var collisionBehavior = "character"

    @Form(FormItemSelectBoxBodyType::class)
    var bodyType = BodyDef.BodyType.DynamicBody.name

    @JsonIgnore
    var radius = 0f

    @JsonIgnore
    var acceleration = 0f

    @JsonIgnore
    var lastFixedPositionX = 0f

    @JsonIgnore
    var lastFixedPositionY = 0f

    @JsonIgnore
    var movingForce = MyVector()

    fun setupDefaults() {
        getBody().linearDamping = defLinearDamping
        getBody().type = BodyDef.BodyType.valueOf(bodyType)
        acceleration = defAcceleration
    }

    @JsonIgnore
    fun getBody() = myBody!!
}