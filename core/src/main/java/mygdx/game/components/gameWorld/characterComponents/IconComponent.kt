package mygdx.game.components.gameWorld.characterComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemCharacterIconSelector
import mygdx.game.icons.Icon

class IconComponent : MyAbstractComponent() {
    companion object {
        val mapper = mapperFor<IconComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var icon: Icon? = null
        private set

    @Form(FormItemCharacterIconSelector::class)
    var icon_id = 0
        private set

    fun setIcon(i: Icon) {
        icon = i
        icon_id = i.id
    }
}