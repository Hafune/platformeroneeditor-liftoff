package mygdx.game.components.gameWorld.characterComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.characterSkins.MotionNames
import mygdx.game.characterSkins.Skin
import mygdx.game.characterSkins.SkinStorage
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemCharacterSkinSelector
import mygdx.game.editor.formItems.FormItemSelectBoxMotionNames

class AnimationSkinComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<AnimationSkinComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var animAngle = 0f
        private set

    @JsonIgnore
    var animationTime = 0f

    @JsonIgnore
    var direction = 270f

    @Form(FormItemSelectBoxMotionNames::class)
    var startMotion = MotionNames.STAND

    @JsonIgnore
    private var currentMotion = MotionNames.STAND

    @JsonIgnore
    var animationName = ""
        private set

    @JsonIgnore
    var offsetX = 0f
    @JsonIgnore
    var offsetY = 0f

    @JsonIgnore
    lateinit var skinStorage: SkinStorage

    @JsonIgnore
    lateinit var skin: Skin
        private set

    @Form(FormItemCharacterSkinSelector::class)
    var skin_id = 0

    @JsonIgnore
    fun setSkin(skin: Skin) {
        skin_id = skin.id
        this.skin = skin
    }

    fun changeMotion(motion: String) {
        val name = skin.calcMotionName(motion, animAngle)
        if (animationName == name) {
            return
        }
        animationName = name
        currentMotion = motion
        animationTime = 0f
    }

    fun changeAngle(angle: Float) {
        animAngle = angle
        changeMotion(currentMotion)
    }
}