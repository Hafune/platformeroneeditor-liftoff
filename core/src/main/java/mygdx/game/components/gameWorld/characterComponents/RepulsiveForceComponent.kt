package mygdx.game.components.gameWorld.characterComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent

class RepulsiveForceComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<RepulsiveForceComponent>()
    }

    @JsonIgnore
    override val serializable = true
}