package mygdx.game.components.gameWorld

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent

class AttachOffsetComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<AttachOffsetComponent>()
    }

    @JsonIgnore
    lateinit var attachBody: BodyComponent

    @JsonIgnore
    var attached = false

    @JsonIgnore
    var x: Float = 0f

    @JsonIgnore
    var y: Float = 0f
}