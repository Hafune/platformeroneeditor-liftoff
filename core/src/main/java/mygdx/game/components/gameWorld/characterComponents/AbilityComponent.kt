package mygdx.game.components.gameWorld.characterComponents

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemSelectBoxAbilities
import mygdx.game.systems.gameWorld.abilities.IAbilitySystem
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem
import kotlin.reflect.KClass

class AbilityComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<AbilityComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var time = 0f

    @JsonIgnore
    var lastCallback: (() -> Unit)? = null

    @JsonIgnore
    var callback: (() -> Unit)? = null
        private set

    @JsonIgnore
    var lastAbility = ""

    @JsonIgnore
    var cancelable = true

    @Form(FormItemSelectBoxAbilities::class)
    var ability = StandAbilitySystem::class.qualifiedName!!

    fun <T : IAbilitySystem> setAbility(kClass: KClass<T>, cb: (() -> Unit)? = null) {
        this.ability = kClass.qualifiedName!!
        lastCallback = callback
        callback = cb
    }

    fun <T : IAbilitySystem> equalAbility(kClass: KClass<T>): Boolean {
        return kClass.qualifiedName == ability
    }
}