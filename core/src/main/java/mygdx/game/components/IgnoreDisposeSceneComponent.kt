package mygdx.game.components

import com.badlogic.ashley.core.Component
import ktx.ashley.mapperFor

class IgnoreDisposeSceneComponent : Component {
    companion object {
        val mapper = mapperFor<IgnoreDisposeSceneComponent>()
    }
}