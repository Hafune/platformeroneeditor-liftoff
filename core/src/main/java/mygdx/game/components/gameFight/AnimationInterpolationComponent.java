package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;

public class AnimationInterpolationComponent extends MyAbstractComponent
{
    public static final ComponentMapper<AnimationInterpolationComponent> map = ComponentMapper.getFor(AnimationInterpolationComponent.class);

    public float positionX;
    public float positionY;

    public float lastPositionX;
    public float lastPositionY;

    public float curX;
    public float curY;
}
