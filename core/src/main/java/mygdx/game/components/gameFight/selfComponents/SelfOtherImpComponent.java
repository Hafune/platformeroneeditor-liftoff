package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.ComponentMapper;

public class SelfOtherImpComponent extends RemovableComponent
{
    public static final ComponentMapper<SelfOtherImpComponent> map = ComponentMapper.getFor(SelfOtherImpComponent.class);

    public float x;
    public float y;

    public SelfOtherImpComponent(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public SelfOtherImpComponent()
    {

    }

    @Override
    public SelfOtherImpComponent setTimer(int frame)
    {
        return (SelfOtherImpComponent) super.setTimer(frame);
    }

    @Override
    public SelfOtherImpComponent setDefTimer(int frame)
    {
        return (SelfOtherImpComponent) super.setDefTimer(frame);
    }
}
