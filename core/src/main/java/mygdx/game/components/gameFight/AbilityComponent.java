package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;

public class AbilityComponent implements Component
{
    public static final ComponentMapper<AbilityComponent> map = ComponentMapper.getFor(AbilityComponent.class);

    public AbstractAbility ability;
    public AbstractAbility lastAbility;
    public boolean cancelLast = false;

    public AbilityComponent(AbstractAbility ability)
    {
        this.ability = ability;
    }

    public AbilityComponent()
    {
    }

    public void changeAbility(AbstractAbility nextAbility)
    {
        if (nextAbility != null)
        {
            ability = nextAbility;
            cancelLast = true;
        }
    }
}
