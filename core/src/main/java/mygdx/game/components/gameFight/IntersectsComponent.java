package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.systems.gameFight.abilities.IntersectsSet;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;

import java.util.HashMap;
import java.util.HashSet;

public class IntersectsComponent extends MyAbstractComponent
{
    public static final ComponentMapper<IntersectsComponent> map = ComponentMapper.getFor(IntersectsComponent.class);

    public HashMap<IntersectBoxeNames, IntersectsSet> intersectsMap = new HashMap<>();
    private final IntersectBoxeNames defName;

    public IntersectsComponent(IntersectBoxeNames defIntersectsSet)
    {
        defName = defIntersectsSet;
        buildIntersectsSet(defIntersectsSet);
    }

    public void clear()
    {
        for (IntersectsSet set : intersectsMap.values())
        {
            set.clear();
        }
    }

    public void clearFrame()
    {
        for (IntersectsSet set : intersectsMap.values())
        {
            set.clearFrame();
        }
    }

    public HashSet<IntersectsSet.EntityHit> getCheckedEntities(IntersectBoxeNames name)
    {
        return intersectsMap.get(name).getCheckedEntities();
    }

    public HashSet<IntersectsSet.EntityHit> getCheckedEntities()
    {
        return intersectsMap.get(defName).getCheckedEntities();
    }

    public void buildIntersectsSet(IntersectBoxeNames name)
    {
        intersectsMap.put(name, new IntersectsSet());
    }
}
