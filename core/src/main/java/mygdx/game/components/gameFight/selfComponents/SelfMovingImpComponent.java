package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

public class SelfMovingImpComponent implements Component
{
    public static final ComponentMapper<SelfMovingImpComponent> map = ComponentMapper.getFor(SelfMovingImpComponent.class);
}
