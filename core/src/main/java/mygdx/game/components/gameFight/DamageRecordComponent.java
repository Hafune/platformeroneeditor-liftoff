package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.systems.gameFight.abilities.effects.DamageTextEffect;

public class DamageRecordComponent extends MyAbstractComponent
{
    public static final ComponentMapper<DamageRecordComponent> map = ComponentMapper.getFor(DamageRecordComponent.class);

    public float totalDamage;
    public DamageTextEffect effect;
}
