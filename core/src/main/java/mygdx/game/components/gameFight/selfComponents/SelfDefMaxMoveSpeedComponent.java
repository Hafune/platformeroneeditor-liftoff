package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

public class SelfDefMaxMoveSpeedComponent implements Component
{
    public static final ComponentMapper<SelfDefMaxMoveSpeedComponent> map = ComponentMapper.getFor(SelfDefMaxMoveSpeedComponent.class);

    public float multiply;

    public SelfDefMaxMoveSpeedComponent(float multiply)
    {
        this.multiply = multiply;
    }
}
