package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;

public class ImpactHitFreezeComponent implements Component, IImpact
{
    public static final ComponentMapper<ImpactHitFreezeComponent> map = ComponentMapper.getFor(ImpactHitFreezeComponent.class);

    public float timer;
    public float defTimer;

    public ImpactHitFreezeComponent(float seconds)
    {
        this.timer = seconds * 60;
        this.defTimer = timer;
    }

    public ImpactHitFreezeComponent()
    {
    }

    @Override
    public void addImpact(Entity parentEntity, Entity targetEntity)
    {
        ImpactHitFreezeComponent impactHitFreezeComponent = new ImpactHitFreezeComponent();
        impactHitFreezeComponent.timer = defTimer;
        impactHitFreezeComponent.defTimer = defTimer;

        targetEntity.add(impactHitFreezeComponent);
    }
}
