package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.lib.MyVector;

public class PositionMoveComponent extends MyAbstractComponent
{
    public static final ComponentMapper<PositionMoveComponent> map = ComponentMapper.getFor(PositionMoveComponent.class);

    public float movedByX = 0;
    public float movedByY = 0;

    public float acceleration = 0;
    public float defAcceleration = 0;

    public float maxMoveSpeed = 0;
    public float defMaxMoveSpeed = 0;

    public float groundFriction = 0;
    public float defGroundFriction = 0;

    public float jumpHeight = 0;
    public float defJumpHeight = 0;
    public float jumpPower = 0;
    public float gravity = -.2f;
    public float flyTime = 0;

    public MyVector movingImp = new MyVector();//импульс шага
    public MyVector characterImp = new MyVector();//итоговый импульс силами персонажа
    public MyVector otherImp = new MyVector();//прочие импульсы полученные из вне

    public float calculateJumpPower()
    {
        float multi = Math.abs(gravity);
        jumpPower = (float) ((-1 + Math.sqrt(1 + (8 / multi) * defJumpHeight)) / (2 / multi));
        flyTime = Math.abs(jumpPower / gravity) * 2;
        return jumpPower;
    }
}
