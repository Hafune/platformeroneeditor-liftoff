package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public final class CharacterLinkComponent implements Component
{
    public final Entity entity;

    public CharacterLinkComponent(Entity entity)
    {
        this.entity = entity;
    }
}
