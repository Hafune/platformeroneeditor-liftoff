package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;

public class FlipXComponent extends MyAbstractComponent
{
    public static final ComponentMapper<FlipXComponent> map = ComponentMapper.getFor(FlipXComponent.class);

    private int sign = 1;
    private boolean flipX = false;

    public void setFlipX(boolean flipX)
    {
        this.flipX = flipX;
        sign = flipX ? -1 : 1;
    }

    public boolean isFlipX()
    {
        return flipX;
    }

    public int getSign()
    {
        return sign;
    }
}
