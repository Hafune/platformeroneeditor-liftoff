package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;

import java.util.ArrayList;

public class IntersectTargetComponent implements Component
{
    public final ArrayList<Entity> parents = new ArrayList<>();
    public final ArrayList<IImpact[]> impacts = new ArrayList<>();
}
