package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

public class AnimationZIndexComponent implements Component
{
    public static final ComponentMapper<AnimationZIndexComponent> map = ComponentMapper.getFor(AnimationZIndexComponent.class);

    public int zIndex = 0;
}
