package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.systems.gameFight.abilities.fighters.TargetType;

public class TargetTypeComponent extends MyAbstractComponent
{
    public static final ComponentMapper<TargetTypeComponent> map = ComponentMapper.getFor(TargetTypeComponent.class);

    public TargetType type;

    public TargetTypeComponent(TargetType type)
    {
        this.type = type;
    }
}
