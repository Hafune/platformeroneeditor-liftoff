package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.systems.gameFight.patterns.Patterns;

public class IIComponent implements Component
{
    public static final ComponentMapper<IIComponent> map = ComponentMapper.getFor(IIComponent.class);

    public Patterns pattern = Patterns.aggressiveAttack;

    public float action;
}
