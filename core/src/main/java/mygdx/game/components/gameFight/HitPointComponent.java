package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;

public class HitPointComponent implements Component
{
    public float maxHp = 100;
    public float hp = maxHp;

    public float getPercent()
    {
        return hp / maxHp;
    }

    public float getLostPercent()
    {
        return 1 - hp / maxHp;
    }

    public void addHp(float hp)
    {
        this.hp += hp;
        if (this.hp < 0) this.hp = 0;
        if (this.hp > maxHp) this.hp = maxHp;
    }
}
