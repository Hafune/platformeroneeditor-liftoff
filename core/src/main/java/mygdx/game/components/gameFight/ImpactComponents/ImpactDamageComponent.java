package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;

import java.util.ArrayList;

public class ImpactDamageComponent implements Component, IImpact
{
    public static final ComponentMapper<ImpactDamageComponent> map = ComponentMapper.getFor(ImpactDamageComponent.class);

    public float damage;
    public ArrayList<Float> list = new ArrayList<>();

    public ImpactDamageComponent()
    {

    }

    public ImpactDamageComponent(float damage)
    {
        this.damage = damage;
    }

    @Override
    public void addImpact(Entity parentEntity, Entity targetEntity)
    {
        ImpactDamageComponent targetDamageComponent = targetEntity.getComponent(ImpactDamageComponent.class);

        if (damage != 0) targetDamageComponent.list.add(damage);
    }
}
