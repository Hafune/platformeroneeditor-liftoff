package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Batch;

public abstract class AbstractAnimationComponent implements Component
{
    public int zIndex = 0;
    public boolean hitFreeze = false;

    public float positionX;
    public float positionY;

    public abstract void draw(Batch batch, float positionX, float positionY, float time);
}
