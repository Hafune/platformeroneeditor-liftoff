package mygdx.game.components.gameFight;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface IMyAnimationComponent
{
    void draw(Batch batch, float positionX, float positionY, float time);
}
