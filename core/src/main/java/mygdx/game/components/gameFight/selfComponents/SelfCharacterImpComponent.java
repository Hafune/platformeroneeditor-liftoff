package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.ComponentMapper;

public class SelfCharacterImpComponent extends RemovableComponent
{
    public static final ComponentMapper<SelfCharacterImpComponent> map = ComponentMapper.getFor(SelfCharacterImpComponent.class);

    public float x;
    public float y;

    public SelfCharacterImpComponent(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public SelfCharacterImpComponent()
    {

    }

    @Override
    public SelfCharacterImpComponent setTimer(int frame)
    {
        return (SelfCharacterImpComponent) super.setTimer(frame);
    }

    @Override
    public SelfCharacterImpComponent setDefTimer(int frame)
    {
        return (SelfCharacterImpComponent) super.setDefTimer(frame);
    }
}
