package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;

public class ImpactHurtComponent implements Component, IImpact
{
    public static final ComponentMapper<ImpactHurtComponent> map = ComponentMapper.getFor(ImpactHurtComponent.class);

    public float timer;
    public float defTimer;

    public ImpactHurtComponent(float second)
    {
        this.timer = second * 60;
        this.defTimer = timer;
    }

    public ImpactHurtComponent()
    {
    }

    @Override
    public void addImpact(Entity parentEntity, Entity targetEntity)
    {
        ImpactHurtComponent impactHurtComponent = new ImpactHurtComponent();
        impactHurtComponent.timer = defTimer;
        impactHurtComponent.defTimer = defTimer;

        targetEntity.add(impactHurtComponent);
    }
}
