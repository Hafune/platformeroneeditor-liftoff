package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

public class AnimationPositionComponent implements Component
{
    public static final ComponentMapper<AnimationPositionComponent> map = ComponentMapper.getFor(AnimationPositionComponent.class);

    public float positionX;
    public float positionY;
}
