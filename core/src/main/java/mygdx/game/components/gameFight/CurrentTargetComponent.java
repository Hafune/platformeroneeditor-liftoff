package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public final class CurrentTargetComponent implements Component
{
    public Entity entity;
}
