package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.g2d.Batch;
import mygdx.game.systems.gameFight.abilities.effects.DamageTextEffect;

public class DamageTextComponent implements IMyAnimationComponent, Component
{
    public static final ComponentMapper<DamageTextComponent> map = ComponentMapper.getFor(DamageTextComponent.class);

    public final DamageTextEffect textFieldEffect;

    public DamageTextComponent(DamageTextEffect textFieldEffect)
    {
        this.textFieldEffect = textFieldEffect;
    }

    @Override
    public void draw(Batch batch, float positionX, float positionY, float time)
    {
        if (textFieldEffect != null) textFieldEffect.draw(batch, positionX, positionY, time);
    }
}
