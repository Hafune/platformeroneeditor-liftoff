package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.g2d.Batch;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;

public class AnimationLayersComponent implements IMyAnimationComponent, Component
{
    public static final ComponentMapper<AnimationLayersComponent> map = ComponentMapper.getFor(AnimationLayersComponent.class);

    public AnimationLayers animation;

    @Override
    public void draw(Batch batch, float positionX, float positionY, float time)
    {
        if (animation != null) animation.draw(batch, positionX, positionY, time);
    }
}
