package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;

public class PositionComponent extends MyAbstractComponent
{
    public static final ComponentMapper<PositionComponent> map = ComponentMapper.getFor(PositionComponent.class);

    public float positionX;
    public float positionY;
}
