package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Entity;

public interface IImpact
{
    void addImpact(Entity parentEntity, Entity targetEntity);
}
