package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import mygdx.game.components.gameFight.PositionComponent;

public class ImpactExplosionImpComponent implements Component, IImpact
{
    public static final ComponentMapper<ImpactExplosionImpComponent> map = ComponentMapper.getFor(ImpactExplosionImpComponent.class);

    public float x;
    public float y;


    public ImpactExplosionImpComponent(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public void addImpact(Entity parentEntity, Entity targetEntity)
    {
        PositionComponent parentPosition = parentEntity.getComponent(PositionComponent.class);
        PositionComponent positionComponent = targetEntity.getComponent(PositionComponent.class);

        float targetX = positionComponent.positionX;
        float parentX = parentPosition.positionX;

        float x = this.x * Math.signum(targetX - parentX);

        targetEntity.add(new ImpactCharacterImpComponent(x, y));
    }
}
