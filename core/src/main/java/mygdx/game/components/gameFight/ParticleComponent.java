package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.g2d.Batch;
import mygdx.game.systems.gameFight.abilities.effects.MyParticleEffect;

public class ParticleComponent implements IMyAnimationComponent, Component
{
    public static final ComponentMapper<ParticleComponent> map = ComponentMapper.getFor(ParticleComponent.class);

    public final MyParticleEffect particleEffect;

    public ParticleComponent(MyParticleEffect particleEffect)
    {
        this.particleEffect = particleEffect;
    }

    @Override
    public void draw(Batch batch, float positionX, float positionY, float time)
    {
        if (particleEffect != null) particleEffect.draw(batch, positionX, positionY, time);
    }
}
