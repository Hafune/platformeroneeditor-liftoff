package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import mygdx.game.lib.MyVector;
import mygdx.game.components.gameFight.PositionComponent;

public class SelfRoundTrajectoryImpComponent extends RemovableComponent
{
    public static final ComponentMapper<SelfRoundTrajectoryImpComponent> map = ComponentMapper.getFor(SelfRoundTrajectoryImpComponent.class);

    public MyVector myVector = new MyVector(100, 0);
    public PositionComponent parentPosition;

    public SelfRoundTrajectoryImpComponent()
    {
    }

    public SelfRoundTrajectoryImpComponent(Entity entityWithPositionComponent)
    {
        this.parentPosition = entityWithPositionComponent.getComponent(PositionComponent.class);
    }

    @Override
    public SelfRoundTrajectoryImpComponent setTimer(int frame)
    {
        return (SelfRoundTrajectoryImpComponent) super.setTimer(frame);
    }

    @Override
    public SelfRoundTrajectoryImpComponent setDefTimer(int frame)
    {
        return (SelfRoundTrajectoryImpComponent) super.setDefTimer(frame);
    }
}
