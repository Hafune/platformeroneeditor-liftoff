package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;

import java.util.ArrayList;

public class AbilitiesListComponent extends MyAbstractComponent
{
    public static final ComponentMapper<AbilitiesListComponent> map = ComponentMapper.getFor(AbilitiesListComponent.class);

    public ArrayList<AbstractAbility> abilities = new ArrayList<>();
}
