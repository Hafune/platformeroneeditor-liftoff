package mygdx.game.components.gameFight.selfComponents;

import com.badlogic.ashley.core.Component;

public abstract class RemovableComponent implements Component
{
    public int timer;
    public int defTimer;

    public Component setTimer(int frame)
    {
        this.timer = frame;
        return this;
    }

    public Component setDefTimer(int frame)
    {
        this.defTimer = frame;
        return this;
    }

    public void restoreTimerFromDefTimer()
    {
        if (defTimer > 0) timer = defTimer;
    }
}
