package mygdx.game.components.gameFight;

import com.badlogic.ashley.core.ComponentMapper;
import mygdx.game.components.MyAbstractComponent;
import mygdx.game.systems.gameFight.abilities.fighters.Teams;

public class TeamComponent extends MyAbstractComponent
{
    public static final ComponentMapper<TeamComponent> map = ComponentMapper.getFor(TeamComponent.class);

    public Teams team;

    public TeamComponent(Teams team)
    {
        this.team = team;
    }
}
