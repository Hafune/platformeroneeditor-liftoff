package mygdx.game.components.gameFight.ImpactComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import mygdx.game.components.gameFight.FlipXComponent;

public class ImpactCharacterImpComponent implements Component, IImpact
{
    public static final ComponentMapper<ImpactCharacterImpComponent> map = ComponentMapper.getFor(ImpactCharacterImpComponent.class);

    public float x;
    public float y;

    public ImpactCharacterImpComponent(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public ImpactCharacterImpComponent()
    {
    }

    @Override
    public void addImpact(Entity parentEntity, Entity targetEntity)
    {
        int mx = 1;
        FlipXComponent flipXComponent = parentEntity.getComponent(FlipXComponent.class);
        if (flipXComponent != null) mx = flipXComponent.getSign();

        targetEntity.add(new ImpactCharacterImpComponent(x * mx, y));
    }
}
