package mygdx.game.components

import com.badlogic.ashley.core.Component
import com.fasterxml.jackson.annotation.JsonIgnore
import mygdx.game.lib.MergeObjectWithMap
import mygdx.game.lib.MyObjectMapper

@Suppress("UNCHECKED_CAST")
abstract class MyAbstractComponent : Component {

    @JsonIgnore
    open val serializable = false

    val qualifiedName = this::class.qualifiedName!!

    var uuid = ""

    open fun exportParam(): HashMap<String, Any> = MyObjectMapper.buildMap(this)

    open fun applyParam(map: HashMap<String, Any>)  = MergeObjectWithMap.merge(this, map)

    open fun copy(): Component {
        val values = MyObjectMapper.writeValueAsString(this)
        return MyObjectMapper.readValue(values, this::class.java)
    }
}