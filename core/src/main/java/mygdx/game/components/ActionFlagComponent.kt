package mygdx.game.components

import ktx.ashley.mapperFor

class ActionFlagComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<ActionFlagComponent>()
    }
}