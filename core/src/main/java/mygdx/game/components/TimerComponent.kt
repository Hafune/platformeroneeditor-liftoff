package mygdx.game.components

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor

class TimerComponent : MyAbstractComponent() {

    @JsonIgnore
    var time = 0f

    @JsonIgnore
    var callback: (() -> Unit)? = null

    companion object {
        val mapper = mapperFor<TimerComponent>()
    }
}