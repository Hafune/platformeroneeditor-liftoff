package mygdx.game.components

import ktx.ashley.mapperFor

class UserActivityComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<UserActivityComponent>()
    }
}