package mygdx.game.components

@Suppress("UNCHECKED_CAST")
interface ByMapProperties {

    val map: HashMap<String, Any>

    fun fillMap(values: HashMap<String, Any>) {
        values.forEach { (key, v) ->
            if (this.map.containsKey(key)) {
                this.map[key] = v
            }
        }
    }

    fun mapString(name: String, default: String = ""): HashMap<String, Any> = map.also { it[name] = default }
    fun mapInt(name: String, default: Int = 0): HashMap<String, Any> = map.also { it[name] = default }
    fun mapFloat(name: String, default: Float = 0f): HashMap<String, Any> = map.also { it[name] = default }
    fun mapBoolean(name: String, default: Boolean = false): HashMap<String, Any> = map.also { it[name] = default }
}