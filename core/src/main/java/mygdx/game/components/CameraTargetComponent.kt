package mygdx.game.components

import com.badlogic.ashley.core.Component
import ktx.ashley.mapperFor

class CameraTargetComponent : Component {

    companion object {
        val mapper = mapperFor<CameraTargetComponent>()
    }

    var weight = 1
}