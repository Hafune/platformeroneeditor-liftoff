package mygdx.game.components

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor

class PlatformIgnoreComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<PlatformIgnoreComponent>()
    }

    @JsonIgnore
    override val serializable = true
}