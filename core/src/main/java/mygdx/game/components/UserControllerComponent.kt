package mygdx.game.components

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.inputs.MyPadButtons
import mygdx.game.lib.MyVector

class UserControllerComponent : MyAbstractComponent(), IMyButtonTarget {

    @JsonIgnore
    var pressedButtons = HashMap<MyPadButtons, Boolean>()

    @JsonIgnore
    var justPressedButtons = HashMap<MyPadButtons, Boolean>()

    @JsonIgnore
    var justReleasedButtons = HashMap<MyPadButtons, Boolean>()

    @JsonIgnore
    var pressedTime = ArrayList<Float>()

    @JsonIgnore
    var buttonsSequence = ArrayList<MyPadButtons>()

    @JsonIgnore
    var currentTime = 0f

    @JsonIgnore
    var lastFlipX = false

    @JsonIgnore
    var axisVector = MyVector()

    override fun buttonDown(button: MyPadButtons) {
        pressedButtons[button] = true
        justPressedButtons[button] = true
    }

    override fun buttonUp(button: MyPadButtons) {
        pressedButtons[button] = false
        justPressedButtons[button] = false
        justReleasedButtons[button] = true
    }

    companion object {
        val mapper = mapperFor<UserControllerComponent>()
    }

    /**
     * В карту записываются MyPadButtons
     */
    init {
        for (value in MyPadButtons.values()) {
            pressedButtons[value] = false
            justPressedButtons[value] = false
            justReleasedButtons[value] = false
        }
    }
}