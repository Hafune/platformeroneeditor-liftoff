package mygdx.game.components

import com.badlogic.gdx.math.Vector2
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.mapperFor

class PlatformComponent : MyAbstractComponent() {

    companion object {
        val mapper = mapperFor<PlatformComponent>()
    }

    @JsonIgnore
    override val serializable = true

    @JsonIgnore
    var validPosition = false

    @JsonIgnore
    val lastPosition = Vector2()
}