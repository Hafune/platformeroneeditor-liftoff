package mygdx.game.shaders

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import mygdx.game.lib.bottom
import mygdx.game.lib.left

class ShaderBatch(var testShader: TestShader) {
    private val frameBuffer = FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.width, Gdx.graphics.height, false)

    fun draw(batch: Batch, camera: OrthographicCamera) {
        batch.shader = testShader.shader
        testShader.update()

        val texture = frameBuffer.colorBufferTexture

        batch.draw(
            texture,
            camera.left(),
            camera.bottom(),
            camera.viewportWidth,
            camera.viewportHeight,
            0, 0,
            texture.width, texture.height, false, true
        )
    }

    fun begin() {
        frameBuffer.begin()
    }

//    fun end() {
//        frameBuffer.end()
//    }

    fun endAndDraw(batch: Batch, camera: OrthographicCamera) {
        frameBuffer.end()
        draw(batch, camera)
    }
}
