package mygdx.game.shaders

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import mygdx.game.MyLog
import kotlin.system.exitProcess

class TestShader(batch: Batch) {
    val shader: ShaderProgram = ShaderProgram(
        Gdx.files.internal("shader/test.vert").readString(),
        Gdx.files.internal("shader/test.frag").readString()
    )

    private var time = 0f

    init {
        if (!shader.isCompiled) {
            MyLog.print(shader.log)
            exitProcess(1)
        }
    }

    fun update() {
        time += Gdx.graphics.deltaTime
        MyLog.print(time)
        shader.bind()
        shader.setUniformf("time",time * .1f)
        shader.setUniformf("width",Gdx.graphics.width.toFloat())
        shader.setUniformf("height",Gdx.graphics.height.toFloat())
//        shader.setUniformf("u_amount", 10f)
//        shader.setUniformf("u_speed", .5f)
//        shader.setUniformf("u_time", time)
    }
}