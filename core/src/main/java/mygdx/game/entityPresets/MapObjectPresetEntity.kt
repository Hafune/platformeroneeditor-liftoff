package mygdx.game.entityPresets

import com.badlogic.ashley.core.Entity
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import java.util.stream.Stream

class MapObjectPresetEntity : Entity() {

    init {
        Stream.of(
            AbilityComponent(),
            AnimationSkinComponent(),
            NameComponent(),
            PositionComponent(),
            BodyComponent(),
        ).forEach {
            add(it)
        }
    }
}