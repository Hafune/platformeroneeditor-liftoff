package mygdx.game.entityPresets

import com.badlogic.ashley.core.Entity
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import java.util.stream.Stream

class WorldCellPreset : Entity() {

    init {
        Stream.of(
            TileComponent(),
            MapPositionComponent()
        ).forEach {
            add(it)
        }
    }
}