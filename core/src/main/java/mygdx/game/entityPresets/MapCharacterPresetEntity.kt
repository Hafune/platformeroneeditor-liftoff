package mygdx.game.entityPresets

import com.badlogic.ashley.core.Entity
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.*
import java.util.stream.Stream

class MapCharacterPresetEntity : Entity() {

    init {
        Stream.of(
            AbilityComponent(),
            AnimationSkinComponent(),
            RepulsiveForceComponent(),
            IconComponent(),
            NameComponent(),
            PositionComponent(),
            BodyComponent(),
        ).forEach {
            add(it)
        }
    }
}