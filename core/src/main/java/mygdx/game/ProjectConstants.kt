package mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin

object ProjectConstants {
    val editorSkin = Skin(Gdx.files.internal("skin/skin/clean-crispy-ui.json"))
}