package mygdx.game.views

import com.badlogic.ashley.core.Entity
import ktx.ashley.get
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent

open class TileView(var entity: Entity? = null) {

    fun position(): MapPositionComponent {
        return this.entity!!.get()!!
    }
    fun tile(): TileComponent {
        return this.entity!!.get()!!
    }
    fun interact(): InteractComponent {
        return this.entity!!.get()!!
    }
    fun name(): NameComponent {
        return this.entity!!.get()!!
    }
}