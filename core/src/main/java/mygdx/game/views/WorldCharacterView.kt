package mygdx.game.views

import com.badlogic.ashley.core.Entity
import ktx.ashley.get
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.*

class WorldCharacterView(var entity: Entity? = null) {

    fun ability(): AbilityComponent {
        return this.entity!!.get()!!
    }

    fun position(): PositionComponent {
        return this.entity!!.get()!!
    }

    fun body(): BodyComponent {
        return this.entity!!.get()!!
    }

    fun animation(): AnimationSkinComponent {
        return this.entity!!.get()!!
    }

    fun icon(): IconComponent {
        return this.entity!!.get()!!
    }

    fun controller(): UserControllerComponent {
        return this.entity!!.get()!!
    }

    fun mapPoint(): MapPointComponent {
        return this.entity!!.get()!!
    }

    fun name(): NameComponent {
        return this.entity!!.get()!!
    }
}