package mygdx.game.audio;

public enum FightSounds
{
    IMPT_slappy_punch_1("IMPT-slappy-punch-1.ogg"),
    IMPT_slappy_punch_2("IMPT-slappy-punch-2.ogg"),
    IMPT_slappy_punch_3("IMPT-slappy-punch-3.ogg"),
    IMPT_slappy_punch_4("IMPT-slappy-punch-4.ogg"),
    IMPT_slappy_punch_5("IMPT-slappy-punch-5.ogg"),
    IMPT_slappy_punch_6("IMPT-slappy-punch-6.ogg"),
    IMPT_slappy_punch_7("IMPT-slappy-punch-7.ogg"),
    IMPT_slappy_punch_8("IMPT-slappy-punch-8.ogg"),
    IMPT_punches_chest_2("IMPT-punches-chest-2.ogg"),
    IMPT_punches_chest_3("IMPT-punches-chest-3.ogg");

    private final static String prePath = "audio/Triune Store - Triune Sound Fighting SFX (WAV)/Punches/";
    private final String path;

    FightSounds(String path)
    {
        this.path = prePath + path;
    }

    @Override
    public String toString()
    {
        return path;
    }
}
