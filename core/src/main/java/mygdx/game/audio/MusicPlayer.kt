package mygdx.game.audio

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music

class MusicPlayer(val audioProperties: AudioProperties) {
    private var currentPath: MusicPath? = null
    private var currentMusic: Music? = null

    fun play(path: MusicPath) {

        if (currentPath == path) return
        currentPath = path
        if (currentMusic != null) currentMusic!!.dispose()
        currentMusic = Gdx.audio.newMusic(Gdx.files.internal(path.path))
        currentMusic!!.volume = audioProperties.model.volume
        currentMusic!!.isLooping = true
        currentMusic!!.play()
    }

    fun stop() {
        if (currentMusic != null) currentMusic!!.stop()
    }
}