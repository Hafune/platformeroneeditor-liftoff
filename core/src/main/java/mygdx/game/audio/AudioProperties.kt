package mygdx.game.audio

import mygdx.game.dataBase.services.audioPropertiesService.AudioProperty
import mygdx.game.systems.MyEngine

class AudioProperties(val myEngine: MyEngine) {

    fun saveForUser(user_id: Int) {
        val audio = myEngine.audioProperties.model
        val userAudio = myEngine.db.audioPropertiesService.getByUserId(user_id)
        audio.id = userAudio.id
        audio.user_id = user_id
        myEngine.db.audioPropertiesService.update(audio)
    }

    var model = AudioProperty(HashMap<String, Any?>().also {
        it[AudioProperty::volume.name] = 1f
    })
}