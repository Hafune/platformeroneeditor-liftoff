package mygdx.game.audio;

public enum SoundPath
{
    DefaultOutOfMenu("Decision4.ogg"),
    DefaultOpenMenu("Decision3.ogg"),

    Door1("Door1.ogg"),
    Door2("Door2.ogg"),
    Door3("Door3.ogg"),
    Door4("Door4.ogg"),
    Door5("Door5.ogg"),
    Door6("Door6.ogg"),
    Door7("Door7.ogg"),
    Door8("Door8.ogg"),
    Close1("Close1.ogg"),
    Close2("Close2.ogg"),
    Close3("Close3.ogg"),
    Decision1("Decision1.ogg"),
    Decision2("Decision2.ogg"),
    Decision3("Decision3.ogg"),
    Decision4("Decision4.ogg"),
    Magic7("Magic7.ogg"),
    Cursor1("Cursor1.ogg"),
    Cursor2("Cursor2.ogg"),
    Cursor3("Cursor3.ogg"),
    Cursor4("Cursor4.ogg");

    private final static String prePath = "audio/se/";
    private final String path;

    SoundPath(String path)
    {
        this.path = prePath + path;
    }

    @Override
    public String toString()
    {
        return path;
    }
}
