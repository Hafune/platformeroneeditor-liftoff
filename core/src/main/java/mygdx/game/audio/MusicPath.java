package mygdx.game.audio;

public enum MusicPath
{
    Opening2("audio/2000bgm/2000_Opening2.ogg"),
    Dungeon1("audio/bgm/Dungeon1.ogg"),
    Field1("audio/bgm/Field1.ogg"),
    Town1("audio/bgm/Town1.ogg");


    private String path;

    MusicPath(String path)
    {
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }
}
