package mygdx.game.audio

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound

class SoundPlayer(val audioProperties: AudioProperties) {
    private val map: MutableMap<Enum<*>, Sound> = HashMap()

    fun play(path: Enum<*>?) {
        if (path == null){
            return
        }
        if (!map.containsKey(path)) {
            map[path] = Gdx.audio.newSound(Gdx.files.internal(path.toString()))
        }
        map[path]!!.play(audioProperties.model.volume)
    }

}