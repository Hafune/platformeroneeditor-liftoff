package mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.TimeUtils


/**
 * A nicer class for showing framerate that doesn't spam the console
 * like Logger.log()
 *
 * @author William Hartman
 */
class FrameRate : Disposable {
    private var lastTimeCounted: Long
    private var sinceChange: Float
    private var frameRate: Int
    private var startTime = TimeUtils.millis()
    private var frameTime = TimeUtils.timeSinceMillis(startTime)
    private val font: BitmapFont
    private val batch: SpriteBatch
    private var cam: OrthographicCamera

    private fun resize(screenWidth: Int, screenHeight: Int) {
        cam = OrthographicCamera(screenWidth.toFloat(), screenHeight.toFloat())
        cam.translate((screenWidth / 2).toFloat(), (screenHeight / 2).toFloat())
        cam.update()
        batch.projectionMatrix = cam.combined
    }

    private fun update() {
        if (cam.viewportWidth.toInt() != Gdx.graphics.width || cam.viewportHeight.toInt() != Gdx.graphics.height) {
            resize(Gdx.graphics.width, Gdx.graphics.height)
        }

        val delta = TimeUtils.timeSinceMillis(lastTimeCounted)
        lastTimeCounted = TimeUtils.millis()
        sinceChange += delta.toFloat()
        if (sinceChange >= 1000) {
            sinceChange = 0f
            frameRate = Gdx.graphics.framesPerSecond
        }

        frameTime = TimeUtils.millis() - startTime
        startTime = TimeUtils.millis()
    }

    fun render(elapsedTime: Long) {
        update()
        batch.begin()
        var y = 30f
        font.draw(batch, "fps $frameRate", 3f, (Gdx.graphics.height - y))
        y += font.lineHeight
        font.draw(batch, "frame time $frameTime", 3f, (Gdx.graphics.height - y))
        y += font.lineHeight
        font.draw(batch, "lead time ${elapsedTime / 1000}", 3f, (Gdx.graphics.height - y))
        y += font.lineHeight
        font.draw(batch, "possible fps ${1000000 / elapsedTime.coerceAtLeast(1)}", 3f, (Gdx.graphics.height - y))
        batch.end()
    }

    override fun dispose() {
        font.dispose()
        batch.dispose()
    }

    init {
        lastTimeCounted = TimeUtils.millis()
        sinceChange = 0f
        frameRate = Gdx.graphics.framesPerSecond
        font = BitmapFont()
        batch = SpriteBatch()
        cam = OrthographicCamera(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
    }
}