package mygdx.game.inputs


interface IMyButtonTarget {
    fun buttonDown(button: MyPadButtons) {}
    fun buttonUp(button: MyPadButtons) {}
}