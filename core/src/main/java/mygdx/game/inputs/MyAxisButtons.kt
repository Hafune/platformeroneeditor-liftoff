package mygdx.game.inputs

enum class MyAxisButtons {
    //кнопки направлени
    LEFT, RIGHT, CENTER_X, UP, DOWN, CENTER_Y
}