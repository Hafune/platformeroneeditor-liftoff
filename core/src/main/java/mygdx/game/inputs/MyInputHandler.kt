package mygdx.game.inputs

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.controllers.Controllers
import com.badlogic.gdx.scenes.scene2d.Stage
import com.kotcrab.vis.ui.widget.VisTextField
import mygdx.game.inputs.configs.PadData

/**
 * ввод пользователя
 */
class MyInputHandler {

    private lateinit var stage: Stage
    private var leftClick = false
    private var rightClick = false
    val keyboardHandler0 = KeyboardHandler()
    val keyboardHandler1 = KeyboardHandler()
    private var controllerListener0 = GamePadHandler(keyboardHandler0)
    private var controllerListener1 = GamePadHandler(keyboardHandler0)
    private val padData = PadData()

    fun changeStage(stage: Stage) {
        this.stage = stage
        controllerListener0.stage = stage
        controllerListener1.stage = stage

    }

    init {
        val controllerList = arrayOf(controllerListener0, controllerListener1)
        var i = 0
        Controllers.clearListeners()
        while (i < Controllers.getControllers().size && i < controllerList.size) {
            val controller = Controllers.getControllers()[i]
            controllerList[i].setting = padData.findControllerSetting(controller.name)
            controller.addListener(controllerList[i])
            i++
        }
        val multiplexer = InputMultiplexer()
        Gdx.input.inputProcessor = multiplexer
        multiplexer.addProcessor(object : InputProcessor {

            override fun keyDown(keycode: Int): Boolean {
                if (stage.keyboardFocus !is VisTextField) {
                    keyboardHandler0.keyDownCode(keycode)
                }
                stage.keyDown(keycode)
                return false
            }

            override fun keyUp(keycode: Int): Boolean {
                if (stage.keyboardFocus !is VisTextField) {
                    keyboardHandler0.keyUpCode(keycode)
                }
                stage.keyUp(keycode)
                return false
            }

            override fun keyTyped(character: Char): Boolean {
                return stage.keyTyped(character)
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                if (leftClick || rightClick) return false
                if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) leftClick = true
                if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) rightClick = true
                stage.touchDown(screenX, screenY, pointer, button)
                return false
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                if (leftClick && !Gdx.input.isButtonPressed(Input.Buttons.LEFT) || rightClick && !Gdx.input.isButtonPressed(
                        Input.Buttons.RIGHT
                    )
                ) {
                    leftClick = false
                    rightClick = false
                    stage.touchUp(screenX, screenY, pointer, button)
                }
                return false
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
                stage.touchDragged(screenX, screenY, pointer)
                return false
            }

            override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
                stage.mouseMoved(screenX, screenY)
                return false
            }

            override fun scrolled(amountX: Float, amountY: Float): Boolean {
                stage.scrolled(amountX, amountY)
                return false
            }
        })
//        multiplexer.addProcessor(ScreenContainer.instance.stage)
    }
}