package mygdx.game.inputs

enum class MyPadButtons {
    BACKWARD, FORWARD,  //кнопки направлени
    LEFT, RIGHT, UP, DOWN, UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT,  //все остальные кнопки
    CROSS,  //confirm              крестик
    CIRCLE,  //cancel              круг
    TRIANGLE,  //инвентарь           треугольник
    SQUARE,  //                    квадрат
    L1, L2, R1, R2, START, SELECT
}