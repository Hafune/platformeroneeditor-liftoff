package mygdx.game.inputs

object DecodeKeyboardKeys {
    private val decodeKeyboardKeys = Array<MyPadButtons?>(300) { null }
    val keyCodeMap = HashMap<MyPadButtons, Int>()

    init {
        write(29, MyPadButtons.LEFT)
        write(21, MyPadButtons.LEFT)
        write(32, MyPadButtons.RIGHT)
        write(22, MyPadButtons.RIGHT)
        write(51, MyPadButtons.UP)
        write(19, MyPadButtons.UP)
        write(47, MyPadButtons.DOWN)
        write(20, MyPadButtons.DOWN)
        write(33, MyPadButtons.START)
        write(62, MyPadButtons.CROSS)
        write(45, MyPadButtons.CIRCLE)
        write(37, MyPadButtons.TRIANGLE)
        write(44, MyPadButtons.SQUARE)
    }

    private fun write(keyCode: Int, button: MyPadButtons) {
        decodeKeyboardKeys[keyCode] = button
        keyCodeMap[button] = keyCode
    }

    operator fun get(code: Int) = decodeKeyboardKeys[code]
}