package mygdx.game.inputs

import mygdx.game.components.UserControllerComponent

open class KeyboardHandler {
    var iMyButtonTarget: IMyButtonTarget? = null
        set(value) {
            if (value != null) {
                buttonsPressed.forEach { userControllerComponent.buttonUp(it) }
            } else {
                arrowsPressed.forEach { userControllerComponent.buttonDown(it) }
            }
            field = value
        }

    private var convertDirection = true
    private var lastDirection: MyPadButtons? = null
    private val directionButtons = mutableSetOf<MyPadButtons>()
    private val buttonsPressed = ArrayList<MyPadButtons>()
    private val arrowsPressed = ArrayList<MyPadButtons>()
    private val directionsPossiblyCombination = HashMap<String, MyPadButtons>()
    val userControllerComponent = UserControllerComponent()

    fun setConvertDirection(convertDirection: Boolean) {
        this.convertDirection = convertDirection
        buttonsPressed.clear()
        arrowsPressed.clear()
    }

    private fun convertToDirection(button: MyPadButtons): MyPadButtons {
        var direction: MyPadButtons? = button
        for (s in arrowsPressed) {
            if (directionsPossiblyCombination.containsKey("$button+$s")) {
                direction = directionsPossiblyCombination["$button+$s"]
                break
            }
        }
        return direction!!
    }

    fun keyDownCode(keycode: Int) {
        val button = DecodeKeyboardKeys[keycode] ?: return
        keyDownButton(button)
    }

    fun keyDownButton(button: MyPadButtons) {
        if (arrowsPressed.contains(button) || buttonsPressed.contains(button)) {
            return
        }
        var btn = button
        if (convertDirection && directionButtons.contains(btn) && !arrowsPressed.contains(btn)) {
            if (lastDirection != null && lastDirection != btn) keyUpFiltered(lastDirection!!)
            arrowsPressed.add(btn)
            btn = convertToDirection(btn)
            lastDirection = btn
        }
        keyDownFiltered(btn)
    }

    fun keyUpCode(keycode: Int) {
        keyUpButton(DecodeKeyboardKeys[keycode])
    }

    fun keyUpButton(button: MyPadButtons?) {
        if (!arrowsPressed.contains(button) && !buttonsPressed.contains(button)) {
            return
        }
        var btn = button
        if (convertDirection && directionButtons.contains(btn) && arrowsPressed.contains(btn)) {
            arrowsPressed.remove(btn)
            btn = if (arrowsPressed.size > 0) {
                arrowsPressed[arrowsPressed.size - 1]
            } else null
            if (btn != null) {
                btn = convertToDirection(btn)
            }
            if (lastDirection != null && lastDirection != btn) {
                keyUpFiltered(lastDirection!!)
                lastDirection = btn
                btn?.let { keyDownFiltered(it) }
            }
            return
        }
        keyUpFiltered(btn!!)
    }

    private fun keyDownFiltered(button: MyPadButtons) {
        if (buttonsPressed.contains(button)) return
        buttonsPressed.add(button)
        if (iMyButtonTarget != null) {
            iMyButtonTarget!!.buttonDown(button)
        } else {
            userControllerComponent.buttonDown(button)
        }
    }

    private fun keyUpFiltered(button: MyPadButtons) {
        if (!buttonsPressed.remove(button)) return
        if (iMyButtonTarget != null) {
            iMyButtonTarget!!.buttonUp(button)
        } else {
            userControllerComponent.buttonUp(button)
        }
    }

    init {
        directionButtons.add(MyPadButtons.LEFT)
        directionButtons.add(MyPadButtons.RIGHT)
        directionButtons.add(MyPadButtons.UP)
        directionButtons.add(MyPadButtons.DOWN)
        directionsPossiblyCombination[MyPadButtons.LEFT.toString() + "+" + MyPadButtons.UP] = MyPadButtons.UP_LEFT
        directionsPossiblyCombination[MyPadButtons.UP.toString() + "+" + MyPadButtons.LEFT] = MyPadButtons.UP_LEFT
        directionsPossiblyCombination[MyPadButtons.RIGHT.toString() + "+" + MyPadButtons.UP] = MyPadButtons.UP_RIGHT
        directionsPossiblyCombination[MyPadButtons.UP.toString() + "+" + MyPadButtons.RIGHT] = MyPadButtons.UP_RIGHT
        directionsPossiblyCombination[MyPadButtons.LEFT.toString() + "+" + MyPadButtons.DOWN] = MyPadButtons.DOWN_LEFT
        directionsPossiblyCombination[MyPadButtons.DOWN.toString() + "+" + MyPadButtons.LEFT] = MyPadButtons.DOWN_LEFT
        directionsPossiblyCombination[MyPadButtons.RIGHT.toString() + "+" + MyPadButtons.DOWN] = MyPadButtons.DOWN_RIGHT
        directionsPossiblyCombination[MyPadButtons.DOWN.toString() + "+" + MyPadButtons.RIGHT] = MyPadButtons.DOWN_RIGHT
    }
}