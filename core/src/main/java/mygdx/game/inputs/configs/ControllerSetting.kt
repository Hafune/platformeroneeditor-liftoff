package mygdx.game.inputs.configs

import com.badlogic.gdx.utils.XmlReader
import mygdx.game.inputs.MyAxisButtons
import mygdx.game.inputs.MyPadButtons
import mygdx.game.loadSave.Exportable
import kotlin.math.abs
import kotlin.math.sign

class ControllerSetting : Exportable() {

    var controllerName: String by map.also { it[::controllerName.name] = default }

    val centerInterval = .2f

    val default = "default"

    override val propertyName = "pad"

    private var axisMap: HashMap<String, MyAxisButtons> by map.also {
        it[::axisMap.name] = HashMap<String, MyAxisButtons>()
    }

    var buttonCodes: HashMap<Int, MyPadButtons> by map.also { map ->
        map[::buttonCodes.name] = HashMap<Int, MyPadButtons>().also {
            it[0] = MyPadButtons.CROSS
            it[1] = MyPadButtons.CIRCLE
            it[2] = MyPadButtons.SQUARE
            it[3] = MyPadButtons.TRIANGLE
            it[4] = MyPadButtons.SELECT
            it[6] = MyPadButtons.START
            it[11] = MyPadButtons.UP
            it[12] = MyPadButtons.DOWN
            it[13] = MyPadButtons.LEFT
            it[14] = MyPadButtons.RIGHT
        }
    }

    var axisMatching = HashMap<MyAxisButtons, MyPadButtons>().also {
        it[MyAxisButtons.UP] = MyPadButtons.UP
        it[MyAxisButtons.RIGHT] = MyPadButtons.RIGHT
        it[MyAxisButtons.DOWN] = MyPadButtons.DOWN
        it[MyAxisButtons.LEFT] = MyPadButtons.LEFT
    }

    init {
        addAxis(0, 0, MyAxisButtons.CENTER_X)
        addAxis(1, 0, MyAxisButtons.CENTER_Y)
        addAxis(0, 1, MyAxisButtons.RIGHT)
        addAxis(1, 1, MyAxisButtons.DOWN)
        addAxis(0, -1, MyAxisButtons.LEFT)
        addAxis(1, -1, MyAxisButtons.UP)
    }

    private fun calcName(axisCode: Int, direction: Int) = "$axisCode $direction"

    fun addAxis(axisCode: Int, direction: Int, button: MyAxisButtons) {
        val name = calcName(axisCode, direction)
        axisMap[name] = button
    }

    fun axisMoved(axisCode: Int, value: Float): MyAxisButtons? {
        val direction = if (abs(value) > .2f) sign(value).toInt() else 0
        val name = calcName(axisCode, direction)
        axisMap[name]?.let { return it }
        return null
    }

    @Suppress("UNCHECKED_CAST")
    override fun load(element: XmlReader.Element) {
        super.load(element)
        buttonCodes = (map[::buttonCodes.name] as HashMap<String, String>).map { (key, value) ->
            key.toInt() to MyPadButtons.valueOf(value)
        }.toMap() as HashMap<Int, MyPadButtons>

        axisMap = (map[::axisMap.name] as HashMap<String, String>).map { (key, value) ->
            key to MyAxisButtons.valueOf(value)
        }.toMap() as HashMap<String, MyAxisButtons>
    }
}
