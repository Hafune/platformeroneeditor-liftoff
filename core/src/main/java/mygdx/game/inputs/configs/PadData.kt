package mygdx.game.inputs.configs

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter
import java.io.BufferedWriter
import java.io.OutputStreamWriter

class PadData {

    private val fileName = "system/padData.xml"
    private val padList = ArrayList<ControllerSetting>().apply { add(ControllerSetting()) }

    init {
        reload()
    }

    fun findControllerSetting(name: String): ControllerSetting {
        val controller = padList.find { it.controllerName == name }
        return controller ?: ControllerSetting()
    }

    fun exportControllerSettings(controller: ControllerSetting) {
        reload()

        val index = padList.indexOfFirst { it.controllerName == controller.controllerName }
        if (index == -1) {
            padList.add(controller)
        } else {
            padList[index] = controller
        }
        exportAll(padList)
    }

    private fun reload() {
        loadControllersSettings(initializeFile())
    }

    private fun initializeFile(): FileHandle {
        val file = Gdx.files.internal(fileName)

        if (!file.exists()) {
            exportAll(padList)
        }
        return file
    }

    private fun loadControllersSettings(file: FileHandle) {
        val xmlReader = XmlReader()
        val root = xmlReader.parse(file)
        padList.clear()

        for (i in 0 until root.childCount) {
            val padItem = root.getChild(i)
            val controller = ControllerSetting()
            controller.load(padItem)
            padList.add(controller)
        }
    }

    private fun exportAll(list: ArrayList<ControllerSetting>) {
        val out = BufferedWriter(
            OutputStreamWriter(
                Gdx.files.local(fileName).write(false)
            )
        )
        val document = XmlWriter(out)
        val root = document.element("root")

        list.forEach { it.exportNodeItem(root) }

        root.pop()
        document.close()
    }
}