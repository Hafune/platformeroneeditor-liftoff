package mygdx.game.inputs

import com.badlogic.gdx.controllers.Controller
import com.badlogic.gdx.controllers.ControllerListener
import com.badlogic.gdx.scenes.scene2d.Stage
import mygdx.game.inputs.MyAxisButtons.*
import mygdx.game.inputs.configs.ControllerSetting
import mygdx.game.lib.MyVector

class GamePadHandler(private val keyHandler: KeyboardHandler) : ControllerListener {

    lateinit var stage: Stage
    lateinit var setting: ControllerSetting
    private val axisVector = MyVector()

    override fun axisMoved(controller: Controller, axisCode: Int, value: Float): Boolean {
        axisVector.x = -controller.getAxis(1)
        axisVector.y = -controller.getAxis(0)
        axisVector.angle += 90f

        val axisButtons = setting.axisMoved(axisCode, value) ?: return false
        return myAxisMoved(axisButtons)
    }

    private fun myAxisMoved(button: MyAxisButtons): Boolean {
        when (button) {
            LEFT -> upIfPressed(RIGHT)
            RIGHT -> upIfPressed(LEFT)
            UP -> upIfPressed(DOWN)
            DOWN -> upIfPressed(UP)
            CENTER_X -> {
                upIfPressed(RIGHT)
                upIfPressed(LEFT)
            }
            CENTER_Y -> {
                upIfPressed(UP)
                upIfPressed(DOWN)
            }
        }
        if (button == CENTER_X || button == CENTER_Y) {
            return true
        }

        keyHandler.userControllerComponent.axisVector = axisVector
        if (axisVector.value > setting.centerInterval) {
            val axisButton = setting.axisMatching[button]!!
            keyHandler.keyDownButton(axisButton)
            stage.keyDown(DecodeKeyboardKeys.keyCodeMap[axisButton]!!)
        }

        return true
    }

    private fun upIfPressed(button: MyAxisButtons) {
        keyHandler.keyUpButton(setting.axisMatching[button])
        stage.keyUp(DecodeKeyboardKeys.keyCodeMap[setting.axisMatching[button]]!!)
    }

    override fun connected(controller: Controller) {
        println("connected $controller")
    }

    override fun disconnected(controller: Controller) {
        println("disconnected $controller")
    }

    override fun buttonDown(controller: Controller, buttonCode: Int): Boolean {
        println("this $this controller name ${setting.controllerName}")
        if (setting.buttonCodes.containsKey(buttonCode)) {
            val button = setting.buttonCodes[buttonCode]!!
            keyHandler.keyDownButton(button)
            stage.keyDown(DecodeKeyboardKeys.keyCodeMap[button]!!)
        }
        return true
    }

    override fun buttonUp(controller: Controller, buttonCode: Int): Boolean {
        if (setting.buttonCodes.containsKey(buttonCode)) {
            val button = setting.buttonCodes[buttonCode]
            keyHandler.keyUpButton(button)
            stage.keyUp(DecodeKeyboardKeys.keyCodeMap[button]!!)
        }
        return true
    }


}
