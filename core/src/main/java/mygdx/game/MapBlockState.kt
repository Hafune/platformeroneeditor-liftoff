package mygdx.game

import com.badlogic.gdx.physics.box2d.Body
import ktx.ashley.get
import mygdx.game.components.gameWorld.tileComponents.WallComponent
import mygdx.game.systems.MyBox2dHandler

class MapBlockState(private val tileMapRender: TileMapRender, private val box2dHandler: MyBox2dHandler) {

    var map = arrayOf<BooleanArray>()
    private lateinit var bodyMap: Array<Array<Body?>>

    fun isBlocked(x: Int, y: Int): Boolean {
        if (map.isEmpty() || x !in map.indices || y !in map[0].indices) {
            return true
        }
        return map[x][y]
    }

    fun createNewMap(width: Int, height: Int) {
        map = Array(width) { BooleanArray(height) }
        bodyMap = Array(width) { Array(height) { null } }
    }

    fun refreshCell(x: Int, y: Int) {
        var block = false
        for (z in 0..TileMapRender.WORLD_LAYERS) {
            if (tileMapRender.getWorldCell(x, y, z)?.get<WallComponent>() != null) {
                block = true
                break
            }
        }
        if (map[x][y] == block) return
        map[x][y] = block

        when (block) {
            true -> bodyMap[x][y] = box2dHandler.createWall(x, y)
            else -> {
                box2dHandler.destroyBody(bodyMap[x][y]!!)
                bodyMap[x][y] = null
            }
        }
    }
}