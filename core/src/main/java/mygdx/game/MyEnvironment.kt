package mygdx.game

object MyEnvironment {
//    private val prop = MyProperties("env")

//    var GRAPHIC_RESOURCES_LOAD_ASSETS = prop["GRAPHIC_RESOURCES_LOAD_ASSETS"].toBoolean()
//    var EDITOR_ENABLE = prop["EDITOR_ENABLE"].toBoolean()
    var GRAPHIC_RESOURCES_LOAD_ASSETS = true
    var EDITOR_ENABLE = true
    val EDITOR = System.getenv("EDITOR")
}