package mygdx.game.inventory

import mygdx.game.systems.MyEngine

class Inventory(val myEngine: MyEngine) {
    fun saveForUser(user_id: Int) {
        myEngine.db.itemService.saveUserItems(user_id, items)
    }

    var items = myEngine.db.itemService.getNewUserItems()
    fun getGoldModel() = items.first { it.id == 3 }
}