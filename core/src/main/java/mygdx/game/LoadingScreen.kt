package mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.kotcrab.vis.ui.widget.VisLabel
import com.kotcrab.vis.ui.widget.VisProgressBar
import kotlin.math.floor
import kotlin.math.min

class LoadingScreen {

    private val progressBar = VisProgressBar(0f, 1f, .01f, false).apply { debug() }
    private val label = VisLabel("0%").apply { debug() }
    private val stage = Stage(ScreenViewport()).apply {
        addActor(label)
        addActor(progressBar)
    }
    private var lastWidth = 0
    private var lastHeight = 0

    fun draw(percent: Float) {
        if (lastWidth != Gdx.graphics.width || lastHeight != Gdx.graphics.height) {
            stage.viewport.update(Gdx.graphics.width, Gdx.graphics.height)
            lastWidth = Gdx.graphics.width
            lastHeight = Gdx.graphics.height
        }
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        label.setText("${floor(percent * 100).toInt()}%")
        progressBar.value = percent

        label.x = (Gdx.graphics.width / 2) - label.width / 2
        label.y = (Gdx.graphics.height / 2).toFloat()
        progressBar.x = (Gdx.graphics.width / 2) - progressBar.width / 2
        progressBar.y = (Gdx.graphics.height / 2) - label.height

        stage.act(min(Gdx.graphics.deltaTime, 1 / 30f))
        stage.draw()
    }
}