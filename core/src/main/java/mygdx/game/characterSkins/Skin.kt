package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion

interface Skin {

    val id: Int

    val main_skin_id: Int

    var src_body: String?

    val radius: Float

    fun initialize(
        id: Int,
        main_skin_id: Int,
        texture: TextureRegion,
        position: Int,
        width: Int,
        height: Int,
        originX: Float,
        originY: Float,
        radius: Float,
    ): Skin

    fun calcMotionName(motion: String, animAngle: Float = 0f): String

    val iconTexture: TextureRegion

    val motions: Motions
}