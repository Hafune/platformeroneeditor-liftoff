package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures

open class SkinStaticObject : AbstractSkin() {

    override val nameCalculator = MotionNameCalculator8Directions()

    override fun initialize(
        id: Int,
        main_skin_id: Int,
        texture: TextureRegion,
        position: Int,
        width: Int,
        height: Int,
        originX: Float,
        originY: Float,
        radius: Float,
    ): Skin {

        this.id = id
        this.main_skin_id = main_skin_id
        this.radius = radius
        motions = Motions()

        val regions =
            CuttingTextures.getArrayTextureRegion(
                texture,
                width = width,
                height = height,
            )

        iconTexture = regions[0]
        motions.setOrigin(
            if (originX == 0f) iconTexture.regionWidth.toFloat() / 2 else originX,
            if (originY == 0f) iconTexture.regionHeight.toFloat() / 2 else originY
        )
        motions.addMotion(calcMotionName(MotionNames.STAND), *regions.toTypedArray())

        return this
    }
}