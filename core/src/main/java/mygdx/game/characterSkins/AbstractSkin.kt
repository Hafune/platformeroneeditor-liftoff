package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion

abstract class AbstractSkin : Skin {

    override var id: Int = 0

    override var main_skin_id: Int = 0

    override var src_body: String? = null

    override lateinit var iconTexture: TextureRegion

    override var radius: Float = 0f

    override lateinit var motions: Motions

    abstract val nameCalculator: MotionNameCalculator

    override fun calcMotionName(motion: String, animAngle: Float): String {
        return nameCalculator.calcMotionName(motion, animAngle)
    }
}