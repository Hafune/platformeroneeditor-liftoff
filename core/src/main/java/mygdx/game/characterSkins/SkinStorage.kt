package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.dataBase.DatabaseServices
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.TextureStorage

class SkinStorage(db: DatabaseServices) {

    val storageMap = HashMap<String, HashMap<Int, Skin>>()
    val storageIdMap = HashMap<Int, Skin>()
    private val models = db.skinService.getSkins()

    fun getById(id: Int): Skin {
        return storageIdMap[id]!!
    }

    init {
        models.forEach {
            if (storageMap[it.editor_group] == null) {
                storageMap[it.editor_group] = HashMap()
                storageMap["z_empty"] = HashMap()
            }

            val skin: Skin = MyClassForName.get("mygdx.game.characterSkins.${it.type.skin_class}")
            val type = it.type
            skin.src_body = it.src_body

            skin.initialize(
                id = it.id,
                main_skin_id = it.main_skin_id ?: it.id,
                texture = TextureRegion(TextureStorage[it.src]),
                position = it.idx,
                width = type.width,
                height = type.height,
                originX = type.origin_x,
                originY = type.origin_y,
                radius = type.radius
            )

            storageIdMap[it.id] = skin
            storageMap[it.editor_group]!![it.id] = skin
        }
    }
}