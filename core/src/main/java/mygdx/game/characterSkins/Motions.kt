package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.tileSets.dynamicTiles.drawItems.MySlide

object MotionNames {
    const val STAND = "stand"
    const val MOVE = "move"
    const val DASH = "dash"
}

class Motions {

    var originX = 0f
        private set
    var originY = 0f
        private set
    private lateinit var lastAnimationName: String
    private val map = HashMap<String, MySlide>()

    fun draw(batch: Batch, x: Float, y: Float, animationName: String, animationTime: Float) {
        if (map[animationName] == null) {
            map[lastAnimationName]!!.draw(batch, x, y, animationTime)
            return
        }
        map[animationName]!!.draw(batch, x, y, animationTime)
        lastAnimationName = animationName
    }

    fun setOrigin(originX: Float, originY: Float) {
        this.originX = originX
        this.originY = originY
        map.forEach { (_, u) ->
            u.setOrigin(originX, originY)
        }
    }

    fun addMotion(name: String, vararg textureRegion: TextureRegion) {
        lastAnimationName = name
        map[name] = MySlide().also {
            it.setRegions(*textureRegion)
            it.setOrigin(originX, originY)
        }
    }
}