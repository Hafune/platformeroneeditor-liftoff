package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures

class SkinMv : AbstractSkin() {

    override val nameCalculator = MotionNameCalculator4Directions()

    override fun initialize(
        id:Int,
        main_skin_id:Int,
        texture: TextureRegion,
        position: Int,
        width: Int,
        height: Int,
        originX: Float,
        originY: Float,
        radius: Float,
    ): Skin {

        this.id = id
        this.main_skin_id = main_skin_id
        this.radius = radius
        motions = Motions()
        motions.setOrigin(originX, originY)

        val f = CuttingTextures.getArrayTextureRegion(texture, width = width, height = height, columns = 3, rows = 4)

        motions.addMotion(calcMotionName(MotionNames.STAND, 270f), f[1])
        motions.addMotion(calcMotionName(MotionNames.STAND, 180f), f[4])
        motions.addMotion(calcMotionName(MotionNames.STAND, 0f), f[7])
        motions.addMotion(calcMotionName(MotionNames.STAND, 90f), f[10])

        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 270f),
            f[0],
            f[1],
            f[2]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 180f),
            f[3],
            f[4],
            f[5]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 0f),
            f[6],
            f[7],
            f[8]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 90f),
            f[9],
            f[10],
            f[11]
        )

        motions.addMotion(calcMotionName(MotionNames.DASH, 270f), f[1])
        motions.addMotion(calcMotionName(MotionNames.DASH, 180f), f[4])
        motions.addMotion(calcMotionName(MotionNames.DASH, 0f), f[7])
        motions.addMotion(calcMotionName(MotionNames.DASH, 90f), f[10])

        iconTexture = f[1]

        return this
    }
}