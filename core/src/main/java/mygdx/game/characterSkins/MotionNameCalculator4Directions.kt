package mygdx.game.characterSkins

class MotionNameCalculator4Directions : MotionNameCalculator {
    override fun calcMotionName(motion: String, animAngle: Float): String {
        return when (animAngle) {
            in 224.0..316.0 -> "$motion down"
            in 44.0..136.0 -> "$motion up"
            in 135.0..225.0 -> "$motion left"
            else -> "$motion right"
        }
    }
}