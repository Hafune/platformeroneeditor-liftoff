package mygdx.game.characterSkins

class MotionNameCalculator8Directions : MotionNameCalculator {
    override fun calcMotionName(motion: String, animAngle: Float): String {
        return when (animAngle) {
            in 22.5..67.5 -> "$motion up right"
            in 67.5..112.5 -> "$motion up"
            in 112.5..157.5 -> "$motion up left"
            in 157.5..202.5 -> "$motion left"
            in 202.5..247.5 -> "$motion down left"
            in 247.5..292.5 -> "$motion down "
            in 292.5..337.5 -> "$motion down right"
            else -> "$motion right"
        }
    }
}