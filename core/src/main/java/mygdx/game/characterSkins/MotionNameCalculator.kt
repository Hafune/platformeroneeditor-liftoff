package mygdx.game.characterSkins

interface MotionNameCalculator {
    fun calcMotionName(motion: String, animAngle: Float): String
}