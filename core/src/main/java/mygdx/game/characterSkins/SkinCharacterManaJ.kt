package mygdx.game.characterSkins

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures

open class SkinCharacterManaJ : AbstractSkin() {

    override val nameCalculator = MotionNameCalculator8Directions()

    override fun initialize(
        id:Int,
        main_skin_id:Int,
        texture: TextureRegion,
        position: Int,
        width: Int,
        height: Int,
        originX: Float,
        originY: Float,
        radius: Float,
    ): Skin {

        this.id = id
        this.main_skin_id = main_skin_id
        this.radius = radius
        motions = Motions()
        motions.setOrigin(originX, originY)

        val straight =
            CuttingTextures.getArrayTextureRegion(
                texture,
                width = width,
                height = height,
                columns = 3,
                rows = 4
            )
        val diagonal = CuttingTextures.getArrayTextureRegion(
            texture,
            width = width,
            height = height,
            columns = 3,
            rows = 4,
            startPositionX = width * 3
        )

        motions.addMotion(calcMotionName(MotionNames.STAND, 270f), straight[1])
        motions.addMotion(calcMotionName(MotionNames.STAND, 180f), straight[4])
        motions.addMotion(calcMotionName(MotionNames.STAND, 0f), straight[7])
        motions.addMotion(calcMotionName(MotionNames.STAND, 90f), straight[10])

        motions.addMotion(calcMotionName(MotionNames.STAND, 225f), diagonal[1])
        motions.addMotion(calcMotionName(MotionNames.STAND, 315f), diagonal[4])
        motions.addMotion(calcMotionName(MotionNames.STAND, 135f), diagonal[7])
        motions.addMotion(calcMotionName(MotionNames.STAND, 45f), diagonal[10])

        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 270f),
            straight[0],
            straight[1],
            straight[2],
            straight[1]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 180f),
            straight[3],
            straight[4],
            straight[5],
            straight[4]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 0f),
            straight[6],
            straight[7],
            straight[8],
            straight[7]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 90f),
            straight[9],
            straight[10],
            straight[11],
            straight[10]
        )

        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 225f),
            diagonal[0],
            diagonal[1],
            diagonal[2],
            diagonal[1]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 315f),
            diagonal[3],
            diagonal[4],
            diagonal[5],
            diagonal[4]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 135f),
            diagonal[6],
            diagonal[7],
            diagonal[8],
            diagonal[7]
        )
        motions.addMotion(
            calcMotionName(MotionNames.MOVE, 45f),
            diagonal[9],
            diagonal[10],
            diagonal[11],
            diagonal[10]
        )

        iconTexture = straight[1]

        return this
    }
}