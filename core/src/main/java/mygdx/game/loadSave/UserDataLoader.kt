package mygdx.game.loadSave

import mygdx.game.dataBase.services.usersService.UserModel
import mygdx.game.systems.MyEngine
import org.jetbrains.exposed.sql.transactions.transaction

class UserDataLoader(private val myEngine: MyEngine) {

    fun load(userModel: UserModel) {

        val data = myEngine.userData
        data.model = userModel
        data.isCharacterCreated = false

        transaction {
            data.doors = myEngine.db.doorService.getByUserId(userModel.id)
            data.characters = myEngine.db.characterService.getByUserId(userModel.id)

            myEngine.userInventory.items = myEngine.db.itemService.getUserItems(userModel.id)
            myEngine.locationsChests.userChestModels = myEngine.db.chestService.getByUserId(
                userModel.id
            )
            myEngine.audioProperties.model = myEngine.db.audioPropertiesService.getByUserId(userModel.id)
        }

        start()
    }

    fun start() {
        val data = myEngine.userData
        SceneLoader().load(myEngine, data.model.location) { data.runOnLoadAction() }
    }
}