package mygdx.game.loadSave

import com.badlogic.gdx.utils.XmlReader
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ktx.async.KtxAsync
import ktx.async.newSingleThreadAsyncContext
import mygdx.game.character.CharacterBuilder
import mygdx.game.lib.FileStorage
import mygdx.game.systems.MyEngine

class SceneLoader {

    companion object {
        const val locationsPath = "locations/"
        const val locationsNpcPath = "locationsNpc/"
        const val locationsTeleportPath = "locationsTeleport/"
    }

    fun load(myEngine: MyEngine, locationName: String, callback: () -> Unit) {
        myEngine.loading = true
        val executor = newSingleThreadAsyncContext()
        KtxAsync.launch {
            withContext(executor) {
                val file = FileStorage["$locationsPath$locationName.xml"]

                if (file.exists()) {
                    myEngine.locationsChests.refreshChestList(locationName)
                    MapLoader().loadMap(
                        myEngine.engine,
                        myEngine.screenContainer.tileMapRender,
                        file
                    )
                } else {
                    myEngine.screenContainer.tileMapRender.createCleanMap(10, 10)
                }

                val characters = FileStorage["$locationsNpcPath${locationName}Npc.xml"]

                if (characters.exists()) {
                    val xmlReader = XmlReader()
                    val root = xmlReader.parse(characters)
                    CharacterBuilder(myEngine).loadNpc(root)
                }

                val teleports = FileStorage["$locationsTeleportPath${locationName}Teleport.xml"]

                myEngine.locationsTeleports.list.clear()
                if (teleports.exists()) {
                    val xmlReader = XmlReader()
                    val root = xmlReader.parse(teleports)
                    myEngine.locationsTeleports.loadTeleports(root)
                }
            }
            callback()
            myEngine.loading = false
        }
    }
}