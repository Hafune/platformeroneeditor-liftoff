package mygdx.game.loadSave

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.XmlWriter
import mygdx.game.character.CharacterBuilder
import mygdx.game.systems.MyEngine
import java.io.BufferedWriter
import java.io.OutputStreamWriter

class SceneExporter(private val myEngine: MyEngine) {

    fun exportMap() {
        val out = BufferedWriter(
            OutputStreamWriter(
                Gdx.files.local("${SceneLoader.locationsPath}${myEngine.screenContainer.tileMapRender.locationName}.xml")
                    .write(false)
            )
        )

        val document = XmlWriter(out)
        val root = document.element("root")

        MapExporter(myEngine.screenContainer.tileMapRender).exportGdxXml(root)
        myEngine.screenContainer.tileMapRender.lightMap.exportGdxXml(root)

        root.pop()
        document.close()
    }

    fun exportCharacters() {
        val out = BufferedWriter(
            OutputStreamWriter(
                Gdx.files.local("${SceneLoader.locationsNpcPath}${myEngine.screenContainer.tileMapRender.locationName}Npc.xml")
                    .write(false)
            )
        )
        val document = XmlWriter(out)
        val root = document.element("root")

        CharacterBuilder(myEngine).exportGdxXml(root)

        root.pop()
        document.close()
    }

    fun exportTeleport() {
        val out = BufferedWriter(
            OutputStreamWriter(
                Gdx.files.local("${SceneLoader.locationsTeleportPath}${myEngine.screenContainer.tileMapRender.locationName}Teleport.xml")
                    .write(false)
            )
        )
        val document = XmlWriter(out)
        val root = document.element("root")

        myEngine.locationsTeleports.exportTeleports(root)

        root.pop()
        document.close()
    }
}