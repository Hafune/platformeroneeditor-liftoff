package mygdx.game.loadSave

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.XmlWriter
import ktx.ashley.get
import mygdx.game.MyLog
import mygdx.game.components.MyAbstractComponent
import mygdx.game.components.gameWorld.IgnoreSaveComponent
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.lib.MyObjectMapper
import java.util.*

object ComponentExporter : IMyButtonTarget {
    private const val elementName = "itemModel"
    val uuidHash = HashSet<String>()

    fun exportGdxXml(document: XmlWriter, entity: Entity) {
        if (entity.get<IgnoreSaveComponent>() != null) {
            return
        }
        val element = document.element(elementName)
        for (component in entity.components) {
            val myComponent = if (component is MyAbstractComponent) component else null
            if (myComponent != null && myComponent.serializable) {
                if (myComponent.uuid == "") {
                    myComponent.uuid = generateUuid()
                }
                val map = myComponent.exportParam()
                val child = element.element("component")

                child.text(MyObjectMapper.writeValueAsString(map))
                child.pop()
            }
        }
        element.pop()
    }

    private fun generateUuid(): String {
        var uuid = ""
        var count = 0
        return try {
            do {
                if (uuid != "") {
                    MyLog["generated a duplicate uuid"]
                    count++
                    if (count > 2){
                        throw Exception("many duplicate uuid")
                    }
                }
                uuid = UUID.randomUUID().toString()
            } while (uuidHash.contains(uuid))
            uuidHash.add(uuid)
            uuid
        } catch (e: Exception) {
            "duplicate uuid $uuid"
        }
    }
}