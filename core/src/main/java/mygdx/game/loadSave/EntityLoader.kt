package mygdx.game.loadSave

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.XmlReader

class EntityLoader {

    fun load(
        tiles: XmlReader.Element
    ): ArrayList<Entity> {
        val arr = ArrayList<Entity>()

        for (i in 0 until tiles.childCount) {
            val item = tiles.getChild(i)
            val entity = ComponentLoader.loadAndApplyComponents(item, Entity())
            arr.add(entity)
        }
        ComponentLoader.hash.clear()
        return arr
    }

}