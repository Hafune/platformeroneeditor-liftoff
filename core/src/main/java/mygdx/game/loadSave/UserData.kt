package mygdx.game.loadSave

import com.badlogic.gdx.Gdx
import ktx.ashley.get
import mygdx.game.character.CharacterBuilder
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.dataBase.services.usersService.UserModel
import mygdx.game.editor.ActionEditorBuilder
import mygdx.game.lib.MyObjectMapper
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView
import java.util.*
import kotlin.collections.HashSet

class UserData(private val myEngine: MyEngine) {

    companion object {
        const val Hero = "Hero"
    }

    var model = myEngine.db.userService.getUserRawByName("NewGame")
    var doors = HashSet<Int>()
    var characters = myEngine.db.characterService.getByUserId(model.id)
    var isCharacterCreated = false

    fun calcGameTime(): String {
        val time = model.game_time + Date().time - model.startTime

        var seconds = (time / 1000 % 60).toString()
        var minutes = (time / 1000 / 60).toString()
        var hours = (time / 1000 / 60 / 60).toString()

        if (hours.length == 1) hours = "0$hours"
        if (hours.length == 2) hours = "0$hours"
        if (minutes.length == 1) minutes = "0$minutes"
        if (seconds.length == 1) seconds = "0$seconds"

        return "$hours:$minutes:$seconds"
    }

    fun buildCharacterFromUserData() {
        if (isCharacterCreated) return
        isCharacterCreated = true

        val entity = CharacterBuilder(myEngine).buildCharacter(
            model.skin_id,
            model.icon_id,
            Hero
        )

        entity.add(myEngine.myInputHandler.keyboardHandler0.userControllerComponent)

        WorldCharacterView(entity).apply {
            position().setPosition(
                model.x,
                model.y
            )
        }
    }

    fun saveProgressTo(userModel: UserModel) {
        model.id = userModel.id
        model.name = "GuiData"
        saveForUser(model)
    }

    fun quickSave() {
        val user = myEngine.db.userService.getUserRawByName("Auto")
        model.id = user.id
        model.name = "Auto"
        saveForUser(model)
    }

    private fun saveForUser(userModel: UserModel) {
        val entity = CharacterBuilder(myEngine).getCharacterByName(Hero)
        val position = entity.get<PositionComponent>()!!
        userModel.x = position.x
        userModel.y = position.y
        userModel.location = myEngine.screenContainer.tileMapRender.locationName
        userModel.variables_json = MyObjectMapper.writeValueAsString(userModel.variables)
        userModel.game_time = userModel.game_time + Date().time - userModel.startTime

        myEngine.db.userService.update(userModel)

        myEngine.audioProperties.saveForUser(userModel.id)

        myEngine.userInventory.saveForUser(userModel.id)

        myEngine.locationsChests.saveForUser(userModel.id)

        myEngine.db.doorService.saveForUser(userModel.id, doors)
    }

    fun runOnLoadAction() {
        val file = Gdx.app.files.internal(model.on_load_action)
        ActionEditorBuilder(myEngine).buildAction(scriptFileName = file.name(), command = file.readString()).also {
            it.callback = {
                buildCharacterFromUserData()
            }
            it.start()
        }
    }
}
