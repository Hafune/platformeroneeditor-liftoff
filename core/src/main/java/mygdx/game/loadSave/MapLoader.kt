package mygdx.game.loadSave

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.XmlReader
import mygdx.game.MapData
import mygdx.game.TileMapRender
import mygdx.game.editor.UndoRedo
import mygdx.game.lib.MyObjectMapper

class MapLoader {

    companion object {
        const val TILE_XML_CONTAINER_NAME = "WorldNode"
    }

    @Suppress("UNCHECKED_CAST")
    fun loadMap(
        engine: Engine,
        tileMapRender: TileMapRender,
        file: FileHandle
    ) {
        val xmlReader = XmlReader()
        val root = xmlReader.parse(file)

        tileMapRender.lightMap.removeLamps()
        UndoRedo.clear()

        val tileNode = root.getChildByName(TILE_XML_CONTAINER_NAME)
        val mapData = MapData(MyObjectMapper.readValue(tileNode.text, HashMap::class.java) as HashMap<String, Any?>)

        tileMapRender.createCleanMap(mapData.width!!, mapData.height!!)
        tileMapRender.locationName = file.nameWithoutExtension()

        EntityLoader().load(tileNode).forEach {
            engine.addEntity(it)
        }
//          MusicPlayer.play(TileMapRender.instance.locName.musicPath)
        tileMapRender.lightMap.refreshLocationLight(tileMapRender.locationName)
        tileMapRender.lightMap.load(root)
    }
}