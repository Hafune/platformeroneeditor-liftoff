package mygdx.game.loadSave

import com.badlogic.gdx.utils.XmlWriter
import com.fasterxml.jackson.databind.ObjectMapper
import mygdx.game.MapData
import mygdx.game.TileMapRender

class MapExporter(private val tileMapRender: TileMapRender) {

    private val tileMapContainer = "WorldNode"
    private val objectMapper = ObjectMapper()

    fun exportGdxXml(document: XmlWriter) {

        val mapParam = document.element(tileMapContainer)
        val mapData = MapData(HashMap())
        mapData.width = tileMapRender.worldWidth
        mapData.height = tileMapRender.worldHeight

        mapParam.text(objectMapper.writeValueAsString(mapData.map))

        tileMapRender.tileMapContainer.tiles.forEach {
            ComponentExporter.exportGdxXml(mapParam, it)
        }
        ComponentExporter.uuidHash.clear()
        mapParam.pop()
    }
}