package mygdx.game.loadSave

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.XmlReader
import com.fasterxml.jackson.databind.ObjectMapper
import mygdx.game.components.MyAbstractComponent
import mygdx.game.inputs.IMyButtonTarget

object ComponentLoader : IMyButtonTarget {
    val hash = HashMap<String, MyAbstractComponent>()

    @Suppress("UNCHECKED_CAST")
    fun loadAndApplyComponents(root: XmlReader.Element, entity: Entity): Entity {
        val objectMapper = ObjectMapper()
        for (i in 0 until root.childCount) {
            val child = root.getChild(i)
            val map = objectMapper.readValue(child.text, HashMap::class.java) as HashMap<String, Any>
            val qualifiedName = map[MyAbstractComponent::qualifiedName.name] as String

            val component: MyAbstractComponent =
                if (map[MyAbstractComponent::uuid.name] != "" &&
                    hash.containsKey(map[MyAbstractComponent::uuid.name])
                ) {
                    hash[map[MyAbstractComponent::uuid.name]]!!
                } else {
                    (entity.components.firstOrNull { it::class.qualifiedName == qualifiedName }
                        ?: Class.forName(qualifiedName).constructors[0].newInstance()) as MyAbstractComponent
                }

            component.applyParam(map)
            entity.add(component)

            hash[component.uuid] = component
        }
        return entity
    }
}