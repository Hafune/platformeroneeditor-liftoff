package mygdx.game.loadSave

import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter
import mygdx.game.lib.MyObjectMapper

@Suppress("UNCHECKED_CAST")
abstract class Exportable {

    val map = HashMap<String, Any>()

    open val propertyName: String = ""

    open fun exportNodeItem(document: XmlWriter, name: String) {
        val param = document.element(name)
        param.text(MyObjectMapper.writeValueAsString(map))
        param.pop()
    }

    open fun load(element: XmlReader.Element) {
        val map = MyObjectMapper.readValue(element.text, HashMap::class.java) as HashMap<String, Any>
        map.forEach { (key, v) -> this.map[key] = v }
    }

    @Deprecated("use exportNodeItem(document: XmlWriter, name: String)")
    open fun exportNodeItem(document: XmlWriter) {
        val param = document.element(propertyName)
        param.text(MyObjectMapper.writeValueAsString(map))
        param.pop()
    }

    @Deprecated("use load(element: XmlReader.Element)")
    open fun loadParam(document: XmlReader.Element) {
        if (document.hasChild(propertyName)) {
            val element = document.getChildByName(propertyName)
            val map = MyObjectMapper.readValue(element.text, HashMap::class.java) as HashMap<String, Any>
            map.forEach { (key, v) -> this.map[key] = v }
        }
    }
}