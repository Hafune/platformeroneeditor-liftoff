package mygdx.game.loadSave

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.Array

object MyFileHandler {

    /**
     * пара возвращает стандартный массив с файлами
     * и массив gdx типа с именами для удобного их вывода в виджеты gdx
     */
    fun getFilesFromFolder(
        path: String,
        startsWith: String = "",
        endsWith: String = ""
    ): Pair<ArrayList<FileHandle>, Array<String>> {
        val pair = Pair(ArrayList<FileHandle>(), Array<String>())
        val folder = Gdx.files.local(path)
        if (folder.exists() && folder.isDirectory && folder.list().isNotEmpty()) {
            for (file in folder.list()) {
                val name = file.name()
                if (name.startsWith(startsWith) && name.endsWith(endsWith)) {
                    pair.first.add(file)
                    pair.second.add(name)
                }
            }
        }
        return pair
    }
}