package mygdx.game.utils

import mygdx.game.dataBase.DatabaseServices
import mygdx.game.dataBase.services.skinsService.SkinModel
import mygdx.game.dataBase.services.skinsService.SkinsService
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.nio.file.Paths


private val path = Paths.get("assets").toAbsolutePath().toString()

fun main() {
    val database: Database = Database.connect(
        url = "jdbc:sqlite:$path/sqlite.db",
        driver = "org.sqlite.JDBC",
    )

    insertSkins(DatabaseServices(database).skinService)
}

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun insertSkins(service: SkinsService) {
    val fromFolder = "characterSkins/nekura_pochi_CC/woman"
    val toSrc = "characterSkins/nekura_pochi_CC/woman"

    val type_key = "nekura_pochi_CC"
    val group = "woman"

    val existingModels = service.getSkins()
    var count = 0
    transaction {
        File("$path/$fromFolder").list().filter { f -> existingModels.find { "$toSrc/$f" == it.src } == null }.forEach {
            println(it)
            val model = SkinModel(HashMap())
            model.name = ""
            model.type_key = type_key
            model.editor_group = group
            model.src = "$toSrc/${it}"
            println("$toSrc/${it}")
            service.insert(model).id
            count++
        }
    }
    println("total inserted $count")
}