package mygdx.game.utils

import java.io.File
import java.nio.file.Paths

private val path = Paths.get("assets").toAbsolutePath().toString()

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun main() {

    val folder = path

    val ids = mutableSetOf<String>()
    File(folder).walkTopDown().forEach { file ->
        if (!file.isDirectory) {
            file.readText().replace(Regex("(skin_id\":\\d+)")) {
                ids.addAll(it.groupValues)
                it.groupValues.first()
            }
        }
    }
    println(ids.map { it.replace("skin_id\":", "") })

}