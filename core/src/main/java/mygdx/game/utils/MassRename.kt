package mygdx.game.utils

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

private val path = Paths.get("assets").toAbsolutePath().toString()

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun main() {

    val folder = "$path/characterSkins/nekura_pochi_CC/man-kids"
    val pattern = "-gigapixel-standard-scale-0_80x"
    val replaceTo = ""
//    val pattern = Regex("(.+).png")
//    val replaceTo = "$1-kids"


    File(folder).listFiles().forEach {
        val newName = it.name.replace(pattern, replaceTo)
        val source: Path = it.toPath()
        println("rename ${it.name} to $newName")
        Files.move(source, source.resolveSibling(newName))
    }
}