package mygdx.game

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import ktx.actors.onClickEvent
import ktx.ashley.get
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.tileMapMenu.LayersForm
import mygdx.game.effects.Effects
import mygdx.game.effects.MyParticleEffect
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.lib.bottom
import mygdx.game.lib.left
import mygdx.game.lib.right
import mygdx.game.lib.top
import mygdx.game.light.LightMap
import mygdx.game.systems.MyEngine
import mygdx.game.tileSets.DrawItemEraser
import mygdx.game.views.TileView
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

class TileMapRender(private val myEngine: MyEngine, screenContainer: ScreenContainer) : Group(),
    IMyButtonTarget {

    companion object {
        const val TILE_HEIGHT = 48f
        const val TILE_WIDTH = 48f
        const val WORLD_LAYERS = 12
        const val charDrawLvl = 6
    }

    val mapBlockState = MapBlockState(this, myEngine.myBox2dHandler)
    val lightMap = LightMap(screenContainer.camera)

    lateinit var tileMapContainer: TileContainer

    var worldWidth = 0
        private set
    var worldHeight = 0
        private set

    var locationName: String = ""
    val layersForm = LayersForm(WORLD_LAYERS)


    fun createCleanMap(width: Int, height: Int) {
        if (::tileMapContainer.isInitialized) {
            tileMapContainer.dispose()
        }
        tileMapContainer = object : TileContainer(width, height, WORLD_LAYERS, myEngine.engine) {
            val tileView = TileView()
            override fun addEntityTile(entity: Entity) {
                super.addEntityTile(entity)
                tileView.entity = entity
                mapBlockState.refreshCell(tileView.position().worldX, tileView.position().worldY)
            }

            override fun removeEntityTile(entity: Entity) {
                super.removeEntityTile(entity)
                tileView.entity = entity
                mapBlockState.refreshCell(tileView.position().worldX, tileView.position().worldY)
            }
        }
        worldWidth = width
        worldHeight = height

        mapBlockState.createNewMap(width, height)
        setWidth((width * TILE_WIDTH))
        setHeight((height * TILE_HEIGHT))
        lightMap.createNewMap(width, height)
    }

    fun getWorldCell(x: Int, y: Int, z: Int): Entity? {
        return tileMapContainer.worldCells[x][y][z]
    }

    fun cellOnScreen(x: Int, y: Int, z: Int): Boolean {
        val camera = myEngine.screenContainer.camera
        return x * TILE_WIDTH + TILE_WIDTH > camera.left() &&
                x * TILE_WIDTH < camera.right() &&
                y * TILE_HEIGHT + TILE_HEIGHT > camera.bottom() &&
                y * TILE_HEIGHT < camera.top() &&
                x >= 0 &&
                x < worldWidth &&
                y >= 0 &&
                y < worldHeight &&
                z >= 0 &&
                z < WORLD_LAYERS
    }

    fun checkWorldEdges(x: Int, y: Int, z: Int): Boolean {
        return x in 0 until worldWidth && y >= 0 && y < worldHeight && z >= 0 && z < WORLD_LAYERS
    }

    fun setWorldCells(entity: Entity) {
        val tile = entity.get<TileComponent>()!!
        val position = entity.get<MapPositionComponent>()!!
        with(position) {
            clearWorldCell(worldX, worldY, worldZ)
            if (tile.drawItemValue != DrawItemEraser.value) {
                myEngine.engine.addEntity(entity)
            }
        }
    }

    fun clearWorldCell(x: Int, y: Int, z: Int) {
        val old = tileMapContainer.worldCells[x][y][z]
        if (old != null) {
            tileMapContainer.worldCells[x][y][z] = null
            myEngine.engine.removeEntity(old)
        }
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        val deltaTime = min(Gdx.graphics.deltaTime, 1 / 30f)

        //цвет нужен что бы исчезновение диалога не влияло на всю сцену
        val batchColor = batch.color
        batch.color = color

        val camera = myEngine.screenContainer.camera
        val minX = max(0, (floor(camera.left() / TILE_WIDTH)).toInt())
        val maxX =
            min(
                worldWidth,
                (ceil((camera.right()) / TILE_WIDTH)).toInt()
            )
        val minY = max(0, (floor(camera.bottom() / TILE_HEIGHT)).toInt())
        val maxY = min(
            worldHeight,
            (ceil((camera.top()) / TILE_HEIGHT)).toInt()
        )

        cont@ for (z in 0 until WORLD_LAYERS) {
            if (charDrawLvl == z) {
                Effects.drawFindAWay(batch)
                myEngine.worldProcessingSystem.animationCharacterSystem.draw(batch)
            }
            if (layersForm.isLayerDisabled(z)) continue@cont

            for (y in minY until maxY) {
                for (x in minX until maxX) {
                    val tile = tileMapContainer.getWorldCellIfExist(x, y, z)?.get<TileComponent>()
                    if (tile != null) {
                        tile.container.draw(batch, parentAlpha)
                        if (myEngine.worldProcessingSystem.animationCharacterSystem.checkProcessing()) {
                            tile.container.act(deltaTime)
                            tile.animationUpdate(myEngine.worldProcessingSystem.time, deltaTime)
                        }
                    }
                }
            }
        }
        MyParticleEffect.getInstance().update(batch)
//        myEngine.worldProcessingSystem.imageAnimationSystem.draw(batch)
        //Свет мешает отображаться меню редактора, без этого не работает
        batch.end()
//        lightMap.update()
        batch.begin()
        batch.color = batchColor

        super.draw(batch, parentAlpha)
    }

    init {
        onClickEvent { _, x, y ->
            myEngine.worldProcessingSystem.clickFindPathSystem.addClick(x / TILE_WIDTH, y / TILE_HEIGHT)
        }
    }
}