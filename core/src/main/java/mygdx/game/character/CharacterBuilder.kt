package mygdx.game.character

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.IgnoreSaveComponent
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.entityPresets.MapCharacterPresetEntity
import mygdx.game.loadSave.ComponentExporter
import mygdx.game.loadSave.ComponentLoader
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView

class CharacterBuilder(private val myEngine: MyEngine) {
    private val characterContainer = "CharacterBuilder"

    fun buildCharacter(skin_id: Int, icon_id: Int, characterName: String): Entity {

        val characterView = WorldCharacterView(MapCharacterPresetEntity())
        characterView.animation().setSkin(myEngine.skinStorage.getById(skin_id))

        characterView.icon().setIcon(myEngine.iconStorage.buildById(icon_id))
        characterView.entity!!.add(IgnoreSaveComponent())

        characterView.name().name = characterName

        myEngine.engine.addEntity(characterView.entity)
        return characterView.entity!!
    }

    fun getCharacterByName(name: String): Entity {
        return myEngine.engine.getEntitiesFor(allOf(NameComponent::class, PositionComponent::class).get())
            .first { it.get<NameComponent>()!!.name == name }
    }

    fun exportGdxXml(document: XmlWriter) {
        val param = document.element(characterContainer)
        val characters =
            myEngine.engine.getEntitiesFor(allOf(PositionComponent::class, AnimationSkinComponent::class).get())
        for (entity in characters) {
            ComponentExporter.exportGdxXml(param, entity)
        }
        ComponentExporter.uuidHash.clear()
        param.pop()
    }

    fun loadNpc(root: XmlReader.Element) {
        if (root.hasChild(characterContainer)) {
            val myChars = root.getChildByName(characterContainer)
            for (i in 0 until myChars.childCount) {
                val item = myChars.getChild(i)
                val entity = Entity()
                ComponentLoader.loadAndApplyComponents(item, entity)
                myEngine.engine.addEntity(entity)
            }
            ComponentLoader.hash.clear()
        }
    }
}