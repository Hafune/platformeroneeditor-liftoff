package mygdx.game.character;

import java.io.Serializable;

public class MyCharacterStatus implements Serializable
{
    private int maxHealthPoint = 76;
    private int curHealthPoint = maxHealthPoint;


    public int getMaxHealthPoint()
    {
        return maxHealthPoint;
    }

    public void setMaxHealthPoint(int maxHealthPoint)
    {
        this.maxHealthPoint = maxHealthPoint;
    }

    public int getCurHealthPoint()
    {
        return curHealthPoint;
    }

    public void setCurHealthPoint(int curHealthPoint)
    {
        this.curHealthPoint = curHealthPoint;
    }

    public void addCurHealthPoint(float addHealthPoint)
    {
        this.curHealthPoint += (int) addHealthPoint;
        if (this.curHealthPoint > maxHealthPoint) this.curHealthPoint = maxHealthPoint;
    }
}
