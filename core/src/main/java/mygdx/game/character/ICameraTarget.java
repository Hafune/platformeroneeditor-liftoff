package mygdx.game.character;

public interface ICameraTarget
{
    float getPositionX();

    float getPositionY();
}
