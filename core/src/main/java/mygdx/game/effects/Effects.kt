package mygdx.game.effects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import mygdx.game.TileMapRender
import mygdx.game.lib.FindRoute.lastPlace
import mygdx.game.lib.FindRoute.lastTrace

object Effects {

    private val myFont = BitmapFont(Gdx.files.internal("fonts/MyFont.fnt"), Gdx.files.internal("fonts/MyFont.png"), false)
    fun drawFindAWay(batch: Batch?) {
        if (lastTrace != null) {
            val place = lastPlace
            val trace = lastTrace
            for (ints in trace!!) {
                myFont.draw(
                    batch,
                    place!![ints[0]][ints[1]].toString(),
                    (ints[0] * TileMapRender.TILE_WIDTH + 5).toFloat(),
                    ints[1] * TileMapRender.TILE_HEIGHT + myFont.lineHeight - 10
                )
            }
        }
    }
}