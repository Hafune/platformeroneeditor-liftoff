package mygdx.game.effects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import mygdx.game.light.LampLight;
import mygdx.game.light.LightMap;
import mygdx.game.systems.WorldProcessingSystem;
import mygdx.game.systems.gameFight.abilities.effects.ParticleFiles;

public class MyParticleEffect {
    private static final MyParticleEffect instance = new MyParticleEffect();

    private ParticleEffect teleport;
    private LampLight light;
    private float time;

    public void test(float x, float y) {
        teleport = new ParticleEffect();
        teleport.load(ParticleFiles.Teleport.effectFile, ParticleFiles.Teleport.imagesDir);
        teleport.start();

        teleport.setPosition(x, y);
//        time = WorldProcessingSystem.instance.getTime();
//        light = LightMap.INSTANCE.getLampLight();
        light.setPosition(x, y);
        light.setDistance(800);
        light.setColor(.1f, .1f, .1f, 1);
    }

    public static MyParticleEffect getInstance() {
        return instance;
    }

    public void update(Batch batch) {
        if (light != null) {
//            float t = WorldProcessingSystem.instance.getTime() - time;
//            if (t > 180 && t <= 240) {
//                light.setDistance(800 * ((240 - t) / 60f));
//            }
            teleport.update(0.0169491f);
            teleport.draw(batch);
//            if (teleport.isComplete() && t == 300) {
//                light.remove();
//                light = null;
//            }
        }
    }
}
