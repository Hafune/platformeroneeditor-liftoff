package mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.kotcrab.vis.ui.widget.VisTextField
import com.kotcrab.vis.ui.widget.spinner.Spinner
import mygdx.game.lib.actorToCenter
import mygdx.game.systems.MyEngine


class ScreenContainer(val myEngine: MyEngine) {

    companion object {
        const val appScreenWidth = 1920f
        const val appScreenHeight = 1080f
        const val windowSettingName = "launcher"
    }

    private val frameRate = FrameRate()
    var elapsedTime = 0L

    val interfaceRoot = Group()
    val stage = Stage(ScreenViewport())

    val camera = stage.camera as OrthographicCamera

    val tileMapRender: TileMapRender
//    private val bufferHandler = ShaderBatch(TestShader(batch))

    fun bufferDraw() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT)
    }

    fun bufferStageActStageDraw() {
        camera.actorToCenter(interfaceRoot)

        stage.act()
        stage.draw()

        myEngine.myBox2dHandler.debugRendererUpdate()
        frameRate.render(elapsedTime)
        MyLog.render()
    }

    fun dispose() {
        stage.dispose()
    }

    init {
//        val viewport = FitViewport(appScreenWidth, appScreenHeight, camera)
//        stage = Stage(viewport)

        stage.root.addCaptureListener(object : InputListener() {
            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if ((stage.keyboardFocus is VisTextField || stage.keyboardFocus is Spinner) &&
                    event.target != stage.keyboardFocus
                ) stage.keyboardFocus = null
                return false
            }
        })

        tileMapRender = TileMapRender(myEngine, this)
        stage.addActor(tileMapRender)
        stage.addActor(interfaceRoot)
        interfaceRoot.width = appScreenWidth
        interfaceRoot.height = appScreenHeight
        interfaceRoot.touchable = Touchable.childrenOnly
    }
}