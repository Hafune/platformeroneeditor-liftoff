package mygdx.game.lib

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.OrderedMap
import ktx.collections.set

class MyProperties() : OrderedMap<String, String>() {

    constructor(fileName: String) : this() {
        val suffix = ".properties"
        val calcName = if (fileName.endsWith(suffix)) fileName else fileName + suffix

        val file = Gdx.files.internal(calcName)
        handleFile(file)
    }

    constructor(file: FileHandle) : this() {
        handleFile(file)
    }

    private fun handleFile(file: FileHandle) {
        if (file.exists()) {
            val text = file.file().readLines()

            text.filter { it.first() != '#' && it.trim().isNotEmpty() }.associate {
                val index = it.indexOfFirst { char -> char == '=' }
                it.substring(0, index) to
                        it.substring(index + 1, it.length).trim()
            }.forEach { (key, value) -> this[key] = value }
        }
    }
}