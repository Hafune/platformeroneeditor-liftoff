package mygdx.game.lib

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.ui.List
import mygdx.game.inputs.MyPadButtons

class UIListController<T>(private val actor: List<T>) : UIKeyController(actor) {

    init {
        actor.removeListener(actor.keyListener)

        actor.addListener(object : InputListener() {
            override fun mouseMoved(event: InputEvent?, x: Float, y: Float): Boolean {
                actor.getItemAt(y)?.let {
                    actor.selected = it
                    return true
                }
                return false
            }
        })
    }

    override fun buttonDown(button: MyPadButtons) {
        when (button) {
            MyPadButtons.UP -> this@UIListController.down(button)
            MyPadButtons.DOWN -> this@UIListController.down(button)
            MyPadButtons.CROSS -> this@UIListController.down(button)
            else -> super.buttonDown(button)
        }
    }

    fun down(button: MyPadButtons) {
        var index = actor.selectedIndex
        when (button) {
            MyPadButtons.DOWN -> {
                index++
                index = if (index > actor.items.size - 1) 0 else index
                actor.selectedIndex = index
            }
            MyPadButtons.UP -> {
                index--
                index = if (index < 0) actor.items.size - 1 else index
                actor.selectedIndex = index
            }
            MyPadButtons.CROSS -> {
                val e = getEventDown()
                val v = actor.localToStageCoordinates(Vector2())
                val background = actor.style.background
                if (background != null) {
                    val theight = actor.height - (background.topHeight + background.bottomHeight)
                    e.stageY = v.y + theight - actor.selectedIndex * actor.itemHeight
                } else {
                    e.stageY = v.y + (actor.items.size - actor.selectedIndex) * actor.itemHeight
                }
                actor.fire(e)
                actor.addAction(ActionTimer(.1f) {
                    actor.fire(getEventUp())
                })
            }
            else -> {
            }
        }
    }
}