package mygdx.game.lib

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import ktx.actors.onClick
import ktx.actors.onKeyDown
import mygdx.game.editor.ActorSetting
import mygdx.game.editor.UndoRedo

object DraggableUi {

    fun <T : Actor> setup(element: T) {
        element.clearListeners()
        element.addListener(object : MyDragListener() {
            override fun drag(event: InputEvent, x: Float, y: Float, pointer: Int) {
                if (!Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    element.debug = false
                    return
                }
                element.x += x
                element.y += y
                element.debug = true

                val nx = element.x
                val ny = element.y

                UndoRedo.addAction({
                    element.x = nx
                    element.y = ny
                }, {
                    element.x = nx - x
                    element.y = ny - y
                })
                UndoRedo.writeStep()
            }
        })

        element.onClick {
            element.stage.keyboardFocus = element
            ActorSetting.setupElement(element)
        }
        element.onKeyDown {
            UndoRedo.checkInput()
        }

        if (element.width == 0f){
            element.width = 100f
        }
        if (element.height == 0f){
            element.height = 100f
        }
    }
}