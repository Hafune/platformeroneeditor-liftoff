package mygdx.game.lib

import com.badlogic.gdx.Gdx
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainCoroutineDispatcher
import kotlinx.coroutines.Runnable
import kotlin.coroutines.CoroutineContext

class MyGdxCoroutine : CoroutineScope {

    override val coroutineContext: Test = Test()

    class Test : MainCoroutineDispatcher() {
        override val immediate = this

        override fun dispatch(context: CoroutineContext, block: Runnable) {
            Gdx.app.postRunnable(block)
        }
    }
}

