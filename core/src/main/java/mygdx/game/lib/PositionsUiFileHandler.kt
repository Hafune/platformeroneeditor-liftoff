package mygdx.game.lib

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.fasterxml.jackson.core.type.TypeReference


@Suppress("UNCHECKED_CAST")
open class PositionsUiFileHandler(val fileName: String) {

    private var map = HashMap<String, GuiData>()

    init {
        loadSettings(initializeFile())
    }

    fun findSettingOrCreate(name: String): GuiData {
        if (!map.containsKey(name)) {
            map[name] = GuiData().also { it.name = name }
        }
        return map[name]!!
    }

    private fun initializeFile(): FileHandle {
        val file = FileStorage[fileName]
        if (!file.exists()) {
            exportAll()
        }
        return file
    }

    private fun loadSettings(file: FileHandle) {
        val typeRef = object : TypeReference<HashMap<String, GuiData>>() {}
        map = MyObjectMapper.readValue(file.file(), typeRef)
    }

    fun exportAll() = Gdx.files.local(fileName).writeString(MyObjectMapper.writeValueAsString(map), false)
}