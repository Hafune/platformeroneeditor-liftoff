package mygdx.game.lib

import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Нарезка текстур
 */
object CuttingTextures {
    private var count = 0

    /**
     *
     * @param texture       полотно текстуры
     * @param width         ширина кадра
     * @param height        высота кадра
     * @param columns   количество столбцов
     * @param rows  количество строк
     * @param offset    c какого кадра в полотне начинать нарезку
     * @param limit     сколько всего кадров требуется вернуть
     * @return              массив с нарезанными текстурами
     */
    fun getArrayTextureRegion(
        texture: TextureRegion,
        width: Int = 0,
        height: Int = 0,
        startPositionX: Int = 0,
        startPositionY: Int = 0,
        columns: Int = 0,
        rows: Int = 0,
        offset: Int = 0,
        limit: Int = 0
    ): ArrayList<TextureRegion> {
        var tWidth = width
        var tHeight = height
        if (tWidth == 0) {
            tWidth = texture.regionWidth
        }
        if (tHeight == 0) {
            tHeight = texture.regionHeight
        }
        var tColumns = columns
        var tRows = rows
        if (tColumns == 0) {
            tColumns = texture.regionWidth / tWidth
        }
        if (tRows == 0) {
            tRows = texture.regionHeight / tHeight
        }

        val frames = ArrayList<TextureRegion>()
        for (b in 0 until tRows) {
            for (a in 0 until tColumns) {
                if (a + b * tColumns >= offset) {
                    frames.add(
                        TextureRegion(
                            texture,
                            startPositionX + a * tWidth,
                            startPositionY + b * tHeight,
                            tWidth,
                            tHeight
                        )
                    )
                    count++
                    if (count == limit) break
                }
            }
            if (count == limit) break
        }
        count = 0
        return frames
    }
}