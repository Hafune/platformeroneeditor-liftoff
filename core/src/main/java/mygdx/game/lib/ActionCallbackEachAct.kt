package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Action

open class ActionCallbackEachAct(var callback: ((delta: Float) -> Unit)) : Action() {

    var totalTime = 0f

    override fun act(delta: Float): Boolean {
        totalTime += delta
        callback(delta)
        return false
    }
}