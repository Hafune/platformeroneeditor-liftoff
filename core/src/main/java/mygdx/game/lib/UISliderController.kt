package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.ui.Slider
import mygdx.game.inputs.MyPadButtons

class UISliderController(private val actor: Slider) : UIKeyController(actor) {

    override fun buttonDown(button: MyPadButtons) {
        when (button) {
            MyPadButtons.LEFT -> this@UISliderController.down(button)
            MyPadButtons.RIGHT -> this@UISliderController.down(button)
            else -> super.buttonDown(button)
        }
    }

    fun down(button: MyPadButtons) {
        when (button) {
            MyPadButtons.LEFT -> actor.value -= actor.stepSize
            MyPadButtons.RIGHT -> actor.value += actor.stepSize
            else -> {}
        }
    }
}