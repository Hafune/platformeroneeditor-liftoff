package mygdx.game.lib

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor

private fun width(camera: OrthographicCamera) = camera.viewportWidth * camera.zoom
private fun height(camera: OrthographicCamera) = camera.viewportHeight * camera.zoom

/** Return camera's left edge position value */
fun OrthographicCamera.left() = this.position.x - width(this) / 2

/** Return camera's right edge position value */
fun OrthographicCamera.right() = this.position.x + width(this) / 2

/** Return camera's bottom edge position value */
fun OrthographicCamera.bottom() = this.position.y - height(this) / 2

/** Return camera's top edge position value */
fun OrthographicCamera.top() = this.position.y + height(this) / 2

fun OrthographicCamera.includePosition(x: Float, y: Float) = (x > this.left() &&
        x < this.right() &&
        y > this.bottom() &&
        y < this.top())

fun OrthographicCamera.includeRectangle(r: Rectangle) = (r.x + r.width > this.left() &&
        r.x < this.right() &&
        r.y + r.height > this.bottom() &&
        r.y < this.top())

fun OrthographicCamera.actorToCenter(a:Actor) {
    a.x = left() + (width(this) - a.width) / 2
    a.y = bottom() + (height(this) - a.height) / 2
}