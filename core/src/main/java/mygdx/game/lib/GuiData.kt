package mygdx.game.lib

class GuiData(
    var name: String = "default",
    var x: Float = 0f,
    var y: Float = 0f,
    var width: Float = 0f,
    var height: Float = 0f,
)