package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import ktx.actors.setKeyboardFocus

open class MyMouseMoveFocusListener : InputListener() {
    override fun mouseMoved(event: InputEvent, x: Float, y: Float): Boolean {
        event.listenerActor.setKeyboardFocus()
        event.target
        return true
    }
}