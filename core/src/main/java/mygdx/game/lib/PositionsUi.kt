package mygdx.game.lib

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import mygdx.game.myWidgets.MyWidget

open class PositionsUi(val fileName: String) {

    val storage = PositionsUiFileHandler(fileName)

    fun <T : Actor> setup(
        widget: MyWidget<T>,
        name: String,
        isDraggable: Boolean = false,
    ): MyWidget<T> {
        setup(widget.actor, name, isDraggable = isDraggable)
        return widget
    }

    fun <T : Actor> setup(actor: T, name: String, isDraggable: Boolean = false): T {
        val sWidth = Gdx.graphics.width.toFloat()
        val sHeight = Gdx.graphics.height.toFloat()

        val guiData: GuiData = storage.findSettingOrCreate(name)

        if (guiData.width == 0f) {// resetData ||
            guiData.x = actor.x
            guiData.y = actor.y
            guiData.width = actor.width
            guiData.height = actor.height
        }

        actor.name = name
        actor.x = guiData.x
        actor.y = guiData.y
        actor.setSize(guiData.width, guiData.height)

        if (actor.width > sWidth) actor.width = sWidth / 2
        if (actor.height > sHeight) actor.height = sHeight / 2
        if (actor.x < 0) actor.x = 0f
        if (actor.x + actor.width > sWidth) actor.x = sWidth - actor.width
        if (actor.y < 0) actor.y = 0f
        if (actor.y + actor.height > sHeight) actor.y = sHeight - actor.height

        if (!isDraggable) return actor

        DraggableUi.setup(actor)

        actor.addListener {
            guiData.x = actor.x
            guiData.y = actor.y
            guiData.width = actor.width
            guiData.height = actor.height
            true
        }
        return actor
    }
}