package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Actor
import ktx.actors.setKeyboardFocus
import mygdx.game.myWidgets.WidgetList

class UIWidgetListController<T : Actor>(actor: WidgetList<T>) : UIKeyController(actor) {

    init {
        var unlock = true
        onFocus = {
            if (unlock) actor.focusItem?.let {
                unlock = false
                actor.focusItem!!.setKeyboardFocus()
            }
            unlock = true
        }
    }
}