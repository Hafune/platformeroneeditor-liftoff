package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Actor

object MyActorMath {
    fun toCenterOfParent(actor: Actor) {
        actor.x = (actor.parent.width - actor.width) / 2
        actor.y = (actor.parent.height - actor.height) / 2
    }

    fun fillParent(actor: Actor) {
        actor.x = 0f
        actor.y = 0f
        actor.width = actor.parent.width
        actor.height = actor.parent.height
    }

    fun toCenterOfActorX(toCenter: Actor, center: Actor) {
        toCenter.x = center.x + (center.width - toCenter.width) / 2
    }

    fun toCenterOfActorY(toCenter: Actor, center: Actor) {
        toCenter.y = center.y + (center.height - toCenter.height) / 2
    }

    fun toCenterOfActor(toCenter: Actor, center: Actor) {
        toCenter.x = center.x + (center.width - toCenter.width) / 2
        toCenter.y = center.y + (center.height - toCenter.height) / 2
    }

    fun resize(actor: Actor, percent: Float) {
        resizeWidth(actor, percent)
        resizeHeight(actor, percent)
    }

    fun resizeWidth(actor: Actor, percent: Float) {
        actor.width = actor.width * percent
    }

    fun resizeHeight(actor: Actor, percent: Float) {
        actor.height = actor.height * percent
    }

    @JvmOverloads
    fun copySize(copyTo: Actor, copyFrom: Actor, resizePercent: Float = 1f) {
        copyTo.setSize(copyFrom.width, copyFrom.height)
        resize(copyTo, resizePercent)
    }

    fun copySize(copyTo: Actor, copyFrom: Actor, resizePercentWidth: Float, resizePercentHeight: Float) {
        copyTo.setSize(copyFrom.width, copyFrom.height)
        resizeWidth(copyTo, resizePercentWidth)
        resizeHeight(copyTo, resizePercentHeight)
    }

    fun setBounds(setTo: Actor, from: Actor) {
        setTo.setBounds(from.x, from.y, from.width, from.height)
    }
}