package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

open class MyDragHandler : ClickListener() {

    override fun touchDragged(event: InputEvent, x: Float, y: Float, pointer: Int) {
        val offsetX = x - touchDownX
        val offsetY = y - touchDownY

        if (event.isStopped) return
        event.listenerActor.x += offsetX
        event.listenerActor.y += offsetY
        event.stop()
    }
}