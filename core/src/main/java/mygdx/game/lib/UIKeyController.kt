package mygdx.game.lib

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import ktx.actors.*
import mygdx.game.inputs.DecodeKeyboardKeys
import mygdx.game.inputs.MyPadButtons
import kotlin.math.abs

open class UIKeyController(
    private val actor: Actor,
    var onCancel: (() -> Unit)? = null,
    var onConfirm: (() -> Unit)? = null,
    var onSwitchMiss: ((button: MyPadButtons) -> Unit)? = null,
    var onFocus: (() -> Unit)? = null,
    var onFocusLost: (() -> Unit)? = null
) {
    companion object {
        private val pos = Vector2()
        private lateinit var parent: Actor

        fun <T : Group> updateKeyControllers(group: T) {
            val list = calc(group)
            list.forEach {
                it.controller.arrayList = list
            }
        }

        private fun calc(group: Group): ArrayList<Element> {
            parent = group
            val arrayList = ArrayList<Element>()
            recurs(group, arrayList)
            return arrayList
        }

        private fun recurs(actor: Group, list: ArrayList<Element>) {
            equal(actor, list)
            for (child in actor.children) {
                if (child is Group) recurs(
                    child,
                    list
                ) else equal(child, list)
            }
        }

        private fun equal(actor: Actor, list: ArrayList<Element>) {
            val listener: ControllerListener? =
                actor.listeners.firstOrNull { it is ControllerListener } as ControllerListener?
            if (listener != null) {
                pos.x = actor.width / 2
                pos.y = actor.height / 2
                actor.localToActorCoordinates(parent, pos)
                list.add(Element(actor, pos.x, pos.y, listener.controller))
            }
        }
    }

    var arrayList = ArrayList<Element>()

    private var upElement: Actor? = null
    private var downElement: Actor? = null
    private var leftElement: Actor? = null
    private var rightElement: Actor? = null

    init {
        actor.addListener(ControllerListener(this))

        actor.onKeyDownEvent(true) { event, keyCode ->
            DecodeKeyboardKeys[keyCode]?.let {
                buttonDown(it)
                event.stop()
            }
        }
        actor.onTouchDown {
            onConfirm?.invoke()
        }
        actor.onKeyboardFocus {
            onFocus?.invoke()
        }
        actor.onKeyboardFocusEvent {
            if (!it.isFocused) onFocusLost?.invoke()
        }
    }

    private fun move(element: Actor): Actor {
        actor.fire(getEventExit())
        element.setKeyboardFocus()
        element.fire(getEventEnter())
        return element
    }

    private fun changeFocus(el: Actor?, angle: Float) =
        if (el != null) move(el) else getElement(angle)?.let { move(it) }

    open fun buttonDown(button: MyPadButtons) {
        when (button) {
            MyPadButtons.UP -> {
                upElement = changeFocus(upElement, 90f)
                upElement ?: onSwitchMiss?.invoke(MyPadButtons.UP)
            }
            MyPadButtons.DOWN -> {
                downElement = changeFocus(downElement, 270f)
                downElement ?: onSwitchMiss?.invoke(MyPadButtons.DOWN)
            }
            MyPadButtons.LEFT -> {
                leftElement = changeFocus(leftElement, 180f)
                leftElement ?: onSwitchMiss?.invoke(MyPadButtons.LEFT)
            }
            MyPadButtons.RIGHT -> {
                rightElement = changeFocus(rightElement, 0f)
                rightElement ?: onSwitchMiss?.invoke(MyPadButtons.RIGHT)
            }
            MyPadButtons.CROSS -> {
                actor.fire(getEventDown())
                actor.addAction(ActionTimer(.1f) {
                    actor.fire(getEventUp())
                })
            }
            MyPadButtons.CIRCLE -> {
                onCancel?.let {
                    it.invoke()
                    actor.fire(getEventExit())
                }
            }
            else -> {}
        }
    }

    fun changeAny() {
        getElement(270f, 180f)?.setKeyboardFocus()
    }

    private fun getElement(directionAngle: Float, searchAngle: Float = 88f): Actor? {
        val centerX = actor.width / 2 + actor.x
        val centerY = actor.height / 2 + actor.y
        val foundedElement =
            arrayList.filter {
                actor !== it.actor &&
                        MyMath.getDifferentFromAngle(
                            MyMath.normalizeAngle(MyMath.getAngleDeg(centerX, centerY, it.x, it.y) - directionAngle),
                            0f
                        ) < searchAngle
            }.minByOrNull {
                it.getDistance(
                    centerX,
                    centerY,
                    directionAngle
                ).toInt()
            }
        return foundedElement?.actor
    }

    class Element(
        val actor: Actor,
        val x: Float,
        val y: Float,
        val controller: UIKeyController
    ) {
        fun getDistance(x: Float, y: Float, directionAngle: Float): Float {
            var multiX = 1f
            var multiY = 1f
            if (directionAngle == 0f || directionAngle == 180f) multiX *= .5f
            if (directionAngle == 90f || directionAngle == 270f) multiY *= .5f
            val difX = abs(this.x - x) * multiX
            val difY = abs(this.y - y) * multiY
            return MyMath.getDistance(0f, 0f, difX, difY)
        }
    }

    fun getEventUp(): InputEvent {
        val event = InputEvent()
        event.type = InputEvent.Type.touchUp
        return event
    }

    fun getEventDown(): InputEvent {
        val event = InputEvent()
        event.type = InputEvent.Type.touchDown
        return event
    }

    fun getEventEnter(): InputEvent {
        val event = InputEvent()
        event.type = InputEvent.Type.enter
        event.pointer = -1
        return event
    }

    fun getEventExit(): InputEvent {
        val event = InputEvent()
        event.type = InputEvent.Type.exit
        event.pointer = -1
        return event
    }

    open class ControllerListener(val controller: UIKeyController) : InputListener()
}