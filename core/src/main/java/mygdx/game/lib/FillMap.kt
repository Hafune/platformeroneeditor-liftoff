package mygdx.game.lib

class FillMap {
    fun <K, T> fillMap(from: HashMap<K, T>, to: HashMap<K, T>) {
        from.forEach { (key, v) ->
            if (to.containsKey(key)) {
                to[key] = v
            }
        }
    }
}