package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener

inline fun <T : Actor> T.onTouchDownClose(crossinline listener: T.() -> Unit): InputListener {
    val inputListener = object : InputListener() {
        override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
            listener()
            event.stop()
            return true
        }
    }
    addListener(inputListener)
    return inputListener
}

inline fun <T : Actor> T.onTouchDownCloseEvent(crossinline listener: T.(event: InputEvent, x: Float, y: Float) -> Unit): InputListener {
    val inputListener = object : InputListener() {
        override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
            listener(event, x, y)
            event.stop()
            return true
        }
    }
    addListener(inputListener)
    return inputListener
}