package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import java.lang.Float.min

open class MyResizeHandler : ClickListener() {

    private var knobSize = 20f
    private var top = 0f
    private var left = 0f
    private var right = 0f
    private var bot = 0f

    override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
        val actor = event.listenerActor
        knobSize = min(20f, actor.width / 4f)
        knobSize = min(knobSize, actor.height / 4f)

        right = actor.width - x
        left = x
        top = actor.height - y
        bot = y
        return true
    }

    override fun touchDragged(event: InputEvent, x: Float, y: Float, pointer: Int) {

        if (event.isStopped) return

        val actor = event.listenerActor

        val botLeft = x <= knobSize && y <= knobSize
        val topLeft = x <= knobSize && y >= actor.height - knobSize
        val botRight = x >= actor.width - knobSize && y <= knobSize
        val topRight = x >= actor.width - knobSize && y >= actor.height - knobSize

        if (topRight) {
            actor.width = x + right
            actor.height = y + top
        } else if (botRight) {
            actor.width = x + right
            actor.height = actor.height - y + bot
            actor.y += y - bot
        } else if (topLeft) {
            actor.width = actor.width - x + top
            actor.height = y + left
            actor.x += x - top
        } else if (botLeft) {
            actor.width = actor.width - x + left
            actor.height = actor.height - y + bot
            actor.x += x - left
            actor.y += y - bot
        }

        if (botLeft ||
            topLeft ||
            botRight ||
            topRight
        ) event.stop()
    }
}