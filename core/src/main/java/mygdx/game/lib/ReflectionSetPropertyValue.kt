package mygdx.game.lib

import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class ReflectionSetPropertyValue {

    @Suppress("UNCHECKED_CAST")
    fun setupValue(item: Any, valName: String, value: Any) {
        val prop = item.javaClass.kotlin.memberProperties.firstOrNull { it.name == valName } ?: return
        if (prop !is KMutableProperty<*>) return
        prop.isAccessible = true

        prop as KMutableProperty<Any>

        prop.setter.call(item, value)
    }

}