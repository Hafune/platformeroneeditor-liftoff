package mygdx.game.lib

import mygdx.game.TileMapRender

object FindRoute {
    var lastPlace: com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Array<Int>>? = null
        private set
    var map: Array<BooleanArray>? = null
    private var nextX: com.badlogic.gdx.utils.Array<Int>? = null
    private var nextY: com.badlogic.gdx.utils.Array<Int>? = null
    private var finX = 0
    private var finY = 0
    private var founded = false
    private var trasX: com.badlogic.gdx.utils.Array<Int>? = null
    private var trasY: com.badlogic.gdx.utils.Array<Int>? = null
    var lastTrace: Array<IntArray>? = null
        private set

    private var tileMapRender: TileMapRender? = null

    fun findRoute(x: Int, y: Int, targetX: Int, targetY: Int, tileMapRender: TileMapRender): Array<IntArray> {

        map = tileMapRender.mapBlockState.map
        this.tileMapRender = tileMapRender
        lastTrace = null
        lastPlace = null
        trasX = null
        trasY = null
        nextX = com.badlogic.gdx.utils.Array()
        nextY = com.badlogic.gdx.utils.Array()
        if (map == null || map!![x][y]) {
            val trace = Array(1) { IntArray(2) }
            trace[0][0] = targetX
            trace[0][1] = targetY
            return trace
        }
        nextX!!.add(x)
        nextY!!.add(y)
        clearPlace()
        finX = targetX
        finY = targetY
        founded = false
        lastPlace!![nextX!![0]][nextY!![0]] = 1
        for (i in 0..49) markCell()
        if (!founded) {
            trasX = null
            trasY = null
        }
        var trace: Array<IntArray>? = null
        if (trasX != null) {
            trace = Array(trasX!!.size) { IntArray(2) }
            for (i in 0 until trasX!!.size) {
                trace[i][0] = trasX!![trasX!!.size - i - 1]
                trace[i][1] = trasY!![trasY!!.size - i - 1]
            }
        } else if (x == targetX && y == targetY) {
            trace = Array(1) { IntArray(2) }
            trace[0][0] = targetX
            trace[0][1] = targetY
        }
        if (trace != null) {
            lastTrace = trace
        } else {
            trace = Array(1) { IntArray(2) }
            trace[0][0] = x
            trace[0][1] = y
        }
        return trace
    }

    private fun clearPlace() {
        lastPlace = com.badlogic.gdx.utils.Array(tileMapRender!!.worldWidth)
        for (i in 0 until tileMapRender!!.worldWidth) {
            lastPlace!!.add(com.badlogic.gdx.utils.Array(tileMapRender!!.worldHeight))
            for (a in 0 until tileMapRender!!.worldHeight) {
                lastPlace!![i].add(0)
            }
        }
    }

    private fun markCell() {
        val curX = nextX
        val curY = nextY
        nextX = com.badlogic.gdx.utils.Array()
        nextY = com.badlogic.gdx.utils.Array()
        for (i in 0 until curX!!.size) {
            recordInMap(curX[i], curY!![i], 1, 0)
            recordInMap(curX[i], curY[i], 0, 1)
            recordInMap(curX[i], curY[i], -1, 0)
            recordInMap(curX[i], curY[i], 0, -1)
            if (funMarkCell(curX[i], curY[i], 1, 1)) recordInMap(curX[i], curY[i], 1, 1)
            if (funMarkCell(curX[i], curY[i], -1, 1)) recordInMap(curX[i], curY[i], -1, 1)
            if (funMarkCell(curX[i], curY[i], -1, -1)) recordInMap(curX[i], curY[i], -1, -1)
            if (funMarkCell(curX[i], curY[i], 1, -1)) recordInMap(curX[i], curY[i], 1, -1)
        }
        if (founded) backTrasert()
    }

    private fun funMarkCell(x: Int, y: Int, offsetX: Int, offsetY: Int): Boolean {
        return checkEdge(x + offsetX, y) && checkEdge(x, y + offsetY)
    }

    private fun checkEdge(x: Int, y: Int): Boolean {
        return funCheckEdge(x, y) && !map!![x][y]
    }

    private fun funCheckEdge(x: Int, y: Int): Boolean {
        return x >= 0 && x < lastPlace!!.size && y >= 0 && y < lastPlace!![0].size
    }

    private fun recordInMap(x: Int, y: Int, offsetX: Int, offsetY: Int) {
        val `val` = lastPlace!![x][y]
        if (funCheckEdge(x + offsetX, y + offsetY)) {
            if (!map!![x + offsetX][y + offsetY]) {
                if (lastPlace!![x + offsetX][y + offsetY] == 0) {
                    lastPlace!![x + offsetX][y + offsetY] = `val` + 1
                    nextX!!.add(x + offsetX)
                    nextY!!.add(y + offsetY)
                    if (x + offsetX == finX && y + offsetY == finY) {
                        founded = true
                    }
                }
            } else if (map!![x + offsetX][y + offsetY]) {
                if (lastPlace!![x + offsetX][y + offsetY] == 0) {
                    lastPlace!![x + offsetX][y + offsetY] = `val` + 1
                    if (x + offsetX == finX && y + offsetY == finY) {
                        founded = true
                        finX = x
                        finY = y
                    }
                }
            }
        }
    }

    private fun backTrasert() {
        val `val` = lastPlace!![finX][finY]
        trasX = com.badlogic.gdx.utils.Array()
        trasY = com.badlogic.gdx.utils.Array()
        trasX!!.add(finX)
        trasY!!.add(finY)
        for (i in 0 until `val` - 2) {
            recordTrace(trasX!![i], trasY!![i])
        }
    }

    private fun recordTrace(x: Int, y: Int) {
        if (checkTrace(x, y, 1, 0)) return
        if (checkTrace(x, y, 0, 1)) return
        if (checkTrace(x, y, -1, 0)) return
        if (checkTrace(x, y, 0, -1)) return
        if (checkTrace(x, y, 1, 1)) return
        if (checkTrace(x, y, -1, 1)) return
        if (checkTrace(x, y, -1, -1)) return
        checkTrace(x, y, 1, -1)
    }

    private fun checkTrace(x: Int, y: Int, offsetX: Int, offsetY: Int): Boolean {
        if (checkEdge(x + offsetX, y + offsetY) && lastPlace!![x + offsetX][y + offsetY] + 1 == lastPlace!![x][y]) {
            trasX!!.add(x + offsetX)
            trasY!!.add(y + offsetY)
            return true
        }
        return false
    }
}