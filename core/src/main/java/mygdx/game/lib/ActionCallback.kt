package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Action

class ActionCallback(var callback: (() -> Unit)) : Action() {

    override fun act(delta: Float): Boolean {
        callback()
        return true
    }
}