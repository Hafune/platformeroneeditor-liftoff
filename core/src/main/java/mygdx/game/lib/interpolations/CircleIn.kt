package mygdx.game.lib.interpolations

import com.badlogic.gdx.math.Interpolation as GdxInterpolations

object CircleIn : Interpolation {
    override fun apply(a: Float) = GdxInterpolations.circleIn.apply(a)
}