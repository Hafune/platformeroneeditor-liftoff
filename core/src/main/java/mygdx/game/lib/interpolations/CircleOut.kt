package mygdx.game.lib.interpolations

import com.badlogic.gdx.math.Interpolation as GdxInterpolations

object CircleOut : Interpolation {
    override fun apply(a: Float) = GdxInterpolations.circleOut.apply(a)
}