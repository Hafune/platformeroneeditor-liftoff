package mygdx.game.lib.interpolations

interface Interpolation {
    fun apply(a: Float): Float

    fun apply(start: Float, end: Float, a: Float): Float {
        return start + (end - start) * apply(a)
    }
}