package mygdx.game.lib.interpolations

import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper

class MyInterpolation {

    private val map = HashMap<String, Any>()
    var interpolation: Interpolation by map.also { it[::interpolation.name] = Linear }
    var reverse: Boolean by map.also { it[::reverse.name] = false }
    var valueOffset: Float by map.also { it[::valueOffset.name] = 0f }
    var scale: Float by map.also { it[::scale.name] = 1f }

    fun apply(a: Float): Float {
        if (reverse) return interpolation.apply(1 - a) * scale + valueOffset
        return interpolation.apply(a) * scale + valueOffset
    }

    fun export(): String {
        map[::interpolation.name] = interpolation::class.qualifiedName!!
        return MyObjectMapper.writeValueAsString(map)
    }

    fun import(values: String) {
        val params = MyObjectMapper.readValue(values, HashMap::class.java)
        params.forEach { (key, v) ->
            if (map.containsKey(key)) {
                map[key as String] = v
            }
        }
        interpolation = MyClassForName.getInstance(params[::interpolation.name] as String)
    }
}