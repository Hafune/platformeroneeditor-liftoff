package mygdx.game.lib.interpolations

import com.badlogic.gdx.math.Interpolation as GdxInterpolations

object Linear : Interpolation {
    override fun apply(a: Float) = GdxInterpolations.linear.apply(a)
}