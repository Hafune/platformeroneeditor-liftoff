package mygdx.game.lib

import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class ReflectionFindDelegatePropertyNames(any: Any) : ArrayList<String>() {
    init {
        any.javaClass.kotlin.memberProperties.forEach {
            if (it !is KMutableProperty<*>) return@forEach
            val access = it.isAccessible
            it.isAccessible = true
            if (it.getDelegate(any) != null){
                add(it.name)
            }
            it.isAccessible = access
        }
    }
}