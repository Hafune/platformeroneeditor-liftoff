package mygdx.game.lib

import com.badlogic.gdx.scenes.scene2d.Action

class ActionTimer(var time: Float = 0f, var callback: (() -> Unit)? = null) : Action() {

    override fun act(delta: Float): Boolean {
        time -= delta
        if (time <= 0) {
            callback?.invoke()
            return true
        }
        return false
    }
}