package mygdx.game.lib

open class PositionsUiStorage {

    private val map = HashMap<String, PositionsUi>()
    private val uiRootPath = "ui"

    operator fun get(key: String): PositionsUi {
        val path = "$uiRootPath/$key"
        if (!map.containsKey(path)) {
            map[path] = PositionsUi(path)
        }
        return map[path]!!
    }

    fun exportAll() {
        map.values.forEach {
            it.storage.exportAll()
        }
    }
}