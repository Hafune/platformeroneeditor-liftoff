@file:Suppress("UNCHECKED_CAST")

package mygdx.game.lib

import java.math.BigDecimal
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KType
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.starProjectedType
import kotlin.reflect.jvm.isAccessible

object MergeObjectWithMap {
    private val exception = Exception("type not found")

    private val BOOLEAN = Boolean::class.starProjectedType
    private val INT = Int::class.starProjectedType
    private val FLOAT = Float::class.starProjectedType
    private val DOUBLE = Double::class.starProjectedType
    private val BIG_DECIMAL = BigDecimal::class.starProjectedType
    private val STRING = String::class.starProjectedType
    private val STRING_BUILDER = StringBuilder::class.starProjectedType

    fun merge(o: Any, m: Map<String, Any>) {
        o.javaClass.kotlin.memberProperties.forEach {
            if (!m.containsKey(it.name) || it !is KMutableProperty<*>) return@forEach

            it.isAccessible = true
            it.setter.call(o, cast(it.returnType, m[it.name]!!))
            it.isAccessible = false
        }
    }

    private fun cast(type: KType, value: Any): Any {
        return when (type) {
            BOOLEAN -> value as Boolean
            INT -> value as Int
            FLOAT -> toFloat(value)
            DOUBLE -> toFloat(value)
            BIG_DECIMAL -> toFloat(value)
            STRING -> value as String
            STRING_BUILDER -> value as String
            else -> throw exception
        }
    }

    private fun toFloat(v: Any): Any {
        return (v as? Double)?.toFloat() ?: v as? Float ?: (v as? Int)?.toFloat() ?: v
    }
}