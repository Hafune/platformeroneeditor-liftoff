package mygdx.game.lib

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector3
import mygdx.game.components.UserControllerComponent
import mygdx.game.inputs.MyPadButtons
import kotlin.math.*

object MyMath {

    fun getDistance(x0: Float, y0: Float, x1: Float, y1: Float): Float {
        return sqrt(((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1)).toDouble()).toFloat()
    }

    fun getAngleDeg(x0: Float, y0: Float, x1: Float, y1: Float): Float {
        var angle = (atan2((y1 - y0).toDouble(), (x1 - x0).toDouble()) / Math.PI * 180).toFloat()
        angle = if (angle < 0) angle + 360 else angle //Без этого диапазон от 0...180 и -1...-180
        return angle
    }

    /**
     *
     * @param angle угол направления вектора
     * @param value длинна вектора
     * @return получаем отклонение вектора по оси Х
     */
    fun getRangeX(angle: Float, value: Float): Float {
        val v = (cos(Math.toRadians(angle.toDouble())) * value).toFloat()
        return if (equal(v, 0f)) 0f else v
    }

    /**
     *
     * @param angle угол направления вектора
     * @param value длинна вектора
     * @return получаем отклонение вектора по оси Y
     */
    fun getRangeY(angle: Float, value: Float): Float {
        val v = (sin(Math.toRadians(angle.toDouble())) * value).toFloat()
        return if (equal(v, 0f)) 0f else v
    }

    /**
     * Переводит угол из радиан в градусы.
     *
     * @param radians - угол в радианах.
     * @return угол в градусах.
     */
    fun toDegrees(radians: Float): Float {
        return (radians * 180 / Math.PI).toFloat()
    }

    /**
     * Переводит угол из градусов в радианы.
     *
     * @param degrees - угол в градусах.
     * @return угол в радианах.
     */
    fun toRadians(degrees: Float): Float {
        return (degrees * Math.PI / 180).toFloat()
    }

    /**
     * Возвращает случайное число в диапазоне от lower до upper.
     *
     * @param lower - наименьшее число диапазона.
     * @param upper - наибольшее число диапазона.
     * @return случайное целое число.
     */
    fun random(lower: Float, upper: Float): Float {
        return (Math.random() * (upper - lower) + lower).toFloat()
    }

    /**
     * Сравнивает два значения с заданной погрешностью.
     *
     * @param a, b - сравниваемые значения.
     * @return возвращает true если значения равны, или false если не равны.
     */
    fun equal(a: Float, b: Float): Boolean {
        val diff = 0.0001f //float diff = 0.00001f;
        return abs(a - b) <= diff
    }

    /**
     * Возвращает процент значения current от общего значения total.
     *
     * @param current - текущее значение.
     * @param total   - общее значение.
     * @return percent.
     */
    fun toPercent(current: Float, total: Float): Float {
        return current / total * 100
    }

    /**
     * Возвращает текущее значене исходя из процентного соотношения к общему числу.
     *
     * @param percent - текущий процент.
     * @param total   - общее значение.
     * @return возвращает текущее значение.
     */
    fun fromPercent(percent: Float, total: Float): Float {
        return percent * total / 100
    }

    fun getCollisionMoveByX(
        movedByX: Float,
        x: Float,
        y: Float,
        semiWidth: Float,
        semiHeight: Float,
        wallX: Float,
        wallY: Float,
        semiWidthWall: Float,
        semiHeightWall: Float
    ): Float {
        var byX = movedByX
        val signX = sign(x - wallX)
        val force = byX * signX
        var freeX = abs(x - wallX) - (semiWidth + semiWidthWall)
        var freeY = abs(y - wallY) - (semiHeight + semiHeightWall)
        if (equal(freeX, 0f)) freeX = 0f
        if (equal(freeY, 0f)) freeY = 0f
        if (freeY < 0 && force < 0 && freeX + force < 0) {
            byX = if (freeX < 0) 0f else (freeX) * sign(byX)
        }
        return byX
    }

    fun getCollisionMoveByY(
        movedByY: Float,
        x: Float,
        y: Float,
        semiWidth: Float,
        semiHeight: Float,
        wallX: Float,
        wallY: Float,
        semiWidthWall: Float,
        semiHeightWall: Float
    ): Float {
        var byY = movedByY
        val signY = sign(y - wallY)
        val force = byY * signY
        var freeX = abs(x - wallX) - (semiWidth + semiWidthWall)
        var freeY = abs(y - wallY) - (semiHeight + semiHeightWall)
        if (equal(freeX, 0f)) freeX = 0f
        if (equal(freeY, 0f)) freeY = 0f
        if (freeX < 0 && force < 0 && freeY + force < 0) {
            byY = if (freeY < 0) 0f else (freeY) * sign(byY)
        }
        return byY
    }

    fun tendsToValue(number: Float, tendsTo: Float, minStep: Float, percent: Float): Float {
        var n = tendsTo - number
        if (n != 0f) {
            var temp = abs(n) - minStep
            temp = if (temp > 0) temp else 0f
            n -= n * percent
            if (abs(n) > temp) {
                n = temp * sign(n)
            }
        }
        return tendsTo - n
    }

    fun contains(v: Vector3, r: Rectangle): Boolean {
        return v.x > r.x && v.x < r.x + r.width && v.y > r.y && v.y < r.y + r.height
    }

    fun getImpFromVector(desiredVector: MyVector, currentVector: MyVector): MyVector {
        val vector = MyVector(desiredVector.x, desiredVector.y)
        val currentForce = currentVector.getDirectionForce(vector.angle)
        val totalForce = max(vector.value - currentForce,0f)

        vector.value = totalForce

        return vector
    }

    fun normalizeAngle(angle: Float): Float {
        var ang = angle
        val min = 0f
        val max = 360f
        if (ang < min || ang >= max) {
            ang = ang - (ang / max).toInt() * max + min
            if (ang < min) {
                ang += max
            }
        }
        return ang
    }

    fun getDifferentFromAngle(a0: Float, a1: Float): Float {
        var dif = abs(a0 - a1)
        dif = if (dif > 180) 360 - dif else dif
        return dif
    }

    fun tendsToAngle(angle: Float, toAngle: Float, step: Float): Float {
        var invert = 1
        var dec = abs(angle - toAngle)
        if (dec > 180) {
            dec = 360 - dec
            if (dec > 180) invert = -1 else if (dec < 180 && toAngle > angle) invert = -1
        } else if (angle > toAngle) {
            invert = -1
        }
        return if (dec < step) toAngle else {
            normalizeAngle(angle + step * invert)
        }
    }

    fun findCommand(controller: UserControllerComponent, vararg buttons: MyPadButtons): Boolean {
        val buttonsSequence = controller.buttonsSequence
        val pressedTime = controller.pressedTime
        val maxInterval = .3f
        var currentIndex = buttonsSequence.size
        if (buttons.size <= buttonsSequence.size) {
            for (i in buttons.indices) {
                val desiredButton = buttons[i]
                val desiredInterval = i * maxInterval + .2f
                var buttonIndex = currentIndex
                for (j in currentIndex - 1 downTo 0) {
                    if (buttonsSequence[j] == desiredButton) {
                        buttonIndex = j
                        break
                    }
                }
                currentIndex = if (buttonIndex < currentIndex) {
                    val currentInterval = controller.currentTime - pressedTime[buttonIndex]
                    if (currentInterval <= desiredInterval) {
                        buttonIndex
                    } else return false
                } else return false
            }
            return true
        }
        return false
    }
}