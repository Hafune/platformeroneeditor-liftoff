package mygdx.game.lib;

public class MyVector {

    private float angle = 0;//угол
    private float value = 0;//сила
    private float x = 0;
    private float y = 0;

    public MyVector() {
        this(0, 0);
    }

    public MyVector(float x, float y) {
        this.x = x;
        this.y = y;
        refreshAngleAndValue();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public boolean equals(MyVector myVector) {
        return x == myVector.x && y == myVector.y;
    }

    public void setAngle(float a) {
        angle = a;
        angle = MyMath.INSTANCE.normalizeAngle(angle);
        refreshXY();
    }

    public void addAngle(float a) {
        a += getAngle();
        setAngle(a);
    }

    public float getAngle() {
        return angle;
    }

    public void setValue(float v) {
        if (value != v) {
            value = v;
            if (value < 0) value = 0;
            refreshXY();
        }
    }

    public void addValue(float v) {
        value += v;
        if (value < 0) value = 0;
        refreshXY();
    }

    //получить значение силы
    public float getValue() {
        return value;
    }

    public void setXY(float X, float Y) {
        x = X;
        y = Y;
        refreshAngleAndValue();
    }

    public void addX(float X) {
        x += X;
        refreshAngleAndValue();
    }

    public void setX(float X) {
        if (x != X) {
            x = X;
            refreshAngleAndValue();
        }
    }

    public void addY(float Y) {
        y += Y;
        refreshAngleAndValue();
    }

    public void setY(float Y) {
        if (y != Y) {
            y = Y;
            refreshAngleAndValue();
        }
    }

    /**
     * складывает данный вектор с заданным вектором v
     *
     * @param v прибовляемый вектор
     */
    public MyVector plus(MyVector v) {
        x += v.x;
        y += v.y;
        refreshAngleAndValue();
        return this;
    }

    private void refreshAngleAndValue() {
        angle = MyMath.INSTANCE.getAngleDeg(0, 0, x, y);
        value = MyMath.INSTANCE.getDistance(0, 0, x, y);
    }

    private void refreshXY() {
        x = MyMath.INSTANCE.getRangeX(angle, value);
        y = MyMath.INSTANCE.getRangeY(angle, value);
    }

    public void set(MyVector v) {
        this.x = v.x;
        this.y = v.y;
        this.value = v.value;
        this.angle = v.angle;
    }

    public float getDirectionForce(float angle) {
        MyVector v = new MyVector(x, y);
        v.setAngle(angle - this.angle);
        return v.x;
    }
}
