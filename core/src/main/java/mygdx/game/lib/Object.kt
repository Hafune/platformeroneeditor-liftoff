package mygdx.game.lib

import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties

object Object {
    fun getKeys(o: Any): Array<String> {
        return o::class.memberProperties.map { it.name }.toTypedArray()
    }

    fun getValues(o: Any): Array<Any?> {
        return o::class.memberProperties.map { it.getter.call() }.toTypedArray()
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getValuesMap(o: Any): Map<String, T> {
        return o::class.memberProperties.associate { it.name to it.getter.call() } as Map<String, T>
    }

    fun getPropertyKey(o: Any, prop: String): String {
        return "${o::class.qualifiedName}.${prop}"
    }

    fun getObjectKey(o: Any): String {
        return "${o::class.qualifiedName}"
    }

    fun getObjectKey(o: KClass<*>): String {
        return "${o::class.qualifiedName}"
    }
}