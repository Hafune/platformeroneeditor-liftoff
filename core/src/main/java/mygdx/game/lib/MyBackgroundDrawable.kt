package mygdx.game.lib

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Align
import ktx.actors.alpha
import java.lang.Integer.min

class MyBackgroundDrawable(
    var filename: String,
    var width: Float = 0f,
    var height: Float = 0f,
    val align: Int = Align.center,
    var parent: Actor? = null,
    private val hoverImage: Image? = null,
    private val fillParent: Boolean = false
) :
    Drawable {

    private lateinit var sprite: Image
    private var color = Color.WHITE

    private var offsetX = 0f
    private var offsetY = 0f

//    private val image = Image(TextureRegionDrawable(TextureStorage[filename]), Scaling.none, align)

    private fun calcOffsetX(v: Float): Float {
        return when (align) {
            Align.center -> v / 2
            Align.bottom -> v / 2
            Align.top -> v / 2
            Align.left -> 0f
            Align.right -> 0f
            else -> v
        }
    }

    private fun calcOffsetY(v: Float): Float {
        return when (align) {
            Align.center -> v
            Align.left -> v
            Align.right -> v
            Align.top -> v * 2
            Align.bottom -> 0f
            else -> v
        }
    }

    private fun buildRegion(texture: Texture, x: Float, y: Float): TextureRegion {
        if (fillParent) {
            return TextureRegion(texture)
        }

        var tx = x.toInt()
        var ty = y.toInt()
        if (x < 0) {
            offsetX = calcOffsetX(-x)
            tx = 0
        }
        if (y < 0) {
            offsetY = calcOffsetY(-y)
            ty = 0
        }

        return TextureRegion(
            texture,
            tx,
            ty,
            min(width.toInt(), texture.width),
            min(height.toInt(), texture.height)
        )
    }

    private fun setSprite(filename: String): Image {
        val texture = TextureStorage[filename]
        val region = when (align) {
            Align.center -> {
                val tx = (texture.width - width) / 2
                val ty = (texture.height - height) / 2
                buildRegion(texture, tx, ty)
            }
            Align.bottom -> {
                val tx = (texture.width - width) / 2
                val ty = texture.height - height
                buildRegion(texture, tx, ty)
            }
            else -> buildRegion(texture, 0f, 0f)
        }

        val sprite = Image(region)
        sprite.color = color

        return sprite

    }

    fun setColor(r: Int, g: Int, b: Int, a: Int) {
        color = Color(r / 255f, g / 255f, b / 255f, a / 255f)
    }

    private fun updateSprite(x: Float, y: Float, width: Float, height: Float) {
        if (!::sprite.isInitialized) {
            this.width = width
            this.height = height
            sprite = setSprite(filename)
            if (fillParent) {
                sprite.width = width
                sprite.height = height
            }
        }
        sprite.setPosition(x + offsetX, y + offsetY)
    }

    override fun draw(batch: Batch, x: Float, y: Float, width: Float, height: Float) {
        updateSprite(x, y, width, height)

        if (parent != null) {
            sprite.draw(batch, parent!!.alpha)
        } else sprite.draw(batch, 1f)

//        image.x = x
//        image.y = y
//        image.width = width
//        image.height = height

//        image.clipBegin()
//        image.draw(batch, 1f)
//        image.clipEnd()

        if (hoverImage != null && parent != null && parent!!.hasKeyboardFocus()) {
            hoverImage.x = x
            hoverImage.y = y
            hoverImage.width = width
            hoverImage.height = height
            hoverImage.draw(batch, 1f)
        }
    }

    override fun getLeftWidth() = 0f

    override fun setLeftWidth(leftWidth: Float) {}
    override fun getRightWidth() = 0f

    override fun setRightWidth(rightWidth: Float) {}
    override fun getTopHeight() = 0f

    override fun setTopHeight(topHeight: Float) {}
    override fun getBottomHeight() = 0f

    override fun setBottomHeight(bottomHeight: Float) {}
    override fun getMinWidth() = 0f

    override fun setMinWidth(minWidth: Float) {}
    override fun getMinHeight() = 0f

    override fun setMinHeight(minHeight: Float) {}
}