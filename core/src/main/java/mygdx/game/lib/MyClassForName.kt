package mygdx.game.lib

import kotlin.reflect.KClass

object MyClassForName {

    @Suppress("UNCHECKED_CAST")
    fun <T> get(c: KClass<*>): T {
        return Class.forName(c.qualifiedName).getConstructor().newInstance() as T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> get(c: KClass<*>, vararg args: Any): T {
        return Class.forName(c.qualifiedName).getConstructor(*args.map { it::class.java }.toTypedArray())
            .newInstance(*args) as T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> get(c: String, vararg args: Any): T {
        return Class.forName(c).getConstructor(*args.map { it::class.java }.toTypedArray())
            .newInstance(*args) as T
    }

    fun <T> get(s: String): T {
        return get(getType(s))
    }

    fun getType(c: String): KClass<*> {
        return Class.forName(c).kotlin
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getInstance(c: String): T {
        return Class.forName(c).kotlin.objectInstance as T
    }
}