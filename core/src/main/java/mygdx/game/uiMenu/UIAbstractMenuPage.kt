package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.Group
import mygdx.game.ScreenContainer
import mygdx.game.audio.SoundPath
import mygdx.game.systems.MyEngine

abstract class UIAbstractMenuPage(private val myEngine: MyEngine) : Group() {

    protected var callback: (() -> Unit)? = null

    open fun open(callback: (() -> Unit)? = null) {
        this.callback = callback
        myEngine.soundPlayer.play(SoundPath.DefaultOpenMenu)
        myEngine.screenContainer.interfaceRoot.addActor(this)
    }

    open fun close() {
        remove()
        myEngine.soundPlayer.play(SoundPath.DefaultOutOfMenu)
        callback?.invoke()
    }

    init {
        width = ScreenContainer.appScreenWidth
        height = ScreenContainer.appScreenHeight
    }
}