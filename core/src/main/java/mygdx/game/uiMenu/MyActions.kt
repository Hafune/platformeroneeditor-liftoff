package mygdx.game.uiMenu

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction
import ktx.actors.plus
import ktx.actors.plusAssign
import mygdx.game.ScreenContainer

object MyActions {

    private val startOffsetY = ScreenContainer.appScreenHeight * .1f
    private val time = .2f
    private val interpolationCircleOut = Interpolation.circleOut
    private val interpolationCircleIn = Interpolation.circleIn

    fun <T : Actor> characterCardTopFadeIn(actor: T) {
        actor += Actions.moveBy(0f, startOffsetY) + Actions.moveBy(
            0f,
            -startOffsetY,
            time,
            interpolationCircleOut
        )
        actor += Actions.fadeOut(0f)
        actor += Actions.fadeIn(time)
    }

    fun <T : Actor> characterCardBottomFadeIn(actor: T) {
        actor += Actions.moveBy(0f, -startOffsetY) + Actions.moveBy(0f, startOffsetY, time, interpolationCircleOut)
        actor += Actions.fadeOut(0f)
        actor += Actions.fadeIn(time)
    }

    fun <T : Actor> characterCardTopFadeOut(actor: T) {
        actor += Actions.moveBy(0f, startOffsetY, time, interpolationCircleIn)
        actor += Actions.fadeOut(time)
    }

    fun <T : Actor> characterCardBottomFadeOut(actor: T) {
        actor += Actions.moveBy(0f, -startOffsetY, time, interpolationCircleIn)
        actor += Actions.fadeOut(time)
    }

    fun <T : Actor> hintRightFadeIn(actor: T) {
        actor += Actions.moveBy(startOffsetY, 0f) + Actions.moveBy(-startOffsetY, 0f, time, interpolationCircleOut)
        actor += Actions.fadeOut(0f)
        actor += Actions.fadeIn(time)
    }

    fun <T : Actor> hintRightFadeOut(actor: T): SequenceAction {
        actor += Actions.moveBy(startOffsetY, 0f, time, interpolationCircleIn)

        val sequenceAction = SequenceAction(Actions.fadeOut(time))
        actor += sequenceAction
        return sequenceAction
    }

    fun <T : Actor> hintLeftFadeIn(actor: T) {
        actor += Actions.moveBy(-startOffsetY, 0f) + Actions.moveBy(startOffsetY, 0f, time, interpolationCircleOut)
        actor += Actions.fadeOut(0f)
        actor += Actions.fadeIn(time)
    }
}