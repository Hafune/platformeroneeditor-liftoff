package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import ktx.actors.setKeyboardFocus
import ktx.actors.txt
import mygdx.game.editor.widgetEditor.ActorBuilder
import mygdx.game.language.GuiText
import mygdx.game.lib.FileStorage
import mygdx.game.lib.UIKeyController
import mygdx.game.myWidgets.StoreSellItemLabel
import mygdx.game.systems.MyEngine

class UIInventoryMenu(val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {

    val asd = myEngine.widgets.imageButtons.button_back()

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)

        asd.actor.setKeyboardFocus()
        ActorBuilder(myEngine.widgets.skin)
    }


    init {

//        asd.controller.onCancel = { close() }

        val positionsUi = myEngine.positionsUiStorage["UIInventoryMenu.json"]

        myEngine.actorBuilder.build(FileStorage["ui/MenuBackground.json"]).first().also {
            addActor(it)
        }

        myEngine.actorBuilder.build(FileStorage["ui/UiInventoryButtonHint.json"]).forEach {
            it as Group
            it.findActor<Label>("exit").also { l -> l.txt = GuiText["exit"] }
            it.findActor<Label>("drop").also { l -> l.txt = GuiText["drop"] }
            addActor(it)
        }

        val widgetItemScrollPane = myEngine.widgets.myWidgetListsVertical.default<StoreSellItemLabel>("Все")
        val itemScrollPane = widgetItemScrollPane.actor
        addActor(positionsUi.setup(itemScrollPane, "itemScrollPane"))
        addActor(asd.actor)
        UIKeyController.updateKeyControllers(this)

        val contentTable = itemScrollPane.contentTable

        myEngine.userInventory.items.forEach { it ->
//            if (it.category_key == categories[index].key) {
            val actor = StoreSellItemLabel(myEngine, it)
            val controller = UIKeyController(actor)

            contentTable.add(actor).growX()

            if (itemScrollPane.focusItem == null) {
                itemScrollPane.focusItem = actor
            }
            controller.onFocus = {
                itemScrollPane.showItem(actor)
                actor.background = actor.hoverBackground
                itemScrollPane.focusItem = actor
            }
            controller.onFocusLost = {
                itemScrollPane.showItem(actor)
                actor.background = null
            }
            controller.onSwitchMiss = {
                widgetItemScrollPane.controller.buttonDown(it)
            }
            contentTable.row()
//            }
        }
        itemScrollPane.validate()
        UIKeyController.updateKeyControllers(contentTable)
    }
}