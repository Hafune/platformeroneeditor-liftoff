package mygdx.game.uiMenu

import ktx.actors.setKeyboardFocus
import mygdx.game.lib.MyActorMath
import mygdx.game.lib.UIKeyController
import mygdx.game.systems.MyEngine

class UISaveMenu(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {

    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()
    private val users = myEngine.db.userService.getUsers()

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)

        val myList = listWidget.actor
        myList.setItems(*users.map { "${it.name}: ${it.id}" }.toTypedArray(), "Новое сохранение")
        myList.setKeyboardFocus()
        myList.pack()

        listWidget.controller.onConfirm = { confirm() }
        listWidget.controller.onCancel = { close() }

        MyActorMath.toCenterOfParent(myList)

        UIKeyController.updateKeyControllers(this)
    }

    private fun confirm() {
        if (listWidget.actor.selectedIndex == listWidget.actor.items.size - 1) {
            val user = myEngine.db.userService.newRaw()
            myEngine.userData.saveProgressTo(user)
        } else myEngine.userData.saveProgressTo(users[listWidget.actor.selectedIndex])

        remove()
        super.close()
    }

    init {
//        val category = "UISaveMenu"
//        val positionsUi = myEngine.positionsUiStorage["$category.json"]

        val background = myEngine.guiImages.backgrounds.game_menu_background()
        addActor(background)
        MyActorMath.fillParent(background)
//        addActor(positionsUi.setup(background, "background"))

        addActor(listWidget.actor)
    }
}