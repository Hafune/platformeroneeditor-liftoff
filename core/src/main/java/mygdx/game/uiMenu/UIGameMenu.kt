package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Label
import ktx.actors.setKeyboardFocus
import ktx.actors.txt
import mygdx.game.audio.SoundPath
import mygdx.game.language.GameText
import mygdx.game.language.GuiText
import mygdx.game.language.KeyWord
import mygdx.game.lib.ActionCallback
import mygdx.game.lib.ActionCallbackEachAct
import mygdx.game.lib.FileStorage
import mygdx.game.lib.UIKeyController
import mygdx.game.myWidgets.CharacterCard
import mygdx.game.systems.MyEngine

class UIGameMenu(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {

    private val listWidget = myEngine.widgets.lists.bahnschrift_32()
    private val charactersGroup: HorizontalGroup
    private lateinit var moneyAndTimeHint: Group
    private lateinit var buttonsHint: Group
    var isFirstOpen = true

    init {
        val category = "UIGameMenu"
        val positionsUi = myEngine.positionsUiStorage["$category.json"]

//        val background = myEngine.guiImages.backgrounds.game_menu_background()
//        addActor(background)
//        MyActorMath.fillParent(background)UiMoneyTimeHint
        myEngine.actorBuilder.build(FileStorage["ui/MenuBackground.json"]).first().also {
            addActor(it)
        }

        charactersGroup = HorizontalGroup().also { it.center() }
        addActor(positionsUi.setup(charactersGroup, "charactersGroup"))

        myEngine.userData.characters.forEachIndexed { index, it ->
            val data = CharacterCard.Model(
                background = "characterCardBackgrounds/" + it.card_background,
                control = if (it.player_id >= 0) it.control else "",
                hitPoint = it.level * 100,
                maxHitPoint = it.level * 100,
                osvValue = 33,
                playerMark = when (it.player_id) {
                    1 -> GameText["mygdx.game.myWidgets.CharacterCard.Player1"]
                    2 -> GameText["mygdx.game.myWidgets.CharacterCard.Player2"]
                    3 -> GameText["mygdx.game.myWidgets.CharacterCard.Player3"]
                    4 -> GameText["mygdx.game.myWidgets.CharacterCard.Player4"]
                    else -> ""
                },
                level = it.level,
                name = it.name,
                progressBar = 100f,
                progressBarMax = 250f
            )
            val widget = myEngine.widgets.characterCards.default(data, height + 200)
            val table = widget.actor
            table.y -= 100

            if (index % 2 == 0) MyActions.characterCardTopFadeIn(table)
            else MyActions.characterCardBottomFadeIn(table)

            charactersGroup.addActor(table)
        }
        charactersGroup.validate()

        val buttonBackground = positionsUi.setup(Actor(), "buttonBackground")
        addActor(buttonBackground)

        val myList = positionsUi.setup(listWidget.actor, "${category}.myList")
        listWidget.audio.mute = true
        listWidget.audio.soundConfirm = null
        listWidget.controller.onConfirm = { confirm() }
        listWidget.controller.onCancel = { close() }
        myList.setItems(
            GameText[KeyWord.Items.name],
            GameText[KeyWord.Skills.name],
            GameText[KeyWord.Equipment.name],
            GameText[KeyWord.Class.name],
            GameText[KeyWord.Status.name],
            GameText[KeyWord.Formation.name],
            GameText[KeyWord.Save.name],
            GameText[KeyWord.System.name],
            GameText[KeyWord.QuitInMainMenu.name]
        )
        addActor(myList)

//        val money = myEngine.widgets.labels.bahnschrift_22(GameText["mygdx.game.uiMenu.UIGameMenu.currency"])
//        val moneyValue =
//            myEngine.widgets.labels.bahnschrift_28_hitpoint(myEngine.userInventory.getGoldModel().capacity.toString())
//        moneyValue.setAlignment(Align.right)
//        val gameTime = myEngine.widgets.labels.bahnschrift_22(GameText["mygdx.game.uiMenu.UIGameMenu.gameTime"])
//
//        val gameTimeValue = myEngine.widgets.labels.bahnschrift_28(myEngine.userData.calcGameTime())
//        gameTimeValue.addAction(ActionCallbackEachAct {
//            gameTimeValue.txt = myEngine.userData.calcGameTime()
//        })
//
//        moneyAndTimeHint = Group().also {
//            it.addActor(positionsUi.setup(money, "money"))
//            it.addActor(positionsUi.setup(moneyValue, "moneyValue"))
//            it.addActor(positionsUi.setup(gameTime, "gameTime"))
//            it.addActor(positionsUi.setup(gameTimeValue, "gameTimeValue"))
//
//            MyActions.hintRightFadeIn(it)
//        }
//        addActor(positionsUi.setup(moneyAndTimeHint, "moneyAndTime"))
        myEngine.actorBuilder.build(FileStorage["ui/ButtonHintBackground.json"]).first().also {
            addActor(it)
        }
        myEngine.actorBuilder.build(FileStorage["ui/UiMoneyTimeHint.json"]).forEach {
            it as Group
            moneyAndTimeHint = it
            it.findActor<Label>("money").also { l -> l.txt = GuiText["money"] }
            it.findActor<Label>("money-value")
                .also { l -> l.txt = myEngine.userInventory.getGoldModel().capacity.toString() }
            it.findActor<Label>("game-time").also { l -> l.txt = GuiText["game-time"] }
            it.findActor<Label>("game-time-value").also { l ->
                addAction(ActionCallbackEachAct {
                    l.txt = myEngine.userData.calcGameTime()
                })
            }
            MyActions.hintRightFadeIn(it)
            addActor(it)
        }

//        val escapeButton = myEngine.guiImages.xBox.buttonB()
//        val confirmButton = myEngine.guiImages.xBox.buttonA()
//        val characterSelectButton = myEngine.guiImages.xBox.dpadDown()
//        val escape = myEngine.widgets.labels.bahnschrift_28_hitpoint(GuiText["exit"])
//        val confirm = myEngine.widgets.labels.bahnschrift_28_hitpoint(GuiText["apply"])
//        val characterSelect =
//            myEngine.widgets.labels.bahnschrift_28_hitpoint(GuiText["character-select"])
//
//        val buttons_hint_background = myEngine.guiImages.backgrounds.buttons_hint_background()
//        positionsUi.setup(buttons_hint_background, "buttonsHintBackground")

//        buttonsHint = Table().also {
//            it.left()
//            it.add(escapeButton).padLeft(150f)
//            it.add(escape)
//            it.add(confirmButton).padLeft(50f)
//            it.add(confirm)
//            it.add(characterSelectButton).padLeft(50f).padBottom(-10f)
//            it.add(characterSelect)
//        }
//        addActor(positionsUi.setup(buttons_hint_background, "buttonsHintBackground"))
//        addActor(positionsUi.setup(buttonsHint, "buttonsHint"))
        myEngine.actorBuilder.build(FileStorage["ui/UiGameMenuButtonHint-0.json"]).forEach {
            it as Group
            buttonsHint = it
            it.findActor<Label>("exit").also { l -> l.txt = GuiText["exit"] }
            it.findActor<Label>("apply").also { l -> l.txt = GuiText["apply"] }
            it.findActor<Label>("character-select").also { l -> l.txt = GuiText["character-select"] }
            addActor(it)
        }

        val imgButtons = myEngine.widgets.imageButtons
        listOf(
            positionsUi.setup(imgButtons.arrow_prev(), "${category}.imgButtons.arrow_prev"),
            positionsUi.setup(imgButtons.arrow_next(), "${category}.imgButtons.arrow_next"),
            positionsUi.setup(imgButtons.volume(), "${category}.imgButtons.volume"),
            positionsUi.setup(imgButtons.help(), "${category}.imgButtons.help"),
            positionsUi.setup(imgButtons.config(), "${category}.imgButtons.config"),
            positionsUi.setup(imgButtons.auto(), "${category}.imgButtons.auto"),
            positionsUi.setup(imgButtons.close(), "${category}.imgButtons.close"),
            positionsUi.setup(imgButtons.load(), "${category}.imgButtons.load"),
            positionsUi.setup(imgButtons.log(), "${category}.imgButtons.log"),
            positionsUi.setup(imgButtons.menu(), "${category}.imgButtons.menu"),
            positionsUi.setup(imgButtons.qload(), "${category}.imgButtons.qload"),
            positionsUi.setup(imgButtons.qsave(), "${category}.imgButtons.qsave"),
            positionsUi.setup(imgButtons.save(), "${category}.imgButtons.save"),
            positionsUi.setup(imgButtons.screen(), "${category}.imgButtons.screen"),
            positionsUi.setup(imgButtons.share(), "${category}.imgButtons.share"),
            positionsUi.setup(imgButtons.skip(), "${category}.imgButtons.skip"),
            positionsUi.setup(imgButtons.tips(), "${category}.imgButtons.tips"),
            positionsUi.setup(imgButtons.title(), "${category}.imgButtons.title"),
            positionsUi.setup(imgButtons.voice(), "${category}.imgButtons.voice"),
        ).forEach {
            addActor(it.actor)
            it.controller.onCancel = { myList.setKeyboardFocus() }
        }

        UIKeyController.updateKeyControllers(this)
    }

    private fun confirm() {
        when (listWidget.actor.selected) {
            GameText[KeyWord.Items.name] -> {
                touchable = Touchable.disabled
                clearListeners()
                charactersGroup.children.forEachIndexed { index, actor ->
                    if (index % 2 == 0) MyActions.characterCardTopFadeOut(actor)
                    else MyActions.characterCardBottomFadeOut(actor)
                }
                MyActions.hintRightFadeOut(moneyAndTimeHint).addAction(ActionCallback {
                    remove()
                    UIInventoryMenu(myEngine).open {
                        UIGameMenu(myEngine).also { it.isFirstOpen = false }.open(callback)
                    }
                })
            }
            GameText[KeyWord.System.name] -> UIVolumeSettings(myEngine).open { listWidget.actor.setKeyboardFocus() }
            GameText[KeyWord.Save.name] -> UISaveMenu(myEngine).open { listWidget.actor.setKeyboardFocus() }
            GameText[KeyWord.QuitInMainMenu.name] -> myEngine.restart()
        }
    }

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)

        myEngine.soundPlayer.play(SoundPath.DefaultOpenMenu)
        listWidget.actor.setKeyboardFocus()
        listWidget.actor.selectedIndex = 0

        if (isFirstOpen) MyActions.hintLeftFadeIn(buttonsHint)
    }
}