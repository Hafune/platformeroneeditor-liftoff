package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import ktx.actors.plus
import ktx.actors.setKeyboardFocus
import mygdx.game.actions.MyTimer
import mygdx.game.language.GameText
import mygdx.game.language.KeyWord
import mygdx.game.lib.MyActorMath
import mygdx.game.systems.MyEngine

class UIContinuesMenu(private val myEngine: MyEngine, private val uILoginScreenMenu: UILoginScreenMenu) :
    UIAbstractMenuPage(myEngine) {

    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()

    val users = myEngine.db.userService.getUsers()

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)
        listWidget.actor.setKeyboardFocus()

        listWidget.audio.mute = true
        if (users.isNotEmpty()) {
            listWidget.actor.setItems(*users.map { "${it.name}: ${it.id}" }.toTypedArray())
            listWidget.controller.onConfirm = {
                confirm()
            }
        } else {
            listWidget.actor.setItems(GameText[KeyWord.No_saves.name])
            listWidget.controller.onConfirm = null
        }
        listWidget.actor.pack()
        MyActorMath.toCenterOfParent(listWidget.actor)
        listWidget.audio.mute = false
    }

    private fun confirm() {
        touchable = Touchable.disabled
        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
        MyTimer(1f).apply {
            myEngine = this@UIContinuesMenu.myEngine
            callback = { load() }
            start()
        }
        hide()
        uILoginScreenMenu.hide()
    }

    private fun hide() = addAction(Actions.fadeOut(1f) + Actions.removeActor())

    private fun load() {
        myEngine.userDataLoader.load(users[listWidget.actor.selectedIndex])
    }

    init {
        addActor(listWidget.actor)
        listWidget.controller.onCancel = { this.close() }
    }
}