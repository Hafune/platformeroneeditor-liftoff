package mygdx.game.uiMenu

import com.badlogic.gdx.controllers.Controller
import com.badlogic.gdx.controllers.ControllerListener
import com.badlogic.gdx.controllers.Controllers
import mygdx.game.inputs.MyAxisButtons
import mygdx.game.inputs.MyPadButtons
import mygdx.game.inputs.configs.ControllerSetting
import mygdx.game.inputs.configs.PadData
import mygdx.game.lib.MyActorMath
import mygdx.game.systems.MyEngine
import kotlin.math.abs

class UIPadSettingMenu(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine), ControllerListener {

    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()

    private val setting = ControllerSetting()
    private var index = 0
    private val customizableButtons = listOf(
        MyPadButtons.UP,
        MyPadButtons.RIGHT,
        MyPadButtons.DOWN,
        MyPadButtons.LEFT,
        MyPadButtons.CROSS,
        MyPadButtons.CIRCLE,
        MyPadButtons.TRIANGLE,
        MyPadButtons.SQUARE,
        MyPadButtons.START,
        MyPadButtons.SELECT,
    )

    private var axisX = "Axis X"
    private var axisY = "Axis Y"

    init {
        addActor(listWidget.actor)
    }

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)

        listWidget.actor.setItems(customizableButtons[index].name)

        listWidget.audio.mute = true
        MyActorMath.toCenterOfParent(listWidget.actor)
        listWidget.audio.mute = false

        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
        Controllers.addListener(this)
    }

    private fun buttonDown(buttonCode: Int) {
        setting.buttonCodes[buttonCode] = customizableButtons[index]
        if (!nextItem()) {
            listWidget.actor.setItems(axisX)
        }
    }

    private fun nextItem(): Boolean {
        index++
        if (index < customizableButtons.size) {
            listWidget.actor.setItems(customizableButtons[index].name)
            return true
        }
        return false
    }

    override fun buttonDown(controller: Controller, buttonCode: Int): Boolean {
        if (index >= customizableButtons.size) {
            return true
        }
        checkName(controller) ?: return false
        buttonDown(buttonCode)
        return true
    }

    private fun checkName(controller: Controller): Boolean? {
        if (setting.controllerName == setting.default) {
            setting.controllerName = controller.name
            addActor(
                myEngine.widgets.labels.default(controller.name).apply {
                x += 100
                y += 100
            })
        }
        if (setting.controllerName == controller.name) {
            return true
        }
        return null
    }

    override fun connected(controller: Controller) {

    }

    override fun disconnected(controller: Controller) {

    }

    override fun buttonUp(controller: Controller, buttonCode: Int): Boolean {
        return true
    }

    override fun axisMoved(controller: Controller, axisCode: Int, value: Float): Boolean {
        checkName(controller) ?: return false
        val v = abs(value)
        if (v.toInt() != 1) {
            return false
        }
        if (listWidget.actor.selected == MyPadButtons.UP.name || listWidget.actor.selected == MyPadButtons.DOWN.name) {
            nextItem()
            return true
        }
        if (listWidget.actor.selected == MyPadButtons.LEFT.name || listWidget.actor.selected == MyPadButtons.RIGHT.name) {
            nextItem()
            return true
        }
        if (listWidget.actor.selected == axisX) {
            setting.addAxis(axisCode, 1, MyAxisButtons.RIGHT)
            setting.addAxis(axisCode, -1, MyAxisButtons.LEFT)
            listWidget.actor.setItems(axisY)
            return true
        }
        if (listWidget.actor.selected == axisY) {
            setting.addAxis(axisCode, 1, MyAxisButtons.DOWN)
            setting.addAxis(axisCode, -1, MyAxisButtons.UP)
            Controllers.removeListener(this)
            PadData().apply {
                exportControllerSettings(setting)
                myEngine.myInputHandler.changeStage(myEngine.screenContainer.stage)// = MyInputHandler(myEngine.screenContainer.stage)
            }
            close()
            return true
        }
        return false
    }
}