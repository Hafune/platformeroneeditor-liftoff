package mygdx.game.uiMenu.fight;

import mygdx.game.systems.MyEngine;
import mygdx.game.uiMenu.UIAbstractMenuPage;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.inputs.IMyButtonTarget;
import mygdx.game.inputs.MyPadButtons;
import org.jetbrains.annotations.NotNull;

public abstract class UIPauseMenu extends UIAbstractMenuPage implements IMyButtonTarget
{
//    private final MyTextButton textButton;

    public UserControllerComponent controllerComponent;

    public UIPauseMenu(@NotNull MyEngine myEngine) {
        super(myEngine);
    }

//    private UIPauseMenu()
//    {
//        textButton = new MyTextButton("Pause", ScreenContainer.Companion.getGameSkin(), TextButtonsStyles.game_menu);
//        addActor(textButton);
//    }

//    @Override
//    public void init()
//    {
//        textButton.setSize(textButton.getMinWidth(), textButton.getMinHeight());
//        MyActorMath.INSTANCE.toCenterOfParent(textButton);

//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(this);
//    }

    @Override
    public void buttonDown(MyPadButtons button)
    {
        controllerComponent.buttonDown(button);
        if (button == MyPadButtons.START)
        {
            controllerComponent = null;
            close();
        }
    }

    @Override
    public void buttonUp(MyPadButtons button)
    {
        controllerComponent.buttonUp(button);
    }
}
