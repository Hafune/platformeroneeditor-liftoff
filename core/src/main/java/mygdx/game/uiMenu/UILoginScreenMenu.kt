package mygdx.game.uiMenu

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Interpolation.SwingOut
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.Align
import ktx.actors.plus
import ktx.actors.setKeyboardFocus
import mygdx.game.audio.MusicPath
import mygdx.game.audio.SoundPath
import mygdx.game.language.GameText
import mygdx.game.lib.ActionTimer
import mygdx.game.lib.MyActorMath
import mygdx.game.myWidgets.MyImage
import mygdx.game.systems.MyEngine

open class UILoginScreenMenu(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {

    private val myImage = MyImage("backgrounds/MainScreen.png")

    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()

    private val begin = GameText["loginScreen.begin"]
    private val resume = GameText["loginScreen.resume"]
    private val setting = GameText["loginScreen.setting"]

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)
        myImage.setAlpha(0f)
        myEngine.musicPlayer.play(MusicPath.Opening2)

        callback()
        myImage.addAction(Actions.fadeIn(.2f))
        MyActorMath.toCenterOfParent(myImage)

        val list = listWidget.actor
        MyActorMath.toCenterOfParent(list)

        list.selectedIndex = 0
        list.addAction(Actions.alpha(0f))
        list.addAction(Actions.fadeIn(.5f, Interpolation.circle))
        list.addAction(Actions.moveBy(0f, -120f, 1.5f, SwingOut(1.5f)))
        myImage.y -= 120f
        myImage.addAction(Actions.moveBy(0f, 120f, 1.5f, SwingOut(1.5f)))

        listWidget.audio.mute = false
    }

    private fun callback() {
        listWidget.actor.setKeyboardFocus()
    }

    private fun confirm() {
        when (listWidget.actor.selected) {
            begin -> {
                touchable = Touchable.disabled
                myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
                addAction(Actions.sequence(Actions.fadeOut(1f), ActionTimer {
                    startNewGame()
                }))
            }
            resume -> {
                UIContinuesMenu(myEngine, this).open { callback() }
            }
            setting -> {
//                UIPadSettingMenu(myEngine).open {
//                    callback()
//                }
            }
        }
    }

    private fun startNewGame() {
        remove()
        myEngine.userDataLoader.start()
    }

    init {
        addActor(myImage)

        val myList = listWidget.actor
        listWidget.audio.mute = true

        myList.setItems(
            begin,
            resume,
//            setting,
        )
        myList.setAlignment(Align.left)

        addActor(myList)
        myList.pack()
        listWidget.audio.soundConfirm = SoundPath.Decision1
        listWidget.controller.onConfirm = { confirm() }
    }

    fun hide() = addAction(Actions.fadeOut(1f) + Actions.removeActor())
}
