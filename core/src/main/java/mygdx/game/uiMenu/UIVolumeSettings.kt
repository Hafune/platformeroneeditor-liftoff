package mygdx.game.uiMenu

import com.badlogic.gdx.utils.Align
import ktx.actors.onChange
import ktx.actors.setKeyboardFocus
import mygdx.game.language.GameText
import mygdx.game.lib.MyActorMath
import mygdx.game.systems.MyEngine

class UIVolumeSettings(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {

    private val volumeLabel = myEngine.widgets.labels.bahnschrift_32(GameText["music"])
    private val volumeValue = myEngine.widgets.labels.bahnschrift_32("100")
    private val volumeSliderWidget = myEngine.widgets.sliders.default_horizontal(0f, 100f, 1f)
    private val volumeSlider = volumeSliderWidget.actor

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)

        volumeSlider.value = myEngine.audioProperties.model.volume * 100
        volumeValue.setText(volumeSlider.value.toInt().toString())
        volumeSlider.setKeyboardFocus()
    }

    init {
        val background = myEngine.guiImages.backgrounds.game_menu_background()
        addActor(background)
        MyActorMath.fillParent(background)

        volumeValue.setAlignment(Align.right)

        val category = "UIVolumeSettings"
        val positionsUi = myEngine.positionsUiStorage["$category.json"]
        addActor(positionsUi.setup(volumeLabel, "volumeLabel"))
        addActor(positionsUi.setup(volumeValue, "volumeValue"))
        addActor(positionsUi.setup(volumeSlider, "volumeSlider"))

        volumeSlider.onChange {
            volumeValue.setText(volumeSlider.value.toInt().toString())
            myEngine.audioProperties.model.volume = volumeSlider.value / 100
        }

        volumeSliderWidget.controller.onCancel = { close() }
    }
}