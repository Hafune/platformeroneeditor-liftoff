package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.badlogic.gdx.utils.Align
import ktx.actors.centerPosition
import ktx.actors.setKeyboardFocus
import mygdx.game.language.GameText
import mygdx.game.language.KeyWord
import mygdx.game.systems.MyEngine
import mygdx.game.uiSkins.UiSkins

class ConfirmOption(
    vararg text: String,
    myEngine: MyEngine,
    var onCancel: (() -> Unit)? = null,
    var onConfirm: (() -> Unit)? = null
) :
    UIAbstractMenuPage(myEngine) {

    //    private val audio = MyAudio()
    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()

    private val background = Label(null, UiSkins.skin)

    init {
        open()

        val vContainer = VerticalGroup()
        text.forEach { vContainer.addActor(myEngine.widgets.labels.bahnschrift_32(it)) }

        val myList = listWidget.actor
        myList.setItems(GameText[KeyWord.Yes.name], "        ${GameText[KeyWord.No.name]}        ")
        myList.setAlignment(Align.center)
        listWidget.controller.onConfirm = {
            if (myList.selectedIndex == 0) {
                onConfirm?.invoke()
            } else {
                onCancel?.invoke()
            }
        }
        listWidget.controller.onCancel = {
            onCancel?.invoke()
        }

        vContainer.addActor(myList)

        addActor(background)
        addActor(vContainer)
        vContainer.centerPosition()
        vContainer.y += vContainer.prefHeight / 2
        background.width = vContainer.prefWidth + 100
        background.height = vContainer.prefHeight + 100
        background.centerPosition()

        myList.setKeyboardFocus()
    }
}

