package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import ktx.actors.centerPosition
import mygdx.game.systems.MyEngine
import mygdx.game.uiSkins.UiSkins

class Notification(
    val text: List<String>,
    myEngine: MyEngine,
    var onConfirm: (() -> Unit)? = null,
) :
    UIAbstractMenuPage(myEngine) {

    private val background = Label(null, UiSkins.skin)

    init {
        open()

        val vContainer = VerticalGroup()
        val okButton = myEngine.widgets.textButtons.default("Ok")

        okButton.controller.onConfirm = {
            close()
            onConfirm?.invoke()
        }

        okButton.controller.onCancel = {
            close()
            onConfirm?.invoke()
        }

        text.forEach {
            vContainer.addActor(myEngine.widgets.labels.bahnschrift_32(it))
        }

        addActor(background)
        addActor(vContainer)
        addActor(okButton.actor)

        vContainer.centerPosition()
        vContainer.y += vContainer.prefHeight / 2
        background.width = vContainer.prefWidth + 100
        background.height = vContainer.prefHeight + 100
        background.centerPosition()
        okButton.actor.centerPosition()
        okButton.actor.y -= background.height / 3f
    }
}

