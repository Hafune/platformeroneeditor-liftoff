package mygdx.game.uiMenu

import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.utils.Align
import ktx.actors.centerPosition
import ktx.actors.onClick
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.inputs.MyPadButtons
import mygdx.game.myWidgets.IntSpinner
import mygdx.game.myWidgets.propertyes.MyAudio
import mygdx.game.systems.MyEngine

class CountOption(
    maxCount: Int,
    myEngine: MyEngine,
    var onChange: (() -> Unit)? = null,
    var onCancel: (() -> Unit)? = null,
    var onConfirm: ((count: Int) -> Unit)? = null
) :
    UIAbstractMenuPage(myEngine), IMyButtonTarget {

    private val buttonLeftWidget = myEngine.widgets.imageButtons.arrow_prev()
    private val buttonRightWidget = myEngine.widgets.imageButtons.arrow_next()

    private val label = myEngine.widgets.labels.bahnschrift_32("1")
    private val spinner = IntSpinner(1, 1, maxCount, 1)
    private val audio = MyAudio(myEngine)

    init {
        buttonLeftWidget.actor.onClick {
            buttonDown(MyPadButtons.LEFT)
        }
        buttonRightWidget.actor.onClick {
            buttonDown(MyPadButtons.RIGHT)
        }

        val container = HorizontalGroup()
        open()
        container.addActor(buttonLeftWidget.actor)
        container.addActor(Container(label).also {
            it.width(30f)
        })
        container.addActor(buttonRightWidget.actor)
        addActor(container)

        label.setAlignment(Align.center)
        container.centerPosition()

        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = this
    }

    override fun buttonDown(button: MyPadButtons) {
        fun calc(i: Int) {
            audio.playChange()
            spinner.value += i
            label.setText(spinner.value)
        }
        when (button) {
            MyPadButtons.UP -> {
                calc(1)
            }
            MyPadButtons.DOWN -> {
                calc(-1)
            }
            MyPadButtons.LEFT -> {
                calc(-1)
            }
            MyPadButtons.RIGHT -> {
                calc(1)
            }
            MyPadButtons.CROSS -> {
                audio.playConfirm()
                onConfirm?.invoke(spinner.value)
            }
            MyPadButtons.CIRCLE -> {
                audio.playChange()
                onCancel ?: close()
                onCancel?.invoke()
            }
            else -> {
            }
        }
    }
}

