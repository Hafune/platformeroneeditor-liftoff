package mygdx.game.uiMenu.store

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import mygdx.game.lib.MyActorMath
import mygdx.game.myWidgets.HorizontalFlexGroup
import mygdx.game.myWidgets.StoreBuyItemLabel
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.ConfirmOption
import mygdx.game.uiMenu.CountOption
import mygdx.game.uiMenu.UIAbstractMenuPage

class StoreSell(
    private val myEngine: MyEngine,
) : UIAbstractMenuPage(myEngine) {

    private val userInventory = myEngine.userInventory
    private val myItemScrollPane0 = myEngine.widgets.myItemScrollPanes.default<StoreBuyItemLabel>().actor

    private val gold = myEngine.widgets.labels.default()
    private val goldItem = userInventory.items.first { it.id == 3 }

    private val categories = myEngine.db.itemCategoriesService.getCategoriesForStores()
    private var index = 0

    private fun refreshViewStoreInfo() {
        myItemScrollPane0.clearItems()
        userInventory.items.forEach {
            if (it.category_key == categories[index].key) {
                val item = StoreBuyItemLabel(myEngine, myEngine.iconStorage, it, it.capacity)
                myItemScrollPane0.addItem(item)
            }
        }

        gold.setText(goldItem.capacity)
        gold.x = width - gold.width
        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = myItemScrollPane0
    }

    init {
        myItemScrollPane0.onDownRight = {
            index++
            if (index >= categories.size) index = 0
            refreshViewStoreInfo()
        }
        myItemScrollPane0.onDownLeft = {
            index--
            if (index < 0) index = categories.size - 1
            refreshViewStoreInfo()
        }

        myItemScrollPane0.onSelect = { itemLabel ->
            val item = itemLabel.itemModel
            if (item.capacity > 0) {
                val countOption = CountOption(item.capacity, myEngine)
                countOption.onCancel = {
                    countOption.close()
                    myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = myItemScrollPane0
                }

                countOption.onConfirm = { count ->
                    val confirmOption = ConfirmOption(item.name, "x$count", "Продать?", myEngine = myEngine)
                    confirmOption.onCancel = {
                        confirmOption.close()
                        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = countOption
                    }

                    confirmOption.onConfirm = {
                        item.capacity -= count
                        if (item.capacity == 0) {
                            userInventory.items.remove(item)
                        }
                        goldItem.capacity += count * item.price
                        countOption.close()
                        confirmOption.close()
                        refreshViewStoreInfo()
                    }
                }
            }
        }
        myItemScrollPane0.onCancel = {
            close()
        }

        open()

        myItemScrollPane0.x = parent.height * .1f
        myItemScrollPane0.width = width / 20 * 11
        myItemScrollPane0.height = parent.height * .4f
        MyActorMath.toCenterOfActorY(myItemScrollPane0, this)

        gold.y = myItemScrollPane0.top
        gold.setAlignment(Align.right)
        gold.setSize(width / 2, 50f)

        val hGroup = HorizontalFlexGroup()
        categories.forEachIndexed { idx, category ->
            val icon = myEngine.iconStorage.buildById(category.icon_id)
            val b = ImageButton(TextureRegionDrawable(icon.region), TextureRegionDrawable(icon.region))
            b.onClick {
                index = idx
                refreshViewStoreInfo()
            }
            hGroup.addActor(b)
        }

        addActor(myItemScrollPane0)
        addActor(gold)
        addActor(hGroup)

        refreshViewStoreInfo()

        hGroup.width = myItemScrollPane0.width
        hGroup.x = myItemScrollPane0.x
        hGroup.y = myItemScrollPane0.top
    }
}