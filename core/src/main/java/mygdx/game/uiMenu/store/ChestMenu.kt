package mygdx.game.uiMenu.store

import mygdx.game.dataBase.services.itemsService.ItemModel
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.Notification
import mygdx.game.uiMenu.UIAbstractMenuPage

class ChestMenu(
    items: Iterable<ItemModel>,
    myEngine: MyEngine,
    callback: () -> Unit
) : UIAbstractMenuPage(myEngine) {

    private val userInventory = myEngine.userInventory

    init {
        Notification(items.map { "${it.name}x${it.capacity}" }, myEngine) {
            callback()
        }
        items.forEach { item ->
            val userItem =
                userInventory.items.firstOrNull { it::class == item::class } ?: item

            if (!userInventory.items.contains(userItem)) {
                userInventory.items.add(userItem)
            } else {
                userItem.capacity += item.capacity
            }
        }
    }
}