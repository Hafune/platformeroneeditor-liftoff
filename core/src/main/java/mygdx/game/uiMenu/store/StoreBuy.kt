package mygdx.game.uiMenu.store

import com.badlogic.gdx.utils.Align
import ktx.actors.setKeyboardFocus
import mygdx.game.dataBase.services.itemsService.ItemModel
import mygdx.game.lib.MyActorMath
import mygdx.game.myWidgets.StoreBuyItemLabel
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.ConfirmOption
import mygdx.game.uiMenu.CountOption
import mygdx.game.uiMenu.UIAbstractMenuPage
import kotlin.math.min

class StoreBuy(
    private val items: Iterable<ItemModel>,
    private val myEngine: MyEngine,
) : UIAbstractMenuPage(myEngine) {

    private val userInventory = myEngine.userInventory
    private val myItemScrollPane0 = myEngine.widgets.myItemScrollPanes.default<StoreBuyItemLabel>().actor

    private val gold = myEngine.widgets.labels.default()
    private val goldItem = userInventory.getGoldModel()

    private fun refreshViewStoreInfo() {
        myItemScrollPane0.clearItems()
        items.forEach {
            val item = StoreBuyItemLabel(myEngine, myEngine.iconStorage, it, getCapacity(it))
            myItemScrollPane0.addItem(item)
        }

        gold.setText(goldItem.capacity)
        gold.x = width - gold.width
        myItemScrollPane0.setKeyboardFocus()
    }

    private fun getCapacity(itemModel: ItemModel): Int {
        return userInventory.items.firstOrNull { i ->
            i::class == itemModel::class
        }?.capacity ?: 0
    }

    init {
        myItemScrollPane0.onSelect = { itemLabel ->
            val maxBuy = goldItem.capacity / itemLabel.itemModel.price
            val canBuy = min(15 - getCapacity(itemLabel.itemModel), maxBuy)
            if (canBuy >= 0) {
                val countOption = CountOption(canBuy, myEngine)
                countOption.onCancel = {
                    countOption.close()
                    myItemScrollPane0.setKeyboardFocus()
                }

                countOption.onConfirm = { count ->
                    val confirmOption = ConfirmOption(itemLabel.itemModel.name, "x$count", "Купить?", myEngine = myEngine)
                    confirmOption.onCancel = {
                        confirmOption.close()
                        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = countOption
                    }

                    confirmOption.onConfirm = {
                        val userItem =
                            userInventory.items.firstOrNull { it::class == itemLabel.itemModel::class } ?: itemLabel.itemModel
                        goldItem.capacity -= count * itemLabel.itemModel.price

                        userItem.capacity += count
                        if (!userInventory.items.contains(userItem)) {
                            userInventory.items.add(userItem)
                        }
                        countOption.close()
                        confirmOption.close()
                        refreshViewStoreInfo()
                    }
                }
            }
        }
        myItemScrollPane0.onCancel = {
            close()
        }

        myItemScrollPane0.x = parent.height * .1f
        myItemScrollPane0.width = width / 20 * 11
        myItemScrollPane0.height = parent.height * .4f
        MyActorMath.toCenterOfActorY(myItemScrollPane0, this)

        gold.y = myItemScrollPane0.top
        gold.setAlignment(Align.right)
        gold.setSize(width / 2, 50f)
        addActor(myItemScrollPane0)
        addActor(gold)

        refreshViewStoreInfo()
    }
}