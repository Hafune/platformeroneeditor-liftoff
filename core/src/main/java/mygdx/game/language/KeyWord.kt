package mygdx.game.language

enum class KeyWord {
    Yes, No, Sell, Buy, Cancel, Drop, PickUp, Items, Skills, Equipment, Class, Status, Formation, Save, System, DescriptionHealthPotion, No_saves, RandomTraderPhrase010, RandomTraderPhrase020, QuitInMainMenu
}