package mygdx.game.language

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.I18NBundle
import mygdx.game.MyLog

object GuiText {

    private val bundle: I18NBundle = I18NBundle.createBundle(Gdx.files.internal("i18n/gui/i18n"))

    operator fun get(key: String): String {
        try {
            return bundle[key]
        } catch (e: RuntimeException) {
            MyLog.print("GuiText: $key")
        }
        return key.split(".").last()
    }
}