package mygdx.game

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.kotcrab.vis.ui.VisUI
import ktx.actors.onKeyDownEvent
import ktx.async.KtxAsync
import mygdx.game.assetsClasses.GraphicResources
import mygdx.game.characterSkins.SkinStorage
import mygdx.game.dataBase.DatabaseServices
import mygdx.game.editor.ActorSetting
import mygdx.game.editor.EditorRoot
import mygdx.game.icons.IconStorage
import mygdx.game.inputs.MyInputHandler
import mygdx.game.lib.EditorPositionsUiFileHandler
import mygdx.game.lib.MyClassForName
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.UIAbstractMenuPage
import mygdx.game.uiMenu.UIGameMenu
import mygdx.game.uiMenu.UILoginScreenMenu
import org.jetbrains.exposed.sql.Database
import java.lang.Float.min
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime


class Main : ApplicationAdapter() {
    private var assetsIsLoaded = false

    private lateinit var database: Database
    private lateinit var services: DatabaseServices
    private lateinit var graphicResources: GraphicResources
    private lateinit var skinStorage: SkinStorage
    private lateinit var iconStorage: IconStorage

    private lateinit var loadingScreen: LoadingScreen
    private lateinit var myEngine: MyEngine
    private lateinit var inputHandler: MyInputHandler
    private var firstMenu = UILoginScreenMenu::class.qualifiedName!!

    override fun create() {
        test()

        val start = System.currentTimeMillis()
        KtxAsync.initiate()
        VisUI.load()

        database = Database.connect(
            url = "jdbc:sqlite:${Gdx.files.localStoragePath}sqlite.db",
            driver = "org.sqlite.JDBC",
        )
        services = DatabaseServices(database)
        graphicResources = GraphicResources()
        skinStorage = SkinStorage(services)
        iconStorage = IconStorage(services)

        loadingScreen = LoadingScreen()

        val data = EditorPositionsUiFileHandler.findSettingOrCreate(ScreenContainer.windowSettingName)

        if (data.width == 0f) {
            data.width = ScreenContainer.appScreenWidth
            data.height = ScreenContainer.appScreenHeight
        }
        Gdx.graphics.setWindowedMode(data.width.toInt(), data.height.toInt())

        MyLog["Время create() загрузки ${System.currentTimeMillis() - start}"]
    }

    private fun test() {
        MyEnvironment.GRAPHIC_RESOURCES_LOAD_ASSETS = false
        MyEnvironment.EDITOR_ENABLE = false

        firstMenu = UIGameMenu::class.qualifiedName!!
    }

    private fun test2(menu: UIAbstractMenuPage) {
        menu.onKeyDownEvent { _, keyCode ->
            if (keyCode == Keys.R) {
                menu.remove()
                val n = MyClassForName.get<UIAbstractMenuPage>(firstMenu, myEngine).apply { open() }
                test2(n)
            }
        }
    }

    fun restart() {
        if (!::inputHandler.isInitialized) inputHandler = MyInputHandler()
        myEngine = MyEngine(
            inputHandler = inputHandler,
            db = services,
            iconStorage = iconStorage,
            skinStorage = skinStorage,
            graphicResources = graphicResources
        ) { restart() }
        ActorSetting.myEngine = myEngine

        if (MyEnvironment.EDITOR_ENABLE) EditorRoot(myEngine)

        val menu = MyClassForName.get<UIAbstractMenuPage>(firstMenu, myEngine).apply { open() }
        test2(menu)
    }

    @OptIn(ExperimentalTime::class)
    override fun render() {
        if (!assetsIsLoaded) {
            loadingScreen.draw(graphicResources.loadingPercent())
            if (graphicResources.loadingPercent() == 1f) {
                assetsIsLoaded = true
                restart()
            }
            return
        }
        if (myEngine.loading) {
            loadingScreen.draw(.5f)
            return
        }

        myEngine.screenContainer.elapsedTime = measureTime {
            myEngine.update(min(Gdx.graphics.deltaTime, 1 / 30f))
            myEngine.screenContainer.bufferDraw()
            myEngine.screenContainer.bufferStageActStageDraw()
            myEngine.myBox2dHandler.debugRendererUpdate()
        }.inWholeMicroseconds
    }

    override fun resize(width: Int, height: Int) {
        if (!::myEngine.isInitialized) return

        myEngine.screenContainer.stage.viewport.update(width, height)

        val data = EditorPositionsUiFileHandler.findSettingOrCreate(ScreenContainer.windowSettingName)
        data.width = width.toFloat()
        data.height = height.toFloat()

        with(myEngine.screenContainer.stage.viewport)
        {
            myEngine.screenContainer.tileMapRender.lightMap.useCustomViewport(
                screenX,
                screenY,
                screenWidth,
                screenHeight
            )
        }
    }

    override fun pause() {
//		dispose ();
//		Gdx.app.exit();
    }

    override fun resume() {}
    override fun dispose() {
        EditorPositionsUiFileHandler.exportAll()
        myEngine.screenContainer.dispose()
    }
}