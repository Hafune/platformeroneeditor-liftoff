package mygdx.game.assetsClasses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public enum MyShader {
    screenCloseOpen("screenCloseOpen"),
    fade("fade"),
    hitEffect("hitEffect");


    private final ShaderProgram shaderProgram;

    MyShader(String fragShaderName) {
        String path = "shader/";
        shaderProgram = new ShaderProgram(Gdx.files.internal(path + "shad.vert"), Gdx.files.internal("shader/screenCloseOpen.frag"));
    }

    public ShaderProgram get() {
        return shaderProgram;
    }
}
