package mygdx.game.assetsClasses

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import kotlinx.coroutines.launch
import ktx.assets.async.AssetStorage
import ktx.async.KtxAsync
import ktx.collections.component1
import ktx.collections.component2
import mygdx.game.MyEnvironment
import mygdx.game.MyLog
import mygdx.game.editor.tileMapMenu.DrawLevels
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper
import mygdx.game.lib.MyProperties
import mygdx.game.lib.TextureStorage
import mygdx.game.tileSets.DrawItemEraser
import mygdx.game.tileSets.dynamicTiles.*
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import kotlin.reflect.KClass

class GraphicResources {

    private val map = HashMap<String, ArrayList<Map<String, IDrawItem>>>()
    private val props = MyProperties("img/tilesets")
    private val assetStorage = AssetStorage()
    private var totalAssets = 1
    private var loaded = 0f

    private val endWithTemplateTypes = HashMap<String, KClass<*>>().also {
        it["A1"] = TilesetTemplateA1::class
        it["A2"] = TilesetTemplateA2::class
        it["A3"] = TilesetTemplateA3::class
        it["A4"] = TilesetTemplateA4::class
        it["A5"] = TilesetTemplateA5::class
        it["B"] = TilesetTemplateTwoHalfSolo::class
        it["C"] = TilesetTemplateTwoHalfSolo::class
        it["D"] = TilesetTemplateTwoHalfSolo::class
        it["E"] = TilesetTemplateTwoHalfSolo::class
    }
    private val startWithTemplateTypes = HashMap<String, KClass<*>>().also {
        it["!"] = TilesetTemplateChest::class
    }

    @Suppress("UNCHECKED_CAST")
    fun getDrawItem(values: String): IDrawItem {
        if (values == "") return DrawItemEraser
        val map = MyObjectMapper.readValue(values, HashMap::class.java) as HashMap<String, Any>
        val tileset = getTilesetFromPath(map[IDrawItem::imagePath.name] as String)
        return tileset[map[IDrawItem::indexFromSet.name] as Int][map[IDrawItem::tileName.name]]!!
    }

    private fun exportDrawItemParam(drawItem: IDrawItem): String {
        if (drawItem == DrawItemEraser) return ""
        val map = HashMap<String, Any>()
        map[IDrawItem::imagePath.name] = drawItem.imagePath
        map[IDrawItem::indexFromSet.name] = drawItem.indexFromSet
        map[IDrawItem::tileName.name] = drawItem.tileName
        return MyObjectMapper.writeValueAsString(map)
    }

    private fun getTilesetFromPath(key: String): ArrayList<Map<String, IDrawItem>> {
        val file = Gdx.files.local(props[key])
        println(file.path())
        return map[file.path()]!!
    }

    private fun asyncInitializeTileset(
        texture: Texture,
        path: String,
        name: String,
        key: String,
    ): ArrayList<Map<String, IDrawItem>>? {

        val endsType = endWithTemplateTypes.keys.find { t -> name.endsWith(t) }
        val startsType = startWithTemplateTypes.keys.find { t -> name.startsWith(t) }

        var type: ITileSet? = null
        if (endsType != null) {
            type = MyClassForName.get(endWithTemplateTypes[endsType]!!)
        } else if (startsType != null) {
            type = MyClassForName.get(startWithTemplateTypes[startsType]!!)
        }

        type?.run {
            if (map[path] == null) {
                val props = MyProperties(path.split(".").first())
                type.initialize(key, TextureRegion(texture), props)
                map[path] = type.set

                if (props.containsKey("all")) {
                    type.set.forEach { it["0"]!!.drawLevel = DrawLevels.valueOf(props["all"]!!) }
                }
                props.forEach { (key, value) ->
                    if (key.toIntOrNull() != null) {
                        try {
                            type.set[key.toInt()]["0"]!!.drawLevel = DrawLevels.valueOf(value!!)
                        } catch (e: RuntimeException) {

                        }
                    }
                }
            }
            return map[path]!!
        }
        return null
    }

    val tileSetPages = ArrayList<ArrayList<IDrawItem>>().also { it.add(ArrayList()) }

    init {
        if (MyEnvironment.GRAPHIC_RESOURCES_LOAD_ASSETS) loadAssets()
    }

    private fun loadAssets() {
        KtxAsync.launch {
            assetStorage.apply {
                val textures = arrayListOf<Texture>()
                val values = props.map { it.value }
                totalAssets += values.size
                for (i in values.indices) {
                    textures.add(load(values[i]))
                    loaded++
                }
                listOf("characterSkins/shadow.png").forEach {
                    TextureStorage[it]
                }

                // Assets are loaded and we already have references to all of them:
                goToNextView(textures)
                loaded++
            }
        }
    }

    private fun goToNextView(textures: List<Texture>) {
        props.forEachIndexed { index, prop ->
            val set = asyncInitializeTileset(
                textures[index],
                path = prop.value,
                name = prop.value.split("/").last().split(".").first(),
                key = prop.key
            )
            set?.run { addITileset(set) }
        }
        MyLog[tileSetPages.size]
    }

    private fun addITileset(set: ArrayList<Map<String, IDrawItem>>) {
        val list = tileSetPages[tileSetPages.size - 1]
        for (map in set) {
            map.values.forEach { drawItem ->
                list.add(drawItem)
                drawItem.value = exportDrawItemParam(drawItem)
            }
        }
        if (list.size > 120) {
            tileSetPages.add(ArrayList())
        }
    }

    fun loadingPercent() = if (MyEnvironment.GRAPHIC_RESOURCES_LOAD_ASSETS) loaded / totalAssets else 1f
}