package mygdx.game

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.UndoRedo
import mygdx.game.tileSets.TileFriendlyData
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

open class TileContainer(val worldWidth: Int, val worldHeight: Int, val worldLayers: Int, private val engine: Engine) {

    private var entityListener: EntityListener

    val tiles = mutableListOf<Entity>()

    var worldCells: Array<Array<Array<Entity?>>> =
        Array(worldWidth + 1) { Array(worldHeight + 1) { Array(worldLayers + 1) { null } } }

    private fun checkWorldEdges(x: Int, y: Int, z: Int): Boolean {
        return x in 0 until worldWidth && y >= 0 && y < worldHeight && z >= 0 && z < worldLayers
    }

    open fun addEntityTile(entity: Entity) {
        val mapPosition = entity.get<MapPositionComponent>()!!
        with(mapPosition) {
            if (checkWorldEdges(worldX, worldY, worldZ)
            ) {
                val oldEntity = worldCells[worldX][worldY][worldZ]
                if (oldEntity != null && oldEntity != entity) {
                    engine.removeEntity(oldEntity)
                }
                worldCells[worldX][worldY][worldZ] = entity
            }
        }
    }

    open fun removeEntityTile(entity: Entity) {
        val mapPosition = entity.get<MapPositionComponent>()!!
        with(mapPosition) {
            if (checkWorldEdges(worldX, worldY, worldZ)
            ) {
                worldCells[worldX][worldY][worldZ] = null
            }
        }
    }

    fun dispose() {
        for (i in tiles.size - 1 downTo 0) {
            engine.removeEntity(tiles[i])
        }
        engine.removeEntityListener(entityListener)
    }

    fun switchTopPlace(x: Int, y: Int, width: Int, height: Int) {
        for (cx in x..x + width) {
            for (cy in y..y + height) {
                switchTopCell(cx, cy)
            }
        }
    }

    private fun switchTopCell(x: Int, y: Int) {
        val zTop = findTileTopLvl(x, y) ?: return
        val zSub = findTileTopLvl(x, y, zTop - 1) ?: return

        val top = getWorldCellIfExist(x, y, zTop)
        val sub = getWorldCellIfExist(x, y, zSub)

        if (top != null && sub != null) {
            val slot = worldCells[x][y]
            slot[zTop] = sub
            slot[zSub] = top
            top.get<MapPositionComponent>()!!.worldZ = zSub
            sub.get<MapPositionComponent>()!!.worldZ = zTop

            UndoRedo.addAction({
                slot[zTop] = sub
                slot[zSub] = top
                top.get<MapPositionComponent>()!!.worldZ = zSub
                sub.get<MapPositionComponent>()!!.worldZ = zTop
            }, {
                slot[zTop] = top
                slot[zSub] = sub
                top.get<MapPositionComponent>()!!.worldZ = zTop
                sub.get<MapPositionComponent>()!!.worldZ = zSub
            })
        }
    }

    private fun findTileTopLvl(x: Int, y: Int, startZ: Int = worldLayers - 1): Int? {
        for (z in startZ downTo 0) {
            if (getWorldCellIfExist(x, y, z) != null) {
                return z
            }
        }
        return null
    }

    fun getWorldCellIfExist(x: Int, y: Int, z: Int): Entity? {
        return if (!checkWorldEdges(x, y, z)) null
        else worldCells[x][y][z]
    }

    fun findDrawItem(x: Int, y: Int, drawItem: IDrawItem): Entity? {
        for (z in 0 until worldLayers) {
            val entity = getWorldCellIfExist(x, y, z)
            if (entity != null) {
                val item = entity.get<TileComponent>()!!.drawItem
                if (item == drawItem) {
                    return entity
                }
            }
        }
        return null
    }

    fun getFreeTopLvl(x: Int, y: Int): Int {
        for (z in worldLayers - 1 downTo 0) {
            if (getWorldCellIfExist(x, y, z) != null) {
                return z + 1
            }
        }
        return 0
    }

    fun forEachFloorToTopLvl(x: Int, y: Int, startZLvl: Int = 0, callback: (entity: Entity, z: Int) -> Unit) {
        for (z in startZLvl until worldLayers) {
            val entity = getWorldCellIfExist(x, y, z)
            if (entity != null) {
                callback(entity, z)
            }
        }
    }

    fun getTileFriendlyDataArray(x0: Int, x1: Int, y0: Int, y1: Int): Array<Array<Array<String?>>> {
        val arr = Array(3) { Array(3) { Array<String?>(worldLayers) { null } } }
        for (xx in x0..x1) {
            for (yy in y0..y1) {
                for (zz in 0 until worldLayers) {
                    val entity = getWorldCellIfExist(xx, yy, zz)
                    if (entity != null) {
                        arr[xx - x0][yy - y0][zz] = entity.get<TileComponent>()!!.getTileFriendlyValue()
                    }
                }
            }
        }
        return arr
    }

    fun applyTileFriendlyDataArray(x: Int, y: Int, arrAfter: Array<Array<Array<String?>>>) {
        for (xx in x until x + arrAfter.size) {
            for (yy in y until y + arrAfter[0].size) {
                for (zz in 0 until TileMapRender.WORLD_LAYERS) {
                    val entity = getWorldCellIfExist(xx, yy, zz)
                    val data = arrAfter[xx - x][yy - y][zz]
                    if (entity != null && data != null) {
                        entity.get<TileComponent>()!!.setTileFriendly(TileFriendlyData().also { it.setValue(data) })
                    }
                }
            }
        }
    }

    init {
        entityListener = object : EntityListener {
            override fun entityAdded(entity: Entity) {
                tiles.add(entity)
                addEntityTile(entity)

                val position = entity.get<MapPositionComponent>()!!
                val tile = entity.get<TileComponent>()!!

                tile.container.x = (position.worldX * TileMapRender.TILE_WIDTH).toFloat()
                tile.container.y = (position.worldY * TileMapRender.TILE_HEIGHT).toFloat()
            }

            override fun entityRemoved(entity: Entity) {
                tiles.remove(entity)
                removeEntityTile(entity)
            }
        }
        engine.addEntityListener(
            allOf(TileComponent::class, MapPositionComponent::class).get(),//MapPositionComponent PositionComponent
            entityListener
        )
    }
}