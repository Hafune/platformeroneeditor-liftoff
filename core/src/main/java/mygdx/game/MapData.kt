package mygdx.game

class MapData(val map: HashMap<String, Any?>) {
    var width: Int? by map
    var height: Int? by map
}
