package mygdx.game.icons

import mygdx.game.dataBase.DatabaseServices

class IconStorage(db: DatabaseServices) {

    private val storageMap = HashMap<String, HashMap<Int, Icon>>()
    private val storageIdMap = HashMap<Int, Icon>()
    private val storageNameMap = HashMap<String, Icon>()

    private val models = db.iconService.getIcons()

    fun getCharacterIcons() = storageMap["characters"]!!

    fun buildById(id: Int): Icon {
        val it = storageIdMap[id]!!
        return cloneIcon(it)
    }

    fun buildByName(name: String): Icon {
        val it = storageNameMap[name]!!
        return cloneIcon(it)
    }

    private fun cloneIcon(it: Icon): Icon {
        return Icon(
            path = it.path,
            iconWidth = it.iconWidth,
            iconHeight = it.iconHeight,
            position = it.position,
            id = it.id
        ).apply {
//            sprite.setFlip(it.sprite.isFlipX, it.sprite.isFlipY)
//            sprite.setOrigin(it.sprite.originX, it.sprite.originY)
            image.setScale(it.image.scaleX, it.image.scaleY)
            image.setOrigin(it.image.originX, it.image.originY)
        }
    }

    init {
        models.forEach {
            if (storageMap[it.type_key] == null) {
                storageMap[it.type_key] = HashMap()
            }
            val icon = Icon(
                path = it.src,
                iconWidth = it.type.width,
                iconHeight = it.type.height,
                position = it.idx,
                id = it.id
            )
//            icon.sprite.setFlip(it.type.flip_x, it.type.flip_y)
//            icon.sprite.setOrigin(it.type.origin_x, it.type.origin_y)
            icon.image.setScale(
                if (it.type.flip_x) -1f else 1f,
                if (it.type.flip_y) -1f else 1f
            )
            icon.image.setOrigin(it.type.origin_x, it.type.origin_y)

            storageIdMap[it.id] = icon
            storageNameMap[it.name] = icon
            storageMap[it.type_key]!![icon.id] = icon
        }
    }
}