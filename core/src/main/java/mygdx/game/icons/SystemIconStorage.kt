package mygdx.game.icons

import com.badlogic.gdx.graphics.Texture
import mygdx.game.loadSave.MyFileHandler

object SystemIconStorage {

    private val storageNameMap = HashMap<String, Icon>()

    private val models = MyFileHandler.getFilesFromFolder("system/", endsWith = ".png").first

    fun buildByName(name: String): Icon {
        val it = storageNameMap[name]!!
        return cloneIcon(it)
    }

    private fun cloneIcon(it: Icon): Icon {
        return Icon(
            path = it.path,
            iconWidth = it.iconWidth,
            iconHeight = it.iconHeight,
            position = it.position,
            id = it.id
        )
    }

    init {
        models.forEach {
            val texture = Texture(it)

            val icon = Icon(
                path = it.path(),
                iconWidth = texture.width,
                iconHeight = texture.height,
            )

            storageNameMap[it.nameWithoutExtension()] = icon
        }
    }
}