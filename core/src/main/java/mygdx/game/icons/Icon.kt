package mygdx.game.icons

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Table
import mygdx.game.lib.CuttingTextures

class Icon(
    val path: String,
    val iconWidth: Int,
    val iconHeight: Int,
    val position: Int = 0,
    val cutColumns: Int = 0,
    val cutRows: Int = 0,
    val id: Int = 0,
) : Table() {

    companion object {
        private val textureMap = HashMap<String, TextureRegion>()
    }

    var region: TextureRegion
        private set

    var image:Image
        private set

    private fun cutTexture(texture: TextureRegion): ArrayList<TextureRegion> {
        return CuttingTextures.getArrayTextureRegion(
            texture,
            width = iconWidth,
            height = iconHeight,
            columns = cutColumns,
            rows = cutRows
        )
    }

    init {
        width = iconWidth.toFloat()
        height = iconHeight.toFloat()

        if (textureMap[path] == null) {
            val file = Gdx.files.local(path)
            val texture = TextureRegion(Texture(file))
            textureMap[path] = texture
        }
        val texture = textureMap[path]!!

        region = cutTexture(texture)[position]
        image = Image(region)
        add(image)
    }
}