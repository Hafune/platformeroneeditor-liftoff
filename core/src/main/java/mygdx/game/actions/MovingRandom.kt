package mygdx.game.actions

import com.badlogic.gdx.math.MathUtils.random
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import mygdx.game.components.TimerComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.lib.MyMath
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.views.WorldCharacterView
import kotlin.math.round

@VisibleInActionMenu
class MovingRandom : Await() {

    @FormDefault
    var minRotation = 0f

    @FormDefault
    var maxRotation = 360f

    @FormDefault
    var minTime = 0f

    @FormDefault
    var maxTime = 0f

    @JsonIgnore
    private val character = WorldCharacterView()

    override fun start() {

        character.entity = actionContext.entity ?: interactEntity

        val direction = if (round(random()) == 0f) -1 else 1
        val angle = random(minRotation, maxRotation) * direction
        character.position().angle = MyMath.normalizeAngle(character.position().angle + angle)

        val time = random(minTime, maxTime)

        if (time > 0) {
            val timer = TimerComponent()
            timer.time = time

            character.entity!!.add(timer)

            timer.callback = {
                callback?.invoke()
            }
        } else {
            callback?.invoke()
        }
        character.entity!!.get<AbilityComponent>()!!.setAbility(MovingAbilitySystem::class)
    }
}