package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.FormDefault

@VisibleInActionMenu
class SetRelativePositionXY : Await() {

    @FormDefault
    var x = 0f

    @FormDefault
    var y = 0f

    override fun start() {

        val position = actionContext.entity?.get<PositionComponent>() ?: interactEntity!!.get()!!
        actionContext.x = x + position.x
        actionContext.y = y + position.y
        callback?.invoke()
    }
}
