package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemSelectBoxPrimitiveTypes
import mygdx.game.editor.formItems.FormItemSelectOperatorTypes


@VisibleInActionMenu
class SetUserVariable : Await() {

    @FormDefault
    var name = ""

    @FormDefault
    var value = ""

    @Form(FormItemSelectBoxPrimitiveTypes::class)
    var type = ""

    @Form(FormItemSelectOperatorTypes::class)
    var operator = OperatorTypes.ASSIGN

    override fun start() {

        val trimValue = value.trim()
        var v: Any = trimValue

        when (type) {
            PrimitiveTypes.BOOLEAN -> v = trimValue.toBoolean()
            PrimitiveTypes.INT -> v = trimValue.toInt()
            PrimitiveTypes.FLOAT -> v = trimValue.toFloat()
        }

        val vars = myEngine.userData.model.variables
        when (operator) {
            OperatorTypes.ASSIGN -> vars[name] = v
            OperatorTypes.PLUS -> {
                val cv = vars[name]
                if (cv is Int) vars[name] = cv + v as Int
                if (cv is Float) vars[name] = cv + v as Float
            }
            OperatorTypes.MINUS -> {
                val cv = vars[name]
                if (cv is Int) vars[name] = cv - v as Int
                if (cv is Float) vars[name] = cv - v as Float
            }
        }
        callback?.invoke()
    }
}
