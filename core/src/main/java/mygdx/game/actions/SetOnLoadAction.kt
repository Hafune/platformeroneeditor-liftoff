package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemFilePathPicker

@VisibleInActionMenu
class SetOnLoadAction : Await() {

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    override fun start() {
        myEngine.userData.model.on_load_action = fileName
        callback?.invoke()
    }
}