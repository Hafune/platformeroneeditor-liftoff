package mygdx.game.actions

@VisibleInActionMenu
class AddControllerFirst : Await() {

    override fun start() {
        actionContext.entity!!.add(myEngine.myInputHandler.keyboardHandler0.userControllerComponent)
        myEngine.worldProcessingSystem.clickFindPathSystem.setProcessing(true)
        callback?.invoke()
    }
}