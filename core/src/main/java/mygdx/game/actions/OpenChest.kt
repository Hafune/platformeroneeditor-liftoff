package mygdx.game.actions

@VisibleInActionMenu
class OpenChest : Await() {

    override fun start() {
        myEngine.locationsChests.openChest(interactEntity!!)
        callback?.invoke()
    }
}