package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemCharacterIconSelector
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class ChangeSkin : Await() {

    @Form(FormItemCharacterIconSelector::class)
    var skin_id = 0

    override fun start() {

        val view = WorldCharacterView(interactEntity)
        view.animation().setSkin(myEngine.skinStorage.getById(skin_id))
        myEngine.myBox2dHandler.destroyBody(view.body().getBody())
        view.body().myBody = myEngine.myBox2dHandler.createBodyFromSkinAndApplyOrigins(
            view.animation().skin,
            view.body().collisionBehavior
        ).first

        callback?.invoke()
    }
}