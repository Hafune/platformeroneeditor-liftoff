package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Rectangle
import com.fasterxml.jackson.annotation.JsonIgnore
import mygdx.game.components.IgnoreDisposeSceneComponent
import mygdx.game.components.gameWorld.characterComponents.TriggerPositionComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemTouchRegion
import mygdx.game.editor.formItems.FormSupport

@VisibleInActionMenu
@FormSupport(FormItemTouchRegion::class)
class PositionTrigger : Await() {

    @FormDefault
    var targetName = ""

    @FormDefault
    var detectorName = ""

    @FormDefault
    var x = 0

    @FormDefault
    var y = 0

    @FormDefault
    var width = 0

    @FormDefault
    var height = 0

    @FormDefault
    var locationName = ""

    @JsonIgnore
    var rect = Rectangle()

    override fun start() {
        rect = Rectangle(x.toFloat(), y.toFloat(), width.toFloat(), height.toFloat())
        val entity = Entity()

        if (locationName == "") {
            locationName = myEngine.screenContainer.tileMapRender.locationName
        }

        val component =
            TriggerPositionComponent(Rectangle(x.toFloat(), y.toFloat(), width.toFloat(), height.toFloat())).also {
                it.callback = callback
                it.targetName = targetName
                it.detectorName = detectorName
                it.locationName = locationName
            }
        entity.add(component)
        entity.add(IgnoreDisposeSceneComponent())

        myEngine.engine.addEntity(entity)
    }
}