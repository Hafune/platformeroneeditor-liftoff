package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import mygdx.game.systems.MyEngine

abstract class Await : IAwait {

    @JsonIgnore
    override var scriptFileName = ""

    @JsonIgnore
    override lateinit var myEngine: MyEngine

    @JsonIgnore
    override lateinit var actionContext: ActionContext

    @JsonIgnore
    override var interactEntity: Entity? = null

    @JsonIgnore
    override var callback: (() -> Unit)? = null
}