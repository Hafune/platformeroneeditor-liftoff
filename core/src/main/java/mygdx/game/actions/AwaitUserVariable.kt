package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import mygdx.game.components.gameWorld.characterComponents.AwaitUserVariableComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemSelectOperatorComparisonTypes

@VisibleInActionMenu
class AwaitUserVariable : Await() {

    @FormDefault
    var name = ""

    @FormDefault
    var value = ""

    @Form(FormItemSelectOperatorComparisonTypes::class)
    var operator = OperatorComparisonTypes.EQUALS

    override fun start() {

        val entity = Entity()

        val trigger = AwaitUserVariableComponent()
        trigger.name = name
        trigger.value = value
        trigger.operator = operator
        trigger.callback = callback

        entity.add(trigger)
        myEngine.engine.addEntity(entity)
    }
}
