package mygdx.game.actions

object OperatorComparisonTypes {
    const val EQUALS = "=="
    const val NOT_EQUALS = "!="
    const val GT = ">"
    const val GTE = ">="
    const val LT = "<"
    const val LTE = "<="
}