package mygdx.game.actions

import com.badlogic.gdx.math.Vector2
import com.fasterxml.jackson.annotation.JsonIgnore
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class MovingFromPoint : Await() {

    @FormDefault
    var offsetX = 0f

    @FormDefault
    var offsetY = 0f

    @FormDefault
    var time = 0f

    @JsonIgnore
    private val character = WorldCharacterView()

    override fun start() {

        character.entity = actionContext.entity ?: interactEntity

        val v = Vector2(offsetX, offsetY)
        character.position().angle = v.angleDeg()

        character.ability().setAbility(MovingAbilitySystem::class)

        if (time > 0) {
            MyTimer(time).also {
                it.myEngine = myEngine
                it.callback = callback
                it.start()
            }
        }
        else{
            callback?.invoke()
        }
    }
}