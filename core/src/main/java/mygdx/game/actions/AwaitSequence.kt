package mygdx.game.actions

import com.fasterxml.jackson.annotation.JsonIgnore

@VisibleInActionMenu
class AwaitSequence : Await() {

    @JsonIgnore
    var arr = emptyArray<IAwait>()

    //    var arr: ArrayList<IAwait> by map.also { it[Arr] = "" }
    @JsonIgnore
    private var index = 0

    @JsonIgnore
    private lateinit var currentScriptEntityCheck: CurrentScriptEntityCheck

    override fun start() {
        index = 0
        currentScriptEntityCheck = CurrentScriptEntityCheck(interactEntity)
        next()
    }

    fun next() {
        if (!currentScriptEntityCheck.check()) {
            return
        }

        if (arr.size == index) {
            currentScriptEntityCheck.setupOldContext()

            callback?.invoke()
            return
        }

        arr[index].also {
            index++
            it.scriptFileName = scriptFileName
            it.callback = { next() }
            it.actionContext = actionContext
            it.interactEntity = interactEntity

            try {
                it.start()
            } catch (e: Exception) {
                throw Exception("${it.scriptFileName} ${e.message}", e)
            }
        }
    }
}