package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem

@VisibleInActionMenu
class Stand : Await() {

    override fun start() {
        val entity = actionContext.entity ?: interactEntity
        entity!!.get<AbilityComponent>()!!.setAbility(StandAbilitySystem::class)
        callback?.invoke()
    }
}