package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.fasterxml.jackson.annotation.JsonIgnore
import com.rafaskoberg.gdx.typinglabel.TypingLabel
import ktx.actors.onClick
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemCharacterIconSelector
import mygdx.game.icons.Icon
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.inputs.MyPadButtons
import mygdx.game.language.GameText
import mygdx.game.systems.MyEngine

@VisibleInActionMenu
class ShowText : Group(), IMyButtonTarget, IAwait {

    @JsonIgnore
    override var scriptFileName = "ShowText"

    @JsonIgnore
    private var closed = false

    @JsonIgnore
    override lateinit var myEngine: MyEngine

    @JsonIgnore
    override lateinit var actionContext: ActionContext

    @JsonIgnore
    override var interactEntity: Entity? = null

    @FormDefault
    var text = ""

    @Form(FormItemCharacterIconSelector::class)
    var icon_id = 0

    @JsonIgnore
    override var callback: (() -> Unit)? = null

    @JsonIgnore
    private val image = Image()

    @JsonIgnore
    private val table = Table()

    @JsonIgnore
    private lateinit var border: Label

    @JsonIgnore
    private lateinit var label: TypingLabel

    @JsonIgnore
    private lateinit var scrollPane: ScrollPane

    @FormDefault
    var smoothStart = false

    @FormDefault
    var smoothEnd = false

    init {

        width = 10000f
        height = 10000f

        addActor(table)

        onClick {
            touchDown()
        }
    }

    private fun fadeIn() {
        y = -30f
        myEngine.screenContainer.interfaceRoot.addActor(this)
        addAction(
            Actions.sequence(
                Actions.fadeOut(0f),
                Actions.parallel(
                    Actions.fadeIn(.6f, Interpolation.fade),
                    Actions.moveBy(0f, 30f, .6f, Interpolation.fade)
                )
            )
        )
    }

    fun setIcon(icon: Icon) {
        image.drawable = TextureRegionDrawable(icon.region)
    }

    private fun fadeOut() {
        y = 0f
        addAction(
            Actions.sequence(
                Actions.parallel(
                    Actions.fadeOut(.6f, Interpolation.fade),
                    Actions.moveBy(0f, -30f, .6f, Interpolation.fade)
                ),
                Actions.removeActor()
            )
        )
    }

    override fun start() {
        if (!::border.isInitialized) {
            border = myEngine.widgets.labels.default()
            label = myEngine.widgets.typingLabels.bahnschrift_32()
            scrollPane = myEngine.widgets.scrollPanes.default(label)

            table.addActor(border)
            table.add(image)
            table.add(scrollPane)
            table.height = 144f
        }

        if (smoothStart) fadeIn()
        else myEngine.screenContainer.interfaceRoot.addActor(this)

        table.x = parent.width / 10
        table.y = parent.height / 10

        border.width = parent.width - table.x * 2
        val offset = 16
        border.height = scrollPane.height + offset
        border.x = -offset / 2f
        border.y = -offset / 2f

        scrollPane.width = parent.width / 10 * 8
        scrollPane.height = label.style.font.lineHeight * 4
        scrollPane.x = offset.toFloat()

        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = this

        label.restart(GameText[text])

        if (icon_id != 0) {
            val icon = myEngine.iconStorage.buildById(icon_id)
            image.drawable = TextureRegionDrawable(icon.region)
        }
        table.pack()
        table.height = 144f

    }

    fun close() {
        if (smoothEnd) fadeOut()
        else remove()
        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
        closed = true
    }

    override fun buttonDown(button: MyPadButtons) {
        touchDown()
    }

    private fun touchDown() {
        if (closed) return
        when {
            !label.hasEnded() -> {
                label.skipToTheEnd()
            }
            else -> {
                close()
                callback?.invoke()
            }
        }
    }

    override fun toString(): String {
        return this::class.simpleName!!
    }
}