package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemSelectBoxSkinStates
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class ChangeSkinForState : Await() {

    @Form(FormItemSelectBoxSkinStates::class)
    var mapCharacterState = ""

    override fun start() {

        val view = WorldCharacterView(interactEntity)
        val skin_id =
            myEngine.db.skinService.stateSkinService.getSkinIdForState(
                mapCharacterState,
                view.animation().skin.main_skin_id
            )
        view.animation().setSkin(myEngine.skinStorage.getById(skin_id))

        callback?.invoke()
    }
}