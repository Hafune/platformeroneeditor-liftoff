package mygdx.game.actions

import mygdx.game.editor.ActionEditorBuilder
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemFilePathPicker
import mygdx.game.lib.FileStorage
import mygdx.game.lib.FillMap

@VisibleInActionMenu
class BuildAction : Await() {

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    @FormDefault
    var waitCompletion = true

    override fun start() {
        if (fileName == "") {
            return
        }
        val file = FileStorage[fileName]

        val await = ActionEditorBuilder(myEngine).buildAction(scriptFileName = file.name(), command = file.readString())

        if (waitCompletion) {
            await.actionContext = actionContext
            await.callback = callback
            await.interactEntity = interactEntity
            await.start()
        } else {
            await.actionContext = ActionContext()
            FillMap().fillMap(from = actionContext.map, to = await.actionContext.map)
            await.start()

            callback?.invoke()
        }
    }
}