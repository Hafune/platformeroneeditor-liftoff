package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.lib.FindRoute
import mygdx.game.lib.MyMath
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class FindPathToRandomPoint : Await() {

    @FormDefault
    var radius = 0

    override fun start() {

        val character = WorldCharacterView(interactEntity)
        val centerX = character.position().defX
        val centerY = character.position().defY
        val cellX = MyMath.random((centerX - radius), (centerX + radius)).toInt()
        val cellY = MyMath.random((centerY - radius), (centerY + radius)).toInt()

        val route =
            FindRoute.findRoute(
                character.position().getWorldX(),
                character.position().getWorldY(),
                cellX,
                cellY,
                myEngine.screenContainer.tileMapRender
            )

        interactEntity!!.add(MapPointComponent().also {
            it.route = route
            it.cellX = cellX
            it.cellY = cellY
        })
        interactEntity!!.get<AbilityComponent>()!!
            .setAbility(MovingAbilitySystem::class, callback)
    }
}