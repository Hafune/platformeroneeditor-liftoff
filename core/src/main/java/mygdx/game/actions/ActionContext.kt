package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import mygdx.game.lib.FillMap

@Suppress("UNCHECKED_CAST")
class ActionContext {
    val map = HashMap<String, Any?>()
    private val defaults = HashMap<String, Any?>()

    var entity: Entity? by map.also { it[::entity.name] = null }
    var attachEntity: Entity? by map.also { it[::attachEntity.name] = null }

    var x: Float by map.also { it[::x.name] = 0f }
    var y: Float by map.also { it[::y.name] = 0f }
    var z: Float by map.also { it[::z.name] = 0f }

    var name: String by map.also { it[::name.name] = "" }

    operator fun <T> get(key: String): T {
        return map[key] as T
    }

    operator fun <T> set(key: String, value: T?) {
        map[key] = value
    }

    init {
        map.keys.forEach { defaults[it] = null }
        FillMap().fillMap(map, defaults)
    }

    fun reset() {
        FillMap().fillMap(defaults, map)
    }
}