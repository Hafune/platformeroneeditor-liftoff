package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import mygdx.game.components.gameWorld.IgnoreSaveComponent
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.*
import mygdx.game.views.WorldCharacterView
import java.util.stream.Stream

@VisibleInActionMenu
@FormSupport(FormItemTouchCell::class)
class BuildBody : Await() {

    @FormDefault
    var x = 0f

    @FormDefault
    var y = 0f

    @FormDefault
    var z = 0f

    @FormDefault
    var scale = 0f

    @Form(FormItemFilePathPicker::class)
    var bodyFileName = ""

    @Form(FormItemSelectBoxBodyType::class)
    var bodyType = ""

    @Form(FormItemSelectBoxCollisionBehavior::class)
    var collisionBehavior = ""

    @FormDefault
    var defAcceleration = 70f

    @FormDefault
    var defLinearDamping = 20f


    override fun start() {

        val entity = Entity().also {
            Stream.of(
                AbilityComponent(),
                NameComponent().also { c ->
                    if (actionContext.name != "") {
                        c.name = actionContext.name
                        actionContext.name = ""
                    }
                },
                PositionComponent(),
                BodyComponent(),
                IgnoreSaveComponent()
            ).forEach { c ->
                it.add(c)
            }
        }

        val view = WorldCharacterView(entity)

        val (body, origin) = myEngine.myBox2dHandler.createBodyFromFile(bodyFileName, scale, collisionBehavior)
        view.body().collisionBehavior = collisionBehavior
        view.body().bodyType = bodyType
        view.body().myBody = body
        view.body().defAcceleration = defAcceleration
        view.body().defLinearDamping = defLinearDamping

        view.also {
            it.position().x += x + origin.x * scale + actionContext.x
            it.position().y += y + origin.y * scale + actionContext.y
            it.position().z = z + actionContext.z
            it.position().defX = it.position().x
            it.position().defY = it.position().y
            it.position().defZ = it.position().z
        }

        actionContext.entity = entity
        myEngine.engine.addEntity(entity)

        callback?.invoke()
    }
}
