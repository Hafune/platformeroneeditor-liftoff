package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemSelectBoxActionContextVariables

@VisibleInActionMenu
class MoveVariable : Await() {

    @Form(FormItemSelectBoxActionContextVariables::class)
    var name = ""

    @Form(FormItemSelectBoxActionContextVariables::class)
    var toName = ""

    override fun start() {
        val m = actionContext.map
        m[toName] = m[name]
        callback?.invoke()
    }
}
