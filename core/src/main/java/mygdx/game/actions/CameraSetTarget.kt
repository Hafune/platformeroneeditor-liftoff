package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.CameraTargetComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent

@VisibleInActionMenu
class CameraSetTarget : Await() {

    override fun start() {
        val entity = actionContext.entity!!

        entity.add(CameraTargetComponent())

        val position = entity.get<PositionComponent>()!!
        myEngine.screenContainer.camera.position.run {
            x = position.x
            y = position.y
        }

        callback?.invoke()
    }
}