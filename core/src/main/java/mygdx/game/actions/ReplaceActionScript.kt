package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemFilePathPicker

@VisibleInActionMenu
class ReplaceActionScript : Await() {

    @FormDefault
    @JsonIgnore
    lateinit var entity: Entity

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    override fun start() {
        entity.get<InteractComponent>()!!.fileName = fileName
        callback?.invoke()
    }
}