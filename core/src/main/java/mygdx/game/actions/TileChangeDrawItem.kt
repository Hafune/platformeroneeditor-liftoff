package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import ktx.ashley.get
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.editor.formItems.*

@VisibleInActionMenu
class TileChangeDrawItem : Await() {

    @FormDefault
    var x = 0
    @FormDefault
    var y = 0
    @FormDefault
    var z = 0

    @Form(FormItemDrawItemSelector::class)
    var drawItemValue = ""

    @Form(FormItemSelectBoxTileAnimation::class)
    var tileAnimation = ""

    @Form(FormItemSelectBoxTileFrame::class)
    var tileFrame = ""

    override fun start() {

        var isNew = false
        val entity =
            myEngine.screenContainer.tileMapRender.getWorldCell(x, y, z) ?: Entity().also { isNew = true }

        val drawItem = myEngine.graphicResources.getDrawItem(drawItemValue)

        val tile = entity.get() ?: TileComponent().also { entity.add(it) }
        tile.setDrawItem(drawItem)
        tile.animationType = tileAnimation
        tile.setFrameValue(tileFrame)

        val position = entity.get() ?: MapPositionComponent().also { entity.add(it) }
        position.setupWorldPosition(x, y, z)

        if (isNew) {
            myEngine.engine.addEntity(entity)
        }
        callback?.invoke()
    }
}