package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.ActionFlagComponent

class CurrentScriptEntityCheck(private val interactEntity: Entity?) {

    @JsonIgnore
    private var currentAction = ActionFlagComponent()

    @JsonIgnore
    private var lastAction: ActionFlagComponent? = null

    init {
        if (interactEntity != null) {
            lastAction = interactEntity.get()
            interactEntity.add(currentAction)
        }
    }

    fun check(): Boolean {
        if (interactEntity == null) return true
        return interactEntity.get<ActionFlagComponent>() === currentAction
    }

    fun setupOldContext() {
        if (interactEntity != null) {
            interactEntity.remove<ActionFlagComponent>()
            lastAction?.let { interactEntity.add(lastAction) }
        }
    }
}