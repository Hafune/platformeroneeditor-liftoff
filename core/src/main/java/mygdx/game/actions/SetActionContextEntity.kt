package mygdx.game.actions

import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.editor.formItems.FormDefault

@VisibleInActionMenu
class SetActionContextEntity : Await() {

    @FormDefault
    var name = ""

    override fun start() {
        val entity = myEngine.engine.getEntitiesFor(allOf(NameComponent::class).get())
            .first { it.get<NameComponent>()!!.name == name }

        actionContext.entity = entity

        callback?.invoke()
    }
}