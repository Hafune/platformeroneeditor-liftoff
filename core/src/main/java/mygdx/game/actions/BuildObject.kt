package mygdx.game.actions

import com.badlogic.gdx.physics.box2d.BodyDef
import mygdx.game.components.gameWorld.IgnoreSaveComponent
import mygdx.game.editor.formItems.*
import mygdx.game.entityPresets.MapObjectPresetEntity
import mygdx.game.systems.MyBox2dHandler
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
@FormSupport(FormItemTouchCell::class)
class BuildObject : Await() {

    @FormDefault
    var x = 0f

    @FormDefault
    var y = 0f

    @FormDefault
    var z = 0f

    @Form(FormItemCharacterSkinSelector::class)
    var skin_id = 0

    @Form(FormItemSelectBoxCollisionBehavior::class)
    var collisionBehavior = ""

    @Form(FormItemSelectBoxBodyType::class)
    var bodyType = BodyDef.BodyType.DynamicBody.name

    @FormDefault
    var defAcceleration = 70f

    @FormDefault
    var defLinearDamping = 20f


    override fun start() {

        val entity = MapObjectPresetEntity()
        entity.add(IgnoreSaveComponent())

        val view = WorldCharacterView(entity)

        view.animation().setSkin(myEngine.skinStorage.getById(skin_id))

        val (body, origin) = myEngine.myBox2dHandler.createBodyFromSkinAndApplyOrigins(
            view.animation().skin,
            collisionBehavior
        )
        view.body().collisionBehavior = collisionBehavior
        view.body().bodyType = bodyType
        view.body().myBody = body
        view.body().defAcceleration = defAcceleration
        view.body().defLinearDamping = defLinearDamping
        view.name().also { c ->
            if (actionContext.name != "") {
                c.name = actionContext.name
                actionContext.name = ""
            }
        }

        view.also {
            it.position().x += x + origin.x * it.animation().skin.iconTexture.regionWidth / MyBox2dHandler.worldToPixelScale + actionContext.x
            it.position().y += y + origin.y * it.animation().skin.iconTexture.regionWidth / MyBox2dHandler.worldToPixelScale + actionContext.y
            it.position().z = z + actionContext.z
            it.position().defX = it.position().x
            it.position().defY = it.position().y
            it.position().defZ = it.position().z
        }

        actionContext.entity = entity
        myEngine.engine.addEntity(entity)

        callback?.invoke()
    }
}
