package mygdx.game.actions

@VisibleInActionMenu
class RemoveCharacter : Await() {

    override fun start() {
        myEngine.engine.removeEntity(actionContext.entity)
        callback?.invoke()
    }
}