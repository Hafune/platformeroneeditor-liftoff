package mygdx.game.actions

import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.lib.FindRoute
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class FindPathToPoint : Await() {

    @FormDefault
    var cellX = 0

    @FormDefault
    var cellY = 0

    @JsonIgnore
    var character: WorldCharacterView? = null

    override fun start() {

        val entity = actionContext.entity!!

        character ?: let { character = WorldCharacterView(entity) }

        val route =
            FindRoute.findRoute(
                character!!.position().getWorldX(),
                character!!.position().getWorldY(),
                cellX,
                cellY,
                myEngine.screenContainer.tileMapRender
            )

        entity.add(MapPointComponent().also {
            it.route = route
            it.cellX = cellX
            it.cellY = cellY
        })
        entity.get<AbilityComponent>()!!.setAbility(MovingAbilitySystem::class, callback)
    }
}