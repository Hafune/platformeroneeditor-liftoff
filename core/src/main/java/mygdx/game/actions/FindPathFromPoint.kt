package mygdx.game.actions

import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.lib.FindRoute
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem

@VisibleInActionMenu
class FindPathFromPoint : Await() {

    @FormDefault
    var offsetX = 0

    @FormDefault
    var offsetY = 0

    override fun start() {

        val entity = actionContext.entity!!
        val position = entity.get<PositionComponent>()!!

        val x = position.getWorldX() + offsetX
        val y = position.getWorldY() + offsetY
        val route =
            FindRoute.findRoute(
                position.getWorldX(),
                position.getWorldY(),
                x,
                y,
                myEngine.screenContainer.tileMapRender
            )

        entity.remove<MapPointComponent>()
        entity.add(MapPointComponent().also {
            it.route = route
            it.cellX = x
            it.cellY = y
        })

        entity.get<AbilityComponent>()!!.setAbility(MovingAbilitySystem::class, callback)
    }
}