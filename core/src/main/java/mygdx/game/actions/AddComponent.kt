package mygdx.game.actions

import mygdx.game.components.MyAbstractComponent
import mygdx.game.editor.FormItemFactory
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormItemSelectBoxComponents
import mygdx.game.lib.MyClassForName
import mygdx.game.lib.MyObjectMapper

@VisibleInActionMenu
class AddComponent : Await() {

    @Form(FormItemSelectBoxComponents::class)
    var value = ""

    @Suppress("UNCHECKED_CAST")
    override fun start() {
        val map = MyObjectMapper.readValue(value, HashMap::class.java) as HashMap<String, Any>
        val component: MyAbstractComponent = MyClassForName.get(map[FormItemFactory.K_CLASS_NAME] as String)

        actionContext.entity!!.add(component)
        callback?.invoke()
    }
}