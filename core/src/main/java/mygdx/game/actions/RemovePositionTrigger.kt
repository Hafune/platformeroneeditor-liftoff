package mygdx.game.actions

import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.characterComponents.TriggerPositionComponent
import mygdx.game.editor.formItems.FormDefault

@VisibleInActionMenu
class RemovePositionTrigger : Await() {

    @FormDefault
    var detectorName = ""

    override fun start() {

        myEngine.engine.getEntitiesFor(allOf(TriggerPositionComponent::class).get()).forEach {
            val detector = it.get<TriggerPositionComponent>()!!
            if (detector.detectorName == detectorName) {
                it.remove<TriggerPositionComponent>()
            }
        }
        callback?.invoke()
    }
}