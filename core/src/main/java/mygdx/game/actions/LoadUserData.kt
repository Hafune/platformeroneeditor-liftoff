package mygdx.game.actions

@VisibleInActionMenu
class LoadUserData : Await() {

    override fun start() {
        myEngine.userData.buildCharacterFromUserData()
        callback?.invoke()
    }
}