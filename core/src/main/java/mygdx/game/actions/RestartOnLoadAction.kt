package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.BuildActionComponent

@VisibleInActionMenu
class RestartOnLoadAction : Await() {

    override fun start() {
        val component = interactEntity!!.get<BuildActionComponent>()
        if (component != null) {
            val entity = interactEntity
            BuildAction().also {
                it.fileName = component.fileName
                it.myEngine = myEngine
                it.actionContext = ActionContext()
                it.interactEntity = entity
            }.start()
        }
    }
}