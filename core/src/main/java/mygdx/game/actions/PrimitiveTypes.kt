package mygdx.game.actions

object PrimitiveTypes {
    const val BOOLEAN = "Boolean"
    const val INT = "Int"
    const val FLOAT = "Float"
    const val STRING = "String"
}