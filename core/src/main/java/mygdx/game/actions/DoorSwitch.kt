package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.WallComponent

@VisibleInActionMenu
class DoorSwitch : Await() {

    @JsonIgnore
    private lateinit var entity: Entity

    override fun start() {
        entity = interactEntity!!

        val action = TileStartAnimation()
        action.myEngine = this.myEngine
        action.interactEntity = entity
        action.actionContext = actionContext

        val doorId = entity.get<NameComponent>()?.name?.toInt()

        if (entity.get<WallComponent>() == null) {
            if (cellIsFree(entity)) {
                action.start()
                changeBlockState()
                if (doorId != null) {
                    myEngine.userData.doors.remove(doorId)
                }
            }
        } else {
            action.callback = { changeBlockState() }
            action.start()
            if (doorId != null) {
                myEngine.userData.doors.add(doorId)
            }
        }
    }

    private fun changeBlockState() {
        if (entity.get<WallComponent>() == null) {
            entity.add(WallComponent())
        } else {
            entity.remove<WallComponent>()
        }
    }

    private fun cellIsFree(entity: Entity): Boolean {
        val tile = entity.get<MapPositionComponent>()!!
        myEngine.engine.getEntitiesFor(allOf(PositionComponent::class).get()).forEach {
            val char = it.get<PositionComponent>()!!
            if (tile.worldX == char.getWorldX() && tile.worldY == char.getWorldY()) {
                return false
            }
        }
        return true
    }
}