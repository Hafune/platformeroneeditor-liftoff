package mygdx.game.actions

import mygdx.game.editor.formItems.*

@VisibleInActionMenu
class SetActionVariable : Await() {

    @Form(FormItemSelectBoxActionContextVariables::class)
    var name = ""

    @FormDefault
    var value = ""

    @Form(FormItemSelectBoxPrimitiveTypes::class)
    var type = ""

    @Form(FormItemSelectOperatorTypes::class)
    var operator = OperatorTypes.ASSIGN

    override fun start() {

        val trimValue = value.trim()
        var v: Any = trimValue

        when (type) {
            PrimitiveTypes.BOOLEAN -> v = trimValue.toBoolean()
            PrimitiveTypes.INT -> v = trimValue.toInt()
            PrimitiveTypes.FLOAT -> v = trimValue.toFloat()
            PrimitiveTypes.STRING -> v = trimValue
        }
        when (operator) {
            OperatorTypes.ASSIGN -> actionContext.map[name] = v
            OperatorTypes.PLUS -> {
                val cv = actionContext.map[name]
                if (cv is Int) actionContext.map[name] = cv + v as Int
                if (cv is Float) actionContext.map[name] = cv + v as Float
            }
            OperatorTypes.MINUS -> {
                val cv = actionContext.map[name]
                if (cv is Int) actionContext.map[name] = cv - v as Int
                if (cv is Float) actionContext.map[name] = cv - v as Float
            }
        }
        actionContext.map[name] = v
        callback?.invoke()
    }
}
