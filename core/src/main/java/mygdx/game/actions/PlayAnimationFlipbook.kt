package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.TileMapRender
import mygdx.game.components.gameWorld.characterComponents.AnimationImageComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemFilePathPicker
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.TextureStorage

@VisibleInActionMenu
class PlayAnimationFlipbook : Await() {

    @FormDefault
    var width = 0

    @FormDefault
    var height = 0

    @FormDefault
    var playTime = 0f

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    override fun start() {
        val texture = TextureStorage[fileName]
        val textures = CuttingTextures.getArrayTextureRegion(TextureRegion(texture), width, height)
        val animation = Animation(
            playTime / textures.size,
            *textures.toTypedArray()
        )

        val entity = Entity()
        val position = PositionComponent().also {
            it.setPosition(
                x = actionContext.x - width / TileMapRender.TILE_WIDTH / 2,
                y = actionContext.y - height / TileMapRender.TILE_HEIGHT / 2,
                z = actionContext.z
            )
        }

        val animationComponent = AnimationImageComponent()
        animationComponent.animation = animation
        animationComponent.callback = {
            myEngine.engine.removeEntity(entity)
            callback?.invoke()
        }

        entity.add(animationComponent)
        entity.add(position)

        myEngine.engine.addEntity(entity)
    }
}