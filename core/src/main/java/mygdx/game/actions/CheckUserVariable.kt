package mygdx.game.actions

import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemFilePathPicker
import mygdx.game.editor.formItems.FormItemSelectOperatorComparisonTypes

@VisibleInActionMenu
class CheckUserVariable : Await() {

    @FormDefault
    var name = ""

    @FormDefault
    var value = ""

    @Form(FormItemSelectOperatorComparisonTypes::class)
    var operator = OperatorComparisonTypes.EQUALS

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    override fun start() {

        val userValue = myEngine.userData.model.variables[name]

        when (operator) {
            OperatorComparisonTypes.EQUALS -> if (userValue == value) run()

            OperatorComparisonTypes.NOT_EQUALS -> if (userValue != value) run()
        }

        callback?.invoke()
    }

    private fun run() = BuildAction().also {
        it.fileName = fileName
        it.myEngine = myEngine
        it.actionContext = actionContext
        it.interactEntity = interactEntity
    }.start()
}
