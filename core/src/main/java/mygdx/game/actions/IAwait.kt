package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import mygdx.game.systems.MyEngine

interface IAwait {

    var scriptFileName: String

    var myEngine: MyEngine

    var actionContext: ActionContext

    var interactEntity: Entity?

    var callback: (() -> Unit)?

    fun start()
}