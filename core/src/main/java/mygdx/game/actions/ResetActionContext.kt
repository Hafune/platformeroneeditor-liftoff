package mygdx.game.actions

@VisibleInActionMenu
class ResetActionContext : Await() {

    override fun start() {
        actionContext.reset()
        callback?.invoke()
    }
}
