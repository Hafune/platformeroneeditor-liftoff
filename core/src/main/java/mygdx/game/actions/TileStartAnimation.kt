package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.tileComponents.TileComponent

class TileStartAnimation : Await() {

    override fun start() {
        val tile = interactEntity!!.get<TileComponent>()!!
        tile.startAnimation {
            callback?.invoke()
        }
    }
}