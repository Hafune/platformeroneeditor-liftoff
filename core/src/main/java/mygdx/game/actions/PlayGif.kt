package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Animation
import mygdx.game.components.gameWorld.characterComponents.AnimationImageComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.*
import mygdx.game.lib.FileStorage
import mygdx.game.lib.GifDecoder

@VisibleInActionMenu
@FormSupport(FormItemTouchCell::class)
class PlayGif : Await() {

    @FormDefault
    var x = 0f
    @FormDefault
    var y = 0f

    @Form(FormItemFilePathPicker::class)
    var fileName = ""

    @Form(FormItemSelectBoxTileLayer::class)
    var tileLayer = ""

    @Form(FormItemSelectBoxAnimationMode::class)
    var animationModel = ""

    override fun start() {
        val file = FileStorage[fileName]
        val gif = GifDecoder.loadGIFAnimation(Animation.PlayMode.valueOf(animationModel), file)

        val entity = Entity()

        val position = PositionComponent().also {
            it.setPosition(x, y)
        }

        val animation = AnimationImageComponent()
        animation.animation = gif
        animation.callback = callback

        entity.add(animation)
        entity.add(position)

        myEngine.engine.addEntity(entity)
    }
}