package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import mygdx.game.components.TimerComponent
import mygdx.game.editor.formItems.FormDefault

@VisibleInActionMenu
class MyTimer() : Await() {

    constructor(second: Float) : this() {
        this.second = second
    }

    @FormDefault
    var second = 0f

    override fun start() {

        val timer = TimerComponent()
        timer.time = second

        val entity = Entity().add(timer)

        timer.callback = {
            myEngine.engine.removeEntity(entity)
            callback?.invoke()
        }

        myEngine.engine.addEntity(entity)
    }
}