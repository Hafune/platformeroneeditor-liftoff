package mygdx.game.actions

import com.fasterxml.jackson.annotation.JsonIgnore

@VisibleInActionMenu
class AwaitParallel : Await() {

    @JsonIgnore
    var arr = emptyArray<IAwait>()

    @JsonIgnore
    private var finished = 0

    @JsonIgnore
    private lateinit var currentScriptEntityCheck: CurrentScriptEntityCheck

    override fun start() {
        finished = 0

        currentScriptEntityCheck = CurrentScriptEntityCheck(interactEntity)

        arr.forEach {
            it.scriptFileName = scriptFileName
            it.callback = { finish() }
            it.actionContext = actionContext
            it.interactEntity = interactEntity

            try {
                it.start()
            } catch (e: Exception) {
                throw Exception(it.scriptFileName, e)
            }
        }
    }

    private fun finish() {
        callback ?: return
        finished++

        if (arr.size == finished) {

            if (currentScriptEntityCheck.check()) {
                currentScriptEntityCheck.setupOldContext()

                callback?.invoke()
            }
        }
    }
}