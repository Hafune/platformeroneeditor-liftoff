package mygdx.game.actions

import mygdx.game.editor.formItems.FormDefault

class ChestTaken : Await() {

    @FormDefault
    var x = 0

    @FormDefault
    var y = 0

    @FormDefault
    var z = 0

    override fun start() {
        myEngine.locationsChests.takeCallback = {
            myEngine.locationsChests.takeCallback = null
            callback?.invoke()
        }
    }
}