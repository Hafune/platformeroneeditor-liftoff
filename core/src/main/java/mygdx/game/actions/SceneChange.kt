package mygdx.game.actions

import mygdx.game.editor.formItems.FormDefault
import mygdx.game.loadSave.SceneLoader

@VisibleInActionMenu
class SceneChange : Await() {

    @FormDefault
    var locationName = ""

    override fun start() {
        val tMap = myEngine.screenContainer.tileMapRender
        if (tMap.locationName != locationName) {
            SceneLoader().load(myEngine, locationName)
            { callback?.invoke() }
        }
    }
}