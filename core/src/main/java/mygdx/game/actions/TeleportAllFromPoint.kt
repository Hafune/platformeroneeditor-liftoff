package mygdx.game.actions

import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.TileMapRender
import mygdx.game.components.PlatformComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.FormDefault
import kotlin.math.floor

@VisibleInActionMenu
class TeleportAllFromPoint : Await() {

    @FormDefault
    var x = 0f

    @FormDefault
    var y = 0f

    override fun start() {
        myEngine.engine.getEntitiesFor(
            allOf(
                BodyComponent::class,
                PositionComponent::class,
            ).get()
        ).forEach {
            val position = it.get<BodyComponent>()!!.getBody().position
            it.get<BodyComponent>()!!.getBody().setTransform(position.x + x, position.y + y, 0f)
            it.get<PlatformComponent>()?.let { p -> p.lastPosition.x += x }
            it.get<PlatformComponent>()?.let { p -> p.lastPosition.y += y }
        }
        myEngine.screenContainer.camera.translate(
            floor(x * TileMapRender.TILE_WIDTH),
            floor(y * TileMapRender.TILE_HEIGHT)
        )

        callback?.invoke()
    }
}