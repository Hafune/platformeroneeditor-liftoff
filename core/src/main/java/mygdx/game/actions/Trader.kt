package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.IconComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.language.KeyWord
import mygdx.game.myWidgets.Options
import mygdx.game.uiMenu.store.StoreBuy
import mygdx.game.uiMenu.store.StoreSell

@VisibleInActionMenu
class Trader : Await() {

    @JsonIgnore
    private var options: Options? = null

    @FormDefault
    val store_key = "default"

    override fun start() {
        if (options == null) {
            options = Options(myEngine).also {
                it.setOptions(
                    KeyWord.Buy,
                    KeyWord.Sell,
                    KeyWord.Cancel
                )
            }
        }

        val entity = interactEntity
        val showText = ShowText().apply {
            entity!!.get<IconComponent>()?.icon?.let { setIcon(it) }
            text = KeyWord.RandomTraderPhrase010.name
            callback = { step0020(entity) }
        }
        showText.myEngine = myEngine
        showText.start()
    }

    private fun step0020(entity: Entity) {
        options!!.open { optionSelected(entity) }
    }

    private fun optionSelected(entity: Entity) {
        val i = options!!.selection
        if (i == -1 || i == 2) {
            preEnd(entity)
        }
        if (i == 0) {
            val items = myEngine.db.itemService.getStoreItems(store_key)
            StoreBuy(items, myEngine).also {
                it.open {
                    myEngine.worldProcessingSystem.setProcessing(true)
                    myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
                    preEnd(entity)
                }
            }
        }
        if (i == 1) {
            StoreSell(myEngine).also {
                it.open { preEnd(entity) }
            }
        }
    }

    private fun preEnd(entity: Entity) {
        val showText = ShowText().apply {
            entity.get<IconComponent>()!!.icon?.let { setIcon(it) }
            text = KeyWord.RandomTraderPhrase020.name
            callback = {
                this@Trader.callback?.invoke()
            }
        }
        showText.myEngine = myEngine
        showText.start()
    }
}