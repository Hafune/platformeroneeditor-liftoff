package mygdx.game.actions

object OperatorTypes {
    const val ASSIGN = "="
    const val PLUS = "+"
    const val MINUS = "-"
}