package mygdx.game.actions

@VisibleInActionMenu
class UserAutoCreateCancel : Await() {

    override fun start() {
        myEngine.userData.isCharacterCreated = true
        callback?.invoke()
    }
}