package mygdx.game.actions

import com.badlogic.gdx.physics.box2d.BodyDef
import mygdx.game.character.CharacterBuilder
import mygdx.game.editor.formItems.Form
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.editor.formItems.FormItemCharacterIconSelector
import mygdx.game.editor.formItems.FormItemCharacterSkinSelector
import mygdx.game.views.WorldCharacterView

@VisibleInActionMenu
class BuildCharacter : Await() {

    @FormDefault
    var name = ""

    @FormDefault
    var x = 0f

    @FormDefault
    var y = 0f

    @FormDefault
    var angle = 270f

    @Form(FormItemCharacterSkinSelector::class)
    var skin_id = 0

    @Form(FormItemCharacterIconSelector::class)
    var icon_id = 0

    override fun start() {

        val entity = CharacterBuilder(myEngine).buildCharacter(
            skin_id,
            icon_id,
            name
        )

        WorldCharacterView(entity).apply {
            position().x = x + actionContext.x
            position().y = y + actionContext.y
            position().angle = angle
            body().bodyType = BodyDef.BodyType.DynamicBody.name
            body().collisionBehavior = "character"
        }

        callback?.invoke()
    }
}