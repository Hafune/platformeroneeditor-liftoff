package mygdx.game.actions

import ktx.ashley.get
import mygdx.game.components.gameWorld.AttachOffsetComponent

@VisibleInActionMenu
class AttachEntityPositionToContextAttachEntity : Await() {

    override fun start() {
        val offsetComponent = AttachOffsetComponent()
        offsetComponent.attachBody = actionContext.attachEntity!!.get()!!
        actionContext.entity!!.add(offsetComponent)

        callback?.invoke()
    }
}
