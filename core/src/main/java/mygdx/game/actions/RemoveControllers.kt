package mygdx.game.actions

import ktx.ashley.allOf
import ktx.ashley.remove
import mygdx.game.components.UserControllerComponent

@VisibleInActionMenu
class RemoveControllers : Await() {

    override fun start() {

        myEngine.engine.getEntitiesFor(allOf(UserControllerComponent::class).get()).forEach {
            it.remove<UserControllerComponent>()
        }
        myEngine.worldProcessingSystem.clickFindPathSystem.setProcessing(false)
        callback?.invoke()
    }
}