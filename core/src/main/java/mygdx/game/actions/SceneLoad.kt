package mygdx.game.actions

import mygdx.game.editor.formItems.FormDefault
import mygdx.game.loadSave.SceneLoader

@VisibleInActionMenu
class SceneLoad : Await() {

    @FormDefault
    var locationName = ""

    override fun start() {
        SceneLoader().load(myEngine, locationName)
        { callback?.invoke() }
    }
}