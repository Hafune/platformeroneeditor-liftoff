package mygdx.game.actions

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.annotation.JsonIgnore
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.editor.formItems.FormDefault
import mygdx.game.systems.gameWorld.abilities.DashAbilitySystem

@VisibleInActionMenu
class DashAbility : Await() {

    @FormDefault
    @JsonIgnore
    lateinit var entity: Entity
    //var entity: Entity by map.also { it[EntityName] = "" }

    @FormDefault
    var direction = 0f

    override fun start() {
        entity.get<PositionComponent>()!!.angle = direction
        entity.get<AbilityComponent>()!!.setAbility(DashAbilitySystem::class, callback)
    }
}