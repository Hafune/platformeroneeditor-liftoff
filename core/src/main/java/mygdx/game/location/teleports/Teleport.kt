package mygdx.game.location.teleports

import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter

class Teleport(
    fromX: Int = 0,
    fromY: Int = 0,
    locationTo: String = "",
    toX: Int = 0,
    toY: Int = 0,
    direction: Float = 0f,
) {
    companion object {
        private const val ITEM = "itemModel"
    }

    var fromX = fromX
        private set
    var fromY = fromY
        private set
    var locationTo = locationTo
        private set
    var toX = toX
        private set
    var toY = toY
        private set
    var direction = direction
        private set

    fun exportGdxXml(document: XmlWriter) {
        val el = document.element(ITEM)
        el.attribute(::fromX.name, fromX)
        el.attribute(::fromY.name, fromY)
        el.attribute(::locationTo.name, locationTo)
        el.attribute(::toX.name, toX)
        el.attribute(::toY.name, toY)
        el.attribute(::direction.name, direction)
        el.pop()
    }

    fun importGdxXml(model: XmlReader.Element) {
        fromX = model.getInt(::fromX.name)
        fromY = model.getInt(::fromY.name)
        locationTo = model.get(::locationTo.name)
        toX = model.getInt(::toX.name)
        toY = model.getInt(::toY.name)
        direction = model.getFloat(::direction.name)
    }
}