package mygdx.game.location.teleports

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.XmlReader
import com.badlogic.gdx.utils.XmlWriter
import mygdx.game.editor.UndoRedo
import kotlin.math.max
import kotlin.math.min

class LocationsTeleports {

    companion object {
        private const val TELEPORTS = "teleports"
    }

    fun loadTeleports(root: XmlReader.Element) {
        if (root.hasChild(TELEPORTS)) {
            val models = root.getChildByName(TELEPORTS)
            list.clear()
            for (i in 0 until models.childCount) {
                val item = models.getChild(i)
                importTeleport(item)
            }
        }
    }

    fun exportTeleports(document: XmlWriter) {
        val param = document.element(TELEPORTS)
        list.forEach { it.exportGdxXml(param) }
        param.pop()
    }

    private fun importTeleport(item: XmlReader.Element) {
        val teleport = Teleport()
        teleport.importGdxXml(item)
        addTeleport(teleport)
    }

    var list = ArrayList<Teleport>()

    @Suppress("UNCHECKED_CAST")
    fun buildTeleportArea(
        toName: String,
        toX: Int,
        toY: Int,
        direction: Float,
        xy0: Vector2,
        xy1: Vector2,
    ) {
        val startX = min(xy0.x, xy1.x).toInt()
        val startY = min(xy0.y, xy1.y).toInt()
        val endX = max(xy0.x, xy1.x).toInt()
        val endY = max(xy0.y, xy1.y).toInt()

        val beforeList: ArrayList<Teleport> = list.clone() as ArrayList<Teleport>
        for (x in startX..endX) {
            for (y in startY..endY) {
                buildTeleport(x, y, toName, toX, toY, direction)
            }
        }
        val forwardList: ArrayList<Teleport> = list.clone() as ArrayList<Teleport>

        UndoRedo.addAction(
            {
                list = forwardList
            }, {
                list = beforeList
            })
        UndoRedo.writeStep()
    }

    @Suppress("UNCHECKED_CAST")
    fun removeTeleportArea(
        xy0: Vector2,
        xy1: Vector2,
    ) {
        val startX = min(xy0.x, xy1.x).toInt()
        val startY = min(xy0.y, xy1.y).toInt()
        val endX = max(xy0.x, xy1.x).toInt()
        val endY = max(xy0.y, xy1.y).toInt()

        val beforeList: ArrayList<Teleport> = list.clone() as ArrayList<Teleport>

        list.removeIf { it.fromX in startX..endX && it.fromY in startY..endY }

        val forwardList: ArrayList<Teleport> = list.clone() as ArrayList<Teleport>

        UndoRedo.addAction(
            {
                list = forwardList
            }, {
                list = beforeList
            })
        UndoRedo.writeStep()
    }

    private fun buildTeleport(
        fromX: Int,
        fromY: Int,
        locationTo: String,
        toX: Int,
        toY: Int,
        direction: Float
    ) {
        val teleport = Teleport(
            fromX,
            fromY,
            locationTo,
            toX,
            toY,
            direction
        )

        addTeleport(teleport)
    }

    private fun addTeleport(teleport: Teleport) {
        list.removeIf { it.fromX == teleport.fromX && it.fromY == teleport.fromY }
        list.add(teleport)
    }

    fun findTeleport(x: Int, y: Int): Teleport? {
        return list.firstOrNull { it.fromX == x && it.fromY == y }
    }
}