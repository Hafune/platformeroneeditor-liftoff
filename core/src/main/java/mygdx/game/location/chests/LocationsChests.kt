package mygdx.game.location.chests

import com.badlogic.ashley.core.Entity
import ktx.ashley.get
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.dataBase.services.chestsService.ChestModel
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.store.ChestMenu
import org.jetbrains.exposed.sql.transactions.transaction

class LocationsChests(private val myEngine: MyEngine) {
    private var locationsChestModels = ArrayList<ChestModel>()
    var userChestModels = ArrayList<ChestModel>()
    var takeCallback: (() -> Unit)? = null

    fun openChest(chestEntity: Entity) {
        val chestName = chestEntity.get<NameComponent>()!!

        val chest = locationsChestModels.firstOrNull { it.id == chestName.name.toInt() } ?: return

        val tileComponent: TileComponent = chestEntity.get()!!

        myEngine.worldProcessingSystem.setProcessing(false)
        myEngine.worldProcessingSystem.animationCharacterSystem.setProcessing(true)
        tileComponent.startAnimation {
            ChestMenu(chest.itemModels, myEngine) {
                callbackFromLootWindow(chest)
            }
        }
        return
    }

    fun hasOpenChest(chest_id: Int): Boolean {
        userChestModels.firstOrNull { it.id == chest_id } ?: return false
        return true
    }

    private fun callbackFromLootWindow(chestModel: ChestModel) {
        locationsChestModels.remove(chestModel)
        userChestModels.add(chestModel)

        takeCallback?.invoke()
        myEngine.worldProcessingSystem.setProcessing(true)
    }

    fun refreshChestList(location: String) {
        val chestService = myEngine.db.chestService
        val itemService = myEngine.db.itemService

        transaction {
            val chestIds = userChestModels.map { it.id }
            locationsChestModels =
                arrayListOf(*chestService.getByLocation(location).filterNot { chestIds.contains(it.id) }
                    .toTypedArray())
            locationsChestModels.forEach {
                it.itemModels = itemService.getChestItems(it.id)
            }
        }
    }

    fun saveForUser(user_id: Int) {
        myEngine.db.chestService.saveForUser(user_id, userChestModels)
    }
}