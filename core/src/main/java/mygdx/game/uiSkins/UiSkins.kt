package mygdx.game.uiSkins

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin

object UiSkins {
    val skinPath = "skin/game-ui/menu-ui.json"
    val skin = Skin(Gdx.files.internal(skinPath))

    //TODO убрать,
    object Labels {
        fun touch_region_background(text: String? = null) = create(text, "touch_region_background")

        private fun create(text: String?, style: String): Label {
            return Label(text, skin, style)
        }
    }
}