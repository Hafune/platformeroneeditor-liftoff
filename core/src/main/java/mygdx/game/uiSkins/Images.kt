package mygdx.game.uiSkins

import com.badlogic.gdx.scenes.scene2d.ui.Image
import mygdx.game.lib.TextureStorage

class Images {
    class Xbox {
        private val root = "system/menu-assets/XBOX BUTTONS - Premium Assets"
        private val buttonsPath = "$root/Digital Buttons/ABXY-standart"

        fun buttonA() = Image(TextureStorage["$buttonsPath/button_xbox_digital_a_1-standart.png"])
        fun buttonB() = Image(TextureStorage["$buttonsPath/button_xbox_digital_b_1-standart.png"])
        fun buttonX() = Image(TextureStorage["$buttonsPath/button_xbox_digital_x_1-standart.png"])
        fun buttonY() = Image(TextureStorage["$buttonsPath/button_xbox_digital_y_1-standart.png"])


        private val dpadPath = "$root/D-Pad-standart"

        fun dpadDown() = Image(TextureStorage["$dpadPath/button_xbox_dpad_dark_3-standart.png"])
    }

    class Backgrounds {
        private val path = "system/menu-assets"

        fun game_menu_background() = Image(TextureStorage["$path/game_menu_background.png"])
        fun background_hover() = Image(TextureStorage["$path/background_hover.png"])
        fun buttons_hint_background() = Image(TextureStorage["$path/buttons_hint_background.png"])
    }

    val xBox = Xbox()
    val backgrounds = Backgrounds()
}