@file:Suppress("FunctionName")

package mygdx.game.uiSkins

import com.badlogic.gdx.scenes.scene2d.ui.Skin

class Drawables(val skin: Skin) {
    fun button_select_02_ten() = skin.getDrawable("button_select_02_ten")!!
}