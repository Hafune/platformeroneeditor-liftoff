package mygdx.game.tileSets

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.tileSets.dynamicTiles.drawItems.AbstractDrawItem

object DrawItemEraser : AbstractDrawItem() {


    override val imagePath = ""
    override var indexFromSet: Int = 0
    override var tileName: String = ""

    override fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float) {}
    override val animationDuration: Float
        get() = 0f
    override var frameDuration = 0f

    override fun addTexture(region: TextureRegion) {}
}