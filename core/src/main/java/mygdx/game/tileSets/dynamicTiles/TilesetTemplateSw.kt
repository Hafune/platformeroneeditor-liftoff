package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleSolo

class TilesetTemplateSw : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val regionWidth = 48
        val regionHeight = 48
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight / 2
        val regions = ArrayList<TextureRegion>()
        regions.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                regionWidth,
                regionHeight,
                columns = column,
                rows = line
            )
        )
        var i = 0
        while (i < regions.size) {
            val drawItem = TileSampleSolo(key, set.size)
            drawItem.addTexture(regions[i])
            drawItem.addTexture(regions[i + 1])
            drawItem.addTexture(regions[i + 2])
            drawItem.addTexture(regions[i + 1])
            addNewMapWithDrawItem(drawItem)
            i += 3
        }
        return this
    }
}