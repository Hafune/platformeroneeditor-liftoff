package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.audio.SoundPath
import mygdx.game.editor.tileMapMenu.DrawLevels
import mygdx.game.tileSets.TileFriendlyData

interface IDrawItem {

    val animationDuration: Float
    var frameDuration: Float
    val imagePath: String
    var indexFromSet: Int
    var tileName: String

    var drawLevel: DrawLevels
    var icon: TextureRegion

    var soundPathForForward: SoundPath?
    var soundPathForBack: SoundPath?

    var value: String

    fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float)

    fun addTexture(region: TextureRegion)
}