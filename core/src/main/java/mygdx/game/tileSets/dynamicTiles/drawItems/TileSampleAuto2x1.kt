package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.tileSets.TileFriendlyData
import mygdx.game.tileSets.dynamicTiles.ITileSet

class TileSampleAuto2x1(
    override val imagePath: String,
    override var indexFromSet: Int,
    override var tileName: String = ITileSet.DEF_NAME
) : AbstractDrawItem() {
    private val twoFragment = TileSpriteTwoFragment()

    override fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float) {
        twoFragment.draw(batch, x, y, data.key1Left, data.key1Right, time)
    }

    override val animationDuration
        get() = twoFragment.totalDuration

    override var frameDuration: Float
        get() = twoFragment.frameDuration
        set(value) {
            twoFragment.frameDuration = value
        }

    override fun addTexture(region: TextureRegion) {
        buildIcon(region)
        val frameWidth = region.regionWidth / 4
        val frameHeight = region.regionHeight
        val column = region.regionWidth / frameWidth
        val line = region.regionHeight / frameHeight
        val frames =
            CuttingTextures.getArrayTextureRegion(region, frameWidth, frameHeight, columns = column, rows = line)
        val str1 = "1"
        val str0 = "0"
        twoFragment.addLeft(str1, frames[1])
        twoFragment.addLeft(str0, frames[0])
        twoFragment.addRight(str1, frames[2])
        twoFragment.addRight(str0, frames[3])
    }
}