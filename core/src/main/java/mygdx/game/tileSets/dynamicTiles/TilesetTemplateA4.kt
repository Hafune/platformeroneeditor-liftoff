package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.ITileSet.Companion.DEF_NAME
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleAuto4x4
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleAuto4x6

class TilesetTemplateA4 : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val width = 96
        val height = 144
        val heightFront = 96
        val column = region.regionWidth / 96
        val line = region.regionHeight / 240
        val setUp = ArrayList<Map<String, IDrawItem>>()
        val setFront = ArrayList<Map<String, IDrawItem>>()
        for (stepX in 0 until column) {
            for (stepY in 0 until line) {
                val up = TextureRegion(region, stepX * width, stepY * (height + heightFront), width, height)
                val front =
                    TextureRegion(region, stepX * width, height + stepY * (height + heightFront), width, heightFront)
                setUp.addAll(cut0(key, up))
                setFront.addAll(cut1(key, front))
            }
        }
        while (set.size < setUp.size + setFront.size) {
            val start = set.size / 2
            for (i in start until 8 + start) {
                setUp[i][DEF_NAME]!!.indexFromSet = set.size
                addNewMapWithDrawItem(setUp[i][DEF_NAME]!!)
            }
            for (i in start until 8 + start) {
                setFront[i][DEF_NAME]!!.indexFromSet = set.size
                addNewMapWithDrawItem(setFront[i][DEF_NAME]!!)
            }
        }
        setUp.clear()
        setFront.clear()
        return this
    }

    private fun cut0(path: String, region: TextureRegion): ArrayList<Map<String, IDrawItem>> {
        val set = ArrayList<Map<String, IDrawItem>>()
        val regionWidth = 96
        val regionHeight = 144
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight
        val regions =
            CuttingTextures.getArrayTextureRegion(region, regionWidth, regionHeight, columns = column, rows = line)
        for (i in regions.indices) {
            val drawItem = TileSampleAuto4x6(path, set.size)
            drawItem.addTexture(regions[i])
            set.add(HashMap<String, IDrawItem>().also { it[DEF_NAME] = drawItem })
        }
        return set
    }

    private fun cut1(path: String, region: TextureRegion): ArrayList<Map<String, IDrawItem>> {
        val set = ArrayList<Map<String, IDrawItem>>()
        val regionWidth = 96
        val regionHeight = 96
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight
        val regions =
            CuttingTextures.getArrayTextureRegion(region, regionWidth, regionHeight, columns = column, rows = line)
        for (i in regions.indices) {
            val drawItem = TileSampleAuto4x4(path, set.size)
            drawItem.addTexture(regions[i])
            set.add(HashMap<String, IDrawItem>().also { it[DEF_NAME] = drawItem })
        }
        return set
    }
}