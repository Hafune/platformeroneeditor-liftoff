package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleAuto4x4

class TilesetTemplateA3  : ITileSet{
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val regionWidth = 96
        val regionHeight = 96
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight
        val regions = CuttingTextures.getArrayTextureRegion(region, regionWidth, regionHeight, columns = column, rows = line)
        for (i in regions.indices) {
            val drawItem = TileSampleAuto4x4(key, set.size)
            drawItem.addTexture(regions[i])
            addNewMapWithDrawItem(drawItem)
        }
        return this
    }
}