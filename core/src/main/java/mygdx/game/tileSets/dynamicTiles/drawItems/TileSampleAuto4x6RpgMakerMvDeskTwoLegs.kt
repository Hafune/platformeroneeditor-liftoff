package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.tileSets.TileFriendlyData

class TileSampleAuto4x6RpgMakerMvDeskTwoLegs(
    override val imagePath: String,
    override var indexFromSet: Int,
    override var tileName: String
) :
    AbstractDrawItem(), IDrawItem {
    private val slide = MySlide()
    override fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float) {
        slide.draw(batch, x, y, time)
    }

    override val animationDuration
        get() = slide.getTotalDuration()

    override var frameDuration: Float
        get() = slide.frameDuration
        set(value) {
            slide.frameDuration = value
        }

    override fun addTexture(region: TextureRegion) {
        region.texture.textureData.prepare()
        val pixmap = region.texture.textureData.consumePixmap()

        val px = (region.texture.width * region.u).toInt()
        val py = (region.texture.height * region.v).toInt()
        val pWidth = 48
        val pHeight = 48
        val newPixmap = Pixmap(pWidth, pHeight, pixmap.format)

        newPixmap.drawPixmap(pixmap, 0, 0, px, py + 126, 24, 18)
        newPixmap.drawPixmap(pixmap, 24, 0, px + 72, py + 126, 24, 18)
        pixmap.dispose()

        val textureRegion = TextureRegion(Texture(newPixmap))
        buildIcon(textureRegion)
        newPixmap.dispose()

        slide.addTextureRegion(textureRegion)
    }
}