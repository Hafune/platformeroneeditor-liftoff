package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion

class TileSpriteFourFragment {
    private val topLeft = HashMap<String, MySlide>()
    private val topRight = HashMap<String, MySlide>()
    private val botLeft = HashMap<String, MySlide>()
    private val botRight = HashMap<String, MySlide>()
    private var curSlide: MySlide? = null
    fun addTopLeft(drawKey: String, region: TextureRegion) {
        if (!topLeft.containsKey(drawKey)) topLeft[drawKey] = MySlide()
        topLeft[drawKey]!!.addTextureRegion(region)
        curSlide = topLeft[drawKey]
    }

    fun addTopRight(drawKey: String, region: TextureRegion) {
        if (!topRight.containsKey(drawKey)) topRight[drawKey] = MySlide()
        topRight[drawKey]!!.addTextureRegion(region)
    }

    fun addBotLeft(drawKey: String, region: TextureRegion) {
        if (!botLeft.containsKey(drawKey)) botLeft[drawKey] = MySlide()
        botLeft[drawKey]!!.addTextureRegion(region)
    }

    fun addBotRight(drawKey: String, region: TextureRegion) {
        if (!botRight.containsKey(drawKey)) botRight[drawKey] = MySlide()
        botRight[drawKey]!!.addTextureRegion(region)
    }

    fun draw(
        batch: Batch,
        x: Float,
        y: Float,
        tLeft: String,
        tRight: String,
        bLeft: String,
        bRight: String,
        time: Float
    ) {
        curSlide = topLeft[tLeft]
        botLeft[bLeft]!!.draw(batch, x, y, time)
        botRight[bRight]!!.draw(batch, x + curSlide!!.getWidth(), y, time)
        topLeft[tLeft]!!.draw(batch, x, y + curSlide!!.getHeight(), time)
        topRight[tRight]!!.draw(batch, x + curSlide!!.getWidth(), y + curSlide!!.getHeight(), time)
    }

    val totalDuration: Float
        get() = curSlide!!.getTotalDuration()
    var frameDuration: Float
        get() = curSlide!!.frameDuration
        set(value) = (topLeft.values + topRight.values + botLeft.values + botRight.values).forEach {
            it.frameDuration = value
        }
}