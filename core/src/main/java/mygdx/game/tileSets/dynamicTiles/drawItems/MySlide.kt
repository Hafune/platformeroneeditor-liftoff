package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion

class MySlide {
    private var currentRegion: TextureRegion? = null

    var frameDuration = 12f / 60f

    private var animation = Animation<TextureRegion>(frameDuration).also { it.playMode = Animation.PlayMode.LOOP }
    private var originX = 0f
    private var originY = 0f

    fun setOrigin(x: Float, y: Float) {
        originX = x
        originY = y
    }

    @Deprecated("calculate array before, and use setRegions")
    fun addTextureRegion(region: TextureRegion) {
        animation =
            Animation(frameDuration, *animation.keyFrames, region).also { it.playMode = Animation.PlayMode.LOOP }
    }

    fun setRegions(vararg regions: TextureRegion) {
        animation = Animation(frameDuration, *regions).also { it.playMode = Animation.PlayMode.LOOP }
    }

    fun getWidth() = currentRegion?.regionWidth?.toFloat() ?: animation.getKeyFrame(0f).regionWidth.toFloat()
    fun getHeight() = currentRegion?.regionHeight?.toFloat() ?: animation.getKeyFrame(0f).regionWidth.toFloat()

    fun getTotalDuration() = animation.animationDuration

    fun draw(batch: Batch, x: Float, y: Float, time: Float) {
        currentRegion = animation.getKeyFrame(time)
        batch.draw(currentRegion, x - originX, y - originY)
    }
}