package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.editor.tileMapMenu.DrawLevels
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.*

class TilesetTemplate4x6 : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val regionWidth = 96
        val regionHeight = 144
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight
        val regions =
            CuttingTextures.getArrayTextureRegion(region, regionWidth, regionHeight, columns = column, rows = line)

        for (i in regions.indices) {
            if (props["$i"] == "DESK") {
                val desk = TileSampleAuto4x6RpgMakerMvDesk(key, set.size)
                val twoLegs = TileSampleAuto4x6RpgMakerMvDeskTwoLegs(key, set.size, "00-twoLegs")
                val leftLeg = TileSampleAuto4x6RpgMakerMvDeskLegsLeft(key, set.size, "01-legLeft")
                val centerLeg = TileSampleAuto4x6RpgMakerMvDeskLegsCenter(key, set.size, "02-legCenter")
                val rightLeg = TileSampleAuto4x6RpgMakerMvDeskLegsRight(key, set.size, "03-legRight")

                desk.addTexture(regions[i])
                leftLeg.addTexture(regions[i])
                centerLeg.addTexture(regions[i])
                rightLeg.addTexture(regions[i])
                twoLegs.addTexture(regions[i])

                desk.drawLevel = DrawLevels.DECORATION
                rightLeg.drawLevel = DrawLevels.DECORATION
                centerLeg.drawLevel = DrawLevels.DECORATION
                leftLeg.drawLevel = DrawLevels.DECORATION
                twoLegs.drawLevel = DrawLevels.DECORATION

                set.add(sortedMapOf<String, IDrawItem>().also {
                    it[desk.tileName] = desk
                    it[twoLegs.tileName] = twoLegs
                    it[leftLeg.tileName] = leftLeg
                    it[centerLeg.tileName] = centerLeg
                    it[rightLeg.tileName] = rightLeg
                })
            } else {
                val drawItem = TileSampleAuto4x6(key, set.size)
                drawItem.addTexture(regions[i])
                addNewMapWithDrawItem(drawItem)
            }
        }
        return this
    }
}