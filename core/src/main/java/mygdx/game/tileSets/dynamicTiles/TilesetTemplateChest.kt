package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleSolo

class TilesetTemplateChest : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val regionWidth = 48
        val regionHeight = 48
        val stepX = regionWidth * 3
        val stepY = regionHeight * 4
        val regions = ArrayList<TextureRegion>()
        for (width in 0..3) {
            for (height in 0..1) {
                regions.addAll(
                    CuttingTextures.getArrayTextureRegion(
                        region,
                        startPositionX = width * stepX,
                        startPositionY = height * stepY,
                        width = regionWidth,
                        height = regionHeight,
                        columns = 1,
                        rows = 4
                    )
                )
            }
        }
        var i = 0
        while (i < regions.size) {
            val drawItem = TileSampleSolo(key, set.size)
            drawItem.addTexture(regions[i])
            drawItem.addTexture(regions[i + 1])
            drawItem.addTexture(regions[i + 2])
            drawItem.addTexture(regions[i + 3])
            addNewMapWithDrawItem(drawItem)
            i += 4
        }
        return this
    }
}