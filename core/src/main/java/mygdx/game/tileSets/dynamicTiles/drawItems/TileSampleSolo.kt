package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.tileSets.TileFriendlyData
import mygdx.game.tileSets.dynamicTiles.ITileSet

class TileSampleSolo(
    override val imagePath: String,
    override var indexFromSet: Int,
    override var tileName: String = ITileSet.DEF_NAME
) :
    AbstractDrawItem() {
    private val slide = MySlide()

    override fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float) {
        slide.draw(batch, x, y, time)
    }

    override val animationDuration
        get() = slide.getTotalDuration()

    override var frameDuration: Float
        get() = slide.frameDuration
        set(value) {
            slide.frameDuration = value
        }

    override fun addTexture(region: TextureRegion) {
        buildIcon(region)
        slide.addTextureRegion(region)
    }
}