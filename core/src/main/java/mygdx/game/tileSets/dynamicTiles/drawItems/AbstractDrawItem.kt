package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.audio.SoundPath
import mygdx.game.editor.tileMapMenu.DrawLevels

abstract class AbstractDrawItem : IDrawItem {
    override lateinit var icon: TextureRegion
    override var drawLevel: DrawLevels = DrawLevels.FLOOR
    override var soundPathForForward: SoundPath? = null
    override var soundPathForBack: SoundPath? = null
    override var value: String = ""

    fun buildIcon(region: TextureRegion) {
        if (::icon.isInitialized) {
            return
        }
        icon = TextureRegion(region, 0, 0, 48, 48)
    }
}