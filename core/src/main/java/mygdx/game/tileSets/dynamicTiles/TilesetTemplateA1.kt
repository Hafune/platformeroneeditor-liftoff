package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleAuto2x1
import mygdx.game.tileSets.dynamicTiles.drawItems.TileSampleAuto4x6

class TilesetTemplateA1 : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val regionWidth = 96
        val regionHeight = 144
        val column = region.regionWidth / regionWidth
        val line = region.regionHeight / regionHeight
        val regions =
            CuttingTextures.getArrayTextureRegion(region, regionWidth, regionHeight, columns = column, rows = line)
        val part1 = intArrayOf(
            0, 4,
            8, 12,
            16, 20,
            24, 28
        )
        for (part in part1) {
            val drawItem = TileSampleAuto4x6(key, set.size)
            drawItem.addTexture(regions[part])
            drawItem.addTexture(regions[part + 1])
            drawItem.addTexture(regions[part + 2])
            drawItem.addTexture(regions[part + 1])
            addNewMapWithDrawItem(drawItem)
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------
        val part3 = intArrayOf(3, 11)
        for (part in part3) {
            val drawItem = TileSampleAuto4x6(key, set.size)
            drawItem.addTexture(regions[part])
            addNewMapWithDrawItem(drawItem)
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------

        val regionWidth48 = 96
        val regionHeight48 = 48
        val x0 = 96
        val y0 = 144
        val regions48 = ArrayList<TextureRegion>()
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 7,
                startPositionY = 0,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 7,
                startPositionY = y0,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 3,
                startPositionY = y0 * 2,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 3,
                startPositionY = y0 * 3,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 7,
                startPositionY = y0 * 2,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        regions48.addAll(
            CuttingTextures.getArrayTextureRegion(
                region,
                startPositionX = x0 * 7,
                startPositionY = y0 * 3,
                width = regionWidth48,
                height = regionHeight48,
                columns = 1,
                rows = 3
            )
        )
        var i = 0
        while (i < regions48.size) {
            val drawItem = TileSampleAuto2x1(key, set.size)
            drawItem.addTexture(regions48[i])
            drawItem.addTexture(regions48[i + 1])
            drawItem.addTexture(regions48[i + 2])
            addNewMapWithDrawItem(drawItem)
            i += 3
        }
        return this
    }
}