package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.CuttingTextures
import mygdx.game.tileSets.TileFriendlyData
import mygdx.game.tileSets.dynamicTiles.ITileSet.Companion.DEF_NAME

class TileSampleAuto4x6RpgMakerMvDesk(
    override val imagePath: String, override var indexFromSet: Int,
    override var tileName: String = DEF_NAME
) : AbstractDrawItem(), IDrawItem {
    private val fragmentFour = TileSpriteFourFragment()
    override fun draw(batch: Batch, x: Float, y: Float, data: TileFriendlyData, time: Float) {
        fragmentFour.draw(batch, x, y, data.key3TopLeft, data.key3TopRight, data.key3BotLeft, data.key3BotRight, time)
    }

    override val animationDuration
        get() = fragmentFour.totalDuration

    override var frameDuration: Float
        get() = fragmentFour.frameDuration
        set(value) {
            fragmentFour.frameDuration = value
        }

    override fun addTexture(region: TextureRegion) {
        buildIcon(region)
        val frameWidth = region.regionWidth / 4
        val frameHeight = region.regionHeight / 6
        val column = region.regionWidth / frameWidth
        val line = region.regionHeight / frameHeight

        region.texture.textureData.prepare()
        val pixmap = region.texture.textureData.consumePixmap()

        val px = (region.texture.width * region.u).toInt()
        val py = (region.texture.height * region.v).toInt()
        val pWidth = 96
        val pHeight = 144
        val newPixmap = Pixmap(pWidth, pHeight, pixmap.format)

        newPixmap.drawPixmap(pixmap, 0, 0, px, py, pWidth, pHeight)
        newPixmap.drawPixmap(pixmap, 0, 120, px, py + 72, pWidth, 24)
        newPixmap.drawPixmap(pixmap, 0, 138, px, py + 120, pWidth, 6)
        newPixmap.drawPixmap(pixmap, 48, 24, px + 24, py + 96, 48, 24)
        newPixmap.drawPixmap(pixmap, 48, 42, px + 48, py + 24, 48, 6)
        pixmap.dispose()

        val textureRegion = TextureRegion(Texture(newPixmap))
        newPixmap.dispose()

        val frames =
            CuttingTextures.getArrayTextureRegion(textureRegion, frameWidth, frameHeight, columns = column, rows = line)
        val str0 = "0"
        val str1 = "1"
        fragmentFour.addTopLeft(str0 + str0 + str0, frames[8])
        fragmentFour.addTopLeft(str1 + str0 + str0, frames[10])
        fragmentFour.addTopLeft(str0 + str0 + str1, frames[16])
        fragmentFour.addTopLeft(str1 + str1 + str1, frames[18])
        fragmentFour.addTopLeft(str1 + str0 + str1, frames[2])
        fragmentFour.addTopRight(str0 + str0 + str0, frames[11])
        fragmentFour.addTopRight(str0 + str0 + str1, frames[9])
        fragmentFour.addTopRight(str1 + str0 + str0, frames[19])
        fragmentFour.addTopRight(str1 + str1 + str1, frames[17])
        fragmentFour.addTopRight(str1 + str0 + str1, frames[3])
        fragmentFour.addBotLeft(str0 + str0 + str0, frames[20])
        fragmentFour.addBotLeft(str0 + str0 + str1, frames[22])
        fragmentFour.addBotLeft(str1 + str0 + str0, frames[12])
        fragmentFour.addBotLeft(str1 + str1 + str1, frames[14])
        fragmentFour.addBotLeft(str1 + str0 + str1, frames[6])
        fragmentFour.addBotRight(str0 + str0 + str0, frames[23])
        fragmentFour.addBotRight(str1 + str0 + str0, frames[21])
        fragmentFour.addBotRight(str0 + str0 + str1, frames[15])
        fragmentFour.addBotRight(str1 + str1 + str1, frames[13])
        fragmentFour.addBotRight(str1 + str0 + str1, frames[7])
    }
}