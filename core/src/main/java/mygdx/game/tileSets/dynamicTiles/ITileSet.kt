package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

interface ITileSet {

    companion object {
        const val DEF_NAME = "0"
    }

    val set: ArrayList<Map<String, IDrawItem>>

    fun addNewMapWithDrawItem(drawItem: IDrawItem) {
        set.add(HashMap<String, IDrawItem>().also { it[drawItem.tileName] = drawItem })
    }

    fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet
}