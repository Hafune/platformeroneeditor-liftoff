package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.ITileSet.Companion.DEF_NAME
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

class TilesetTemplateTwoHalfSolo : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        val half0 = TextureRegion(region, 0, 0, region.regionWidth / 2, region.regionHeight)
        val half1 = TextureRegion(region, region.regionWidth / 2, 0, region.regionWidth / 2, region.regionHeight)
        set.addAll(TilesetTemplateSolo().initialize(key, half0, props).set)
        set.addAll(TilesetTemplateSolo().initialize(key, half1, props).set)
        set.forEachIndexed { index, iDrawItem ->
            iDrawItem[DEF_NAME]!!.indexFromSet = index
        }
        return this
    }
}