package mygdx.game.tileSets.dynamicTiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import mygdx.game.lib.MyProperties
import mygdx.game.tileSets.dynamicTiles.drawItems.IDrawItem

class TilesetTemplateA5 : ITileSet {
    override val set = ArrayList<Map<String, IDrawItem>>()

    override fun initialize(key: String, region: TextureRegion, props: MyProperties): ITileSet {
        set.addAll(TilesetTemplateSolo().initialize(key, region, props).set)
        return this
    }
}