package mygdx.game.tileSets.dynamicTiles.drawItems

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion

class TileSpriteTwoFragment {
    private val slideLeft = HashMap<String, MySlide>()
    private val slideRight = HashMap<String, MySlide>()
    private var curSlide: MySlide? = null
    fun addLeft(key: String, region: TextureRegion) {
        if (!slideLeft.containsKey(key)) slideLeft[key] = MySlide()
        slideLeft[key]!!.addTextureRegion(region)
        curSlide = slideLeft[key]
    }

    fun addRight(key: String, region: TextureRegion) {
        if (!slideRight.containsKey(key)) slideRight[key] = MySlide()
        slideRight[key]!!.addTextureRegion(region)
    }

    fun draw(batch: Batch, x: Float, y: Float, left: String, right: String, time: Float) {
        curSlide = slideLeft[left]
        slideLeft[left]!!.draw(batch, x, y, time)
        slideRight[right]!!.draw(batch, x + curSlide!!.getWidth(), y, time)
    }

    val totalDuration: Float
        get() = curSlide!!.getTotalDuration()
    var frameDuration: Float
        get() = curSlide!!.frameDuration
        set(value) = (slideLeft.values + slideRight.values).forEach {
            it.frameDuration = value
        }
}