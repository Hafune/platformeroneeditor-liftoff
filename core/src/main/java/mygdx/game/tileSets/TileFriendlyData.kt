package mygdx.game.tileSets

class TileFriendlyData {
    var command = ""

    var left = false
    var topLeft = false
    var top = false
    var topRight = false
    var right = false
    var botRight = false
    var bot = false
    var botLeft = false

    var key3TopLeft = "000"
        private set
    var key3TopRight = "000"
        private set
    var key3BotRight = "000"
        private set
    var key3BotLeft = "000"
        private set

    var key2TopLeft = "00"
        private set
    var key2TopRight = "00"
        private set
    var key2BotRight = "00"
        private set
    var key2BotLeft = "00"
        private set

    var key1Left = "0"
        private set
    var key1Right = "0"
        private set

    fun setValue(s: String) {
        left = s[0] == '1'
        topLeft = s[1] == '1'
        top = s[2] == '1'
        topRight = s[3] == '1'
        right = s[4] == '1'
        botRight = s[5] == '1'
        bot = s[6] == '1'
        botLeft = s[7] == '1'

        key3TopLeft = s.substring(0, 3)
        key3TopRight = s.substring(2, 5)
        key3BotRight = s.substring(4, 7)
        key3BotLeft = s.substring(6, 8) + s.substring(0, 1)

        key2TopLeft = s.substring(0, 1) + s.substring(2, 3)
        key2TopRight = s.substring(2, 3) + s.substring(4, 5)
        key2BotRight = s.substring(4, 5) + s.substring(6, 7)
        key2BotLeft = s.substring(6, 7) + s.substring(0, 1)

        key1Left = s.substring(0, 1)
        key1Right = s.substring(4, 5)
    }

    fun getValue(): String {
        command = ""
        listOf(
            left,
            topLeft,
            top,
            topRight,
            right,
            botRight,
            bot,
            botLeft,
        ).forEach { b -> command += if (b) "1" else "0" }
        return command
    }
}