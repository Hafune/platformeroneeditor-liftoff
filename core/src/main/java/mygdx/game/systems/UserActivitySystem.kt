package mygdx.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.remove
import mygdx.game.components.UserActivityComponent
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.TargetComponent

class UserActivitySystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var removeTargets: ImmutableArray<Entity>? = null
    private var removeMapPoint: ImmutableArray<Entity>? = null
    private var activities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        removeTargets = engine.getEntitiesFor(
            allOf(UserActivityComponent::class, TargetComponent::class).get()
        )
        removeMapPoint = engine.getEntitiesFor(
            allOf(UserActivityComponent::class, MapPointComponent::class).get()
        )
        activities = engine.getEntitiesFor(
            allOf(UserActivityComponent::class).get()
        )
    }

    override fun update(deltaTime: Float) {
        removeTargets!!.forEach { it.remove<TargetComponent>() }
        removeMapPoint!!.forEach { it.remove<MapPointComponent>() }
        activities!!.forEach { it.remove<UserActivityComponent>() }
    }
}