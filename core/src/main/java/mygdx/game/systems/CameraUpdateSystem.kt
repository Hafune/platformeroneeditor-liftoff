package mygdx.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.TileMapRender
import mygdx.game.components.CameraTargetComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.lib.MyMath
import kotlin.math.min

class CameraUpdateSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private val camera = myEngine.screenContainer.camera
    private val tileMapRender = myEngine.screenContainer.tileMapRender

    private lateinit var entities: ImmutableArray<Entity>
    var zoom = 1f
    var l = 0f

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(CameraTargetComponent::class, PositionComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        if (entities.size() == 0) return

        var sumX = 0f
        var sumY = 0f
        val currentVector = entities.fold(Vector2()) { vec, value ->
            val position = value.get<PositionComponent>()!!
            val weight = value.get<CameraTargetComponent>()!!.weight
            vec.x += position.x * weight
            vec.y += position.y * weight
            sumX += weight
            sumY += weight
            vec
        }
        currentVector.x = currentVector.x / sumX * TileMapRender.TILE_WIDTH
        currentVector.y = currentVector.y / sumY * TileMapRender.TILE_HEIGHT

        val cameraWidth = if (myEngine.editorMode) 0f else camera.viewportWidth * camera.zoom
        val cameraHeight = if (myEngine.editorMode) 0f else camera.viewportHeight * camera.zoom

        val vector =
            calcInnerCameraPosition(
                currentVector,
                cameraWidth,
                cameraHeight,
                tileMapRender.width,
                tileMapRender.height
            )

        val x = MyMath.tendsToValue(camera.position.x, vector.x, 1f, 5.88f * deltaTime)
        val y = MyMath.tendsToValue(camera.position.y, vector.y, 1f, 5.88f * deltaTime)

        camera.position.x = x
        camera.position.y = y

        camera.zoom = MyMath.tendsToValue(camera.zoom, zoom, .58f * deltaTime, .1f)
    }

    fun calcInnerCameraPosition(
        currentPosition: Vector2,
        cameraWidth: Float,
        cameraHeight: Float,
        sceneWidth: Float,
        sceneHeight: Float
    ): Vector2 {
        val vector = Vector2(currentPosition)
        val freeSpace = Rectangle()

        freeSpace.x = min(cameraWidth, sceneWidth) / 2
        freeSpace.y = min(cameraHeight, sceneHeight) / 2

        freeSpace.width = sceneWidth - freeSpace.x * 2
        freeSpace.height = sceneHeight - freeSpace.y * 2

        if (vector.x < freeSpace.x) vector.x = freeSpace.x
        if (vector.y < freeSpace.y) vector.y = freeSpace.y

        if (vector.x > freeSpace.x + freeSpace.width) vector.x = freeSpace.x + freeSpace.width
        if (vector.y > freeSpace.y + freeSpace.height) vector.y = freeSpace.y + freeSpace.height

        return vector
    }
}