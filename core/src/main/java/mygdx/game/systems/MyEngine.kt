package mygdx.game.systems

import com.badlogic.ashley.core.PooledEngine
import ktx.ashley.exclude
import mygdx.game.ScreenContainer
import mygdx.game.assetsClasses.GraphicResources
import mygdx.game.audio.AudioProperties
import mygdx.game.audio.MusicPlayer
import mygdx.game.audio.SoundPlayer
import mygdx.game.characterSkins.SkinStorage
import mygdx.game.components.IgnoreDisposeSceneComponent
import mygdx.game.dataBase.DatabaseServices
import mygdx.game.editor.widgetEditor.ActorBuilder
import mygdx.game.icons.IconStorage
import mygdx.game.inputs.MyInputHandler
import mygdx.game.inventory.Inventory
import mygdx.game.lib.PositionsUiStorage
import mygdx.game.loadSave.UserData
import mygdx.game.loadSave.UserDataLoader
import mygdx.game.location.chests.LocationsChests
import mygdx.game.location.teleports.LocationsTeleports
import mygdx.game.myWidgets.InteractiveWidgetFactory
import mygdx.game.systems.gameWorld.ComponentInitializer
import mygdx.game.systems.gameWorld.TimerSystem
import mygdx.game.uiSkins.Drawables
import mygdx.game.uiSkins.Images
import mygdx.game.uiSkins.UiSkins
import java.lang.Float.max
import kotlin.math.min

class MyEngine(
    inputHandler: MyInputHandler,
    val skinStorage: SkinStorage,
    val iconStorage: IconStorage,
    val db: DatabaseServices,
    val graphicResources: GraphicResources,
    val restart: () -> Unit,
) : MyAbstractSystem(PooledEngine()) {

    var editorMode = false
    var curFrameTime = 0f
        private set
    var timeScale = 1f
        set(value) {
            field = max(0f, min(1f, value))
        }
    private var lastTimeScale = timeScale
    private var needCleanSystems: (() -> Unit)? = null
    var loading = false
    var sceneAlive = true
        private set

    val myBox2dHandler = MyBox2dHandler(this)
    val screenContainer = ScreenContainer(this)
    var myInputHandler = inputHandler.also { it.changeStage(screenContainer.stage) }

    val skin = UiSkins.skin
    val widgets = InteractiveWidgetFactory(this, skin)
    val drawables = Drawables(skin)
    val guiImages = Images()
    val actorBuilder = ActorBuilder(skin)
    val positionsUiStorage = PositionsUiStorage()

    val worldProcessingSystem = WorldProcessingSystem(this)
    private val timerSystem = TimerSystem(this)
    val cameraUpdateSystem = CameraUpdateSystem(this)
    private val cursorSystem = CursorSystem(this)

    var userData = UserData(this)
    var userInventory = Inventory(this)
    var locationsChests = LocationsChests(this)
    var audioProperties = AudioProperties(this)
    val musicPlayer = MusicPlayer(audioProperties)
    val soundPlayer = SoundPlayer(audioProperties)
    val locationsTeleports = LocationsTeleports()
    val userDataLoader = UserDataLoader(this)
    private val componentInitializer = ComponentInitializer(this)

    override fun update(deltaTime: Float) {
        if (needCleanSystems != null) {
            myBox2dHandler.cleanWorld()
            engine.removeAllEntities(exclude(IgnoreDisposeSceneComponent::class).get())
            needCleanSystems!!.invoke()
            needCleanSystems = null
            sceneAlive = true
        } else {
            if (lastTimeScale != 1f) {
                timeScale = 1f
            }
            lastTimeScale = timeScale
            val dTime = deltaTime * timeScale
            curFrameTime += dTime
            engine.update(dTime)
            myBox2dHandler.update(dTime)

        }
    }

    init {
        myBox2dHandler.gameCamera = screenContainer.camera

        addSystem(worldProcessingSystem)
        addSystem(timerSystem)
        addSystem(cameraUpdateSystem)
//        addSystem(FightSystem.getInstance())
        addSystem(cursorSystem)
    }

    fun beginDisposeScene(callback: () -> Unit) {
        needCleanSystems = callback
        sceneAlive = false
    }
}