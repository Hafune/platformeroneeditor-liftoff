package mygdx.game.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.PooledEngine

abstract class MyAbstractSystem(open val engine: PooledEngine) : EntitySystem() {

    private val subSystems = ArrayList<MyAbstractSystem>()

    fun addSystem(system: MyAbstractSystem) {
        subSystems.add(system)
        engine.addSystem(system)
    }

    override fun setProcessing(processing: Boolean) {
        super.setProcessing(processing)
        subSystems.forEach { it.setProcessing(processing) }
    }
}