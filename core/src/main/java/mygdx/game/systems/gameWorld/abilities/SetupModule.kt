package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.Entity
import ktx.ashley.get
import mygdx.game.characterSkins.MotionNames
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent

class SetupModule(val system: IAbilitySystem, private var defaultMotion: String = MotionNames.STAND) {

    fun setup(entity: Entity) {
        entity.get<AnimationSkinComponent>()?.changeMotion(defaultMotion)
    }

    fun complete(entity: Entity) {
        entity.get<AbilityComponent>()!!.cancelable = true
    }
}
