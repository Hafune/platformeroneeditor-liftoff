package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.math.Interpolation
import ktx.ashley.allOf
import mygdx.game.characterSkins.MotionNames
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.lib.MyVector
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView
import kotlin.math.abs

@Ability
class DashAbilitySystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine), IAbilitySystem {

    private var entities: ImmutableArray<Entity>? = null
    private val interpolation = Interpolation.circleOut
    private val setupModule = SetupModule(this, MotionNames.DASH)

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(
            allOf(
                BodyComponent::class,
                AbilityComponent::class,
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        if (myEngine.editorMode) return

        cont@ for (entity in entities!!) {
            val view = WorldCharacterView(entity)
            if (!equalAbility(view.ability())) {
                continue@cont
            }

            if (view.ability().time == 0f) {
                setup(entity)
            }

            view.body().getBody().linearDamping = 0f
            view.animation().animationTime

            val time = -1 + view.animation().animationTime / 1 * 2

            view.animation().offsetY =
                if (time < 0) interpolation.apply(1 - abs(time)) else interpolation.apply(1 - abs(time))
            view.animation().offsetY *= 16f

            if (view.animation().animationTime in 0.7..1.0) {
                val interval = (.3f - (1 - view.animation().animationTime)) / 10
                view.body().getBody().linearDamping = view.body().defLinearDamping * interval
            }
            if (view.animation().animationTime >= 1f) {
                setupModule.complete(entity)
            }
        }
    }

    private fun setup(entity: Entity) {
        setupModule.setup(entity)
        val character = WorldCharacterView(entity)
        val dashImp = MyVector(200f, 0f).apply { angle = character.position().angle }

        character.body().getBody().applyForceToCenter(dashImp.x, dashImp.y, true)
    }
}