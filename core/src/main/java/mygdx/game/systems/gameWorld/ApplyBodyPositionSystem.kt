package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.systems.MyAbstractSystem

class ApplyBodyPositionSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities =
            engine.getEntitiesFor(
                allOf(
                    BodyComponent::class,
                    PositionComponent::class,
                ).get()
            )
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            val bodyComp = entity.get<BodyComponent>()!!
            val body = bodyComp.getBody()
            val pc = entity.get<PositionComponent>()!!

            if (bodyComp.lastFixedPositionX != pc.x) {
                body.setTransform(pc.x, body.position.y, 0f)
            }
            if (bodyComp.lastFixedPositionY != pc.y) {
                body.setTransform(body.position.x, pc.y, 0f)
            }
            pc.x = body.position.x
            pc.y = body.position.y
            bodyComp.lastFixedPositionX = pc.x
            bodyComp.lastFixedPositionY = pc.y

            bodyComp.movingForce.value = 0f
            bodyComp.movingForce.angle = pc.angle

            bodyComp.setupDefaults()
        }
    }
}