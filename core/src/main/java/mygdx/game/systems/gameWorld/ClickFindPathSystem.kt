package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.ClickOnScreenComponent
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.lib.FindRoute
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.views.WorldCharacterView

class ClickFindPathSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var users: ImmutableArray<Entity>? = null
    private var clicks: ImmutableArray<Entity>? = null
    val characterView = WorldCharacterView()

    override fun addedToEngine(engine: Engine) {
        users = engine.getEntitiesFor(allOf(UserControllerComponent::class, PositionComponent::class).get())
        clicks = engine.getEntitiesFor(allOf(ClickOnScreenComponent::class).get())
    }

    override fun update(deltaTime: Float) {

        for (clickEntity in clicks!!) {
            val click: ClickOnScreenComponent = clickEntity.get()!!

            for (entity in users!!) {
                characterView.entity = entity

                val route =
                    FindRoute.findRoute(
                        characterView.position().getWorldX(),
                        characterView.position().getWorldY(),
                        click.x.toInt(),
                        click.y.toInt(),
                        myEngine.screenContainer.tileMapRender
                    )

                entity.add(MapPointComponent().also {
                    it.route = route
                    it.cellX = click.x.toInt()
                    it.cellY = click.y.toInt()
                })
                entity.get<AbilityComponent>()!!.setAbility(MovingAbilitySystem::class)
            }
        }
    }

    fun addClick(x: Float, y: Float) {
        if (myEngine.editorMode) return

        if (!checkProcessing()) {
            return
        }
        engine.addEntity(Entity().also { it.add(ClickOnScreenComponent(x, y)) })
    }
}