package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.AnimationImageComponent
import mygdx.game.systems.MyAbstractSystem

class ImageAnimationSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        val family = allOf(AnimationImageComponent::class).get()
        entities =
            engine.getEntitiesFor(family)
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            val animationSkinComponent = entity.get<AnimationImageComponent>()!!
            animationSkinComponent.time += deltaTime
        }
        for (entity in entities!!) {
            val anim = entity.get<AnimationImageComponent>()!!

            if (anim.animation.isAnimationFinished(anim.time)) {
                anim.callback?.invoke()
            }
        }
    }
}