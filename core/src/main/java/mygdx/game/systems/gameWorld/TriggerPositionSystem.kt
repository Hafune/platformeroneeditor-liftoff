package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.NameComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.TriggerPositionComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class TriggerPositionSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var entities: ImmutableArray<Entity>? = null
    private var targets: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(TriggerPositionComponent::class).get())
        targets = engine.getEntitiesFor(allOf(PositionComponent::class, NameComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        contEntity@ for (e in 0 until entities!!.size()) {
            val entity = entities!![e]
            val detector = entity.get<TriggerPositionComponent>()!!
            if (detector.locationName != myEngine.screenContainer.tileMapRender.locationName) {
                continue@contEntity
            }
            contTarget@ for (i in 0 until targets!!.size()) {
                val target = targets!![i]
                val name = target.get<NameComponent>()!!.name
                if (detector.targetName != null && name != detector.targetName) {
                    continue@contTarget
                }

                val position = target.get<PositionComponent>()!!

                if (detector.rect.contains(
                        position.x,
                        position.y
                    )
                ) {
                    if (myEngine.sceneAlive) {
                        detector.callback?.invoke()
                        entity.remove<TriggerPositionComponent>()
                    }
                }
            }
        }
    }
}