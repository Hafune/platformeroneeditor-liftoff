package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.SlowDownComponent
import mygdx.game.systems.MyAbstractSystem

class SlowDownSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var slowDownEntities: ImmutableArray<Entity>? = null
    private var movementEntities: ImmutableArray<Entity>? = null
    override fun addedToEngine(engine: Engine) {
        slowDownEntities = engine.getEntitiesFor(
            allOf(SlowDownComponent::class, MapPositionComponent::class).get()
        )

        movementEntities = engine.getEntitiesFor(
            allOf(PositionComponent::class, BodyComponent::class).get()
        )
    }

    override fun update(deltaTime: Float) {
        for (slowDownEntity in slowDownEntities!!) {
            for (movementEntity in movementEntities!!) {

                val mapPosition = slowDownEntity.get<MapPositionComponent>()!!
                val slowDown = slowDownEntity.get<SlowDownComponent>()!!
                val movement = movementEntity.get<PositionComponent>()!!
                val body = movementEntity.get<BodyComponent>()!!
                with(mapPosition) {
                    if (worldX == movement.getWorldX() && worldY == movement.getWorldY()) {
                        body.acceleration = body.acceleration * slowDown.slowPercent
                    }
                }
            }
        }
    }
}