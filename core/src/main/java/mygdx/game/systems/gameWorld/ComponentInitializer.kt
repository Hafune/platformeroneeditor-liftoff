package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.physics.box2d.BodyDef
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.actions.ActionContext
import mygdx.game.actions.BuildAction
import mygdx.game.components.PlatformComponent
import mygdx.game.components.gameWorld.AttachOffsetComponent
import mygdx.game.components.gameWorld.BuildActionComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.characterComponents.*
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.components.gameWorld.tileComponents.TileFrame
import mygdx.game.components.gameWorld.tileComponents.WallComponent
import mygdx.game.lib.TextureStorage
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView
import mygdx.game.views.TileView

class ComponentInitializer(private val myEngine: MyEngine) {

    private val engine = myEngine.engine

    open class MyListener : EntityListener {
        override fun entityAdded(entity: Entity) {

        }

        override fun entityRemoved(entity: Entity) {

        }

    }

    init {

        engine.addEntityListener(
            allOf(ShadowComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    entity.get<ShadowComponent>()!!.shadow =
                        Sprite(TextureStorage["characterSkins/shadow.png"]).also { sprite ->
                            sprite.setOriginCenter()
                            sprite.setAlpha(.4f)
                        }
                }
            })

        engine.addEntityListener(
            allOf(PlatformComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    entity.get<PlatformComponent>()!!.validPosition = false
                }
            })

        engine.addEntityListener(
            allOf(IconComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val view = WorldCharacterView().also { it.entity = entity }
                    with(view.icon()) {
                        if (icon_id > 0) {
                            setIcon(myEngine.iconStorage.buildById(icon_id))
                        }
                    }
                }
            })

        engine.addEntityListener(
            allOf(AnimationSkinComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val view = WorldCharacterView().also { it.entity = entity }
                    with(view.animation()) {
                        skinStorage = myEngine.skinStorage
                        setSkin(skinStorage.getById(skin_id))
                        changeMotion(startMotion)
                    }
                }
            })

        engine.addEntityListener(
            allOf(BodyComponent::class, AnimationSkinComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val view = WorldCharacterView().also { it.entity = entity }
                    if (view.body().myBody == null) {
                        view.body().myBody =
                            myEngine.myBox2dHandler.createBodyFromSkinAndApplyOrigins(
                                view.animation().skin,
                                view.body().collisionBehavior
                            ).first
                        view.body().radius = view.animation().skin.radius
                        view.body().getBody().type = BodyDef.BodyType.valueOf(view.body().bodyType)
                    }
                }
            })

        engine.addEntityListener(
            allOf(BodyComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val view = WorldCharacterView().also { it.entity = entity }
                    if (view.body().myBody != null) {
                        view.body().getBody().type = BodyDef.BodyType.valueOf(view.body().bodyType)
                    }
                }
            })

        engine.addEntityListener(
            allOf(BodyComponent::class, PositionComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val view = WorldCharacterView().also { it.entity = entity }
                    if (view.body().myBody != null) {
                        view.body().getBody().setTransform(
                            view.position().x,
                            view.position().y,
                            0f
                        )
                        view.body().lastFixedPositionX = view.position().x
                        view.body().lastFixedPositionY = view.position().y
                    }
                }

                override fun entityRemoved(entity: Entity) {
                    if (entity.get<BodyComponent>()!!.myBody != null) {
                        myEngine.myBox2dHandler.destroyBody(entity.get<BodyComponent>()!!.getBody())
                        entity.get<BodyComponent>()!!.myBody = null
                    }
                }
            })

        engine.addEntityListener(
            allOf(AttachOffsetComponent::class, BodyComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    val offset = entity.get<AttachOffsetComponent>()!!
                    if (!offset.attached) {
                        offset.attached = true
                        val body = entity.get<BodyComponent>()!!.getBody()
                        offset.x = body.position.x - offset.attachBody.getBody().position.x
                        offset.y = body.position.y - offset.attachBody.getBody().position.y
                    }
                }
            })

        engine.addEntityListener(
            allOf(TileComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    with(entity.get<TileComponent>()!!) {
                        tileFriendly.setValue(getTileFriendlyValue())
                        setFrameValue(frameState)
                        setDrawItem(myEngine.graphicResources.getDrawItem(drawItemValue))
                    }
                }
            })

        engine.addEntityListener(
            allOf(
                MapPositionComponent::class,
                WallComponent::class,
            ).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    with(entity.get<MapPositionComponent>()!!)
                    {
                        myEngine.screenContainer.tileMapRender.mapBlockState.refreshCell(
                            worldX,
                            worldY
                        )
                    }
                }

                override fun entityRemoved(entity: Entity) {
                    with(entity.get<MapPositionComponent>()!!)
                    {
                        myEngine.screenContainer.tileMapRender.mapBlockState.refreshCell(
                            worldX,
                            worldY
                        )
                    }
                }
            })


        engine.addEntityListener(
            allOf(
                TileComponent::class,
                InteractComponent::class,
            ).get(),
            object : MyListener() {
                private var view = TileView()
                override fun entityAdded(entity: Entity) {
                    view.entity = entity
                    if (view.interact().fileName == "actions/presets/OpenChest.json" && myEngine.locationsChests.hasOpenChest(
                            view.name().name.toInt()
                        )
                    ) {
                        view.tile().setFrameValue(TileFrame.LAST_FRAME)
                    }
                    if (view.interact().fileName == "actions/presets/DoorSwitch.json" &&
                        myEngine.userData.doors.contains(view.name().name.toInt())
                    ) {
                        view.tile().setFrameValue(TileFrame.LAST_FRAME)
                        entity.remove<WallComponent>()
                    }
                }
            })



        engine.addEntityListener(
            allOf(BuildActionComponent::class).get(),
            object : MyListener() {
                override fun entityAdded(entity: Entity) {
                    if (myEngine.editorMode) return

                    BuildAction().also {
                        it.myEngine = myEngine
                        it.fileName = entity.get<BuildActionComponent>()!!.fileName
                        it.actionContext = ActionContext()
                        it.interactEntity = entity
                    }.start()
                }
            })
    }
}
