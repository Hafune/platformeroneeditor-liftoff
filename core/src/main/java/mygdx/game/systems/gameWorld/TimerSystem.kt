package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.TimerComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class TimerSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(TimerComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            with(entity.get<TimerComponent>()!!) {
                time -= deltaTime
                if (time <= 0) {
                    if (myEngine.sceneAlive) {
                        entity.remove<TimerComponent>()
                        callback?.invoke()
                    }
                }
            }
        }
    }
}