package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.inputs.MyPadButtons
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem

class InteractButtonSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var users: ImmutableArray<Entity>? = null
    private var targets: ImmutableArray<Entity>? = null
    private var tileTargets: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        users = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                UserControllerComponent::class,
                AnimationSkinComponent::class,
                AbilityComponent::class
            ).get()
        )

        targets = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                InteractComponent::class,
            ).exclude(UserControllerComponent::class).get()
        )

        tileTargets = engine.getEntitiesFor(
            allOf(
                TileComponent::class,
                InteractComponent::class,
                MapPositionComponent::class,
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        cont@ for (entity in users!!) {
            val controller = entity.get<UserControllerComponent>()!!
            val position = entity.get<PositionComponent>()!!
            val animation = entity.get<AnimationSkinComponent>()!!
            val ability = entity.get<AbilityComponent>()!!

            if (!ability.equalAbility(StandAbilitySystem::class)
                && !ability.equalAbility(MovingAbilitySystem::class)
            ) {
                continue@cont
            }
            if (controller.justPressedButtons[MyPadButtons.CROSS]!!) {
                val npc = getCharacterInRadiusAndAngle(
                    position.x,
                    position.y,
                    animation.direction,
                )
                if (npc != null) {
                    val interactComponent = npc.get<InteractComponent>()!!
                    entity.get<AbilityComponent>()!!.setAbility(StandAbilitySystem::class)
                    interactComponent.interact(npc, myEngine)
                } else {
                    val tile = getTileTargetInRadiusAndAngle(
                        position.x,
                        position.y,
                        animation.direction,
                    )
                    if (tile != null) {
                        val interactComponent = tile.get<InteractComponent>()!!
                        entity.get<AbilityComponent>()!!.setAbility(StandAbilitySystem::class)
                        //TODO реализовать поворот к цели
                        //animation.setDirectionToward(chestPosition.positionX, chestPosition.positionY)

                        interactComponent.interact(tile, myEngine)
                    }
                }
            }
        }
    }

    private fun getCharacterInRadiusAndAngle(
        cx: Float,
        cy: Float,
        directionAngle: Float,
    ): Entity? {
        val radius = 1f
        val searchingAngle = 60f

        return targets!!.filter {
            MyMath.getDistance(
                it.get<PositionComponent>()!!.x,
                it.get<PositionComponent>()!!.y,
                cx,
                cy
            ) <= radius
        }.filter {
            MyMath.getDifferentFromAngle(
                MyMath.normalizeAngle(
                    MyMath.getAngleDeg(
                        cx,
                        cy,
                        it.get<PositionComponent>()!!.x,
                        it.get<PositionComponent>()!!.y,
                    ) - directionAngle
                ),
                0f
            ) < searchingAngle
        }.minByOrNull {
            MyMath.getDifferentFromAngle(
                MyMath.getAngleDeg(
                    cx, cy,
                    it.get<PositionComponent>()!!.x,
                    it.get<PositionComponent>()!!.y,
                ),
                directionAngle
            )
        }
    }

    private fun getTileTargetInRadiusAndAngle(
        cx: Float,
        cy: Float,
        directionAngle: Float,
    ): Entity? {
        val radius = 2f
        val searchingAngle = 60f

        return tileTargets!!.filter {
            MyMath.getDistance(
                it.get<MapPositionComponent>()!!.x,
                it.get<MapPositionComponent>()!!.y,
                cx,
                cy
            ) <= radius
        }.filter {
            MyMath.getDifferentFromAngle(
                MyMath.normalizeAngle(
                    MyMath.getAngleDeg(
                        cx,
                        cy,
                        it.get<MapPositionComponent>()!!.x,
                        it.get<MapPositionComponent>()!!.y,
                    ) - directionAngle
                ),
                0f
            ) < searchingAngle
        }.minByOrNull {
            MyMath.getDifferentFromAngle(
                MyMath.getAngleDeg(
                    cx, cy,
                    it.get<MapPositionComponent>()!!.x,
                    it.get<MapPositionComponent>()!!.y
                ),
                directionAngle
            )
        }
    }
}