package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.systems.MyAbstractSystem

class AbilityTimeIncrementSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(AbilityComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            entity.get<AbilityComponent>()!!.time += deltaTime
        }
    }
}