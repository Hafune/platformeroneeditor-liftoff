package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.RepulsiveForceComponent
import mygdx.game.lib.MyMath
import mygdx.game.lib.MyVector
import mygdx.game.systems.MyAbstractSystem

class RepulsiveForceSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(
            allOf(
                AnimationSkinComponent::class,
                BodyComponent::class,
                PositionComponent::class,
                RepulsiveForceComponent::class
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        for (index in 0 until entities!!.size()) {
            calculate(entities!![index])
        }
    }

    private fun calculate(entity0: Entity) {
        val skin = entity0.get<AnimationSkinComponent>()!!.skin
        val body = entity0.get<BodyComponent>()!!.getBody()
        val position0 = entity0.get<PositionComponent>()!!
        for (entity1 in entities!!) {
            val position1 = entity1.get<PositionComponent>()!!
            if (position0 !== position1) {
                val collisionValue = 2f
                val vector = MyVector()
                if (MyMath.getDistance(
                        position0.x,
                        position0.y,
                        position1.x,
                        position1.y
                    ) < skin.radius
                ) {
                    if (position0.x == position1.x && position0.y == position1.y) {
                        vector.value = Math.random().toFloat()
                        vector.angle = (Math.random() * 360).toFloat()
                    } else {
                        vector.angle =
                            MyMath.getAngleDeg(
                                position1.x,
                                position1.y,
                                position0.x,
                                position0.y
                            )
                        vector.value =
                            collisionValue * ((skin.radius * 2 - MyMath.getDistance(
                                position0.x,
                                position0.y,
                                position1.x,
                                position1.y
                            )) / skin.radius)
                    }
                }
                if (vector.value > collisionValue) {
                    vector.value = collisionValue * 15f
                }
                body.applyForceToCenter(vector.x, vector.y, true)
            }
        }
    }
}