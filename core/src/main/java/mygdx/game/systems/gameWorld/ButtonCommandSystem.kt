package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import mygdx.game.components.UserActivityComponent
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.inputs.MyPadButtons
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem
import mygdx.game.uiMenu.UIGameMenu
import mygdx.game.views.WorldCharacterView
import java.lang.Float.min

class ButtonCommandSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var entities: ImmutableArray<Entity>? = null
    private val view = WorldCharacterView()
    private val activityComponent = UserActivityComponent()

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(
            allOf(
                AbilityComponent::class,
                AnimationSkinComponent::class,
                PositionComponent::class,
                BodyComponent::class,
                UserControllerComponent::class,
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        if (myEngine.editorMode) return

        cont@ for (entity in entities!!) {
            view.entity = entity

            val justPressedButtons = view.controller().justPressedButtons
            val pressedButtons = view.controller().pressedButtons
            val ability = view.ability()

            if (pressedButtons.containsValue(true)) {
                entity.add(activityComponent)
            }

            if (justPressedButtons[MyPadButtons.TRIANGLE]!!) {
                myEngine.worldProcessingSystem.setProcessing(false)
                myEngine.screenContainer.tileMapRender.isVisible = false
                UIGameMenu(myEngine).open {
                    myEngine.worldProcessingSystem.setProcessing(true)
                    myEngine.screenContainer.tileMapRender.isVisible = true
                    myEngine.screenContainer.stage.keyboardFocus = null
                }
//                val i = Inventory()
//                i.itemModels.addAll(Array(6) { HealingPotion() })
//                StoreBuy(i, myEngine) {
//                    myEngine.worldProcessingSystem.setProcessing(true)
//                    myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
//                }
                continue@cont
            }

            //Проверка на "дэшь"
//            if (pressedButtons[MyPadButtons.CROSS]!! &&
//                (character.ability().currentAbility != myEngine.worldProcessingSystem.dashAbilitySystem) &&
//                (character.ability().currentAbility == myEngine.worldProcessingSystem.standAbilitySystem ||
//                        character.ability().currentAbility == myEngine.worldProcessingSystem.movingAbilitySystem)
//            ) {
//                myEngine.worldProcessingSystem.dashAbilitySystem.setup(character.entity!!)
//                continue@cont
//            }

            //проверка на команду "идти"
            var buttonForce = true
            var angle = 0f
            when {
                pressedButtons[MyPadButtons.RIGHT]!! -> angle = 0f
                pressedButtons[MyPadButtons.UP]!! -> angle =
                    90f
                pressedButtons[MyPadButtons.LEFT]!! -> angle =
                    180f
                pressedButtons[MyPadButtons.DOWN]!! -> angle =
                    270f
                pressedButtons[MyPadButtons.UP_RIGHT]!! -> angle =
                    45f
                pressedButtons[MyPadButtons.UP_LEFT]!! -> angle =
                    135f
                pressedButtons[MyPadButtons.DOWN_LEFT]!! -> angle =
                    225f
                pressedButtons[MyPadButtons.DOWN_RIGHT]!! -> angle = 315f
                else -> {
                    buttonForce = false
                }
            }

            if (buttonForce &&
                (ability.cancelable ||
                        view.ability().equalAbility(MovingAbilitySystem::class))
            ) {
                val axis = view.controller().axisVector
                if (axis.value > .2f) {
                    val value = min(axis.value, 1f)
                    view.position().angle = axis.angle
                    view.body().acceleration = view.body().defAcceleration * value
                } else {
                    view.position().angle = angle
                }
                view.ability().setAbility(MovingAbilitySystem::class)
                continue@cont
            }

            if (isArrowPressed(view.controller()) && isArrowReleased(view.controller()) &&
                view.ability().equalAbility(MovingAbilitySystem::class)
            ) {
                view.ability().setAbility(StandAbilitySystem::class)
            }

            //================================================================
//            if (justPressedButtons[MyPadButtons.SQUARE]!!) {
//                myEngine.worldProcessingSystem
//                FightSystem.getInstance().setProcessing(true)
//            }
        }
    }

    private fun isArrowPressed(controller: UserControllerComponent): Boolean {
        with(controller) {
            return !(pressedButtons[MyPadButtons.RIGHT]!! &&
                    pressedButtons[MyPadButtons.UP]!! &&
                    pressedButtons[MyPadButtons.LEFT]!! &&
                    pressedButtons[MyPadButtons.DOWN]!! &&
                    pressedButtons[MyPadButtons.UP_RIGHT]!! &&
                    pressedButtons[MyPadButtons.UP_LEFT]!! &&
                    pressedButtons[MyPadButtons.DOWN_LEFT]!! &&
                    pressedButtons[MyPadButtons.DOWN_RIGHT]!!)
        }
    }

    private fun isArrowReleased(controller: UserControllerComponent): Boolean {
        with(controller) {
            return (justReleasedButtons[MyPadButtons.RIGHT]!! ||
                    justReleasedButtons[MyPadButtons.UP]!! ||
                    justReleasedButtons[MyPadButtons.LEFT]!! ||
                    justReleasedButtons[MyPadButtons.DOWN]!! ||
                    justReleasedButtons[MyPadButtons.UP_RIGHT]!! ||
                    justReleasedButtons[MyPadButtons.UP_LEFT]!! ||
                    justReleasedButtons[MyPadButtons.DOWN_LEFT]!! ||
                    justReleasedButtons[MyPadButtons.DOWN_RIGHT]!!)
        }
    }
}