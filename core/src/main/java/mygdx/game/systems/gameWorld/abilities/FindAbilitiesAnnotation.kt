package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.EntitySystem
import mygdx.game.systems.MyEngine
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

class FindAbilitiesAnnotation {

    fun find(any: Any, arr: ArrayList<String> = ArrayList()): ArrayList<String> {
        if (any.javaClass.kotlin.hasAnnotation<Ability>()){
            arr.add(any::class.qualifiedName!!)
        }
        any.javaClass.kotlin.memberProperties.forEach {
            val access = it.isAccessible
            it.isAccessible = true
            val value = it.get(any)
            it.isAccessible = access
            if (value is EntitySystem && value !is MyEngine) {
                find(value, arr)
            }
        }
        return arr
    }
}