package mygdx.game.systems.gameWorld.abilities

import mygdx.game.components.gameWorld.characterComponents.AbilityComponent

interface IAbilitySystem {

    fun equalAbility(abilityComponent: AbilityComponent): Boolean {
        return abilityComponent.ability == this::class.qualifiedName!!
    }
}
