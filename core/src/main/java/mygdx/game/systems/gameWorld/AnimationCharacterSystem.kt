package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Rectangle
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.oneOf
import mygdx.game.TileMapRender
import mygdx.game.components.gameWorld.characterComponents.AnimationImageComponent
import mygdx.game.components.gameWorld.characterComponents.AnimationSkinComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.ShadowComponent
import mygdx.game.lib.MyMath
import mygdx.game.lib.includeRectangle
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class AnimationCharacterSystem(val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private val entities = mutableListOf<Entity>()

    override fun addedToEngine(engine: Engine) {
        myEngine.engine.addEntityListener(
            allOf(PositionComponent::class).oneOf(AnimationSkinComponent::class, AnimationImageComponent::class).get(),
            object : EntityListener {
                override fun entityAdded(entity: Entity) {
                    entities.add(entity)
                }

                override fun entityRemoved(entity: Entity) {
                    entities.remove(entity)
                }
            })
    }

    override fun update(deltaTime: Float) {
        entities.sortBy {
            val position = it.get<PositionComponent>()!!.y
            -position
        }
        entities.sortBy {
            it.get<PositionComponent>()!!.z
        }
        cont@ for (entity in entities) {
            val movementComponent = entity.get<PositionComponent>()!!
            val animationSkinComponent = entity.get<AnimationSkinComponent>() ?: continue@cont
            with(animationSkinComponent)
            {
                if (skin_id != skin.id) {
                    setSkin(skinStorage.getById(skin_id))
                }
            }

            with(animationSkinComponent) {
                direction = movementComponent.angle

                if (animAngle != direction) {
                    animationSkinComponent.changeAngle(MyMath.tendsToAngle(animAngle, direction, 20f))
                }

                animationTime += deltaTime
                offsetY = 0f
            }
        }
    }

    private fun checkOnScreen(entity: Entity): Boolean {
        val position = entity.get<PositionComponent>()!!
        val animation = entity.get<AnimationSkinComponent>() ?: return true

        val halfWidth = animation.skin.iconTexture.regionWidth / 2f
        val halfHeight = animation.skin.iconTexture.regionHeight / 2f
        val x = position.x * TileMapRender.TILE_WIDTH
        val y = position.y * TileMapRender.TILE_HEIGHT

        val r = Rectangle(x - halfWidth, y - halfHeight, halfWidth * 2, halfHeight * 2)
        val camera = myEngine.screenContainer.camera
        if (camera.includeRectangle(r)) {
            return true
        }
        return false
    }

    fun draw(batch: Batch) {
        var lastZ = -999f
        val charList = ArrayList<Entity>()
        cont@ for (entity in entities) {
            val position = entity.get<PositionComponent>()!!

            if (position.z != lastZ) {
                lastZ = position.z
                charList.forEach { drawAnimation(it, batch) }
                charList.clear()
            }
            charList.add(entity)

            val shadow = entity.get<ShadowComponent>()?.shadow ?: continue@cont
            if (checkOnScreen(entity)) {
                shadow.setPosition(
                    position.x * TileMapRender.TILE_WIDTH - shadow.originX,
                    position.y * TileMapRender.TILE_HEIGHT - shadow.originY - 15
                )
                shadow.draw(batch)
            }
        }
        charList.forEach { drawAnimation(it, batch) }
    }

    private fun drawAnimation(entity: Entity, batch: Batch) {
        if (checkOnScreen(entity)) {
            val position = entity.get<PositionComponent>()!!
            val animationImage = entity.get<AnimationImageComponent>()
            if (animationImage != null) with(animationImage)
            {
                val region = animation.getKeyFrame(time)
                batch.draw(
                    region,
                    position.x * TileMapRender.TILE_WIDTH,
                    position.y * TileMapRender.TILE_HEIGHT
                )
            }

            val animationSkin = entity.get<AnimationSkinComponent>()
            if (animationSkin != null) with(animationSkin)
            {
                skin.motions.draw(
                    batch,
                    position.x * TileMapRender.TILE_WIDTH,
                    position.y * TileMapRender.TILE_HEIGHT + offsetY,
                    animationName,
                    animationTime
                )
            }
        }
    }
}