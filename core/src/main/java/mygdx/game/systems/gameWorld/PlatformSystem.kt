package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import mygdx.game.components.PlatformComponent
import mygdx.game.components.PlatformIgnoreComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.systems.MyAbstractSystem

class PlatformSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var platforms: ImmutableArray<Entity>? = null
    private var characters: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        platforms =
            engine.getEntitiesFor(
                allOf(
                    PlatformComponent::class,
                    BodyComponent::class,
                ).get()
            )
        characters =
            engine.getEntitiesFor(
                allOf(
                    BodyComponent::class,
                ).exclude(PlatformComponent::class, PlatformIgnoreComponent::class).get()
            )
    }

    override fun update(deltaTime: Float) {
        cont@ for (index in 0 until platforms!!.size()) {
            val platformEntity = platforms!![index]

            val platformBody = platformEntity.get<BodyComponent>()!!.getBody()
            val platform = platformEntity.get<PlatformComponent>()!!

            if (!platform.validPosition) {
                platform.lastPosition.x = platformBody.position.x
                platform.lastPosition.y = platformBody.position.y
                platform.validPosition = true
            }

            char@ for (entity in characters!!) {
                val characterBody = entity.get<BodyComponent>()!!.getBody()
                if (platformBody.fixtureList.find {
                        it.testPoint(characterBody.position)
                    } == null) {
                    continue@char
                }

                characterBody.setTransform(
                    characterBody.position.x + platformBody.position.x - platform.lastPosition.x,
                    characterBody.position.y + platformBody.position.y - platform.lastPosition.y,
                    0f
                )
            }
            platform.lastPosition.x = platformBody.position.x
            platform.lastPosition.y = platformBody.position.y
        }
    }
}