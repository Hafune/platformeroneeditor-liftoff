package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.InteractRadiusComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import kotlin.math.max

class InteractRadiusSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var users: ImmutableArray<Entity>? = null
    private var activeNpcs: ImmutableArray<Entity>? = null
    private var activeTiles: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        users = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                UserControllerComponent::class,
                AbilityComponent::class
            ).get()
        )

        activeNpcs = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                InteractRadiusComponent::class,
            ).exclude(UserControllerComponent::class).get()
        )

        activeTiles = engine.getEntitiesFor(
            allOf(
                TileComponent::class,
                InteractRadiusComponent::class,
                MapPositionComponent::class,
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        cont@ for (npc in activeNpcs!!) {
            val interact = npc.get<InteractRadiusComponent>()!!
            interact.currentReloadTime = max(interact.currentReloadTime - deltaTime, 0f)
            if (interact.currentReloadTime != 0f) continue@cont

            val position = npc.get<PositionComponent>()!!

            val user = getUserInRadiusAndAngle(
                position.x,
                position.y,
                position.angle,
                interact.radius
            )
            if (user != null) {
                interact.interact(npc, myEngine)
            }
        }
        cont@ for (entity in activeTiles!!) {
            val interact = entity.get<InteractRadiusComponent>()!!
            interact.currentReloadTime = max(interact.currentReloadTime - deltaTime, 0f)
            if (interact.currentReloadTime != 0f) continue@cont

            val position = entity.get<MapPositionComponent>()!!

            val user = getUserInRadiusAndAngle(
                position.x,
                position.y,
                position.angle,
                interact.radius
            )
            if (user != null) {
                interact.interact(entity, myEngine)
            }
        }
    }

    private fun getUserInRadiusAndAngle(
        cx: Float,
        cy: Float,
        directionAngle: Float,
        radius: Float
    ): Entity? {
        val searchingAngle = 360f

        return users!!.filter {
            MyMath.getDistance(
                it.get<PositionComponent>()!!.x,
                it.get<PositionComponent>()!!.y,
                cx,
                cy
            ) <= radius
        }.filter {
            MyMath.getDifferentFromAngle(
                MyMath.normalizeAngle(
                    MyMath.getAngleDeg(
                        cx,
                        cy,
                        it.get<PositionComponent>()!!.x,
                        it.get<PositionComponent>()!!.y,
                    ) - directionAngle
                ),
                0f
            ) < searchingAngle
        }.minByOrNull {
            MyMath.getDifferentFromAngle(
                MyMath.getAngleDeg(
                    cx, cy,
                    it.get<PositionComponent>()!!.x,
                    it.get<PositionComponent>()!!.y
                ),
                directionAngle
            )
        }
    }
}