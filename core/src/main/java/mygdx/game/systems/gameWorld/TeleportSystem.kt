package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.MyLog
import mygdx.game.TileMapRender
import mygdx.game.actions.MyTimer
import mygdx.game.character.CharacterBuilder
import mygdx.game.components.CameraTargetComponent
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.lib.ActionTimer
import mygdx.game.loadSave.SceneLoader
import mygdx.game.loadSave.UserData
import mygdx.game.location.teleports.Teleport
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView
import kotlin.math.floor

class TeleportSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(UserControllerComponent::class, PositionComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            val view = WorldCharacterView(entity)
            val teleport =
                myEngine.locationsTeleports.findTeleport(view.position().getWorldX(), view.position().getWorldY())

            if (teleport != null) {
                MyLog["changeLocation(teleport)"]
                myEngine.screenContainer.tileMapRender.addAction(
                    Actions.sequence(
                        Actions.color(
                            Color(
                                0f,
                                0f,
                                0f,
                                1f
                            ), .22f
                        ),
                        ActionTimer {
                            myEngine.beginDisposeScene {
                                changeLocation(teleport)
                            }
                        }
                    )
                )
                this.setProcessing(false)
                return
            }
        }
    }

    private fun changeLocation(teleport: Teleport) {
        SceneLoader().load(myEngine, teleport.locationTo) {
            afterChangeLocation(teleport)
        }
    }

    private fun afterChangeLocation(teleport: Teleport) {
        val entity =
            CharacterBuilder(myEngine).buildCharacter(
                myEngine.userData.model.skin_id,
                myEngine.userData.model.icon_id,
                UserData.Hero
            )

        entity.add(myEngine.myInputHandler.keyboardHandler0.userControllerComponent)
        entity.add(CameraTargetComponent())

        entity.get<PositionComponent>()!!.setPosition(teleport.toX.toFloat(), teleport.toY.toFloat())
        entity.get<PositionComponent>()!!.angle = teleport.direction

        myEngine.screenContainer.camera.position.set(
            floor(entity.get<PositionComponent>()!!.x * TileMapRender.TILE_WIDTH),
            floor(entity.get<PositionComponent>()!!.y * TileMapRender.TILE_HEIGHT),
            0f
        )

        MyTimer(.0f).apply {
            myEngine = this@TeleportSystem.myEngine
            callback = {
                this@TeleportSystem.myEngine.screenContainer.tileMapRender.addAction(
                    Actions.color(
                        Color(
                            1f,
                            1f,
                            1f,
                            1f
                        ), .22f
                    )
                )
                myEngine.userData.quickSave()
            }
            start()
        }
        this.setProcessing(true)
    }
}