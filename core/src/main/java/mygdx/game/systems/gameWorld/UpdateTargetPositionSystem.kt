package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.TargetComponent
import mygdx.game.lib.FindRoute
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class UpdateTargetPositionSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var aims: ImmutableArray<Entity>? = null
    private var targets: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        aims = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                MapPointComponent::class,
                TargetComponent::class
            ).get()
        )

        targets = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                InteractComponent::class
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        for (entity in aims!!) {
            val targetComponent = entity.get<TargetComponent>()!!
            val mapPoint = entity.get<MapPointComponent>()!!
            val movement = entity.get<PositionComponent>()!!

            if (targets!!.contains(targetComponent.entity)){
                val target = targetComponent.entity

                val movementTarget = target.get<PositionComponent>()!!

                if (mapPoint.lastX != movementTarget.getWorldX() || mapPoint.lastY != movementTarget.getWorldY()) {
                    mapPoint.route =
                        FindRoute.findRoute(
                            movement.getWorldX(),
                            movement.getWorldY(),
                            movementTarget.getWorldX(),
                            movementTarget.getWorldY(),
                            myEngine.screenContainer.tileMapRender
                        )
                }
            }
            else entity.remove<TargetComponent>()
        }
    }
}