package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.TargetComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.components.gameWorld.tileComponents.TileComponent
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem

class InteractClickSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var users: ImmutableArray<Entity>? = null
    private var npcTargets: ImmutableArray<Entity>? = null
    private var tileTargets: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        users = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                AbilityComponent::class,
                MapPointComponent::class,
                TargetComponent::class
            ).get()
        )

        npcTargets = engine.getEntitiesFor(
            allOf(
                PositionComponent::class,
                InteractComponent::class,
            ).get()
        )

        tileTargets = engine.getEntitiesFor(
            allOf(
                TileComponent::class,
                InteractComponent::class,
                MapPositionComponent::class,
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        for (aimer in users!!) {
            val aim = aimer.get<TargetComponent>()!!
            val movement = aimer.get<PositionComponent>()!!

            if (npcTargets!!.contains(aim.entity)){
                val npc = aim.entity
                val movementTarget = npc.get<PositionComponent>()!!
                val interactComponent = npc.get<InteractComponent>()!!

                if (MyMath.getDistance(
                        movement.x,
                        movement.y,
                        movementTarget.x,
                        movementTarget.y
                    ) < 1
                ) {
                    aimer.remove<TargetComponent>()

                    aimer.get<AbilityComponent>()!!.setAbility(StandAbilitySystem::class)

                    interactComponent.interact(npc, myEngine)
                }
            }
            else if (tileTargets!!.contains(aim.entity)){
                val tile = aim.entity

                val mapPosition = tile.get<MapPositionComponent>()!!
                val interactComponent = tile.get<InteractComponent>()!!
                if (MyMath.getDistance(
                        movement.x,
                        movement.y,
                        mapPosition.x,
                        mapPosition.y
                    ) < 1f
                ) {
                    aimer.remove<TargetComponent>()

                    aimer.get<AbilityComponent>()!!.setAbility(StandAbilitySystem::class)

                    interactComponent.interact(tile, myEngine)
                }
            }
            else aimer.remove<TargetComponent>()
        }
    }
}