package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.actions.OperatorComparisonTypes
import mygdx.game.components.gameWorld.characterComponents.AwaitUserVariableComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class AwaitUserVariableSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private lateinit var entities: ImmutableArray<Entity>

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(allOf(AwaitUserVariableComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        cont@ for (entity in entities) {
            val trigger = entity.get<AwaitUserVariableComponent>()!!

            val v: Any = myEngine.userData.model.variables[trigger.name] ?: continue@cont

            when (trigger.operator) {
                OperatorComparisonTypes.EQUALS -> if (trigger.value == v) complete(entity, trigger)

                OperatorComparisonTypes.NOT_EQUALS -> if (trigger.value != v) complete(entity, trigger)
            }
        }
    }

    private fun complete(entity: Entity, trigger: AwaitUserVariableComponent) {
        if (myEngine.sceneAlive) {
            entity.remove<AwaitUserVariableComponent>()
            trigger.callback?.invoke()
        }
    }
}