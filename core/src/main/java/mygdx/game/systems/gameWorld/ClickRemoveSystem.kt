package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import mygdx.game.components.gameWorld.ClickOnScreenComponent
import mygdx.game.systems.MyAbstractSystem

class ClickRemoveSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null
    private val family = allOf(ClickOnScreenComponent::class).get()

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(family)
    }

    override fun update(deltaTime: Float) {
        engine.removeAllEntities(family)
    }
}