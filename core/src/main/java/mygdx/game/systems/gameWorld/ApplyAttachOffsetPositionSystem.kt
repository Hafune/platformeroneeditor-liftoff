package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.AttachOffsetComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.systems.MyAbstractSystem

class ApplyAttachOffsetPositionSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        entities =
            engine.getEntitiesFor(
                allOf(
                    AttachOffsetComponent::class,
                    BodyComponent::class,
                ).get()
            )
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            val offset = entity.get<AttachOffsetComponent>()!!
            val characterBody = entity.get<BodyComponent>()!!.getBody()

            characterBody.setTransform(
                offset.x + offset.attachBody.getBody().position.x,
                offset.y + offset.attachBody.getBody().position.y,
                0f
            )
        }
    }
}