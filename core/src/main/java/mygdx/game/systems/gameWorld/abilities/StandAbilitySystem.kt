package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import mygdx.game.characterSkins.MotionNames
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView

@Ability
class StandAbilitySystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine), IAbilitySystem {

    private var entities: ImmutableArray<Entity>? = null
    private val setupModule = SetupModule(this, MotionNames.STAND)

    override fun addedToEngine(engine: Engine) {
        entities =
            engine.getEntitiesFor(
                allOf(
                    AbilityComponent::class,
                ).get()
            )
    }

    override fun update(deltaTime: Float) {
        if (myEngine.editorMode) return

        for (entity in entities!!) {

            val view = WorldCharacterView(entity)

            if (equalAbility(view.ability())) {
                if (view.ability().time == 0f) {
                    setupModule.setup(entity)
                }
                view.ability().cancelable = true
            } else {
                if (view.ability().cancelable) {
                    setupModule.setup(entity)
                }
            }
        }
    }
}