package mygdx.game.systems.gameWorld.abilities

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.math.Vector2
import ktx.ashley.allOf
import mygdx.game.characterSkins.MotionNames
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.BodyComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine
import mygdx.game.views.WorldCharacterView
import kotlin.math.sign

@Ability
class MovingAbilitySystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine), IAbilitySystem {

    private var entities: ImmutableArray<Entity>? = null

    private val setupModule = SetupModule(this, MotionNames.MOVE)

    override fun addedToEngine(engine: Engine) {
        entities =
            engine.getEntitiesFor(
                allOf(
                    BodyComponent::class,
                    AbilityComponent::class
                ).get()
            )
    }

    override fun update(deltaTime: Float) {
        if (myEngine.editorMode) return

        cont@ for (entity in entities!!) {
            val view = WorldCharacterView(entity)
            if (!equalAbility(view.ability())) {
                continue@cont
            }
            if (view.ability().time == 0f) {
                setupModule.setup(entity)
            }
            val body = view.body().getBody()
            val movingForce = view.body().movingForce

            movingForce.value = view.body().acceleration

            val horizontal = Vector2().also {
                it.x = body.position.x +
                        (view.body().radius + .05f) * sign(movingForce.x)
                it.y = body.position.y
            }
            val vertical = Vector2().also {
                it.x = body.position.x
                it.y = body.position.y +
                        (view.body().radius + .05f) * sign(movingForce.y)
            }

            val blockState = myEngine.screenContainer.tileMapRender.mapBlockState

            val horBlock = blockState.isBlocked(
                horizontal.x.toInt(),
                horizontal.y.toInt()
            )
            val verBlock = blockState.isBlocked(
                vertical.x.toInt(),
                vertical.y.toInt()
            )

            val angle = movingForce.angle
            if (horBlock || verBlock) {
                when (angle) {
                    in 22.5..67.5 -> {
                        movingForce.angle = if (horBlock) 90f else 0f
                    }
                    in 112.5..157.5 -> {
                        movingForce.angle = if (verBlock) 180f else 90f
                    }
                    in 202.5..247.5 -> {
                        movingForce.angle = if (horBlock) 270f else 180f
                    }
                    in 292.5..337.5 -> {
                        movingForce.angle = if (verBlock) 0f else 270f
                    }
                }
                view.position().angle = movingForce.angle
            }
            body.applyForceToCenter(movingForce.x, movingForce.y, true)
        }
    }
}