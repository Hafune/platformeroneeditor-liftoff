package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.remove
import mygdx.game.components.gameWorld.MapPointComponent
import mygdx.game.components.gameWorld.characterComponents.AbilityComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.gameWorld.abilities.MovingAbilitySystem
import mygdx.game.systems.gameWorld.abilities.StandAbilitySystem
import mygdx.game.views.WorldCharacterView

class CheckRouteToPointSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var entities: ImmutableArray<Entity>? = null
    private val view = WorldCharacterView()

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(
            allOf(
                AbilityComponent::class,
                MapPointComponent::class,
                PositionComponent::class
            ).get()
        )
    }

    override fun update(deltaTime: Float) {
        cont@ for (entity in entities!!) {
            view.entity = entity

            if (!view.ability().equalAbility(MovingAbilitySystem::class)) {
                entity.remove<MapPointComponent>()
                continue@cont
            }

            if (view.mapPoint().index >= view.mapPoint().route.size) {
                entity.remove<MapPointComponent>()
                view.ability().setAbility(StandAbilitySystem::class)
                continue@cont
            }

            val node = view.mapPoint().route[view.mapPoint().index]

            val x: Float = node[0] + .5f
            val y: Float = node[1] + .5f

            view.position().setAngleToward(x, y)

            if (MyMath.getDistance(view.position().x, view.position().y, x, y) < 0.3f) {
                view.mapPoint().index++
            }
        }
    }
}