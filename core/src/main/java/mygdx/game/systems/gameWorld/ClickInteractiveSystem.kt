package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import ktx.ashley.remove
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameWorld.ClickOnScreenComponent
import mygdx.game.components.gameWorld.InteractComponent
import mygdx.game.components.gameWorld.characterComponents.PositionComponent
import mygdx.game.components.gameWorld.characterComponents.TargetComponent
import mygdx.game.components.gameWorld.tileComponents.MapPositionComponent
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem

class ClickInteractiveSystem(engine: PooledEngine) : MyAbstractSystem(engine) {

    private var clicks: ImmutableArray<Entity>? = null
    private var characters: ImmutableArray<Entity>? = null
    private var users: ImmutableArray<Entity>? = null
    private var npc: ImmutableArray<Entity>? = null
    private var tiles: ImmutableArray<Entity>? = null
    private var aimers: ImmutableArray<Entity>? = null
    private var targets: ImmutableArray<Entity>? = null

    override fun addedToEngine(engine: Engine) {
        clicks = engine.getEntitiesFor(allOf(ClickOnScreenComponent::class).get())
        characters = engine.getEntitiesFor(allOf(PositionComponent::class).get())
        users = engine.getEntitiesFor(
            allOf(
                UserControllerComponent::class,
                PositionComponent::class
            ).get()
        )
        npc = engine.getEntitiesFor(
            allOf(PositionComponent::class, InteractComponent::class).exclude(
                UserControllerComponent::class
            ).get()
        )
        tiles = engine.getEntitiesFor(
            allOf(MapPositionComponent::class, InteractComponent::class).exclude(
                UserControllerComponent::class
            ).get()
        )
        aimers = engine.getEntitiesFor(allOf(TargetComponent::class).get())
        targets = engine.getEntitiesFor(allOf(InteractComponent::class).get())
    }

    override fun update(deltaTime: Float) {
        for (clickEntity in clicks!!) {
            val click = clickEntity.get<ClickOnScreenComponent>()!!
            for (entityAim in aimers!!) {
                val aim = entityAim.get<TargetComponent>()!!
                if (targets!!.contains(aim.entity)) {
                    entityAim.remove<TargetComponent>()
                }
            }
            val entityNpc = getCharacterInPoint(npc!!, click.x, click.y)
            if (entityNpc != null) {
                for (entityAim in users!!) {
                    val aim = TargetComponent(entityNpc)
                    entityAim.add(aim)
                }
            } else {
                val entityTile = getTileInPoint(click.x, click.y) ?: return
                for (entityAim in users!!) {
                    val aim = TargetComponent(entityTile)
                    entityAim.add(aim)
                }
            }
        }
    }

    //TODO used in character map editor
    fun getAnyCharacterInPoint(x: Float, y: Float): Entity? {
        return getCharacterInPoint(characters!!, x, y)
    }

    private fun getCharacterInPoint(entities: ImmutableArray<Entity>, x: Float, y: Float): Entity? {
        return entities.filter {
            MyMath.getDistance(
                it.get<PositionComponent>()!!.x,
                it.get<PositionComponent>()!!.y,
                x,
                y
            ) < .7f
        }.minByOrNull {
            MyMath.getDistance(
                it.get<PositionComponent>()!!.x,
                it.get<PositionComponent>()!!.y,
                x,
                y
            )
        }
    }

    private fun getTileInPoint(x: Float, y: Float): Entity? {
        return tiles!!.filter {
            MyMath.getDistance(
                it.get<MapPositionComponent>()!!.x,
                it.get<MapPositionComponent>()!!.y,
                x,
                y
            ) < .7
        }.minByOrNull {
            MyMath.getDistance(
                it.get<MapPositionComponent>()!!.x,
                it.get<MapPositionComponent>()!!.y,
                x,
                y
            )
        }
    }
}