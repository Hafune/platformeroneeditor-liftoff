package mygdx.game.systems.gameWorld

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.allOf
import ktx.ashley.get
import mygdx.game.components.gameWorld.InterpolationComponent
import mygdx.game.systems.MyAbstractSystem
import mygdx.game.systems.MyEngine

class TileMapDrawOffsetSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private var entities: ImmutableArray<Entity>? = null
    private val family = allOf(InterpolationComponent::class).get()

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(family)
    }

    override fun update(deltaTime: Float) {
//        val offsetX = myEngine.screenContainer.tileMapRender.worldDrawOffset.offsetX
//        val offsetY = myEngine.screenContainer.tileMapRender.worldDrawOffset.offsetY
//        for (entity in entities!!) {
//            val offset = handle(entity, deltaTime)
//
//            offsetY.add(offset)
//        }
    }

    private fun handle(entity: Entity, deltaTime: Float): Float {
        val component = entity.get<InterpolationComponent>()!!
        component.time += deltaTime
        if (component.time >= component.totalTime) component.time -= component.totalTime

        val totalPercent = component.time / component.totalTime

        val index = (component.interpolations.size * totalPercent).toInt()
        val interpolation = component.interpolations[index]

        val totalPartTime = component.totalTime / component.interpolations.size
        val currentPartTIme = component.time % totalPartTime
        val currentPartPercent = currentPartTIme / totalPartTime

        return interpolation.apply(currentPartPercent) * component.range
    }
}