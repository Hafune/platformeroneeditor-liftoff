package mygdx.game.systems

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.CircleShape
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonValue


/**
 * Loads the collision fixtures defined with the Physics Body Editor
 * application. You only need to give it a body and the corresponding fixture
 * name, and it will attach these fixtures to your body.
 *
 * @author Aurelien Ribon | http://www.aurelienribon.com
 */
class BodyEditorLoader {
    /**
     * **For advanced users only.** Lets you access the internal model of
     * this loader and modify it. Be aware that any modification is permanent
     * and that you should really know what you are doing.
     */
    // Model
    private val internalModel: Model

    // Reusable stuff
    private val vectorPool: MutableList<Vector2?> = ArrayList()
    private val polygonShape = PolygonShape()
    private val circleShape = CircleShape()

    // -------------------------------------------------------------------------
    // Ctors
    // -------------------------------------------------------------------------
    constructor(file: FileHandle) {
        internalModel = readJson(file.readString())
    }

    constructor(str: String) {
        internalModel = readJson(str)
    }
    // -------------------------------------------------------------------------
    // Public API
    // -------------------------------------------------------------------------
    /**
     * Creates and applies the fixtures defined in the editor. The name
     * parameter is used to retrieve the right fixture from the loaded file.
     * <br></br><br></br>
     *
     * The body reference point (the red cross in the tool) is by default
     * located at the bottom left corner of the image. This reference point
     * will be put right over the BodyDef position point. Therefore, you should
     * place this reference point carefully to let you place your body in your
     * world easily with its BodyDef.position point. Note that to draw an image
     * at the position of your body, you will need to know this reference point
     * (see [.getOrigin].
     * <br></br><br></br>
     *
     * Also, saved shapes are normalized. As shown in the tool, the width of
     * the image is considered to be always 1 meter. Thus, you need to provide
     * a scale factor so the polygons get resized according to your needs (not
     * every body is 1 meter large in your game, I guess).
     *
     * @param body The Box2d body you want to attach the fixture to.
     * @param name The name of the fixture you want to load.
     * @param fd The fixture parameters to apply to the created body fixture.
     * @param scale The desired scale of the body. The default width is 1.
     */
    fun attachFixture(body: Body, name: String = "", fd: FixtureDef, scale: Float) {
        val rbModel = internalModel.rigidBodies[name] ?: throw RuntimeException("Name '$name' was not found.")
        val origin  = Vector2().set(rbModel.origin).scl(scale)
        run {
            var i = 0
            val n = rbModel.polygons.size
            while (i < n) {
                val polygon = rbModel.polygons[i]
                val vertices = polygon.buffer
                run {
                    var ii = 0
                    val nn = vertices!!.size
                    while (ii < nn) {
                        vertices[ii] = newVec().set(polygon.vertices[ii]).scl(scale)
                        vertices[ii]!!.sub(origin)
                        ii++
                    }
                }
                polygonShape.set(vertices)
                fd.shape = polygonShape
                body.createFixture(fd)
                var ii = 0
                val nn = vertices!!.size
                while (ii < nn) {
                    free(vertices[ii])
                    ii++
                }
                i++
            }
        }
        var i = 0
        val n = rbModel.circles.size
        while (i < n) {
            val circle = rbModel.circles[i]
            val center = newVec().set(circle.center).scl(scale)
            val radius = circle.radius * scale
            circleShape.position = center
            circleShape.radius = radius
            fd.shape = circleShape
            body.createFixture(fd)
            free(center)
            i++
        }
    }

    /**
     * Gets the image path attached to the given name.
     */
//    fun getImagePath(name: String): String? {
//        val rbModel = internalModel.rigidBodies[name] ?: throw RuntimeException("Name '$name' was not found.")
//        return rbModel.imagePath
//    }
//
//    /**
//     * Gets the origin point attached to the given name. Since the point is
//     * normalized in [0,1] coordinates, it needs to be scaled to your body
//     * size. Warning: this method returns the same Vector2 object each time, so
//     * copy it if you need it for later use.
//     */
    fun getOrigin(name: String ="", scale: Float = 1f): Vector2 {
        val rbModel = internalModel.rigidBodies[name] ?: throw RuntimeException("Name '$name' was not found.")
        return Vector2().set(rbModel.origin).scl(scale)
    }

    // -------------------------------------------------------------------------
    // Json Models
    // -------------------------------------------------------------------------
    class Model {
        val rigidBodies: MutableMap<String?, RigidBodyModel> = HashMap()
    }

    class RigidBodyModel {
        var name: String? = null
        var imagePath: String? = null
        val origin = Vector2()
        val polygons: MutableList<PolygonModel> = ArrayList()
        val circles: MutableList<CircleModel> = ArrayList()
    }

    class PolygonModel {
        val vertices: MutableList<Vector2> = ArrayList()
        var buffer: Array<Vector2?>? = null
    }

    class CircleModel {
        val center = Vector2()
        var radius = 0f
    }

    // -------------------------------------------------------------------------
    // Json reading process
    // -------------------------------------------------------------------------
    private fun readJson(str: String): Model {
        val m = Model()
        val root = JsonReader().parse(str)
        val bodies = root["rigidBodies"]
        var body = bodies.child()
        while (body != null) {
            val rbModel = readRigidBody(body)
            m.rigidBodies[""] = rbModel
//            m.rigidBodies[rbModel.name] = rbModel
            body = body.next()
        }
        return m
    }

    private fun readRigidBody(bodyElem: JsonValue): RigidBodyModel {
        val rbModel = RigidBodyModel()
        rbModel.name = bodyElem.getString("name")
        rbModel.imagePath = bodyElem.getString("imagePath")
        val origin = bodyElem["origin"]
        rbModel.origin.x = origin.getFloat("x")
        rbModel.origin.y = origin.getFloat("y")

        // polygons
        val polygons = bodyElem["polygons"]
        var vertices = polygons.child()
        while (vertices != null) {
            val polygon = PolygonModel()
            rbModel.polygons.add(polygon)
            var vertex = vertices.child()
            while (vertex != null) {
                polygon.vertices.add(Vector2(vertex.getFloat("x"), vertex.getFloat("y")))
                vertex = vertex.next()
            }
            polygon.buffer = arrayOfNulls(polygon.vertices.size)
            vertices = vertices.next()
        }

        // circles
        val circles = bodyElem["circles"]
        var circle = circles.child()
        while (circle != null) {
            val c = CircleModel()
            rbModel.circles.add(c)
            c.center.x = circle.getFloat("cx")
            c.center.y = circle.getFloat("cy")
            c.radius = circle.getFloat("r")
            circle = circle.next()
        }
        return rbModel
    }

//    private fun readRigidBody(bodyElem: OrderedMap<String, *>): RigidBodyModel {
//        val rbModel = RigidBodyModel()
//        rbModel.name = bodyElem.get("name") as String
//        rbModel.imagePath = bodyElem.get("imagePath") as String
//        val originElem = bodyElem.get("origin") as OrderedMap<String, *>
//        rbModel.origin.x = (originElem.get("x") as Float)
//        rbModel.origin.y = (originElem.get("y") as Float)
//
//        // polygons
//        val polygonsElem = bodyElem.get("polygons") as com.badlogic.gdx.utils.Array<*>
//        for (i in 0 until polygonsElem.size) {
//            val polygon = PolygonModel()
//            rbModel.polygons.add(polygon)
//            val verticesElem = polygonsElem[i] as com.badlogic.gdx.utils.Array<*>
//            for (ii in 0 until verticesElem.size) {
//                val vertexElem = verticesElem[ii] as OrderedMap<String, *>
//                val x = vertexElem.get("x") as Float
//                val y = vertexElem.get("y") as Float
//                polygon.vertices.add(Vector2(x, y))
//            }
//            polygon.buffer = arrayOfNulls(polygon.vertices.size)
//        }
//
//        // circles
//        val circlesElem = bodyElem.get("circles") as com.badlogic.gdx.utils.Array<*>
//        for (i in 0 until circlesElem.size) {
//            val circle = CircleModel()
//            rbModel.circles.add(circle)
//            val circleElem = circlesElem[i] as OrderedMap<String, *>
//            circle.center.x = (circleElem.get("cx") as Float)
//            circle.center.y = (circleElem.get("cy") as Float)
//            circle.radius = circleElem.get("r") as Float
//        }
//        return rbModel
//    }

    // -------------------------------------------------------------------------
    // Helpers
    // -------------------------------------------------------------------------
    private fun newVec(): Vector2 {
        return if (vectorPool.isEmpty()) Vector2() else vectorPool.removeAt(0)!!
    }

    private fun free(v: Vector2?) {
        vectorPool.add(v)
    }
}