package mygdx.game.systems

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import ktx.box2d.body
import ktx.box2d.box
import ktx.box2d.circle
import ktx.box2d.createWorld
import mygdx.game.TileMapRender
import mygdx.game.characterSkins.Skin
import mygdx.game.lib.FileStorage
import kotlin.experimental.or

class MyBox2dHandler(myEngine: MyEngine) {

    companion object {
        const val worldToPixelScale = TileMapRender.TILE_WIDTH
    }

    var drawDebug: Boolean = false
    lateinit var gameCamera: OrthographicCamera
    private val box2dCamera = OrthographicCamera()
    private val debugRenderer = Box2DDebugRenderer()

    private val bodySet = HashSet<Body>()
    private val bodiesToDestroy = ArrayList<Body>()

    private val categoryMap: HashMap<String, Short>
    private val maskMap: HashMap<String, Short>

    init {
        val service = myEngine.db.collisionBehaviorService

        var value = 1
        categoryMap = service.getGroups().fold(HashMap()) { acc, s ->
            if (!acc.containsKey(s)) {
                acc[s] = value.toShort()
                value *= 2
            }
            acc
        }

        maskMap = service.getList().fold(HashMap()) { acc, pair ->
            if (acc.containsKey(pair.first)) {
                acc[pair.first] = acc[pair.first]!!.or(categoryMap[pair.second]!!)
            } else acc[pair.first] = categoryMap[pair.second]!!

            if (acc.containsKey(pair.second)) {
                acc[pair.second] = acc[pair.second]!!.or(categoryMap[pair.first]!!)
            } else acc[pair.second] = categoryMap[pair.first]!!

            acc
        }
    }

    private val contactListener = object : ContactListener {

        override fun beginContact(contact: Contact) {}

        override fun endContact(contact: Contact) {}

        override fun preSolve(contact: Contact, oldManifold: Manifold) {}

        override fun postSolve(contact: Contact, impulse: ContactImpulse) {}
    }

    private var world = createWorld(allowSleep = false)
//        .apply { setContactListener(contactListener) }

    fun cleanWorld() {
        bodiesToDestroy.clear()
        bodySet.clear()
        world.dispose()
        world = createWorld(allowSleep = false)
//        world.setContactListener(contactListener)
    }

    fun update(deltaTime: Float) {
        bodiesToDestroy.forEach {
            if (bodySet.remove(it)) {
                it.fixtureList.forEach { f -> it.destroyFixture(f) }
                world.destroyBody(it)
            }
        }
        bodiesToDestroy.clear()

        world.step(deltaTime, 6, 2)

        with(gameCamera) {
            box2dCamera.position.set(
                position.x / worldToPixelScale,
                position.y / worldToPixelScale,
                position.z / worldToPixelScale
            )
            box2dCamera.viewportWidth = viewportWidth / worldToPixelScale
            box2dCamera.viewportHeight = viewportHeight / worldToPixelScale
            box2dCamera.zoom = zoom
        }
        box2dCamera.update()
    }

    fun debugRendererUpdate() {
        if (drawDebug) {
            debugRenderer.render(world, box2dCamera.combined)
        }
    }

    fun createWall(x: Int, y: Int): Body {
        val body = world.body {
            fixedRotation = true
            position.set(Vector2(x + .5f, y + .5f))
            box(1f, 1f) {
                filter.categoryBits = categoryMap["wall"]!!
            }
        }
        bodySet.add(body)
        return body
    }

    private fun createCircleBody(x: Float = 0f, y: Float = 0f, radius: Float, collisionBehavior: String): Body {
        val body = world.body {
            fixedRotation = true
            allowSleep = false
            circle(radius = radius) {
                restitution = 0f
                maskMap[collisionBehavior]?.let { filter.maskBits = it }
                filter.categoryBits = categoryMap[collisionBehavior]!!
            }
        }
        body.setTransform(x, y, 0f)

        bodySet.add(body)
        return body
    }

    fun createBodyFromFile(src: String, scale: Float, collisionBehavior: String): Pair<Body, Vector2> {
        val loader = BodyEditorLoader(FileStorage[src])
        val origin = loader.getOrigin()
        val body = world.body {
            fixedRotation = true
            allowSleep = false
        }
        loader.attachFixture(
            body,
            fd = FixtureDef().apply {
                maskMap[collisionBehavior]?.let { filter.maskBits = it }
                filter.categoryBits = categoryMap[collisionBehavior]!!
            },
            scale = scale
        )
        bodySet.add(body)
        return Pair(body, origin)
    }

    fun destroyBody(body: Body) {
        bodiesToDestroy.add(body)
    }

    fun createBodyFromSkinAndApplyOrigins(skin: Skin, collisionBehavior: String): Pair<Body, Vector2> {
        var vec: Vector2? = null
        val body: Body = if (skin.src_body != null) {
            val scale = skin.iconTexture.regionWidth.toFloat() / worldToPixelScale
            val (body, origin) = createBodyFromFile(
                src = skin.src_body!!,
                scale = scale,
                collisionBehavior = collisionBehavior
            )
            vec = origin
            skin.motions.setOrigin(
                origin.x * scale * worldToPixelScale,
                origin.y * scale * worldToPixelScale
            )
            body
        } else createCircleBody(radius = skin.radius, collisionBehavior = collisionBehavior)

        return Pair(body, vec ?: Vector2())
    }
}