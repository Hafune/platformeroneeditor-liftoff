package mygdx.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.utils.ImmutableArray
import ktx.ashley.get
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.gameFight.FlipXComponent
import mygdx.game.inputs.MyPadButtons

class UserControllerUpdateSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

//    var entityWithController0: Entity? = null
//    var entityWithController1: Entity? = null
    private var controllers: ImmutableArray<Entity>? = null
    private var entities: ImmutableArray<Entity>? = null
    private var currentTime = 0f
    private var focus = false
    override fun addedToEngine(engine: Engine) {
        controllers = engine.getEntitiesFor(
            Family.all(UserControllerComponent::class.java).get()
        )
        entities = engine.getEntitiesFor(
            Family.all(
                FlipXComponent::class.java,
                UserControllerComponent::class.java
            ).get()
        )
    }

//    private val userControllerComponent0: UserControllerComponent
//        get() = myEngine.myInputHandler.keyboardHandler0.userControllerComponent
//    private val userControllerComponent1: UserControllerComponent
//        get() = myEngine.myInputHandler.keyboardHandler1.userControllerComponent

    override fun update(deltaTime: Float) {
        if (currentTime < 0 || currentTime > 6000000) currentTime = 0f
        currentTime += deltaTime
        val time = currentTime / 60f
        if (!focus && myEngine.screenContainer.stage.keyboardFocus == null) {
            focus = true
        }
        for (entity in entities!!) {
            val userControllerComponent = entity.get<UserControllerComponent>()!!
            userControllerComponent.currentTime = time
            val just = userControllerComponent.justPressedButtons
            val pres = userControllerComponent.pressedButtons
            val lastFlipX = userControllerComponent.lastFlipX
            val flipX = FlipXComponent.map[entity].isFlipX
            userControllerComponent.lastFlipX = flipX
            just[MyPadButtons.FORWARD] = false
            just[MyPadButtons.BACKWARD] = false
            pres[MyPadButtons.FORWARD] = false
            pres[MyPadButtons.BACKWARD] = false
            //-------------------
            if (!flipX && just[MyPadButtons.RIGHT]!! || flipX && just[MyPadButtons.LEFT]!!) {
                just[MyPadButtons.FORWARD] = true
            }
            if (!flipX && just[MyPadButtons.LEFT]!! || flipX && just[MyPadButtons.RIGHT]!!) {
                just[MyPadButtons.BACKWARD] = true
            }
            //--   ----    ----    ----
            if (!flipX && pres[MyPadButtons.RIGHT]!! || flipX && pres[MyPadButtons.LEFT]!!) {
                pres[MyPadButtons.FORWARD] = true
            }
            if (!flipX && pres[MyPadButtons.LEFT]!! || flipX && pres[MyPadButtons.RIGHT]!!) {
                pres[MyPadButtons.BACKWARD] = true
            }
            //-------------------
            if (lastFlipX != flipX) {
                val buttonsSequence = userControllerComponent.buttonsSequence
                val size = buttonsSequence.size
                for (i in 0 until size) {
                    if (buttonsSequence[i] == MyPadButtons.FORWARD) buttonsSequence[i] =
                        MyPadButtons.BACKWARD else if (buttonsSequence[i] == MyPadButtons.BACKWARD) buttonsSequence[i] =
                        MyPadButtons.FORWARD
                }
            }
        }
        for (entity in controllers!!) {
            val userControllerComponent = entity.get<UserControllerComponent>()!!
            val justPressed = userControllerComponent.justPressedButtons
            val justReleased = userControllerComponent.justReleasedButtons
            for (button in justPressed.keys) {
                if (justPressed[button]!!) {
                    if (button != MyPadButtons.RIGHT && button != MyPadButtons.LEFT) {
                        addToCommand(
                            userControllerComponent,
                            time,
                            button
                        )
                    }
                }
            }
            justPressed.replaceAll { _: MyPadButtons?, _: Boolean? -> false }
            justReleased.replaceAll { _: MyPadButtons?, _: Boolean? -> false }
        }

    }

    private fun addToCommand(userControllerComponent: UserControllerComponent, time: Float, button: MyPadButtons) {
        userControllerComponent.pressedTime.add(time)
        userControllerComponent.buttonsSequence.add(button)
        while (userControllerComponent.pressedTime.size > 6) {
            userControllerComponent.pressedTime.removeAt(0)
            userControllerComponent.buttonsSequence.removeAt(0)
        }
    }

//    fun applyUserControllerComponent0(character: AbstractFightCharacter?) {
//        var entity: Entity? = null
//        if (character != null) {
//            entity = character.entity
//        }
//        applyUserControllerComponent(entityWithController0, entity, userControllerComponent0)
//        entityWithController0 = entity
//    }
//
//    fun applyUserControllerComponent1(character: AbstractFightCharacter?) {
//        var entity: Entity? = null
//        if (character != null) {
//            entity = character.entity
//        }
//        applyUserControllerComponent(entityWithController1, entity, userControllerComponent1)
//        entityWithController1 = entity
//    }

//    private fun applyUserControllerComponent(
//        entityWithController: Entity?,
//        newEntityForController: Entity?,
//        controller: UserControllerComponent
//    ) {
//        entityWithController?.remove(UserControllerComponent::class.java)
//        newEntityForController?.add(controller)
//    }
}