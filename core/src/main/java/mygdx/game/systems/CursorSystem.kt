package mygdx.game.systems

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.List
import mygdx.game.lib.MyMath.tendsToValue
import mygdx.game.myWidgets.MyImage
import mygdx.game.myWidgets.MyItemScrollPane

class CursorSystem(private val myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    private val target = Vector2()
    private val position = Vector2()
    private val cursor = MyImage(myEngine.iconStorage.buildById(2).region)

    private var lastActor: Actor? = null

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        cursor.remove()

        val actor = myEngine.screenContainer.stage.keyboardFocus
        if (actor != null && actor.isVisible) {
            if (actor != lastActor) {
                lastActor?.fire(getEventExit())
                lastActor = actor
            }
            target.x = 0f
            target.y = 0f
            if (actor is MyItemScrollPane<*>) {
                val b: Actor? = actor.currentItemLabel
                if (b != null) {
                    target.y += b.height
                    b.localToActorCoordinates(myEngine.screenContainer.interfaceRoot, target)
                    myEngine.screenContainer.interfaceRoot.addActor(cursor)
                }
            } else if (actor is List<*>) {
                actor.localToActorCoordinates(myEngine.screenContainer.interfaceRoot, target)
                target.y += actor.height - actor.selectedIndex * actor.itemHeight
                myEngine.screenContainer.interfaceRoot.addActor(cursor)
            } else {
                actor.localToActorCoordinates(myEngine.screenContainer.interfaceRoot, target)
                target.y += actor.height / 2
                target.x += actor.width / 2
                myEngine.screenContainer.interfaceRoot.addActor(cursor)
            }
        }
        position.x = tendsToValue(position.x, target.x, 1f, .3f)
        position.y = tendsToValue(position.y, target.y, 1f, .3f)
        cursor.setPosition(position.x, position.y)
    }

    private fun getEventExit(): InputEvent {
        val event = InputEvent()
        event.type = InputEvent.Type.exit
        event.pointer = -1
        return event
    }
}