package mygdx.game.systems.gameFight.DragonBoneClasses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.esotericsoftware.spine.*;

public class DragonBoneAnimation
{
    private static final SkeletonRenderer skeletonRenderer = new SkeletonRenderer();

    public static SkeletonData buildSkeletonData(String path)
    {
        TextureAtlas atlas = new TextureAtlas(path + ".atlas");
        SkeletonJson json = new SkeletonJson(atlas);
        SkeletonData data = json.readSkeletonData(Gdx.files.internal(path + ".json"));
        data.setHash(path);
        return data;
    }

    private final Skeleton skeleton;
    private final AnimationState state = new AnimationState();
    private final SkeletonBounds bounds = new SkeletonBounds();
    private AnimationState.TrackEntry track;
    private AnimationState.AnimationStateListener listener;
    private final boolean loop;

    public DragonBoneAnimation(SkeletonData data, String animation, boolean loop, AnimationState.AnimationStateListener listener)
    {
        skeleton = new Skeleton(data);

        this.loop = loop;
        state.setData(new AnimationStateData(data));
        track = state.setAnimation(0, animation, loop);
        this.listener = listener;
        track.setListener(listener);
    }

    public void restartAnimation()
    {
        track = state.setAnimation(0, state.getCurrent(0).getAnimation().getName(), loop);
        state.apply(skeleton);
        bounds.update(skeleton, true);
        track.setListener(listener);
    }

    public void resetAnimation(AnimationState.AnimationStateListener listener)
    {
        skeleton.setFlipX(false);
        skeleton.getRootBone().setRotation(0);
        this.listener = listener;
        restartAnimation();
    }

    public void transform(float x, float y, float time)
    {
        skeleton.setPosition(x, y);
        state.update(time / 60f);
        state.apply(skeleton);
        skeleton.updateWorldTransform();
        bounds.update(skeleton, true);
    }

    public SkeletonBounds getSkeletonBounds()
    {
        return bounds;
    }

    public void setFlipX(boolean flipX)
    {
        skeleton.setFlipX(flipX);
        bounds.update(skeleton, true);
    }

    public void setAngle(float angle)
    {
        skeleton.getRootBone().setRotation(angle);
        bounds.update(skeleton, true);
    }

    public void setListener(AnimationState.AnimationStateListener listener)
    {
        this.listener = listener;
    }

    public void draw(Batch batch)
    {
        skeletonRenderer.draw(batch, skeleton);
    }
}
