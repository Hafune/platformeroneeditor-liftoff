package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.CharacterGroundAttack;
import mygdx.game.components.gameFight.AbilityComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public class KnightAttack2 extends CharacterGroundAttack
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "a2/Attack2");

    private AbilityComponent abilityComponent;

    public KnightAttack2(Entity entity)
    {
        super(entity,
                data,
                Impacts.weakGroundStrike.getImpacts());

        setupComponentList.add(new SelfCharacterImpComponent(4, 0).setDefTimer(10));

        abilityComponent = entity.getComponent(AbilityComponent.class);
        useInAbility = KnightAttack1.class;
    }
}
