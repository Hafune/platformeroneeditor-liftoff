package mygdx.game.systems.gameFight.abilities;

import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Event;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;

public final class AnimationLayersBuilder
{
    public static AnimationLayers buildAnimationLayers(SkeletonData data, String name, boolean loop, AnimationState.AnimationStateListener listener)
    {
        return AnimationLayers.getFromPool(data, name, loop, listener);
    }

    public static AnimationState.AnimationStateListener buildListener(IListenerCallback start,
                                                                      IListenerCallback interrupt,
                                                                      IListenerCallback end,
                                                                      IListenerCallback dispose,
                                                                      IAnimationCompleteCallback complete,
                                                                      IListenerEventCallback eventCallback)
    {
        AnimationState.AnimationStateListener listener = new AnimationState.AnimationStateListener()
        {
            @Override
            public void start(AnimationState.TrackEntry trackEntry)
            {
                if (start != null) start.callback(trackEntry);
            }

            @Override
            public void interrupt(AnimationState.TrackEntry trackEntry)
            {
                if (interrupt != null) interrupt.callback(trackEntry);
            }

            @Override
            public void end(AnimationState.TrackEntry trackEntry)
            {
                if (end != null) end.callback(trackEntry);
            }

            @Override
            public void dispose(AnimationState.TrackEntry trackEntry)
            {
                if (dispose != null) dispose.callback(trackEntry);
            }

            @Override
            public void complete(AnimationState.TrackEntry trackEntry)
            {
                if (!trackEntry.getLoop())
                {
                    if (complete != null) complete.callback();
                }
            }

            @Override
            public void event(AnimationState.TrackEntry trackEntry, Event event)
            {
                if (eventCallback != null) eventCallback.eventCallback(trackEntry, event);
            }
        };
        return listener;
    }

    public static AnimationState.AnimationStateListener buildListener(IAnimationCompleteCallback complete,
                                                                      IListenerEventCallback eventCallback)
    {
        AnimationState.AnimationStateListener listener = new AnimationState.AnimationStateListener()
        {
            @Override
            public void start(AnimationState.TrackEntry trackEntry)
            {

            }

            @Override
            public void interrupt(AnimationState.TrackEntry trackEntry)
            {

            }

            @Override
            public void end(AnimationState.TrackEntry trackEntry)
            {

            }

            @Override
            public void dispose(AnimationState.TrackEntry trackEntry)
            {

            }

            @Override
            public void complete(AnimationState.TrackEntry trackEntry)
            {
                if (!trackEntry.getLoop())
                {
                    if (complete != null) complete.callback();
                }
            }

            @Override
            public void event(AnimationState.TrackEntry trackEntry, Event event)
            {
                if (eventCallback != null) eventCallback.eventCallback(trackEntry, event);
            }
        };
        return listener;
    }

    @FunctionalInterface
    public interface IListenerCallback
    {
        void callback(AnimationState.TrackEntry trackEntry);
    }

    @FunctionalInterface
    public interface IListenerEventCallback
    {
        void eventCallback(AnimationState.TrackEntry trackEntry, Event event);
    }

    @FunctionalInterface
    public interface IAnimationCompleteCallback
    {
        void callback();
    }
}
