package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHurtComponent;
import mygdx.game.components.gameFight.PositionComponent;

public abstract class AbstractHurt extends AbstractAbility
{
    private final PositionComponent positionComponent;

    public AbstractHurt(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);

        autoUse = AbilitiesReadyForUse.hurt;

        positionComponent = entity.getComponent(PositionComponent.class);
    }

    @Override
    public void setupParam()
    {
        int max = (int) entity.getComponent(ImpactHurtComponent.class).timer;

        if (max > 0)
        {
            duration = 0;
            defDuration = max;
        }
    }

    @Override
    public void update()
    {
        if (duration > defDuration - 5 && positionComponent.positionY > 0) duration = defDuration - 5;
    }
}
