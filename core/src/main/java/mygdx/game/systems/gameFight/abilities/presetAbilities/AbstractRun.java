package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.inputs.MyPadButtons;
import mygdx.game.systems.gameFight.abilities.*;
import mygdx.game.components.gameFight.selfComponents.SelfDefMaxMoveSpeedComponent;
import mygdx.game.components.gameFight.selfComponents.SelfMovingImpComponent;

public abstract class AbstractRun extends AbstractAbility
{

    public AbstractRun(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
        infinitely = true;
        defCancelable = true;

        setupComponentList.add(new SelfMovingImpComponent());
        setupComponentList.add(new SelfDefMaxMoveSpeedComponent(2));

        command = AbilityCommands.run;
        manualUse = AbilitiesReadyForUse.run;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.distanceReductionPriority = 1;
    }

    @Override
    public void update()
    {
        UserControllerComponent controller = entity.getComponent(UserControllerComponent.class);
        if (controller != null) complete = !controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }
}
