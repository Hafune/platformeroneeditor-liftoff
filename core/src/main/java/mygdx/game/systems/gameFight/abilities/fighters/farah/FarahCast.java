package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIComponent;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIParamMap;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractCast;
import mygdx.game.systems.gameFight.abilities.spells.FireBall0;

public class FarahCast extends AbstractCast
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_hurt");

    public FarahCast(Entity entity)
    {
        super(entity, data, FireBall0.class);
        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.maxAttackRange = 200;
        param.minAttackRange = 150;
        param.attackPriority = 1;
    }
}
