package mygdx.game.systems.gameFight.abilities.fighters;

import com.badlogic.ashley.core.Entity;
import mygdx.game.components.gameFight.TargetTypeComponent;
import mygdx.game.components.gameFight.TeamComponent;

import java.util.HashMap;
import java.util.HashSet;

public final class FightTeamSpectator
{
    public static final HashMap<Teams, HashSet<Entity>> characterEntityMap = new HashMap<>();
    private static final HashSet<Entity> tempSet = new HashSet<>();


    public static void createCharacter(Entity entity, Teams team)
    {
        if (!characterEntityMap.containsKey(team)) characterEntityMap.put(team, new HashSet<>());
        characterEntityMap.get(team).add(entity);
    }

    public static void putToPool(Entity entity)
    {
        Teams team = entity.getComponent(TeamComponent.class).team;
        characterEntityMap.get(team).remove(entity);
    }

    public static HashSet<Entity> getTargetEntitiesFor(Entity entity)
    {
        TeamComponent teamComponent = entity.getComponent(TeamComponent.class);
        TargetTypeComponent targetTypeComponent = entity.getComponent(TargetTypeComponent.class);

        return getTargetEntitiesFor(teamComponent, targetTypeComponent);
    }

    /*public static HashSet<Entity> getCharactersFromTeam(TeamComponent teamComponent)
    {
        return characterEntityMap.get(teamComponent.team);
    }*/

    public static HashSet<Entity> getTargetEntitiesFor(TeamComponent teamComponent, TargetTypeComponent targetTypeComponent)
    {
        tempSet.clear();
        if (targetTypeComponent.type == TargetType.all) characterEntityMap.keySet().forEach(key -> tempSet.addAll(characterEntityMap.get(key)));
        else if (targetTypeComponent.type == TargetType.friend) tempSet.addAll(characterEntityMap.get(teamComponent.team));
        else if (targetTypeComponent.type == TargetType.enemy) characterEntityMap.keySet().
                stream().
                filter(key -> key != teamComponent.team).
                forEach(key -> tempSet.addAll(characterEntityMap.get(key)));

        return tempSet;
    }
}
