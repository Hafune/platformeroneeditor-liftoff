package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractStand;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class KnightStand extends AbstractStand
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Idle/CmdStand"
    );

    public KnightStand(Entity entity)
    {
        super(entity,data);
    }
}
