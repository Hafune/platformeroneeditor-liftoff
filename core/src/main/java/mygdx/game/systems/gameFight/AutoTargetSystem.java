package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.components.gameFight.*;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.fighters.FightTeamSpectator;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;

public class AutoTargetSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public AutoTargetSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(

                TeamComponent.class,
                TargetTypeComponent.class,
                CurrentTargetComponent.class

        ).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            CurrentTargetComponent currentTargetComponent = entity.getComponent(CurrentTargetComponent.class);

            if (currentTargetComponent.entity == null || currentTargetComponent.entity.getComponent(HitPointComponent.class).hp <= 0)
            {
                HashSet<Entity> set = FightTeamSpectator.getTargetEntitiesFor(entity);

                float range = 2.14748365E9F;
                PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
                currentTargetComponent.entity = null;

                for (Entity entity1 : set)
                {
                    PositionComponent positionComponent1 = entity1.getComponent(PositionComponent.class);
                    HitPointComponent pointComponent1 = entity1.getComponent(HitPointComponent.class);
                    float dif = Math.abs(positionComponent0.positionX - positionComponent1.positionX);
                    if (dif < range && pointComponent1.hp > 0)
                    {
                        range = dif;
                        currentTargetComponent.entity = entity1;
                    }
                }
            }
        }
    }
}
