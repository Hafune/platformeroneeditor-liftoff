package mygdx.game.systems.gameFight

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import mygdx.game.components.UserControllerComponent
import mygdx.game.components.UserControllerComponent.Companion.mapper
import mygdx.game.inputs.MyPadButtons
import mygdx.game.systems.MyAbstractSystem

class FightCycleSystem(engine: PooledEngine) : MyAbstractSystem(engine) {
    private var controller: UserControllerComponent? = null
    private var breakFrame = false
    private var entities: ImmutableArray<Entity>? = null
    private val backwardFunction = { closePauseMenu() }

    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(Family.all(UserControllerComponent::class.java).get())
    }

    override fun update(deltaTime: Float) {
        if (breakFrame) {
            breakFrame = false
            return
        }
        for (entity in entities!!) {
            controller = mapper[entity]
            if (controller!!.justPressedButtons[MyPadButtons.START]!!) {
                setProcessing(false)
//                UIPauseMenu.getInstance().controllerComponent = controller
//                UIPauseMenu.getInstance().open(backwardFunction)
                break
            } else if (controller!!.justPressedButtons[MyPadButtons.R1]!!) {
                setProcessing(false)
//                UISelectTargetMenu.instance.controllerComponent = controller
//                UISelectTargetMenu.instance.open(backwardFunction, entity)
                break
            }
        }
    }

    fun closePauseMenu() {
        breakFrame = true
        setProcessing(true)
        /*if (UserControllerUpdateSystem.getInstance().equalController0(controllerComponent))
        {
            MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(controllerComponent);
        }
        else if (UserControllerUpdateSystem.getInstance().equalController1(controllerComponent))
        {
            MyInputHandler.Companion.getInstance().getKeyboardHandler1().setIMyButtonTarget(controllerComponent);
        }*/
    }

    init {
//        enableUpdateAlways();
//        addSystem(AutoTargetSystem(engine))
//        addSystem(FightInputCommandSystem(engine))
//        addSystem(IISystem(engine))
//        addSystem(AbilitiesComponentSetupSystem(engine))
//        addSystem(Hurt_HitFreeze_TimeSystem(engine))
//        addSystem(AbilityUpdateSystem(engine))
//        addSystem(AddImpactOnTargetSystem(engine))
//        addSystem(RemoveHurtComponentSystem(engine))
//        addSystem(ImpactCharacterImpApplySystem(engine))
//        addSystem(ImpactDamageSystem(engine))
//        addSystem(RemoveCharacterSystem(engine))
//        addSystem(AbilitiesSelfSystems(engine))
//        addSystem(AbilityListAutoSwitchSystem(engine)) //после выполнения всех прочих условий
//        addSystem(GravitySystem(engine))
////        addSystem(MovementSystem)
//        addSystem(CollisionSystem(engine))
//        addSystem(PositionApplySystem(engine))
//        addSystem(FlipXSystem(engine))
//        addSystem(IntersectsBoxSystem(engine))
//        addSystem(AnimationAttachPositionSystem(engine))
//        addSystem(AnimationCharacterSystem(engine))
    }
}