package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.components.gameFight.AbilitiesListComponent;
import mygdx.game.components.gameFight.AbilityComponent;
import org.jetbrains.annotations.NotNull;

public class FightInputCommandSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public FightInputCommandSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(UserControllerComponent.class, AbilitiesListComponent.class, AbilityComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            UserControllerComponent controller = UserControllerComponent.Companion.getMapper().get(entity);
            AbilitiesListComponent list = AbilitiesListComponent.map.get(entity);
            AbilityComponent abilityComponent = AbilityComponent.map.get(entity);

            if (!abilityComponent.cancelLast)
            {
                for (AbstractAbility ability : list.abilities)
                {
                    if (ability.manualUse.readyForUse(entity, abilityComponent.ability, ability)
                            && ability.command.hasCommand(controller))
                    {
                        AbilityComponent.map.get(entity).changeAbility(ability);
                        break;
                    }
                }
            }
        }
    }
}
