package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.HurtResistanceComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHurtComponent;
import org.jetbrains.annotations.NotNull;

public class RemoveHurtComponentSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public RemoveHurtComponentSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(ImpactHurtComponent.class, HurtResistanceComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        entities.forEach(entity -> entity.remove(ImpactHurtComponent.class));
    }
}
