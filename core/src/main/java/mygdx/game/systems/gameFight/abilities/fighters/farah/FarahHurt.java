package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractHurt;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class FarahHurt extends AbstractHurt
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_hurt");

    public FarahHurt(Entity entity)
    {
        super(entity, data);
    }
}
