package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.FlipXComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.components.gameFight.selfComponents.SelfMovingImpComponent;
import org.jetbrains.annotations.NotNull;

public class SelfMovingImpSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public SelfMovingImpSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionMoveComponent.class, SelfMovingImpComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            PositionMoveComponent moveComponent = entity.getComponent(PositionMoveComponent.class);
            FlipXComponent flipXComponent = entity.getComponent(FlipXComponent.class);
            int signX = 1;
            if (flipXComponent != null) signX = flipXComponent.getSign();

            moveComponent.movingImp.setXY(moveComponent.acceleration * signX, 0);
        }
    }
}
