package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.*;

public abstract class AbstractStand extends AbstractAbility
{
    public AbstractStand(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
        defCancelable = true;

        command = AbilityCommands.returnFalse;
        autoUse = AbilitiesReadyForUse.autoStand;
        manualUse = AbilitiesReadyForUse.manualStand;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.holdPositionPriority = 1f;
    }

    @Override
    public void update()
    {
        complete = true;
    }
}
