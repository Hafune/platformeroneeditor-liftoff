package mygdx.game.systems.gameFight.patterns;

import com.badlogic.ashley.core.Entity;

public interface IFightPattern
{
    void update(Entity entity);
}
