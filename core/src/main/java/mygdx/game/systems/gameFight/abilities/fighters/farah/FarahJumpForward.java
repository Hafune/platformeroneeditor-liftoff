package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractJumpForward;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class FarahJumpForward extends AbstractJumpForward
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_stand");

    public FarahJumpForward(Entity entity)
    {
        super(entity);
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
    }
}
