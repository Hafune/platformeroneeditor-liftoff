package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Event;
import mygdx.game.components.gameFight.*;
import mygdx.game.systems.MyEngine;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.FightEntityDesigner;
import mygdx.game.systems.gameFight.abilities.effects.MyParticleEffect;
import mygdx.game.systems.gameFight.abilities.effects.ParticleFiles;

public abstract class AbstractSpell extends AbstractAbility
{
    private final AbilityComponent abilityComponent;

    public AbstractSpell()
    {
        entity = new Entity();

        FightEntityDesigner.designerSpellEntity(entity, this);

        abilityComponent = entity.getComponent(AbilityComponent.class);
    }

    public void init(Entity parentEntity)
    {
        PositionComponent parentPosition = parentEntity.getComponent(PositionComponent.class);
        PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
        positionComponent.positionX = parentPosition.positionX;
        positionComponent.positionY = parentPosition.positionY;

        entity.getComponent(FlipXComponent.class).setFlipX(parentEntity.getComponent(FlipXComponent.class).isFlipX());

        entity.add(parentEntity.getComponent(TeamComponent.class));

        abilityComponent.lastAbility = null;
        abilityComponent.cancelLast = true;

//        MyEngine.instance.getEngine().addEntity(entity);
    }

    @Override
    public void stop()
    {
        super.stop();
//        MyEngine.instance.getEngine().removeEntity(entity);
        SpellBuilder.getInstance().putToPool(this);
    }

    @Override
    protected void superEvent(AnimationState.TrackEntry trackEntry, Event event)
    {
        super.superEvent(trackEntry, event);
        MyParticleEffect effect = MyParticleEffect.buildParticleEffect(ParticleFiles.test);

        effect.animationZIndexComponent.zIndex = this.entity.getComponent(AnimationZIndexComponent.class).zIndex;
        effect.entity.add(this.entity.getComponent(AnimationPositionComponent.class));
    }
}
