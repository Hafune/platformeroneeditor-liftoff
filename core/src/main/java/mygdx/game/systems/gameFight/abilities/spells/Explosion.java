package mygdx.game.systems.gameFight.abilities.spells;

import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.IntersectsSet;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractSpell;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import mygdx.game.components.gameFight.AnimationZIndexComponent;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;
import mygdx.game.components.gameFight.ImpactComponents.ImpactExplosionImpComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHurtComponent;
import mygdx.game.components.gameFight.IntersectsComponent;
import mygdx.game.systems.gameFight.FightCameraSystem;

import java.util.HashSet;

public class Explosion extends AbstractSpell
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Spells +
                    "Explosion/Explosion");

    private final IntersectsComponent intersectsComponent = new IntersectsComponent(IntersectBoxeNames.attackBox_hitBox);

    public Explosion()
    {
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "attackBox", false, AnimationLayersBuilder.buildListener(
                this::stop,
                this::superEvent));
        impacts = new IImpact[]{new ImpactHurtComponent(.6f), new ImpactExplosionImpComponent(2, 4)};

        entity.getComponent(AnimationZIndexComponent.class).zIndex = -1;
        entity.add(intersectsComponent);
    }

    @Override
    public void update()
    {
        HashSet<IntersectsSet.EntityHit> hits = intersectsComponent.getCheckedEntities();

//        if (!hits.isEmpty()) FightCameraSystem.getInstance().explosionCrush(3, 2, 20, 3);
    }
}
