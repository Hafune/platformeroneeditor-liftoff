package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.esotericsoftware.spine.SkeletonBounds;
import mygdx.game.components.gameFight.*;
import mygdx.game.lib.MyMath;
import mygdx.game.lib.MyVector;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import org.jetbrains.annotations.NotNull;

public class CollisionSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;
    private MyVector myVector = new MyVector(1, 0);

    public CollisionSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(BodyCollisionComponent.class, AnimationLayersComponent.class, AnimationPositionComponent.class, PositionComponent.class, PositionMoveComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity0 : entities)
        {
            AnimationLayersComponent anim0 = AnimationLayersComponent.map.get(entity0);
            AnimationPositionComponent animPos0 = AnimationPositionComponent.map.get(entity0);
            PositionComponent position0 = PositionComponent.map.get(entity0);
            PositionMoveComponent positionMove0 = PositionMoveComponent.map.get(entity0);

            int size = entities.size();
            for (int i = 0; i < size; i++)
            {
                if (entity0 != entities.get(i))
                {
                    Entity entity1 = entities.get(i);
                    AnimationLayersComponent anim1 = AnimationLayersComponent.map.get(entity1);
                    AnimationPositionComponent animPos1 = AnimationPositionComponent.map.get(entity1);
                    PositionComponent position1 = PositionComponent.map.get(entity1);
                    PositionMoveComponent positionMove1 = PositionMoveComponent.map.get(entity1);

                    SkeletonBounds hitBox0 = anim0.animation.getSkeletonBounds(IntersectBoxeNames.collisionBox);
                    SkeletonBounds hitBox1 = anim1.animation.getSkeletonBounds(IntersectBoxeNames.collisionBox);

                    if (hitBox0 != null && hitBox1 != null &&
                            !MyMath.INSTANCE.equal(hitBox0.getWidth(), 0) &&
                            !MyMath.INSTANCE.equal(hitBox0.getHeight(), 0) &&
                            !MyMath.INSTANCE.equal(hitBox1.getWidth(), 0) &&
                            !MyMath.INSTANCE.equal(hitBox1.getHeight(), 0))
                    {

                        float semiWidth0 = hitBox0.getWidth() / 2;
                        float semiHeight0 = hitBox0.getHeight() / 2;
                        float semiWidth1 = hitBox1.getWidth() / 2;
                        float semiHeight1 = hitBox1.getHeight() / 2;

                        float x0 = hitBox0.getMinX() + semiWidth0 + position0.positionX - animPos0.positionX;
                        float y0 = hitBox0.getMinY() + semiHeight0 + position0.positionY - animPos0.positionY;
                        float x1 = hitBox1.getMinX() + semiWidth1 + position1.positionX - animPos1.positionX;
                        float y1 = hitBox1.getMinY() + semiHeight1 + position1.positionY - animPos1.positionY;

                        float difX = Math.abs(x0 - x1) - (semiWidth0 + semiWidth1);
                        float difY = Math.abs(y0 - y1) - (semiHeight0 + semiHeight1);

                        float movedByX = MyMath.INSTANCE.getCollisionMoveByX(positionMove0.movedByX, x0, y0, semiWidth0, semiHeight0, x1, y1, semiWidth1, semiHeight1);
                        if (positionMove0.movedByX != movedByX)
                        {
                            positionMove0.movedByX = movedByX;
                            myVector.set(positionMove0.characterImp);
                            myVector.setValue(myVector.getValue() / 2);
                            positionMove1.otherImp.addX(myVector.getX());
                        }

                        if (difX < 0 && difY < 0)
                        {
                            int offset = position0.positionX < position1.positionX ? -1 :
                                    position0.positionX > position1.positionX ? 1 : ((int) (Math.round(Math.random())));
                            positionMove0.otherImp.addX(offset * -difX * .1f);
                        }
                    }
                }
            }
        }
    }
}
