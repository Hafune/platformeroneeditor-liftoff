package mygdx.game.systems.gameFight.abilities.fighters.vicky;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractMove;

public class VickyMove extends AbstractMove
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "movement/move0/move0");

    public VickyMove(Entity entity)
    {
        super(entity,data);
    }
}
