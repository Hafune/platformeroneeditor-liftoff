package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public abstract class AbstractJumpUp extends AbstractAbility
{
    protected SelfCharacterImpComponent selfCharacterImpComponent = new SelfCharacterImpComponent().setDefTimer(1);
    protected PositionMoveComponent moveComponent;
    public int fallTime;
    public float startTime = 4;

    public AbstractJumpUp(Entity entity)
    {
        this.entity = entity;

        moveComponent = entity.getComponent(PositionMoveComponent.class);
        selfCharacterImpComponent.y = moveComponent.calculateJumpPower();
        fallTime = (int) (selfCharacterImpComponent.y / Math.abs(moveComponent.gravity));

        command = AbilityCommands.jump;
        manualUse = AbilitiesReadyForUse.onGround;
    }

    @Override
    public void update()
    {
        PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
        if (duration == startTime) addRemovableComponent(selfCharacterImpComponent);
        else if (duration > startTime && positionComponent.positionY == 0) complete = true;
    }
}
