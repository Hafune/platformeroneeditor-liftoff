package mygdx.game.systems.gameFight.abilities.fighters.vicky;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Event;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.CharacterGroundAttack;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public class VickyPunch0 extends CharacterGroundAttack
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "punches/punch0/punch0");

    private final SelfCharacterImpComponent selfCharacterImpComponent = new SelfCharacterImpComponent(2, 0).setDefTimer(5);

    public VickyPunch0(Entity entity)
    {
        super(entity,
                data,
                Impacts.weakGroundStrike.getImpacts());

        setupComponentList.add(selfCharacterImpComponent);

        useInAbility = VickyPunch2.class;
    }

    @Override
    protected void superEvent(AnimationState.TrackEntry trackEntry, Event event)
    {
        super.superEvent(trackEntry, event);
        addRemovableComponent(selfCharacterImpComponent);
    }
}
