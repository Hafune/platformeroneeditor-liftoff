package mygdx.game.systems.gameFight.abilities.fighters.vicky;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractJumpForward_with_start_up_fall_down;

public class VickyJumpForward extends AbstractJumpForward_with_start_up_fall_down
{
    private static final SkeletonData data0 = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "movement/jump/jump0");

    private static final SkeletonData data1 = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "movement/jump/jump1");

    private static final SkeletonData data2 = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "movement/jump/jump2");

    public VickyJumpForward(Entity entity)
    {
        super(entity,
                data2,
                AnimationLayersBuilder.buildAnimationLayers(data0, "hitBox", true, null),
                AnimationLayersBuilder.buildAnimationLayers(data1, "hitBox", false, null),
                data2);
    }
}
