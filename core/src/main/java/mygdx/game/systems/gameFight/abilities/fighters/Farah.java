package mygdx.game.systems.gameFight.abilities.fighters;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.fighters.farah.FarahJumpUp;
import mygdx.game.systems.gameFight.abilities.presetAbilities.TurnBackCharacter;
import mygdx.game.systems.gameFight.abilities.fighters.farah.*;

public class Farah extends AbstractFightCharacter
{
    public Farah()
    {
//        FightEntityDesigner.designerGroundUnitEntity(getEntity(), this);

        Entity entity = getEntity();
        addAbility(new FarahCast(entity));
        addAbility(new FarahJumpForward(entity));
        addAbility(new FarahJumpUp(entity));
        addAbility(new FarahHurt(entity));
        addAbility(new FarahPunch2(entity));
        addAbility(new FarahPunch1(entity));
        addAbility(new FarahPunch0(entity));
        addAbility(new FarahRun(entity));
        addAbility(new FarahMove(entity));
        addAbility(new TurnBackCharacter(entity));
        addAbility(new FarahStand(entity));

        setStartAbility(FarahStand.class);
    }
}
