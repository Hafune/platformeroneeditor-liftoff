package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.audio.FightSounds;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIComponent;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIParamMap;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.CharacterGroundAttack;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;
import mygdx.game.systems.gameFight.FightCameraSystem;

public class FarahPunch1 extends CharacterGroundAttack
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_punch1");

    public FarahPunch1(Entity entity)
    {
        super(entity,
                data,
                Impacts.weakGroundStrike.getImpacts());

        setupComponentList.add(new SelfCharacterImpComponent(1, 0).setDefTimer(2));

        useInAbility = FarahPunch0.class;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.attackPriority = 1;
        param.minAttackRange = 0;
        param.maxAttackRange = 40;

        soundHitEffect = FightSounds.IMPT_slappy_punch_2;
    }

    @Override
    public void update()
    {
        super.update();
//        if (!intersectsComponent.getCheckedEntities().isEmpty()) FightCameraSystem.getInstance().crush(entity, 4, 1, 10f, 3);
    }
}
