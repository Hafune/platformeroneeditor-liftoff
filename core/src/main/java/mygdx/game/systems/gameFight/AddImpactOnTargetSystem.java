package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;
import mygdx.game.components.gameFight.IntersectTargetComponent;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class AddImpactOnTargetSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public AddImpactOnTargetSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(IntersectTargetComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        entities.forEach(e -> {
            IntersectTargetComponent intersectTargetComponent = e.getComponent(IntersectTargetComponent.class);
            for (int i = 0; i < intersectTargetComponent.parents.size(); i++)
            {
                Entity parent = intersectTargetComponent.parents.get(i);
                IImpact[] impact = intersectTargetComponent.impacts.get(i);
                copyToEntity(parent, e, impact);
            }
            intersectTargetComponent.parents.clear();
            intersectTargetComponent.impacts.clear();
        });
    }

    private void copyToEntity(Entity parent, Entity target, IImpact[] income)
    {
        if (income != null) Arrays.stream(income).forEach(iImpact -> copyToEntity(parent, target, iImpact));
    }

    private void copyToEntity(Entity parent, Entity target, IImpact impact)
    {
        impact.addImpact(parent, target);
    }
}
