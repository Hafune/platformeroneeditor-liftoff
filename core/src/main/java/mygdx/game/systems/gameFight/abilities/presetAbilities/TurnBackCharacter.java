package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.*;
import mygdx.game.components.gameFight.FlipXComponent;

public class TurnBackCharacter extends AbstractAbility
{
    private final FlipXComponent flipXComponent;

    public TurnBackCharacter(Entity entity)
    {
        this.entity = entity;
        this.flipXComponent = entity.getComponent(FlipXComponent.class);
        defCancelable = true;

        command = AbilityCommands.backward;
        manualUse = AbilitiesReadyForUse.turnBack;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.turnBackPriority = 1;
    }

    @Override
    public void setupParam()
    {
        flipXComponent.setFlipX(!flipXComponent.isFlipX());
        complete = true;
    }
}
