package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.ImpactComponents.ImpactCharacterImpComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import org.jetbrains.annotations.NotNull;

public class ImpactCharacterImpApplySystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> impacts;

    public ImpactCharacterImpApplySystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        impacts = engine.getEntitiesFor(Family.all(ImpactCharacterImpComponent.class, PositionMoveComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : impacts)
        {
            PositionMoveComponent moveComponent = entity.getComponent(PositionMoveComponent.class);
            ImpactCharacterImpComponent impComponent = entity.getComponent(ImpactCharacterImpComponent.class);

            moveComponent.characterImp.setXY(impComponent.x, impComponent.y);

            entity.remove(ImpactCharacterImpComponent.class);
        }
    }
}
