package mygdx.game.systems.gameFight.abilities;

import mygdx.game.components.UserControllerComponent;
import mygdx.game.lib.MyMath;
import mygdx.game.inputs.MyPadButtons;

public enum AbilityCommands
{
    returnFalse(AbilityCommands::returnFalse),
    move(AbilityCommands::move),
    backward(AbilityCommands::backward),
    run(AbilityCommands::run),
    jump_forward(AbilityCommands::jump_forward),
    jump(AbilityCommands::jump),

    circle(AbilityCommands::circle),

    cross(AbilityCommands::cross),
    forward_cross(AbilityCommands::forward_cross),
    down_cross(AbilityCommands::down_cross),
    down_forward_cross(AbilityCommands::down_forward_cross);

    private final IHasCommand command;

    AbilityCommands(IHasCommand command)
    {
        this.command = command;
    }

    public boolean hasCommand(UserControllerComponent controller)
    {
        return command.hasCommand(controller);
    }

    //----------------------------------------------------------------------------------------------------------------------------------------

    private static boolean returnFalse(UserControllerComponent controller)
    {
        return false;
    }

    private static boolean backward(UserControllerComponent controller)
    {
        return controller.getPressedButtons().get(MyPadButtons.BACKWARD) && !controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }

    private static boolean move(UserControllerComponent controller)
    {
        return controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }

    private static boolean run(UserControllerComponent controller)
    {
        return (MyMath.INSTANCE.findCommand(controller, MyPadButtons.FORWARD, MyPadButtons.FORWARD) &&
                controller.getPressedButtons().get(MyPadButtons.FORWARD));
    }

    private static boolean jump_forward(UserControllerComponent controller)
    {
        return //MyMath.INSTANCE.findCommand(controller, MyPadButtons.UP) &&
                controller.getPressedButtons().get(MyPadButtons.UP) &&
                        controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }

    private static boolean circle(UserControllerComponent controller)
    {
        return controller.getJustPressedButtons().get(MyPadButtons.CIRCLE);
    }

    private static boolean jump(UserControllerComponent controller)
    {
        return //MyMath.INSTANCE.findCommand(controller, MyPadButtons.UP) &&
                controller.getPressedButtons().get(MyPadButtons.UP);
    }

    private static boolean cross(UserControllerComponent controller)
    {
        return MyMath.INSTANCE.findCommand(controller, MyPadButtons.CROSS);
    }

    private static boolean forward_cross(UserControllerComponent controller)
    {
        return MyMath.INSTANCE.findCommand(controller, MyPadButtons.CROSS) &&
                controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }

    private static boolean down_cross(UserControllerComponent controller)
    {
        return MyMath.INSTANCE.findCommand(controller, MyPadButtons.CROSS) &&
                controller.getPressedButtons().get(MyPadButtons.DOWN);
    }

    private static boolean down_forward_cross(UserControllerComponent controller)
    {
        return MyMath.INSTANCE.findCommand(controller, MyPadButtons.CROSS, MyPadButtons.FORWARD, MyPadButtons.DOWN);
    }

    @FunctionalInterface
    private interface IHasCommand
    {
        boolean hasCommand(UserControllerComponent controller);
    }
}
