package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;

public class AbstractCast extends AbstractAbility
{
    private final Class<AbstractSpell>[] spell;

    public AbstractCast(Entity entity, SkeletonData data, Class ...spell)
    {
        this.entity = entity;
        this.spell = spell;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
        defDuration = 10;

        command = AbilityCommands.circle;
        manualUse = AbilitiesReadyForUse.onGround;
    }

    @Override
    public void setupParam()
    {
        SpellBuilder.getInstance().cast(entity, spell);
    }
}
