package mygdx.game.systems.gameFight.abilities.spells;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.ImpactListBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractSpell;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import mygdx.game.components.gameFight.IntersectsComponent;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.components.gameFight.selfComponents.SelfRoundTrajectoryImpComponent;

public class WaterShield extends AbstractSpell
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Spells +
                    "AttackBox/AttackBox");

    private final SelfRoundTrajectoryImpComponent selfRoundTrajectoryImpComponent = new SelfRoundTrajectoryImpComponent();

    public WaterShield()
    {
        defDuration = 360;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, AnimationLayersBuilder.buildListener(null, this::superEvent));
        impacts = ImpactListBuilder.build_Hurt_Freeze_Imp(
                .6f,
                .1f,
                8, 0);

        setupComponentList.add(selfRoundTrajectoryImpComponent);
        IntersectsComponent intersectsComponent = new IntersectsComponent(IntersectBoxeNames.attackBox_hitBox);
        entity.add(intersectsComponent);
    }

    @Override
    public void init(Entity parentEntity)
    {
        selfRoundTrajectoryImpComponent.parentPosition = parentEntity.getComponent(PositionComponent.class);
        super.init(parentEntity);
    }


}
