package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.components.gameFight.*;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIParamMap;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIComponent;
import mygdx.game.systems.gameFight.patterns.Patterns;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class IISystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    private final ArrayList<AbstractAbility> filteredList = new ArrayList<>();

    public IISystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(

                TeamComponent.class,
                TargetTypeComponent.class,
                CurrentTargetComponent.class,

                AbilityComponent.class,
                AbilitiesListComponent.class,
                IIComponent.class
        ).exclude(
                UserControllerComponent.class
        ).get());
    }

    @Override
    public void update(float deltaTime)
    {
        entities.forEach(this::isReadyForAction);
    }

    private void isReadyForAction(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;
        AbilityComponent abilityComponent = entity.getComponent(AbilityComponent.class);
        if (target != null && !abilityComponent.cancelLast)
        {
            IIComponent iiComponent = entity.getComponent(IIComponent.class);
            HitPointComponent hitPointComponent = entity.getComponent(HitPointComponent.class);

            float actionPerSecond = .2f;
            float defSpeed = actionPerSecond / 60;

            iiComponent.action += defSpeed;
            iiComponent.action = Math.min(1, iiComponent.action);

            if (iiComponent.pattern == Patterns.aggressiveAttack)
            {
                if (hitPointComponent.getPercent() > .5f || iiComponent.action > .8)
                {
                    if (attack(entity))
                    {
                        iiComponent.action = 0;
                        return;
                    }
                    if (turnFaceToFace(entity)) return;
                    distanceReduction(entity);
                }
                else
                {
//                    if (attack(entity))
//                    {
//                        iiComponent.action = 0;
//                        return;
//                    }
                    float distance = 200;
                    if (getDistance(entity) < distance)
                    {
                        if (turnBack(entity)) return;
                        if (distanceIncrease(entity, distance)) return;
                    }
                    if (turnFaceToFace(entity)) return;
                    justStand(entity);
                }
            }
        }
    }

    private boolean attack(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;

        PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
        PositionComponent positionComponent1 = target.getComponent(PositionComponent.class);

        int direction = entity.getComponent(FlipXComponent.class).getSign();
        float distance = (positionComponent1.positionX - positionComponent0.positionX) * direction;

        ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;
        list = filterAttackRange(entity, distance, list);

        AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().attackPriority, 1, list);
        activateAbilities(entity, ability);

        return ability != null;
    }

    private boolean turnFaceToFace(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;

        int direction = entity.getComponent(FlipXComponent.class).getSign();
        PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
        PositionComponent positionComponent1 = target.getComponent(PositionComponent.class);

        if (direction == 1 && positionComponent0.positionX > positionComponent1.positionX ||
                direction == -1 && positionComponent0.positionX < positionComponent1.positionX)
        {
            ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;
            AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().turnBackPriority, 1, list);
            activateAbilities(entity, ability);

            return true;
        }
        else return false;
    }

    private boolean turnBack(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;

        int direction = entity.getComponent(FlipXComponent.class).getSign();
        PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
        PositionComponent positionComponent1 = target.getComponent(PositionComponent.class);

        if (direction == 1 && positionComponent0.positionX < positionComponent1.positionX ||
                direction == -1 && positionComponent0.positionX > positionComponent1.positionX)
        {
            ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;
            AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().turnBackPriority, 1, list);
            activateAbilities(entity, ability);

            return true;
        }
        else return false;
    }

    private void justStand(Entity entity)
    {
        ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;
        AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().holdPositionPriority, 1, list);
        activateAbilities(entity, ability);
    }

    private float getDistance(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;

        PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
        PositionComponent positionComponent1 = target.getComponent(PositionComponent.class);

        return Math.abs(positionComponent1.positionX - positionComponent0.positionX);
    }

    private void distanceReduction(Entity entity)
    {
        Entity target = entity.getComponent(CurrentTargetComponent.class).entity;

        PositionComponent positionComponent0 = entity.getComponent(PositionComponent.class);
        PositionComponent positionComponent1 = target.getComponent(PositionComponent.class);

        int direction = entity.getComponent(FlipXComponent.class).getSign();
        float distance = (positionComponent1.positionX - positionComponent0.positionX) * direction;

        if (distance > 0)
        {
            ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;

            float desiredPriority = distance > 200 ? 1 : entity.getComponent(IIComponent.class).action;
            AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().distanceReductionPriority, desiredPriority, list);
            activateAbilities(entity, ability);
        }
    }

    private boolean distanceIncrease(Entity entity, float desiredDistance)
    {
        float distance = getDistance(entity);

        if (distance < desiredDistance)
        {
            ArrayList<AbstractAbility> list = AbilitiesListComponent.map.get(entity).abilities;

            float desiredPriority = distance < desiredDistance / 2 ? 1 : 0;
            AbstractAbility ability = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().distanceReductionPriority, desiredPriority, list);
            activateAbilities(entity, ability);
            return true;
        }
        return false;
    }

    private ArrayList<AbstractAbility> filterAttackRange(Entity entity, float distance, ArrayList<AbstractAbility> list)
    {
        AbilityComponent abilityComponent = AbilityComponent.map.get(entity);

        filteredList.clear();
        list.stream().filter(ability ->
        {
            FightAbilitiesIIComponent property = FightAbilitiesIIParamMap.getInstance().getPropertyFor(ability);
            return property.minAttackRange <= distance &&
                    property.maxAttackRange >= distance &&
                    ability.manualUse.readyForUse(entity, abilityComponent.ability, ability);
        }).forEach(filteredList::add);

        return filteredList;
    }

    private void activateAbilities(Entity entity, AbstractAbility nextAbility)
    {
        entity.getComponent(AbilityComponent.class).changeAbility(nextAbility);
    }
}
