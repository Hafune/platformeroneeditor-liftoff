package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.myWidgets.styles.LabelsStyles;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.effects.DamageTextEffect;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.DamageRecordComponent;
import mygdx.game.components.gameFight.HitPointComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactDamageComponent;
import mygdx.game.components.gameFight.PositionComponent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ImpactDamageSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;
    private ImmutableArray<Entity> damagedHitPoints;

    public ImpactDamageSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionComponent.class, HitPointComponent.class, ImpactDamageComponent.class, DamageRecordComponent.class).get());
        damagedHitPoints = engine.getEntitiesFor(Family.all(HitPointComponent.class, ImpactDamageComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            DamageRecordComponent recordComponent = entity.getComponent(DamageRecordComponent.class);
            ArrayList<Float> damages = entity.getComponent(ImpactDamageComponent.class).list;

            if (!damages.isEmpty())
            {
                AnimationPositionComponent animationPositionComponent = entity.getComponent(AnimationPositionComponent.class);

                if (recordComponent.effect == null || recordComponent.effect.duration >= recordComponent.effect.totalDuration)
                {
                    recordComponent.totalDamage = 0;
                    recordComponent.effect = null;
                }
                else damages.forEach(damage -> {
                    if (damage <= 0)
                    {
                        DamageTextEffect effect = DamageTextEffect.buildDamageText(LabelsStyles.game_menu_no_border, String.valueOf((int) -damage));
                        effect.offsetX = 0;
                        effect.offsetY = 40;

                        effect.animationPositionComponent.positionX = animationPositionComponent.positionX;
                        effect.animationPositionComponent.positionY = animationPositionComponent.positionY;
                        effect.positionComponent.positionX = animationPositionComponent.positionX;
                        effect.positionComponent.positionY = animationPositionComponent.positionY;

                        effect.positionMoveComponent.movingImp.setY(5);

                        effect.entity.add(effect.gravityComponent);
                        effect.entity.add(effect.positionComponent);
                        effect.entity.add(effect.positionMoveComponent);
                        effect.entity.add(effect.interpolationComponent);
                    }
                });

                damages.forEach(damage -> {
                    if (damage <= 0) recordComponent.totalDamage += damage;
                });

                if (recordComponent.totalDamage < 0)
                {
                    if (recordComponent.effect == null)
                    {
                        recordComponent.effect = DamageTextEffect.buildDamageText(LabelsStyles.NoBorder, -recordComponent.totalDamage);
                        recordComponent.effect.scaleMultiply = 1.25f;
                        recordComponent.effect.setPriority(1f);
                        recordComponent.effect.offsetX = 0;
                        recordComponent.effect.offsetY = 40;
                        recordComponent.effect.entity.add(animationPositionComponent);
                    }
                    recordComponent.effect.updateText(-recordComponent.totalDamage);
                    if (!damages.isEmpty()) recordComponent.effect.duration = recordComponent.effect.fadeInTime * .4f;
                }
            }

            DamageTextEffect effect = recordComponent.effect;
            if (effect != null && (effect.entity.isScheduledForRemoval() || effect.duration >= effect.totalDuration))
            {
                recordComponent.effect = null;
                recordComponent.totalDamage = 0;
            }
        }

        for (Entity entity : damagedHitPoints)
        {
            HitPointComponent hitPointComponent = entity.getComponent(HitPointComponent.class);
            ArrayList<Float> damages = entity.getComponent(ImpactDamageComponent.class).list;
            damages.forEach(hitPointComponent::addHp);
            damages.clear();
        }
    }
}
