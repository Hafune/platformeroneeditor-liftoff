package mygdx.game.systems.gameFight.abilities.effects;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.MyEngine;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.IntersectsSet;
import mygdx.game.systems.gameFight.abilities.MyPoolUtil;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;
import mygdx.game.components.gameFight.AnimationLayersComponent;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.AnimationZIndexComponent;
import mygdx.game.components.gameFight.PositionComponent;

import java.util.HashMap;
import java.util.HashSet;

public class AnimationEffect
{
    private static final HashMap<String, HashSet<AnimationEffect>> pool = new HashMap<>();

    public final Entity entity = new Entity();
    private final AnimationZIndexComponent animationZIndexComponent = new AnimationZIndexComponent();
    private final AnimationPositionComponent animationPositionComponent = new AnimationPositionComponent();

    private final AnimationLayersComponent animationLayersComponent = new AnimationLayersComponent();
    private final AnimationLayers animation;
    private final SkeletonData data;
    private final AnimationState.AnimationStateListener listener = AnimationLayersBuilder.buildListener(
            this::stop,
            null);

    public AnimationEffect(SkeletonData data)
    {
        this.data = data;

        animation = AnimationLayersBuilder.buildAnimationLayers(
                data,
                "effect",
                false,
                listener);

        insertToEngine();
    }

    private void insertToEngine()
    {
        animationPositionComponent.positionX = 0;
        animationPositionComponent.positionY = 0;
        animationLayersComponent.animation = animation;
        animationZIndexComponent.zIndex = 10;

        entity.add(animationLayersComponent);
        entity.add(animationZIndexComponent);
        entity.add(animationPositionComponent);
//        MyEngine.instance.getEngine().addEntity(entity);
    }

    public void setFlipX(boolean flipX)
    {
        animation.setFlipX(flipX);
    }

    public void setAngle(float angle)
    {
        animation.setAngle(angle);
    }

    private void stop()
    {
//        MyEngine.instance.getEngine().removeEntity(entity);
        putToPool();
    }

    public void reset()
    {
        entity.removeAll();
        animation.resetAnimationLayers(listener);
        insertToEngine();
    }

    public static AnimationEffect getFromPool(SkeletonData data)
    {
        AnimationEffect effect = MyPoolUtil.getFromPoolMap(data.getHash(), pool);
        if (effect != null) effect.reset();
        else effect = new AnimationEffect(data);
        return effect;
    }

    public void putToPool()
    {
        MyPoolUtil.putToPoolMap(data.getHash(), this, pool);
    }

    public static void buildAnimationEffectsFlipFromPosition(EffectSkeletonData name, HashSet<IntersectsSet.EntityHit> positions, Entity entityWithPositionComponent, float angle)
    {
        PositionComponent positionComponent = entityWithPositionComponent.getComponent(PositionComponent.class);
        positions.forEach(entityHit -> buildAnimationEffectFlipFromPosition(name, entityHit, positionComponent, angle));
    }

    public static void buildAnimationEffectFlipFromPosition(EffectSkeletonData name, IntersectsSet.EntityHit entityHit, PositionComponent positionComponent, float angle)
    {
        boolean flipX = positionComponent.positionX > entityHit.getX();
        buildAnimationEffect(name, entityHit.getX(), entityHit.getY(), flipX, angle);
    }

    public static void buildAnimationEffect(EffectSkeletonData name, float x, float y, boolean flipX, float angle)
    {
        if (name == null) return;
        SkeletonData data = name.getData();

        AnimationEffect effect = AnimationEffect.getFromPool(data);

        effect.animationPositionComponent.positionX = x;
        effect.animationPositionComponent.positionY = y;
        effect.setFlipX(flipX);
        effect.setAngle(angle);
    }
}
