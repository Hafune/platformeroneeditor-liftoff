package mygdx.game.systems.gameFight.abilities.fighters.vicky;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.CharacterGroundAttack;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public class VickyPunch4 extends CharacterGroundAttack
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "punches/punch4/punch4");

    public VickyPunch4(Entity entity)
    {
        super(entity,
                data,
                Impacts.weakGroundStrike.getImpacts());

        setupComponentList.add(new SelfCharacterImpComponent(2, 0).setDefTimer(5));

        command = AbilityCommands.down_forward_cross;
        //useInAbility = VickyPunch3.class;
    }
}
