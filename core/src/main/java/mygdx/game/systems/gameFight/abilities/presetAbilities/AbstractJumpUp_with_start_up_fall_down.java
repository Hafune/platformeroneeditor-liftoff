package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public abstract class AbstractJumpUp_with_start_up_fall_down extends AbstractAbility
{
    private final AnimationLayers flyUp;
    private final AnimationLayers fallDown;
    private final AnimationLayers landing;

    private final PositionComponent positionComponent;

    protected SelfCharacterImpComponent selfCharacterImpComponent = new SelfCharacterImpComponent().setDefTimer(1);
    protected PositionMoveComponent moveComponent;

    public AbstractJumpUp_with_start_up_fall_down(Entity entity, SkeletonData startAnimation, AnimationLayers flyUp, AnimationLayers fallDown, SkeletonData landing)
    {
        this.entity = entity;

        positionComponent = entity.getComponent(PositionComponent.class);

        this.startAnimation = AnimationLayersBuilder.buildAnimationLayers(startAnimation,
                "hitBox",
                false,
                AnimationLayersBuilder.buildListener(
                        this::completeFirst,
                        null));

        this.flyUp = flyUp;
        this.fallDown = fallDown;
        this.landing = AnimationLayersBuilder.buildAnimationLayers(landing,
                "hitBox",
                false,
                AnimationLayersBuilder.buildListener(
                        this::stop,
                        null));

        moveComponent = entity.getComponent(PositionMoveComponent.class);
        selfCharacterImpComponent.y = moveComponent.calculateJumpPower();

        command = AbilityCommands.jump;
        manualUse = AbilitiesReadyForUse.onGround;
    }

    @Override
    public void update()
    {
        AnimationLayers current = getCurrentAnimation();
        if (current != startAnimation)
        {
            if (duration > moveComponent.flyTime / 2 - 2 && current == flyUp)
            {
                nextAnimation(fallDown);
            }
            if (duration > 4 && current == fallDown && positionComponent.positionY == 0)
            {
                nextAnimation(landing);
            }
            if (!cancelable && getCurrentAnimation() == flyUp && positionComponent.positionY > 0) cancelable = true;
        }
    }

    private void completeFirst()
    {
        nextAnimation(flyUp);
        addRemovableComponent(selfCharacterImpComponent);
    }
}
