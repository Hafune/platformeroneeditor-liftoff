package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractCast;
import mygdx.game.systems.gameFight.abilities.spells.Explosion;
import mygdx.game.systems.gameFight.abilities.spells.WaterShield;

public class KnightCast extends AbstractCast
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Take Hit/Hurt");

    public KnightCast(Entity entity)
    {
        super(entity, data, WaterShield.class, Explosion.class);
    }
}
