package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.inputs.MyPadButtons;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.components.gameFight.selfComponents.SelfMovingImpComponent;

public class MapMove extends AbstractAbility
{
    public MapMove(Entity entity)
    {
        this.entity = entity;
        //animationName = "move";
        //loop = true;

        defCancelable = true;
        infinitely = true;
        setupComponentList.add(new SelfMovingImpComponent());

        command = AbilityCommands.move;
        manualUse = AbilitiesReadyForUse.move;
    }

    @Override
    public void update()
    {
        UserControllerComponent controller = entity.getComponent(UserControllerComponent.class);
        if (controller != null) complete = !controller.getPressedButtons().get(MyPadButtons.FORWARD);
    }
}
