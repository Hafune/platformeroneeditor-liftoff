package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.scenes.scene2d.Group;
import mygdx.game.myWidgets.MyImage;
import mygdx.game.systems.MyAbstractSystem;
import org.jetbrains.annotations.NotNull;

public class FightBackGroundSystem extends MyAbstractSystem
{
    private final Group fightScene = new Group();
    private final MyImage background0 = new MyImage("img/battlebacks1/Castle1.png");

    private final MyImage background1 = new MyImage("img/battlebacks2/Castle1.png");

    private final float leftEdge = 0;
    private final float rightEdge = background1.getWidth();
    {
        background0.setY(-background0.getHeight() / 2.5f);
        background1.setY(-background0.getHeight() / 2.5f);
        fightScene.addActor(background0);
        fightScene.addActor(background1);
        fightScene.setVisible(false);
//        enableUpdateAlways();
    }

    public FightBackGroundSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    public Group getFightScene()
    {
        return fightScene;
    }

    public float getLeftEdge()
    {
        return leftEdge;
    }

    public float getRightEdge()
    {
        return rightEdge;
    }

    @Override
    public void update(float deltaTime)
    {
        super.update(deltaTime);
        fightScene.setVisible(true);
    }
}
