package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractRun;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class FarahRun extends AbstractRun
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_run");

    public FarahRun(Entity entity)
    {
        super(entity, data);
    }
}
