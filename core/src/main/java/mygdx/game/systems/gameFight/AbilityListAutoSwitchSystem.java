package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.components.gameFight.AbilitiesListComponent;
import mygdx.game.components.gameFight.AbilityComponent;
import org.jetbrains.annotations.NotNull;

public class AbilityListAutoSwitchSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public AbilityListAutoSwitchSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(AbilityComponent.class, AbilitiesListComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            AbilitiesListComponent list = AbilitiesListComponent.map.get(entity);
            AbilityComponent abilityComponent = AbilityComponent.map.get(entity);

            for (AbstractAbility ability : list.abilities)
            {
                if (ability.autoUse.readyForUse(entity, abilityComponent.ability, ability))
                {
                    AbilityComponent.map.get(entity).changeAbility(ability);
                    break;
                }
            }
        }
    }
}
