package mygdx.game.systems.gameFight.abilities;

public enum AbilitiesPackages
{
    Effects("characters/sv_actors/effects/"),
    Spells("characters/sv_actors/spells/"),

    Farah("characters/sv_actors/fighters/Farah/"),
    Vicky("characters/sv_actors/fighters/Vicky/"),
    Knight("characters/sv_actors/fighters/Knight/");

    String path;

    AbilitiesPackages(String path)
    {
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }

    @Override
    public String toString()
    {
        return getPath();
    }
}
