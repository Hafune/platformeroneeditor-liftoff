package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.AnimationLayersComponent;
import mygdx.game.components.gameFight.FlipXComponent;
import org.jetbrains.annotations.NotNull;

public class FlipXSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public FlipXSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(AnimationLayersComponent.class, FlipXComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            AnimationLayersComponent anim = AnimationLayersComponent.map.get(entity);
            FlipXComponent flipXComponent = entity.getComponent(FlipXComponent.class);

            if (anim.animation != null) anim.animation.setFlipX(flipXComponent.isFlipX());
        }
    }
}
