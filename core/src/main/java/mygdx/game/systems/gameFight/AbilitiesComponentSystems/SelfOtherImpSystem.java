package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.FlipXComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.components.gameFight.selfComponents.SelfOtherImpComponent;
import org.jetbrains.annotations.NotNull;

public class SelfOtherImpSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public SelfOtherImpSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionMoveComponent.class, SelfOtherImpComponent.class).get());

        getEngine().addEntityListener(Family.all(SelfOtherImpComponent.class).get(), new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                SelfOtherImpComponent selfOtherImpComponent = entity.getComponent(SelfOtherImpComponent.class);
                selfOtherImpComponent.restoreTimerFromDefTimer();
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            SelfOtherImpComponent selfOtherImpComponent = entity.getComponent(SelfOtherImpComponent.class);
            PositionMoveComponent moveComponent = entity.getComponent(PositionMoveComponent.class);
            FlipXComponent flipXComponent = entity.getComponent(FlipXComponent.class);

            int signX = 1;
            if (flipXComponent != null) signX = flipXComponent.getSign();

            moveComponent.otherImp.setXY(selfOtherImpComponent.x * signX, selfOtherImpComponent.y);
        }
    }
}
