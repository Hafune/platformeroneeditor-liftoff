package mygdx.game.systems.gameFight.abilities.fighters;

public enum TargetType
{

    all,
    friend,
    enemy
}
