package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

public class FightAbilitiesIIComponent implements Component
{
    public static final ComponentMapper<FightAbilitiesIIComponent> map = ComponentMapper.getFor(FightAbilitiesIIComponent.class);

    public float turnBackPriority = 0;
    public float distanceReductionPriority = 0;
    public float attackPriority = 0;
    public float holdPositionPriority = 0;
    public float minAttackRange = 2.14748365E9F;
    public float maxAttackRange = -2.14748365E9F;
}
