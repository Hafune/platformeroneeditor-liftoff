package mygdx.game.systems.gameFight.abilities.effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public enum ParticleFiles
{
    Teleport(Gdx.files.internal("particles/Teleportation"), Gdx.files.internal("particles")),
    test(Gdx.files.internal("particles/test"), Gdx.files.internal("particles")),
    test1(Gdx.files.internal("particles/test1"), Gdx.files.internal("particles"));

    public final FileHandle imagesDir;
    public final FileHandle effectFile;

    ParticleFiles(FileHandle effectFile, FileHandle imagesDir)
    {
        this.imagesDir = imagesDir;
        this.effectFile = effectFile;
    }
}
