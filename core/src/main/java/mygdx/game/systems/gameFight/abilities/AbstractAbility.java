package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Event;
import mygdx.game.audio.FightSounds;
import mygdx.game.systems.MyEngine;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;
import mygdx.game.systems.gameFight.abilities.effects.EffectSkeletonData;
import mygdx.game.systems.gameFight.abilities.effects.ParticleFiles;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractSpell;
import mygdx.game.components.gameFight.AnimationLayersComponent;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;
import mygdx.game.components.gameFight.IntersectsComponent;

import java.util.ArrayList;
import java.util.HashSet;

public abstract class AbstractAbility
{
    public Entity entity;
    public final Entity id = new Entity();
    public AnimationLayers startAnimation;

    public boolean complete = true;
    public boolean infinitely = false;

    public boolean cancelable = false;
    public boolean defCancelable = false;

    public float duration = 0;
    public float defDuration = 0;

    public AbilityCommands command = AbilityCommands.returnFalse;
    public AbilitiesReadyForUse autoUse = AbilitiesReadyForUse.returnFalse;
    public AbilitiesReadyForUse manualUse = AbilitiesReadyForUse.returnFalse;
    public Class<? extends AbstractAbility> useInAbility = null;

    public ArrayList<Component> setupComponentList = new ArrayList<>();
    public HashSet<Class<? extends Component>> removeSet = new HashSet<>();

    public Class<AbstractSpell>[] spells = null;

    public FightSounds soundHitEffect;
    protected EffectSkeletonData hitEffect = EffectSkeletonData.HitEffect0;
    protected ParticleFiles particleEffect = ParticleFiles.test;
    protected IImpact[] impacts;
    protected IImpact[] selfImpacts;

    public IImpact[] getImpacts()
    {
        return impacts;
    }

    public IImpact[] getSelfImpacts()
    {
        return selfImpacts;
    }

    public FightSounds getSoundHitEffect()
    {
        return soundHitEffect;
    }

    public EffectSkeletonData getHitEffect()
    {
        return hitEffect;
    }

    public ParticleFiles getParticleEffect()
    {
        return particleEffect;
    }

    public Class<AbstractSpell>[] getSpells()
    {
        return spells;
    }

    public void setupParam()
    {

    }

    public void stop()
    {
        complete = true;
        removeSet.forEach(componentClass -> entity.remove(componentClass));
        removeSet.clear();
    }

    public void update()
    {
    }

    /*public void editImpacts()
    {

    }*/

    public void addRemovableComponent(Component component)
    {
        Class componentClass = component.getClass();
        removeSet.add(componentClass);
        entity.remove(componentClass);
        entity.add(component);
    }

    protected AnimationLayers getCurrentAnimation()
    {
        return entity.getComponent(AnimationLayersComponent.class).animation;
    }

    protected void nextAnimation(AnimationLayers next)
    {
        AnimationLayersComponent animationComponent = entity.getComponent(AnimationLayersComponent.class);
        if (next != null && animationComponent != null && animationComponent.animation != next)
        {
            next.restartAnimation();
            animationComponent.animation = next;
        }

    }

    protected void superEvent(AnimationState.TrackEntry trackEntry, Event event)
    {
        String name = event.getData().getName();
        String string = event.getData().getString();

        if (name.equals("cancelable"))
        {
            if (string.equals("true")) cancelable = true;
            else if (string.equals("false")) cancelable = false;
        }
        else if (name.equals("clearIntersectsComponent"))
        {
            entity.getComponent(IntersectsComponent.class).clear();
        }
        else if (name.contains("soundHitEffect"))
        {
            soundHitEffect = FightSounds.valueOf(string);
        }
    }

    protected void dispose()
    {
        entity.removeAll();
//        MyEngine.instance.getEngine().removeEntity(entity);

        startAnimation.putToHash();
        startAnimation = null;

        setupComponentList.clear();
        setupComponentList = null;
        removeSet.clear();
        removeSet = null;

        command = null;
        manualUse = null;
        useInAbility = null;
    }
}
