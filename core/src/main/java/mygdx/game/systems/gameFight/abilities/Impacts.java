package mygdx.game.systems.gameFight.abilities;

import mygdx.game.components.gameFight.ImpactComponents.IImpact;

public enum Impacts
{
    weakGroundStrike(.3f, .3f, .1f, .75f);

    private final float hurtSeconds;
    private final float freezingSeconds;
    private final float characterImpX;
    private final float characterImpY;

    Impacts(float hurtSeconds, float freezingSeconds, float characterImpX, float characterImpY)
    {
        this.hurtSeconds = hurtSeconds;
        this.freezingSeconds = freezingSeconds;
        this.characterImpX = characterImpX;
        this.characterImpY = characterImpY;
    }

    public IImpact[] getImpacts()
    {
        return ImpactListBuilder.build_Hurt_Freeze_Imp(
                hurtSeconds,
                freezingSeconds,
                characterImpX,
                characterImpY);
    }
}
