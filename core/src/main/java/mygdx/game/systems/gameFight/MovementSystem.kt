package mygdx.game.systems.gameFight

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.ashley.utils.ImmutableArray
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent
import mygdx.game.components.gameFight.PositionComponent
import mygdx.game.components.gameFight.PositionMoveComponent
import mygdx.game.lib.MyMath
import mygdx.game.systems.MyAbstractSystem
import kotlin.math.abs
import kotlin.math.sign

class MovementSystem(engine: PooledEngine) : MyAbstractSystem(engine) {
    private var entities: ImmutableArray<Entity>? = null
    override fun addedToEngine(engine: Engine) {
        entities = engine.getEntitiesFor(
            Family.all(
                PositionMoveComponent::class.java,
                PositionComponent::class.java
            ).exclude(ImpactHitFreezeComponent::class.java).get()
        )
    }

    override fun update(deltaTime: Float) {
        for (entity in entities!!) {
            val p = PositionComponent.map[entity]
            val m = PositionMoveComponent.map[entity]

            val curSpeedX = abs(m.characterImp.x)
            var newSpeedX = abs(m.characterImp.x + m.movingImp.x)

            if (newSpeedX > curSpeedX && curSpeedX >= m.maxMoveSpeed) {
                m.movingImp.x = 0f
            }

            newSpeedX = abs(m.characterImp.x + m.movingImp.x)
            m.characterImp.plus(m.movingImp)
            m.movingImp.value = 0f

            if (newSpeedX > curSpeedX && abs(m.characterImp.x) > m.maxMoveSpeed) {
                m.characterImp.x = m.maxMoveSpeed * sign(m.characterImp.x)
            }

            if (p.positionY <= 0 && abs(m.characterImp.x) > m.maxMoveSpeed) {
                m.characterImp.x =
                    MyMath.tendsToValue(m.characterImp.x, m.maxMoveSpeed * sign(m.characterImp.x), .1f, .15f)
            }

            m.movedByX = m.characterImp.plus(m.movingImp).x + m.otherImp.x
            m.movedByY = m.characterImp.plus(m.movingImp).y + m.otherImp.y
            m.otherImp.value = 0f

            if (p.positionY <= 0) {
                val aroundGroundFriction = .05f
                m.characterImp.x =
                    MyMath.tendsToValue(m.characterImp.x, 0f, m.groundFriction + aroundGroundFriction, 0f)
                if (m.characterImp.y < 0) m.characterImp.y = 0f
            }

            m.jumpHeight = m.defJumpHeight
            m.acceleration = m.defAcceleration
            m.maxMoveSpeed = m.defMaxMoveSpeed
            m.groundFriction = m.defGroundFriction
        }
    }
}