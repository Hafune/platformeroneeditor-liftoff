package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.components.UserControllerComponent;
import mygdx.game.inputs.MyPadButtons;
import mygdx.game.systems.gameFight.abilities.*;
import mygdx.game.components.gameFight.selfComponents.SelfMovingImpComponent;

public abstract class AbstractMove extends AbstractAbility
{
    public AbstractMove(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
        defCancelable = true;
        infinitely = true;
        setupComponentList.add(new SelfMovingImpComponent());

        command = AbilityCommands.move;
        manualUse = AbilitiesReadyForUse.move;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.distanceReductionPriority = .4f;
    }

    @Override
    public void update()
    {
        UserControllerComponent controller = entity.getComponent(UserControllerComponent.class);
        if (controller != null) {
            complete = !controller.getPressedButtons().get(MyPadButtons.FORWARD);
        }
    }
}
