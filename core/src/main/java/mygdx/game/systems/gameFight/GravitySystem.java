package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.GravityComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import org.jetbrains.annotations.NotNull;

public class GravitySystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public GravitySystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionMoveComponent.class, GravityComponent.class).exclude(ImpactHitFreezeComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            PositionMoveComponent moveComponent = PositionMoveComponent.map.get(entity);

            moveComponent.characterImp.addY(moveComponent.gravity);
        }
    }
}
