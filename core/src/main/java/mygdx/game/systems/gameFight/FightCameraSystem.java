package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.lib.MyVector;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.CameraPriorityTargetComponent;
import mygdx.game.components.CameraTargetComponent;
import mygdx.game.components.gameFight.FlipXComponent;
import org.jetbrains.annotations.NotNull;

public class FightCameraSystem extends MyAbstractSystem
{
    private float curMinX;
    private float curMaxX;
    public boolean isFirstFrame = true;

    private final MyVector vector = new MyVector();
    private boolean explosion = false;
    private float totalX;
    private float x;
    private float totalY;
    private float y;

    private float totalTime;
    private float time;

    private float totalWave;
    private float wave;
    private float lastWave;

    public FightCameraSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    private ImmutableArray<Entity> entities;
    private ImmutableArray<Entity> priorities;

    {
//        enableUpdateAlways();
    }

    public void reset()
    {
        isFirstFrame = true;
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(CameraTargetComponent.class, AnimationPositionComponent.class).get());
        priorities = engine.getEntitiesFor(Family.all(CameraPriorityTargetComponent.class, AnimationPositionComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        crushUpdate(deltaTime);

//        float minX = FightBackGroundSystem.getInstance().getRightEdge();
//        float maxX = FightBackGroundSystem.getInstance().getLeftEdge();
//
//        ImmutableArray<Entity> targets;
//        float left_right_offset;
//        if (priorities.size() > 0)
//        {
//            targets = priorities;
//            left_right_offset = 150;
//        }
//        else
//        {
//            targets = entities;
//            left_right_offset = 300;
//        }
//
//        for (Entity entity : targets)
//        {
//            AnimationPositionComponent c = AnimationPositionComponent.map.get(entity);
//
//            minX = Math.min(c.positionX, minX);
//            maxX = Math.max(c.positionX, maxX);
//        }
//
//        minX -= left_right_offset;
//        maxX += left_right_offset;
//
//        float minWidth = (FightBackGroundSystem.getInstance().getRightEdge() - FightBackGroundSystem.getInstance().getLeftEdge()) / 4 * 3;
//        float difference = maxX - minX;
//        if (difference < minWidth)
//        {
//            float center = minX + (maxX - minX) / 2;
//            minX = center - minWidth / 2;
//            maxX = center + minWidth / 2;
//        }
//
//        if (minX < FightBackGroundSystem.getInstance().getLeftEdge())
//        {
//            minX = FightBackGroundSystem.getInstance().getLeftEdge();
//            if (maxX - minX < minWidth) maxX = minX + minWidth;
//        }
//        if (maxX > FightBackGroundSystem.getInstance().getRightEdge())
//        {
//            maxX = FightBackGroundSystem.getInstance().getRightEdge();
//            if (maxX - minX < minWidth) minX = maxX - minWidth;
//        }
//
//        if (isFirstFrame)
//        {
//            isFirstFrame = false;
//            curMinX = minX;
//            curMaxX = maxX;
//        }
//        curMinX = MyMath.INSTANCE.tendsToValue(curMinX, minX, .01f, .1f);
//        curMaxX = MyMath.INSTANCE.tendsToValue(curMaxX, maxX, .01f, .1f);
//
//        float width = curMaxX - curMinX;

//        ScreenContainer.instance.getCamera().zoom = width / ScreenContainer.instance.getCamera().viewportWidth;
//
//        ScreenContainer.instance.getCamera().position.x = curMinX + width / 2 + x;
//        ScreenContainer.instance.getCamera().position.y = 0 + y;

    }

    public void explosionCrush(float x, float y, float time, int wave)
    {
        crush(x, y, time, wave);
        vector.setXY(x, y);
        explosion = true;
    }

    public void crush(Entity entityWithFlipXComponent, float x, float y, float time, int wave)
    {
        x *= entityWithFlipXComponent.getComponent(FlipXComponent.class).getSign();
        crush(x, y, time, wave);
    }

    public void crush(float x, float y, float time, int wave)
    {
        this.totalTime = time;
        this.time = time;
        this.totalWave = wave;
        this.wave = wave;
        this.totalX = x;
        this.x = x;
        this.totalY = y;
        this.y = y;
        this.explosion = false;
    }

    private void crushUpdate(float deltaTime)
    {
        if (time > 0)
        {
            time = Math.max(0, time - deltaTime);
            wave = time / totalTime * totalWave;
            float a = (float) Math.floor(wave);
            float b = wave - a;
            float cof = wave / totalWave;

            x = totalX * b * cof;
            y = totalY * b * cof;

            boolean even = Math.floor(a / 2) == a / 2;
            if (even && !explosion)
            {
                x = -x;
                y = -y;
            }
            if (explosion && (int) wave != (int) lastWave)
            {
                vector.addAngle((float) (120 + Math.random() * 120));
                totalX = vector.getX();
                totalY = vector.getY();
            }
            lastWave = wave;
        }
        else
        {
            x = 0;
            y = 0;
            explosion = false;
        }
    }
}
