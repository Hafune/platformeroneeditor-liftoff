package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Group;
import mygdx.game.assetsClasses.MyShader;
import mygdx.game.components.gameFight.*;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.MyEngine;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;

public class AnimationSystem extends MyAbstractSystem {

    private float animTime = 0;
    private final CompareAnim compareAnim = new CompareAnim();
    private final ArrayList<Entity> zSortList = new ArrayList<>();

    private ImmutableArray<Entity> animations;
    private ImmutableArray<Entity> interpolations;

    private final ShaderProgram shader = MyShader.hitEffect.get();

    public AnimationSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

//    public AnimationCharacterSystem() {
//        super();
//        Group box = new Group() {
//            @Override
//            public void draw(Batch batch, float parentAlpha) {
//                super.draw(batch, parentAlpha);
//                AnimationCharacterSystem.this.drawAnimation(batch);
//            }
//        };
//
////        FightBackGroundSystem.getInstance().getFightScene().addActor(box);
////        enableUpdateAlways();
//    }

    @Override
    public void addedToEngine(Engine engine) {
        animations = engine.getEntitiesFor(Family.all(AnimationZIndexComponent.class, AnimationPositionComponent.class).
                one(ParticleComponent.class, AnimationLayersComponent.class, DamageTextComponent.class).get());
        interpolations = engine.getEntitiesFor(Family.all(AnimationPositionComponent.class, AnimationInterpolationComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {
        animTime = deltaTime;
//        float cof = MyEngine.instance.getCurFrameTime() % 1;

        interpolations.forEach(entity -> {
            AnimationInterpolationComponent interpolation = AnimationInterpolationComponent.map.get(entity);
//            interpolation.curX = interpolation.lastPositionX + (interpolation.positionX - interpolation.lastPositionX) * cof;
//            interpolation.curY = interpolation.lastPositionY + (interpolation.positionY - interpolation.lastPositionY) * cof;

            AnimationPositionComponent animationPositionComponent = AnimationPositionComponent.map.get(entity);
            animationPositionComponent.positionX = interpolation.curX;
            animationPositionComponent.positionY = interpolation.curY;
        });
    }

    public void drawAnimation(Batch batch) {
        zSortList.clear();
        animations.forEach(zSortList::add);
        zSortList.sort(compareAnim);

        for (Entity entity : zSortList) {
            float time = animTime;
            AnimationPositionComponent animationPositionComponent = entity.getComponent(AnimationPositionComponent.class);

            IMyAnimationComponent drawableComponent = entity.getComponent(ParticleComponent.class);
            if (drawableComponent == null) drawableComponent = entity.getComponent(AnimationLayersComponent.class);
            if (drawableComponent == null) drawableComponent = entity.getComponent(DamageTextComponent.class);

            ShaderProgram shaderProgram = batch.getShader();
            ImpactHitFreezeComponent freezeComponent = entity.getComponent(ImpactHitFreezeComponent.class);
            if (freezeComponent != null) {
                time = 0;
                shader.begin();
                float white = (1 - (freezeComponent.timer / freezeComponent.defTimer)) * 2;
                white = white < 1 ? white : 2 - white;

                shader.setUniformf("time", white);
                shader.end();
                batch.setShader(shader);
            }
            drawableComponent.draw(batch, animationPositionComponent.positionX, animationPositionComponent.positionY, time);
            batch.setShader(shaderProgram);
        }
        animTime = 0;
    }

    private static class CompareAnim implements Comparator<Entity> {
        @Override
        public int compare(Entity entity0, Entity entity1) {
            AnimationZIndexComponent component0 = entity0.getComponent(AnimationZIndexComponent.class);
            AnimationZIndexComponent component1 = entity1.getComponent(AnimationZIndexComponent.class);
            return component0.zIndex - component1.zIndex;
        }
    }
}
