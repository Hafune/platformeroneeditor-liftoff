package mygdx.game.systems.gameFight.abilities.spells;

import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractSpell;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import mygdx.game.components.gameFight.IntersectsComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public class WaterBall extends AbstractSpell
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Spells +
                    "WaterBall/WaterBall");

    public WaterBall()
    {
        defDuration = 90;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, AnimationLayersBuilder.buildListener(null, this::superEvent));
        impacts = Impacts.weakGroundStrike.getImpacts();


        setupComponentList.add(new SelfCharacterImpComponent(5, 0));
        IntersectsComponent intersectsComponent = new IntersectsComponent(IntersectBoxeNames.attackBox_hitBox);
        entity.add(intersectsComponent);
    }
}
