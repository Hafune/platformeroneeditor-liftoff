package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.AnimationInterpolationComponent;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.PositionComponent;
import org.jetbrains.annotations.NotNull;

public class AnimationAttachPositionSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public AnimationAttachPositionSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        Family family = Family.all(PositionComponent.class, AnimationInterpolationComponent.class).get();

        entities = engine.getEntitiesFor(family);

        engine.addEntityListener(family, new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                AnimationInterpolationComponent animationInterpolationComponent = entity.getComponent(AnimationInterpolationComponent.class);
                PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
                animationInterpolationComponent.lastPositionX = positionComponent.positionX;
                animationInterpolationComponent.lastPositionY = positionComponent.positionY;
                animationInterpolationComponent.positionX = positionComponent.positionX;
                animationInterpolationComponent.positionY = positionComponent.positionY;

                AnimationPositionComponent animationPositionComponent = entity.getComponent(AnimationPositionComponent.class);
                if (animationPositionComponent != null)
                {
                    animationPositionComponent.positionX = positionComponent.positionX;
                    animationPositionComponent.positionY = positionComponent.positionY;
                }
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            PositionComponent positionComponent = PositionComponent.map.get(entity);
            AnimationInterpolationComponent animationInterpolationComponent = AnimationInterpolationComponent.map.get(entity);

            animationInterpolationComponent.lastPositionX = animationInterpolationComponent.positionX;
            animationInterpolationComponent.lastPositionY = animationInterpolationComponent.positionY;

            animationInterpolationComponent.positionX = positionComponent.positionX;
            animationInterpolationComponent.positionY = positionComponent.positionY;
        }
    }
}
