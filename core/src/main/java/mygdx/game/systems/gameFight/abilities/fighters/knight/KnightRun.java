package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractRun;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class KnightRun extends AbstractRun
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Run/Run"
    );

    public KnightRun(Entity entity)
    {
        super(entity, data);
    }
}
