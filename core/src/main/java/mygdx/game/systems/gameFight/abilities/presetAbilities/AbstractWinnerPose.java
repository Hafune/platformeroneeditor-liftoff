package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;

public abstract class AbstractWinnerPose extends AbstractAbility
{
    public AbstractWinnerPose(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data,
                "hitBox",
                true,
                null);

        defCancelable = false;

        manualUse = AbilitiesReadyForUse.manualStand;
    }
}
