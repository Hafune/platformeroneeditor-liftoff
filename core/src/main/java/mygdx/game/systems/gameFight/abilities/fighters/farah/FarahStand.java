package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractStand;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class FarahStand extends AbstractStand
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_stand");

    public FarahStand(Entity entity)
    {
        super(entity, data);
    }
}
