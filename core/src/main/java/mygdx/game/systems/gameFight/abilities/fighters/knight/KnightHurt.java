package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractHurt;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class KnightHurt extends AbstractHurt
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Take Hit/Hurt"
    );

    public KnightHurt(Entity entity)
    {
        super(entity, data);
    }
}
