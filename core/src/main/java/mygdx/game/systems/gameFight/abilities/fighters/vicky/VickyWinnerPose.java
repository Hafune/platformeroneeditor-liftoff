package mygdx.game.systems.gameFight.abilities.fighters.vicky;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractWinnerPose;

public class VickyWinnerPose extends AbstractWinnerPose
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Vicky +
                    "idle/idle2/idle2");

    public VickyWinnerPose(Entity entity)
    {
        super(entity, data);
    }
}
