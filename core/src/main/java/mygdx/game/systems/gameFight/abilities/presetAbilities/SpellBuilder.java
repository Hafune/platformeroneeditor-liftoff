package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.MyPoolUtil;

import java.util.HashMap;
import java.util.HashSet;

public class SpellBuilder
{
    private static final SpellBuilder instance = new SpellBuilder();

    public static SpellBuilder getInstance()
    {
        return instance;
    }

    HashMap<Class, HashSet<AbstractSpell>> poolMap = new HashMap<>();

    public AbstractSpell getFromPool(Class<AbstractSpell> key)
    {
        AbstractSpell spellConstructor = MyPoolUtil.getFromPoolMap(key, poolMap);
        if (spellConstructor == null) spellConstructor = MyPoolUtil.newInstance(key);
        return spellConstructor;
    }

    public void putToPool(AbstractSpell key)
    {
        MyPoolUtil.putToPoolMap(key.getClass(), key, poolMap);
    }

    public void cast(Entity parentEntity, Class<AbstractSpell>[] spells)
    {
        Class<AbstractSpell> spell = null;
        Class<AbstractSpell>[] nextList = null;
        if (spells != null)
        {
            spell = spells[0];
            if (spells.length > 1)
            {
                nextList = new Class[spells.length - 1];
                System.arraycopy(spells, 1, nextList, 0, spells.length - 1);
            }
        }
        if (spell != null)
        {
            AbstractSpell abstractSpell = getFromPool(spell);
            abstractSpell.spells = nextList;
            abstractSpell.init(parentEntity);
        }
    }
}
