package mygdx.game.systems.gameFight.abilities;

import mygdx.game.components.gameFight.ImpactComponents.*;

public final class ImpactListBuilder
{
    public static IImpact[] build_Hurt_Freeze_Imp(float hurtSeconds, float freezingSeconds, float characterImpX, float characterImpY)
    {
        ImpactHurtComponent impactHurtComponent = new ImpactHurtComponent(hurtSeconds);
        ImpactHitFreezeComponent impactHitFreezeComponent = new ImpactHitFreezeComponent(freezingSeconds);
        ImpactCharacterImpComponent impactCharacterImpComponent = new ImpactCharacterImpComponent(characterImpX, characterImpY);
        ImpactDamageComponent impactDamageComponent = new ImpactDamageComponent(-4);

        return new IImpact[]{impactHurtComponent, impactHitFreezeComponent, impactCharacterImpComponent, impactDamageComponent};
    }

    public static IImpact[] build_Hurt_Freeze_Explosion(float hurtSeconds, float freezingSeconds, float characterImpX, float characterImpY)
    {
        ImpactHurtComponent impactHurtComponent = new ImpactHurtComponent(hurtSeconds);
        ImpactHitFreezeComponent impactHitFreezeComponent = new ImpactHitFreezeComponent(freezingSeconds);
        ImpactExplosionImpComponent impactCharacterImpComponent = new ImpactExplosionImpComponent(characterImpX, characterImpY);


        return new IImpact[]{impactHurtComponent, impactHitFreezeComponent, impactCharacterImpComponent};
    }
}
