package mygdx.game.systems.gameFight.abilities.spells;

import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.ImpactListBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractSpell;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import mygdx.game.components.gameFight.IntersectsComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;

public class FireBall0 extends AbstractSpell
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_punch2");

    public FireBall0()
    {
        defDuration = 60;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, AnimationLayersBuilder.buildListener(null, this::superEvent));
        impacts = ImpactListBuilder.build_Hurt_Freeze_Imp(
                .6f,
                .1f,
                2, 8);

        setupComponentList.add(new SelfCharacterImpComponent(5, 0));
        IntersectsComponent intersectsComponent = new IntersectsComponent(IntersectBoxeNames.attackBox_hitBox);
        entity.add(intersectsComponent);
    }
}
