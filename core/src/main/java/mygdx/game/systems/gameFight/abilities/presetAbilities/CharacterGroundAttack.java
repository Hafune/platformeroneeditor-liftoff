package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.*;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent;
import mygdx.game.components.gameFight.IntersectsComponent;

public class CharacterGroundAttack extends AbstractAbility
{
    protected final IntersectsComponent intersectsComponent = new IntersectsComponent(IntersectBoxeNames.attackBox_hitBox);

    public CharacterGroundAttack(Entity entity, SkeletonData data, IImpact[] impacts)
    {
        this.entity = entity;

        startAnimation = AnimationLayersBuilder.buildAnimationLayers(
                data,
                "hitBox",
                false,
                AnimationLayersBuilder.buildListener(
                        this::stop,
                        this::superEvent)
        );

        this.impacts = impacts;
        setupComponentList.add(intersectsComponent);
        selfImpacts = new IImpact[]{new ImpactHitFreezeComponent(.2f)};

        command = AbilityCommands.cross;
        manualUse = AbilitiesReadyForUse.onGround;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.attackPriority = 1;
        param.minAttackRange = 0;
        param.maxAttackRange = 60;
    }
}
