package mygdx.game.systems.gameFight.abilities.fighters;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.fighters.knight.KnightHurt;
import mygdx.game.systems.gameFight.abilities.presetAbilities.TurnBackCharacter;
import mygdx.game.systems.gameFight.abilities.fighters.knight.*;


public class Knight extends AbstractFightCharacter
{
    public Knight()
    {
//        FightEntityDesigner.designerGroundUnitEntity(getEntity(), this);

        Entity entity = getEntity();
        addAbility(new KnightCast(entity));
        addAbility(new KnightJumpForward(entity));
        addAbility(new KnightJumpUp(entity));
        addAbility(new KnightHurt(entity));
        addAbility(new KnightAttack2(entity));
        addAbility(new KnightAttack1(entity));
        addAbility(new KnightRun(entity));
        addAbility(new TurnBackCharacter(entity));
        addAbility(new KnightMove(entity));
        addAbility(new KnightStand(entity));

        setStartAbility(KnightStand.class);
    }
}
