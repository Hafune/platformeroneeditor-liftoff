package mygdx.game.systems.gameFight.abilities.effects;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import mygdx.game.systems.MyEngine;
import mygdx.game.systems.gameFight.abilities.IntersectsSet;
import mygdx.game.systems.gameFight.abilities.MyPoolUtil;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.AnimationZIndexComponent;
import mygdx.game.components.gameFight.ParticleComponent;

import java.util.HashMap;
import java.util.HashSet;

public class MyParticleEffect
{
    private static final HashMap<ParticleFiles, HashSet<MyParticleEffect>> pool = new HashMap<>();

    public final Entity entity = new Entity();

    public float offsetX;
    public float offsetY;
    private final ParticleFiles key;
    private final ParticleEffect effect = new ParticleEffect();
    private final ParticleComponent particleComponent = new ParticleComponent(this);
    private final AnimationPositionComponent animationPositionComponent = new AnimationPositionComponent();
    public final AnimationZIndexComponent animationZIndexComponent = new AnimationZIndexComponent();

    public MyParticleEffect(ParticleFiles files)
    {
        key = files;
        effect.load(files.effectFile, files.imagesDir);

        insertToEngine();
    }

    private void insertToEngine()
    {
        offsetX = 0;
        offsetY = 0;
        animationZIndexComponent.zIndex = 1;
        animationPositionComponent.positionX = 0;
        animationPositionComponent.positionY = 0;
        entity.add(particleComponent);
        entity.add(animationZIndexComponent);
        entity.add(animationPositionComponent);
        effect.start();
//        MyEngine.instance.getEngine().addEntity(entity);
    }

    public void reset()
    {
        effect.reset();
        entity.removeAll();
        insertToEngine();
    }

    public void draw(Batch batch, float positionX, float positionY, float time)
    {
        effect.setPosition(positionX + offsetX, positionY + offsetY);
        effect.update(time * 0.0169491f);
        effect.draw(batch);
        if (effect.isComplete()) putToHash();
    }

    public static MyParticleEffect getFromPool(ParticleFiles key)
    {
        return MyPoolUtil.getFromPoolMap(key, pool);
    }

    public void putToHash()
    {
//        MyEngine.instance.getEngine().removeEntity(entity);
        MyPoolUtil.putToPoolMap(key, this, pool);
    }

    public static void buildParticleEffects(ParticleFiles file, HashSet<IntersectsSet.EntityHit> entitiesWithPositionComponent)
    {
        entitiesWithPositionComponent.forEach(entityHit ->
        {
            AnimationPositionComponent component = entityHit.getEntity().getComponent(AnimationPositionComponent.class);

            MyParticleEffect effect = buildParticleEffect(file);
            effect.offsetX = entityHit.getX() - component.positionX;
            effect.offsetY = entityHit.getY() - component.positionY;
            effect.entity.add(component);
        });
    }

    public static MyParticleEffect buildParticleEffect(ParticleFiles file)
    {
        MyParticleEffect effect = MyParticleEffect.getFromPool(file);

        if (effect != null) effect.reset();
        else effect = new MyParticleEffect(file);
        return effect;
    }
}
