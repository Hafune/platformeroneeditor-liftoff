package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;

public abstract class AbstractJumpForward_with_start_up_fall_down extends AbstractJumpUp_with_start_up_fall_down
{
    public AbstractJumpForward_with_start_up_fall_down(Entity entity, SkeletonData startAnimation, AnimationLayers flyUp, AnimationLayers fallDown, SkeletonData landing)
    {
        super(entity, startAnimation, flyUp, fallDown, landing);

        command = AbilityCommands.jump_forward;
    }

    @Override
    public void setupParam()
    {
        selfCharacterImpComponent.x = Math.max(Math.abs(moveComponent.characterImp.getX()), moveComponent.maxMoveSpeed);
    }
}
