package mygdx.game.systems.gameFight.abilities;

import java.util.HashMap;
import java.util.HashSet;

public final class MyPoolUtil
{
    public static <T, K>  boolean putToPoolMap(K key, T obj, HashMap<K, HashSet<T>> pool)
    {
        if (!pool.containsKey(key)) pool.put(key, new HashSet<T>());
        HashSet<T> set = pool.get(key);
        return set.add(obj);
    }

    public static <T, K> T getFromPoolMap(K key, HashMap<K, HashSet<T>> pool)
    {
        T obj = null;
        if (pool.containsKey(key))
        {
            HashSet<T> set = pool.get(key);
            if (set != null)
            {
                obj = getFromPool(set);
            }
        }
        return obj;
    }

    public static <T, K> void disposePoolMap(HashMap<K, HashSet<T>> pool)
    {
        for (HashSet<T> set : pool.values())
        {
            set.clear();
        }
        pool.clear();
    }

    public static <T> T getFromPool(HashSet<T> pool)
    {
        T obj = null;
        if (pool.iterator().hasNext())
        {
            obj = pool.iterator().next();
            pool.remove(obj);
        }
        return obj;
    }

    public static <T> T newInstance(Class<T> c)
    {
        try
        {
            return c.newInstance();
        } catch (InstantiationException | IllegalAccessException e)
        {
            throw new Error();
        }
    }
}
