package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractWinnerPose;
import mygdx.game.components.gameFight.AbilityComponent;

import java.util.ArrayList;

public class FightAbilitiesIIParamMap
{
    private static final FightAbilitiesIIParamMap instance = new FightAbilitiesIIParamMap();

    public static FightAbilitiesIIParamMap getInstance()
    {
        return instance;
    }

    public FightAbilitiesIIComponent getPropertyFor(AbstractAbility ability)
    {
        FightAbilitiesIIComponent property = ability.id.getComponent(FightAbilitiesIIComponent.class);
        if (property == null)
        {
            property = new FightAbilitiesIIComponent();
            ability.id.add(property);
        }
        return property;
    }

    public final IPriority winnerPose = this::getWinnerPose;
    public final IPriority attackPriority = this::getAttackPriority;
    public final IPriority turnBackPriority = this::getTurnBackPriority;
    public final IPriority holdPositionPriority = this::getHoldPositionPriority;
    public final IPriority distanceReductionPriority = this::getDistanceReductionPriority;

    public AbstractAbility findPriorityAbilities(Entity entity, IPriority priority, float desiredPriority, ArrayList<AbstractAbility> list)
    {
        AbilityComponent abilityComponent = AbilityComponent.map.get(entity);

        AbstractAbility nextAbility = list.stream().filter(ability ->
                priority.get(ability) > 0 &&
                        (ability.manualUse.readyForUse(entity, abilityComponent.ability, ability) ||
                                ability == abilityComponent.ability && ability.infinitely)).

                max((a0, a1) -> {

                    float dif0 = Math.abs(priority.get(a0) - desiredPriority);
                    float dif1 = Math.abs(priority.get(a1) - desiredPriority);

                    return Float.compare(dif1, dif0);
                }).
                orElse(null);

        return abilityComponent.ability != nextAbility ? nextAbility : null;
    }

    private float getWinnerPose(AbstractAbility ability)
    {
        return ability instanceof AbstractWinnerPose ? 1 : 0;
    }

    private float getAttackPriority(AbstractAbility ability)
    {
        return getPropertyFor(ability).attackPriority;
    }

    private float getTurnBackPriority(AbstractAbility ability)
    {
        return getPropertyFor(ability).turnBackPriority;
    }

    private float getHoldPositionPriority(AbstractAbility ability)
    {
        return getPropertyFor(ability).holdPositionPriority;
    }

    private float getDistanceReductionPriority(AbstractAbility ability)
    {
        return getPropertyFor(ability).distanceReductionPriority;
    }

    @FunctionalInterface
    private interface IPriority
    {
        float get(AbstractAbility ability);
    }
}
