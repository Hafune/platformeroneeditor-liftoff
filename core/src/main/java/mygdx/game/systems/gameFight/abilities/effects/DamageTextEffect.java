package mygdx.game.systems.gameFight.abilities.effects;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Batch;
import mygdx.game.components.gameFight.*;
import mygdx.game.lib.MyMath;
import mygdx.game.myWidgets.styles.LabelsStyles;
import mygdx.game.systems.gameFight.abilities.MyPoolUtil;

import java.util.HashMap;
import java.util.HashSet;

public class DamageTextEffect
{
    private static final HashMap<LabelsStyles, HashSet<DamageTextEffect>> pool = new HashMap<>();
    private static int lastZIndex = 0;
    private static int damageTextInScreen = 0;

    public final Entity entity = new Entity();

    public float offsetX;
    public float offsetY;
    public float scaleMultiply = 1;

    public float duration;
    public final float totalDuration = 90;
    public final float fadeInTime = totalDuration * .1f;
    public final float fadeOutAfterTime = totalDuration * .7f;

    private float pointX;
    private float pointY;
    private boolean resetPosition;

    private final LabelsStyles key;
//    private final Label label;
    private final DamageTextComponent damageTextComponent = new DamageTextComponent(this);
    public final AnimationPositionComponent animationPositionComponent = new AnimationPositionComponent();
    public final AnimationZIndexComponent animationZIndexComponent = new AnimationZIndexComponent();

    public final GravityComponent gravityComponent = new GravityComponent();
    public final PositionComponent positionComponent = new PositionComponent();
    public final PositionMoveComponent positionMoveComponent = new PositionMoveComponent();
    public final AnimationInterpolationComponent interpolationComponent = new AnimationInterpolationComponent();

    public DamageTextEffect(LabelsStyles styles, String text)
    {
        key = styles;
//        label = new Label(text, ScreenContainer.Companion.getGameSkin(), styles.name());

        insertToEngine(text);
    }

    private void insertToEngine(String text)
    {
//        label.setText(text);
        animationZIndexComponent.zIndex = 1;
        animationPositionComponent.positionX = 0;
        animationPositionComponent.positionY = 0;
        entity.add(damageTextComponent);
        entity.add(animationZIndexComponent);
        entity.add(animationPositionComponent);

        duration = 0;
        scaleMultiply = 1;
        resetPosition = true;
        animationZIndexComponent.zIndex = lastZIndex++;

//        MyEngine.instance.getEngine().addEntity(entity);
        damageTextInScreen++;
    }

    public void setPriority(float priority)
    {
        animationZIndexComponent.zIndex = (int) (lastZIndex + priority * 100);
    }

    public void restart(String text)
    {
        duration = 0;

        entity.removeAll();
        insertToEngine(text);
    }

    public void draw(Batch batch, float positionX, float positionY, float time)
    {
        positionX += offsetX;
        positionY += offsetY;

        if (resetPosition)
        {
            pointX = positionX;
            pointY = positionY;
            resetPosition = false;
        }
        else
        {
            pointX = MyMath.INSTANCE.tendsToValue(pointX, positionX, .001f * time, .03f * time);
            pointY = MyMath.INSTANCE.tendsToValue(pointY, positionY, .001f * time, .03f * time);
        }

        float alpha;
        float fontScale;
        if (duration < fadeInTime)
        {
            alpha = duration / fadeInTime;
//            fontScale = .3f / ScreenContainer.instance.getCamera().zoom * (2 - alpha);
//            fontScale *= scaleMultiply;
//            label.setFontScale(fontScale);
        }
        else
        {
            float a = duration - fadeOutAfterTime;
            float b = totalDuration - fadeOutAfterTime;
            alpha = a > 0 ? (b - a) / b : 1;
//            fontScale = .3f / ScreenContainer.instance.getCamera().zoom;
//            fontScale *= scaleMultiply;
//            label.setFontScale(fontScale);
//            label.setFontScaleY(fontScale * alpha);
        }

//        label.setPosition(pointX - label.getMinWidth() / 2, pointY);
//        label.draw(batch, alpha);
        duration += time;
        if (duration >= totalDuration) putToHash();
    }

    public static DamageTextEffect getFromPool(LabelsStyles key)
    {
        return MyPoolUtil.getFromPoolMap(key, pool);
    }

    public void putToHash()
    {
//        MyEngine.instance.getEngine().removeEntity(entity);
        MyPoolUtil.putToPoolMap(key, this, pool);
        damageTextInScreen--;
        if (damageTextInScreen == 0) lastZIndex = 0;
    }

    public void updateText(float text)
    {
//        label.setText(String.valueOf((int) text));
        resetPosition = true;
    }

    public static DamageTextEffect buildDamageText(LabelsStyles styles, float text)
    {
        return buildDamageText(styles, String.valueOf((int) text));
    }

    public static DamageTextEffect buildDamageText(LabelsStyles styles, String text)
    {
        DamageTextEffect effect = DamageTextEffect.getFromPool(styles);

        if (effect != null) effect.restart(text);
        else effect = new DamageTextEffect(styles, text);
        return effect;
    }
}
