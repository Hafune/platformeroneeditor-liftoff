package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesReadyForUse;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.components.gameFight.ImpactComponents.RemoveCharacterComponent;

public abstract class AbstractDead extends AbstractAbility
{
    private final RemoveCharacterComponent removeCharacterComponent = new RemoveCharacterComponent();

    public AbstractDead(Entity entity, SkeletonData data)
    {
        this.entity = entity;
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data,
                "hitBox",
                false,
                null);
                //AnimationLayersBuilder.buildListener(this::complete, null));
        defCancelable = false;

        autoUse = AbilitiesReadyForUse.justDead;
    }

    @Override
    public void update()
    {
        if (duration == 180) entity.add(removeCharacterComponent);
    }

    private void complete()
    {

    }
}
