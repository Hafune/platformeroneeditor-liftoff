package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.FlipXComponent;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;
import org.jetbrains.annotations.NotNull;

public class SelfCharacterImpSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public SelfCharacterImpSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionMoveComponent.class, SelfCharacterImpComponent.class).get());

        getEngine().addEntityListener(Family.all(SelfCharacterImpComponent.class).get(), new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                SelfCharacterImpComponent selfCharacterImpComponent = entity.getComponent(SelfCharacterImpComponent.class);
                selfCharacterImpComponent.restoreTimerFromDefTimer();
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            SelfCharacterImpComponent selfCharacterImpComponent = entity.getComponent(SelfCharacterImpComponent.class);
            PositionMoveComponent moveComponent = entity.getComponent(PositionMoveComponent.class);
            FlipXComponent flipXComponent = entity.getComponent(FlipXComponent.class);

            int signX = 1;
            if (flipXComponent != null) signX = flipXComponent.getSign();

            moveComponent.characterImp.setXY(selfCharacterImpComponent.x * signX, selfCharacterImpComponent.y);
        }
    }
}
