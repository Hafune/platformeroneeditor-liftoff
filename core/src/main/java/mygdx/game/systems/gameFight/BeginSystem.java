package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import mygdx.game.TileMapRender;
import mygdx.game.inputs.MyInputHandler;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.WorldProcessingSystem;
import mygdx.game.systems.gameFight.abilities.fighters.*;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.components.gameFight.TargetTypeComponent;
import mygdx.game.components.gameFight.TeamComponent;
import org.jetbrains.annotations.NotNull;

public class BeginSystem extends MyAbstractSystem
{
    public BeginSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void update(float deltaTime)
    {
//        WorldProcessingSystem.instance.setProcessing(false);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(null);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setConvertDirection(false);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler1().setConvertDirection(false);
//
//        TileMapRender.instance.setVisible(false);
//        FightCameraSystem.getInstance().reset();

        //иницыализировать команду игрока
        //иницыализировать команду противника

        /*Vicky vicky = FightersBuilder.createCharacter(Vicky.class);
        addTeam(vicky.getEntity(), Teams.team1);
        UserControllerUpdateSystem.getInstance().applyUserControllerComponent1(vicky);*/

//        Farah farah = FightersBuilder.createCharacter(Farah.class);
//        addTeam(farah.getEntity(), Teams.team1);
//
//        Vicky knight = FightersBuilder.createCharacter(Vicky.class);
//        addTeam(knight.getEntity(), Teams.team0);
//        UserControllerUpdateSystem.getInstance().applyUserControllerComponent0(knight);

        setProcessing(false);
    }

    private void addTeam(Entity entity, Teams team)
    {
        entity.getComponent(TeamComponent.class).team = team;
        entity.getComponent(TargetTypeComponent.class).type = TargetType.enemy;
//        entity.getComponent(PositionComponent.class).positionX = FightBackGroundSystem.getInstance().getRightEdge() / 2;//(float) (Math.random() * 400) + 400;
        entity.getComponent(PositionComponent.class).positionY = 200;

        FightTeamSpectator.createCharacter(entity, team);
    }
}
