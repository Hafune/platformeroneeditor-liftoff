package mygdx.game.systems.gameFight.abilities.presetAbilities;

public enum IntersectBoxeNames
{
    attackBox,
    hitBox,
    attackBox_hitBox,
    hitBox_attackBox,
    collisionBox
}
