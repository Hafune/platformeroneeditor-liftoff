package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.components.gameFight.HitPointComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHurtComponent;
import mygdx.game.components.gameFight.PositionComponent;

import java.util.Arrays;

public enum AbilitiesReadyForUse
{
    returnFalse(AbilitiesReadyForUse::returnFalse),

    autoStand(AbilitiesReadyForUse::currentAbilityComplete,
            AbilitiesReadyForUse::dont_switch_to_himself,
            AbilitiesReadyForUse::positionYZero),

    manualStand(AbilitiesReadyForUse::complete_or_cancelable_and_infinitely,
            AbilitiesReadyForUse::dont_switch_to_himself,
            AbilitiesReadyForUse::positionYZero),

    move(AbilitiesReadyForUse::currentAbilityComplete,
            AbilitiesReadyForUse::positionYZero),

    run(AbilitiesReadyForUse::cancelableOrComplete,
            AbilitiesReadyForUse::dont_switch_to_himself,
            AbilitiesReadyForUse::positionYZero),

    turnBack(AbilitiesReadyForUse::complete_or_cancelable_and_infinitely,
            AbilitiesReadyForUse::positionYZero),

    returnTrue(AbilitiesReadyForUse::returnTrue),

    hurt(AbilitiesReadyForUse::hurt),

    justDead(AbilitiesReadyForUse::hitPointIsZero,
            AbilitiesReadyForUse::dont_switch_to_himself),

    onGround(AbilitiesReadyForUse::cancelableOrComplete,
            AbilitiesReadyForUse::dont_switch_to_himself,
            AbilitiesReadyForUse::positionYZero,
            AbilitiesReadyForUse::useInAbility),

    inAir(AbilitiesReadyForUse::cancelableOrComplete,
            AbilitiesReadyForUse::positionYNotZero,
            AbilitiesReadyForUse::useInAbility);


    private final IReadyForUse[] checkList;

    AbilitiesReadyForUse(IReadyForUse... checkList)
    {
        this.checkList = checkList;
    }

    public boolean readyForUse(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return Arrays.stream(checkList).allMatch(ready -> ready.readyForUse(entity, currentAbility, checkedAbility));
    }

    //----------------------------------------------------------------------------------------------------------------------------------------

    private static boolean hurt(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        ImpactHurtComponent hurtComponent = entity.getComponent(ImpactHurtComponent.class);
        return hurtComponent != null && hurtComponent.timer > 0;
    }

    private static boolean cancelableOrComplete(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return (currentAbility.cancelable || currentAbility.complete);
    }

    private static boolean complete_or_cancelable_and_infinitely(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return ((currentAbility.cancelable && currentAbility.infinitely) || currentAbility.complete);
    }

    private static boolean currentAbilityComplete(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return currentAbility.complete;
    }

    private static boolean positionYZero(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
        return positionComponent.positionY == 0;
    }

    private static boolean positionYNotZero(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
        return positionComponent.positionY > 0;
    }

    private static boolean hitPointIsZero(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        HitPointComponent hitPointComponent = entity.getComponent(HitPointComponent.class);
        return hitPointComponent.hp <= 0;
    }

    private static boolean useInAbility(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return (checkedAbility.useInAbility == null || checkedAbility.useInAbility == currentAbility.getClass());
    }

    private static boolean dont_switch_to_himself(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return (checkedAbility != currentAbility);
    }

    private static boolean returnFalse(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return false;
    }

    private static boolean returnTrue(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility)
    {
        return true;
    }
    //----------------------------------



    @FunctionalInterface
    private interface IReadyForUse
    {
        boolean readyForUse(Entity entity, AbstractAbility currentAbility, AbstractAbility checkedAbility);
    }

}
