package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Entity;

import java.util.HashSet;

public final class IntersectsSet
{
    private static final HashSet<EntityHit> pool = new HashSet<>();

    private final HashSet<Entity> insertTargets = new HashSet<>();
    private final HashSet<EntityHit> checkedTargets = new HashSet<>();

    public HashSet<EntityHit> getCheckedEntities()
    {
        return checkedTargets;
    }

    public void clearFrame()
    {
        pool.addAll(checkedTargets);
        checkedTargets.clear();
    }

    public void clear()
    {
        pool.addAll(checkedTargets);
        insertTargets.clear();
        checkedTargets.clear();
    }

    public void add(Entity entity, float x, float y)
    {
        if (insertTargets.add(entity))
        {
            EntityHit hit = MyPoolUtil.getFromPool(pool);
            if (hit != null) hit.set(entity, x, y);
            else hit = new EntityHit(entity, x, y);

            checkedTargets.add(hit);
        }
    }

    public static class EntityHit
    {
        private float x;
        private float y;
        private Entity entity;

        private EntityHit(Entity entity, float x, float y)
        {
            this.x = x;
            this.y = y;
            this.entity = entity;
        }

        private void set(Entity entity, float x, float y)
        {
            this.x = x;
            this.y = y;
            this.entity = entity;
        }

        public float getX()
        {
            return x;
        }

        public float getY()
        {
            return y;
        }

        public Entity getEntity()
        {
            return entity;
        }
    }
}
