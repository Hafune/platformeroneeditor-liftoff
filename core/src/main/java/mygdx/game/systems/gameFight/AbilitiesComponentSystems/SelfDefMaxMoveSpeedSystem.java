package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.components.gameFight.PositionMoveComponent;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.selfComponents.SelfDefMaxMoveSpeedComponent;
import org.jetbrains.annotations.NotNull;

public class SelfDefMaxMoveSpeedSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public SelfDefMaxMoveSpeedSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(PositionMoveComponent.class, SelfDefMaxMoveSpeedComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            SelfDefMaxMoveSpeedComponent selfDefMaxMoveSpeedComponent = entity.getComponent(SelfDefMaxMoveSpeedComponent.class);
            PositionMoveComponent moveComponent = entity.getComponent(PositionMoveComponent.class);

            moveComponent.maxMoveSpeed = moveComponent.defMaxMoveSpeed * selfDefMaxMoveSpeedComponent.multiply;

        }
    }
}
