package mygdx.game.systems.gameFight.abilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.components.CameraTargetComponent;
import mygdx.game.components.gameFight.AbilityComponent;
import mygdx.game.components.gameWorld.characterComponents.*;
import mygdx.game.components.gameFight.*;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.systems.gameFight.abilities.fighters.TargetType;
import mygdx.game.systems.gameFight.abilities.fighters.Teams;
import mygdx.game.components.gameFight.ImpactComponents.ImpactDamageComponent;

import java.util.stream.Stream;

public final class FightEntityDesigner {
    public static void designerSpellEntity(Entity entity, AbstractAbility ability) {
        Stream.of(
                new FlipXComponent(),
                new PositionComponent(),
                new PositionMoveComponent(),
                new AbilityComponent(ability),
                new AnimationZIndexComponent(),
                new AnimationLayersComponent(),
                new AnimationPositionComponent(),
                new AnimationInterpolationComponent(),
                new TargetTypeComponent(TargetType.enemy)
        ).forEach(entity::add);
    }

    public static void designerGroundUnitEntity(Entity entity, Entity character) {
        PositionMoveComponent moveComponent = new PositionMoveComponent();
        moveComponent.defGroundFriction = .05f;
        moveComponent.defMaxMoveSpeed = 1.5f;
        moveComponent.defAcceleration = .5f;
        moveComponent.defJumpHeight = 60f;

        Stream.of(
                moveComponent,
                new FlipXComponent(),
                new GravityComponent(),
                new PositionComponent(),

                new HitPointComponent(),
                new ImpactDamageComponent(),
                new DamageRecordComponent(),

                new ArenaBoundsComponent(),
                new CameraTargetComponent(),
                new BodyCollisionComponent(),
                new IntersectTargetComponent(),
                new AnimationLayersComponent(),
                new AnimationZIndexComponent(),
                new AnimationPositionComponent(),
                new AnimationInterpolationComponent(),
                new IIComponent(),

                new CurrentTargetComponent(),
                new CharacterLinkComponent(character),

                new TeamComponent(Teams.team0),
                new TargetTypeComponent(TargetType.enemy)
        ).forEach(entity::add);
    }

    public static void designerMapUnitEntity(Entity entity, Entity character) {
        Stream.of(
                new PositionComponent(),
//                new AnimationSkinComponent(),
//                new InteractComponent(),
                new StatusComponent(),
                new DragBallComponent(),
//                new CollisionMapComponent(),
                new RepulsiveForceComponent(),

                new CharacterLinkComponent(character)
        ).forEach(entity::add);
    }
}
