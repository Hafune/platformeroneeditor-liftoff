package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.fighters.FightTeamSpectator;
import mygdx.game.systems.gameFight.abilities.fighters.Teams;
import mygdx.game.components.gameFight.ImpactComponents.RemoveCharacterComponent;
import mygdx.game.components.gameFight.CharacterLinkComponent;
import mygdx.game.components.gameFight.TeamComponent;
import org.jetbrains.annotations.NotNull;

public class RemoveCharacterSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public RemoveCharacterSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(CharacterLinkComponent.class, RemoveCharacterComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        entities.forEach(entity -> {
            entity.remove(RemoveCharacterComponent.class);
//            FightersBuilder.putToPool(entity.getComponent(CharacterLinkComponent.class).entity);

            Teams team = entity.getComponent(TeamComponent.class).team;
            FightTeamSpectator.characterEntityMap.get(team).remove(entity);
        });
    }
}
