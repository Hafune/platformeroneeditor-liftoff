package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import mygdx.game.assetsClasses.MyShader;
import mygdx.game.components.gameFight.HitPointComponent;
import mygdx.game.components.gameFight.TargetTypeComponent;
import mygdx.game.components.gameFight.TeamComponent;
import mygdx.game.inputs.IMyButtonTarget;
import mygdx.game.inputs.MyPadButtons;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.fighters.FightTeamSpectator;
import mygdx.game.systems.gameFight.abilities.fighters.TargetType;
import mygdx.game.systems.gameFight.abilities.fighters.Teams;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;

public abstract class EndOfBattleSystem extends MyAbstractSystem implements IMyButtonTarget
{
    //private ImmutableArray<Entity> impacts;
    private final TeamComponent userTeam = new TeamComponent(Teams.team0);
    private final TargetTypeComponent enemy = new TargetTypeComponent(TargetType.enemy);
    private final TargetTypeComponent friend = new TargetTypeComponent(TargetType.friend);

    private IStepFunc updateFunc = this::checkBattleState;
    private IStepFunc lastUpdateFunc = this::checkBattleState;
    private float stepTime;
    private final float fadeTime = .4f;

    public EndOfBattleSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        //impacts = engine.getEntitiesFor(Family.all(FightCharacterComponent.class, ImpactDeath.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        if (lastUpdateFunc != updateFunc)
        {
            stepTime = 0;
            lastUpdateFunc = updateFunc;
        }
        stepTime += deltaTime * .016;
        if (updateFunc != null) updateFunc.update(deltaTime);
    }

    private void checkBattleState(float deltaTime)
    {
        HashSet<Entity> set = FightTeamSpectator.getTargetEntitiesFor(userTeam, enemy);
        Entity entity = set.stream().filter(e -> e.getComponent(HitPointComponent.class).hp > 0).findAny().orElse(null);
        if (set.isEmpty() || entity == null)
        {
//            UserControllerUpdateSystem.instance.applyUserControllerComponent0(null);
//            UserControllerUpdateSystem.instance.applyUserControllerComponent1(null);
//            MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(this);
//            MyInputHandler.Companion.getInstance().getKeyboardHandler1().setIMyButtonTarget(this);

            updateFunc = this::updateCloseShader;
        }
    }

    private void updateCloseShader(float deltaTime)
    {
        updateShaderScreenCloseOpen(stepTime);
        if (stepTime >= fadeTime) updateFunc = this::resetCharacterPosition;
    }

    private void updateShaderScreenCloseOpen(float time)
    {
//        ShaderProgram shaderProgram = MyShader.screenCloseOpen.get();
//        ScreenContainer.instance.setScreenFilterShaderProgram(shaderProgram);
//        ScreenContainer.instance.getFilterColor().set(1, 1, 1, 0);
//
//        shaderProgram.begin();
//        shaderProgram.setUniformf("time", time / fadeTime * 1.2f);
//        shaderProgram.setUniformf("width", ScreenContainer.instance.getInterfaceWidth());
//        shaderProgram.setUniformf("height", ScreenContainer.instance.getInterfaceHeight());
//        shaderProgram.end();
    }

    private void resetCharacterPosition(float deltaTime)
    {
        HashSet<Entity> team0 = FightTeamSpectator.getTargetEntitiesFor(userTeam, friend);

//        float left = FightBackGroundSystem.getInstance().getLeftEdge();
//        float right = FightBackGroundSystem.getInstance().getRightEdge();
//        float width = right - left;
//        int size = team0.size();
//        float offset = width / (1 + size);
//
//        for (int i = 0; i < size; i++)
//        {
//            Entity entity = team0.iterator().next();
//            team0.remove(entity);
//
//            PositionComponent positionComponent = entity.getComponent(PositionComponent.class);
//            AnimationPositionComponent animationPositionComponent = entity.getComponent(AnimationPositionComponent.class);
//
//            positionComponent.positionX = left + offset * (i + 1);
//            animationPositionComponent.positionX = positionComponent.positionX;
//
//            AbstractAbility winnerPose = FightAbilitiesIIParamMap.getInstance().findPriorityAbilities(entity, FightAbilitiesIIParamMap.getInstance().winnerPose, 1,
//                    entity.getComponent(AbilitiesListComponent.class).abilities);
//
//            entity.getComponent(AbilityComponent.class).changeAbility(winnerPose);
//        }
//        FightCameraSystem.getInstance().reset();
//
//        updateFunc = this::updateOpenShader;
//        updateOpenShader(deltaTime);
    }

    private void updateOpenShader(float deltaTime)
    {
        float time = Math.max(fadeTime - stepTime, 0);
        updateShaderScreenCloseOpen(time);
        if (time == 0) updateFunc = this::fadeOut;
    }

    private void fadeOut(float deltaTime)
    {
        updateShaderFade(stepTime);
        if (stepTime >= 1f) updateFunc = this::fadeIn;
    }

    private void updateShaderFade(float time)
    {
        ShaderProgram shaderProgram = MyShader.fade.get();
//        ScreenContainer.instance.setScreenFilterShaderProgram(shaderProgram);

        shaderProgram.begin();
        shaderProgram.setUniformf("time", time);
        shaderProgram.end();
    }

    private void fadeIn(float deltaTime)
    {
        updateShaderFade(1f - stepTime);
        if (stepTime >= 1) fin();
    }

    private void fin()
    {
        updateFunc = null;
        System.out.println("fadeIn");

//        WorldProcessingSystem.instance.setProcessing(false);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(null);
//        ScreenContainer.instance.getCamera().zoom = CameraControl.instance.getDefZoom();
//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setConvertDirection(true);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler1().setConvertDirection(true);
//
//        TileMapRender.instance.setVisible(true);
//
////        FightersBuilder.putAllToPool();
//
//        FightSystem.getInstance().setProcessing(false);
//        BeginSystem.getInstance().setProcessing(true);
//        WorldProcessingSystem.instance.setProcessing(true);
//        FightBackGroundSystem.getInstance().getFightScene().setVisible(false);
//
//        updateFunc = this::checkBattleState;
//
//        MyInputHandler.Companion.getInstance().getKeyboardHandler0().setIMyButtonTarget(null);
//        MyInputHandler.Companion.getInstance().getKeyboardHandler1().setIMyButtonTarget(null);
    }



    @Override
    public void buttonDown(MyPadButtons button)
    {
        System.out.println("EndOfBattle buttonDown");
    }

    @Override
    public void buttonUp(@NotNull MyPadButtons button) {

    }

    @FunctionalInterface
    private interface IStepFunc
    {
        void update(float deltaTime);
    }
}
