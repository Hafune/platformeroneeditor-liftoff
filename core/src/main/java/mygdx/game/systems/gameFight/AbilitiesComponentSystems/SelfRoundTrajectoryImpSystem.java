package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.lib.MyVector;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.AnimationZIndexComponent;
import mygdx.game.components.gameFight.PositionComponent;
import mygdx.game.components.gameFight.selfComponents.SelfRoundTrajectoryImpComponent;
import org.jetbrains.annotations.NotNull;

public class SelfRoundTrajectoryImpSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public SelfRoundTrajectoryImpSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(AnimationZIndexComponent.class, PositionComponent.class, SelfRoundTrajectoryImpComponent.class).get());

        getEngine().addEntityListener(Family.all(SelfRoundTrajectoryImpComponent.class).get(), new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                SelfRoundTrajectoryImpComponent selfRoundTrajectoryImpComponent = entity.getComponent(SelfRoundTrajectoryImpComponent.class);
                selfRoundTrajectoryImpComponent.restoreTimerFromDefTimer();
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            SelfRoundTrajectoryImpComponent selfRoundTrajectoryImpComponent = entity.getComponent(SelfRoundTrajectoryImpComponent.class);
            MyVector vector = selfRoundTrajectoryImpComponent.myVector;
            PositionComponent parentPosition = selfRoundTrajectoryImpComponent.parentPosition;
            PositionComponent positionComponent = entity.getComponent(PositionComponent.class);

            if (parentPosition != null)
            {
                positionComponent.positionX = parentPosition.positionX + vector.getX();//MyMath.tendsToValue(positionComponent.positionX, parentPosition.positionX + vector.getX(), 0, .05f);
                positionComponent.positionY = parentPosition.positionY + 10 + vector.getY() / 10;//MyMath.tendsToValue(positionComponent.positionY, parentPosition.positionY + 10 + vector.getY() / 10, 0, .1f);
                vector.addAngle(3);

                AnimationZIndexComponent animationZIndexComponent = entity.getComponent(AnimationZIndexComponent.class);
                animationZIndexComponent.zIndex = (int) -vector.getY();
            }
        }
    }
}
