package mygdx.game.systems.gameFight.abilities.presetAbilities;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.AbilityCommands;

public abstract class AbstractJumpForward extends AbstractJumpUp
{
    public AbstractJumpForward(Entity entity)
    {
        super(entity);
        command = AbilityCommands.jump_forward;
    }

    @Override
    public void setupParam()
    {
        selfCharacterImpComponent.x = Math.max(Math.abs(moveComponent.characterImp.getX()), moveComponent.maxMoveSpeed);
        moveComponent.characterImp.setValue(0);
    }
}
