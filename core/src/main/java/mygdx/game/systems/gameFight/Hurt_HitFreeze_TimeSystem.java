package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHurtComponent;
import org.jetbrains.annotations.NotNull;

public class Hurt_HitFreeze_TimeSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> hurts;
    private ImmutableArray<Entity> hitFreezes;

    public Hurt_HitFreeze_TimeSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        Family familyHurt = Family.all(ImpactHurtComponent.class).get();
        hurts = engine.getEntitiesFor(familyHurt);
        Family familyHitFreeze = Family.all(ImpactHitFreezeComponent.class).get();
        hitFreezes = engine.getEntitiesFor(familyHitFreeze);

        engine.addEntityListener(familyHurt, new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                ImpactHurtComponent hurtComponent = entity.getComponent(ImpactHurtComponent.class);
                hurtComponent.timer = hurtComponent.defTimer;
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });

        engine.addEntityListener(familyHitFreeze, new EntityListener()
        {
            @Override
            public void entityAdded(Entity entity)
            {
                ImpactHitFreezeComponent impactHitFreezeComponent = entity.getComponent(ImpactHitFreezeComponent.class);
                impactHitFreezeComponent.timer = impactHitFreezeComponent.defTimer;
            }

            @Override
            public void entityRemoved(Entity entity)
            {

            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        hurts.forEach(entity -> entity.remove(ImpactHurtComponent.class));
        hitFreezes.forEach(entity -> {
            ImpactHitFreezeComponent hitFreezeComponent = entity.getComponent(ImpactHitFreezeComponent.class);
            if (hitFreezeComponent.timer > 0) hitFreezeComponent.timer--;
            if (hitFreezeComponent.timer <= 0) entity.remove(ImpactHitFreezeComponent.class);
        });
    }
}
