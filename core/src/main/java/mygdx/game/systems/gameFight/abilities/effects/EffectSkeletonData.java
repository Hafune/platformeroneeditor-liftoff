package mygdx.game.systems.gameFight.abilities.effects;

import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public enum EffectSkeletonData
{
    HitEffect0(DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Effects +
                    "HitEffect/HitEffect0"));

    private SkeletonData data;

    EffectSkeletonData(SkeletonData data)
    {
        this.data = data;
    }

    public SkeletonData getData()
    {
        return data;
    }
}
