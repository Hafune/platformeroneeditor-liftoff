package mygdx.game.systems.gameFight.abilities.fighters;

import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.fighters.vicky.*;
import mygdx.game.systems.gameFight.abilities.presetAbilities.TurnBackCharacter;

public class Vicky extends AbstractFightCharacter
{
    public Vicky()
    {
//        FightEntityDesigner.designerGroundUnitEntity(getEntity(), this);

        Entity entity = getEntity();
        addAbility(new VickyJumpForward(entity));
        addAbility(new VickyJumpUp(entity));
        addAbility(new VickyHurt(entity));
        addAbility(new VickyDead(entity));
        addAbility(new VickyPunch4(entity));
        addAbility(new VickyPunch3(entity));
        addAbility(new VickyPunch1(entity));
        addAbility(new VickyPunch2(entity));
        addAbility(new VickyPunch0(entity));
        addAbility(new VickyRun(entity));
        addAbility(new VickyMove(entity));
        addAbility(new TurnBackCharacter(entity));
        addAbility(new VickyStand(entity));

        addAbility(new VickyWinnerPose(entity));


        setStartAbility(VickyStand.class);
    }
}
