package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.components.gameFight.AbilityComponent;
import mygdx.game.components.gameFight.AnimationLayersComponent;
import mygdx.game.components.gameFight.AnimationPositionComponent;
import mygdx.game.components.gameFight.IntersectsComponent;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.DragonBoneClasses.AnimationLayers;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import org.jetbrains.annotations.NotNull;

public class AbilitiesComponentSetupSystem extends MyAbstractSystem
{
    public AbilitiesComponentSetupSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    private ImmutableArray<Entity> entities;

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(AbilityComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            AbilityComponent abilityComponent = AbilityComponent.map.get(entity);

            if (abilityComponent.cancelLast && abilityComponent.ability != null)
            {
                if (abilityComponent.lastAbility != null)
                {
                    abilityComponent.lastAbility.stop();
                }
                abilityComponent.cancelLast = false;

                AbstractAbility ability = abilityComponent.ability;
                abilityComponent.lastAbility = ability;



                ability.complete = false;
                ability.cancelable = ability.defCancelable;
                ability.duration = 0;
                if (ability.setupComponentList != null)
                    ability.setupComponentList.forEach(
                            c -> {
                                ability.entity.add(c);
                                ability.removeSet.add(c.getClass());
                            }
                    );
                ability.setupParam();

                AnimationLayers animation = ability.startAnimation;
                AnimationLayersComponent animationLayersComponent = entity.getComponent(AnimationLayersComponent.class);
                AnimationPositionComponent animationPositionComponent = entity.getComponent(AnimationPositionComponent.class);

                if (animation != null && animationLayersComponent != null && animationPositionComponent != null)
                {
                    animation.restartAnimation();
                    animationLayersComponent.animation = animation;
                    animation.transform(animationPositionComponent.positionX, animationPositionComponent.positionY, 0);
                }



                IntersectsComponent intersectsComponent = entity.getComponent(IntersectsComponent.class);
                if (intersectsComponent != null) intersectsComponent.clear();
            }
        }
    }
}
