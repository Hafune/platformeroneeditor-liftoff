package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractJumpForward;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class KnightJumpForward extends AbstractJumpForward
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Jump/Jump"
    );

    public KnightJumpForward(Entity entity)
    {
        super(entity);
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
    }
}
