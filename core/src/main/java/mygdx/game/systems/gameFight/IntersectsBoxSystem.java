package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.esotericsoftware.spine.SkeletonBounds;
import mygdx.game.components.gameFight.*;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.fighters.TargetType;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;
import org.jetbrains.annotations.NotNull;

public class IntersectsBoxSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> intersects;
    private ImmutableArray<Entity> entities;
    private ImmutableArray<Entity> targets;

    public IntersectsBoxSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        intersects = engine.getEntitiesFor(Family.all(IntersectsComponent.class).get());
        entities = engine.getEntitiesFor(Family.all(TargetTypeComponent.class, AbilityComponent.class, AnimationLayersComponent.class, IntersectsComponent.class).get());
        targets = engine.getEntitiesFor(Family.all(TeamComponent.class, AbilityComponent.class, AnimationLayersComponent.class, IntersectTargetComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        intersects.forEach(entity -> entity.getComponent(IntersectsComponent.class).clearFrame());

        for (Entity entity0 : entities)
        {
            for (int i = 0; i < targets.size(); i++)
            {
                Entity target = targets.get(i);

                AbstractAbility ability0 = AbilityComponent.map.get(entity0).ability;
                AbstractAbility ability1 = AbilityComponent.map.get(target).ability;

                if (ability0 != null && ability1 != null)
                {
                    AnimationLayersComponent animationLayersComponent0 = ability0.entity.getComponent(AnimationLayersComponent.class);
                    AnimationLayersComponent animationLayersComponent1 = ability1.entity.getComponent(AnimationLayersComponent.class);

                    if (animationLayersComponent0.animation != null && animationLayersComponent1.animation != null)
                    {
                        TargetTypeComponent targetTypeComponent = entity0.getComponent(TargetTypeComponent.class);
                        TeamComponent team0 = entity0.getComponent(TeamComponent.class);
                        TeamComponent team1 = target.getComponent(TeamComponent.class);

                        if (targetTypeComponent.type == TargetType.enemy && team0.team != team1.team)
                        {
                            SkeletonBounds box0 = animationLayersComponent0.animation.getSkeletonBounds(IntersectBoxeNames.attackBox);
                            SkeletonBounds box1 = animationLayersComponent1.animation.getSkeletonBounds(IntersectBoxeNames.hitBox);

                            IntersectsComponent intersects0 = entity0.getComponent(IntersectsComponent.class);

                            intersect(box0, box1, intersects0, target);
                        }
                        else if (targetTypeComponent.type == TargetType.friend && team0.team == team1.team)
                        {
                            SkeletonBounds box0 = animationLayersComponent0.animation.getSkeletonBounds(IntersectBoxeNames.attackBox);
                            SkeletonBounds box1 = animationLayersComponent1.animation.getSkeletonBounds(IntersectBoxeNames.hitBox);

                            IntersectsComponent intersects0 = entity0.getComponent(IntersectsComponent.class);

                            intersect(box0, box1, intersects0, target);
                        }
                        else if (targetTypeComponent.type == TargetType.all)
                        {
                            SkeletonBounds box0 = animationLayersComponent0.animation.getSkeletonBounds(IntersectBoxeNames.attackBox);
                            SkeletonBounds box1 = animationLayersComponent1.animation.getSkeletonBounds(IntersectBoxeNames.hitBox);

                            IntersectsComponent intersects0 = entity0.getComponent(IntersectsComponent.class);

                            intersect(box0, box1, intersects0, target);
                        }
                    }
                }
            }
        }
    }

    private void intersect(SkeletonBounds box0, SkeletonBounds box1, IntersectsComponent intersects0, Entity target)
    {
        if (box0 != null && box1 != null && box0.aabbIntersectsSkeleton(box1))
        {
            if (intersects0.intersectsMap.containsKey(IntersectBoxeNames.attackBox_hitBox))
            {
                float maxX = Math.min(box0.getMaxX(), box1.getMaxX());
                float minX = Math.max(box0.getMinX(), box1.getMinX());
                float maxY = Math.min(box0.getMaxY(), box1.getMaxY());
                float minY = Math.max(box0.getMinY(), box1.getMinY());

                float x = (maxX + minX) / 2;
                float y = (maxY + minY) / 2;

                intersects0.intersectsMap.get(IntersectBoxeNames.attackBox_hitBox).add(target, x, y);
            }
        }
    }
}
