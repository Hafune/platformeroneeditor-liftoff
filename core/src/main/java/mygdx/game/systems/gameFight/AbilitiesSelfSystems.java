package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.PooledEngine;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.AbilitiesComponentSystems.*;
import org.jetbrains.annotations.NotNull;

public class AbilitiesSelfSystems extends MyAbstractSystem
{
    public AbilitiesSelfSystems(@NotNull PooledEngine engine) {
        super(engine);
    }

//    public AbilitiesSelfSystems()
//    {
//        super();
//        enableUpdateAlways();

//        addSystem(SelfMovingImpSystem.getInstance());
//        addSystem(SelfCharacterImpSystem.getInstance());
//        addSystem(SelfRoundTrajectoryImpSystem.getInstance());
//        addSystem(SelfOtherImpSystem.getInstance());
//        addSystem(SelfDefMaxMoveSpeedSystem.getInstance());
//
//
//        addSystem(AbilitiesRemovableComponentRemoveSystem.getInstance());
//    }
}
