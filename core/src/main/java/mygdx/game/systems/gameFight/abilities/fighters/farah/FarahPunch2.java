package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Event;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.audio.FightSounds;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIComponent;
import mygdx.game.systems.gameFight.abilities.FightAbilitiesIIParamMap;
import mygdx.game.systems.gameFight.abilities.Impacts;
import mygdx.game.systems.gameFight.abilities.presetAbilities.CharacterGroundAttack;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;
import mygdx.game.components.gameFight.selfComponents.SelfOtherImpComponent;
import mygdx.game.systems.gameFight.FightCameraSystem;

public class FarahPunch2 extends CharacterGroundAttack
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_punch2");

    private final SelfOtherImpComponent selfOtherImpComponent = new SelfOtherImpComponent(5, 0).setDefTimer(4);
    private final SelfCharacterImpComponent selfForwardImpComponent = new SelfCharacterImpComponent(5, 0).setDefTimer(4);

    public FarahPunch2(Entity entity)
    {
        super(entity,
                data,
                Impacts.weakGroundStrike.getImpacts());

        setupComponentList.add(new SelfCharacterImpComponent(-2, 0).setDefTimer(2));

        useInAbility = FarahPunch1.class;

        FightAbilitiesIIComponent param = FightAbilitiesIIParamMap.getInstance().getPropertyFor(this);
        param.attackPriority = 1;
        param.minAttackRange = 0;
        param.maxAttackRange = 60;

        soundHitEffect = FightSounds.IMPT_slappy_punch_6;
    }

    @Override
    public void update()
    {
        super.update();
//        if (!intersectsComponent.getCheckedEntities().isEmpty()) FightCameraSystem.getInstance().crush(entity, 0, 3, 10f, 3);
    }

    @Override
    public void superEvent(AnimationState.TrackEntry trackEntry, Event event)
    {
        super.superEvent(trackEntry, event);
        if (event.getData().getName().equals("forwardImp"))
        {
            addRemovableComponent(selfForwardImpComponent);
            addRemovableComponent(selfOtherImpComponent);
        }
    }
}
