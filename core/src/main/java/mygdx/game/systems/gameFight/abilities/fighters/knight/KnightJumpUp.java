package mygdx.game.systems.gameFight.abilities.fighters.knight;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractJumpUp;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class KnightJumpUp extends AbstractJumpUp
{
    private static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Knight +
                    "Jump/Jump"
    );

    public KnightJumpUp(Entity entity)
    {
        super(entity);
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
    }
}
