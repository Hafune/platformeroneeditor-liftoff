package mygdx.game.systems.gameFight.DragonBoneClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.SkeletonBounds;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.MyPoolUtil;
import mygdx.game.systems.gameFight.abilities.presetAbilities.IntersectBoxeNames;

import java.util.HashMap;
import java.util.HashSet;

public final class AnimationLayers
{
    private static final HashMap<String, HashSet<AnimationLayers>> pool = new HashMap<>();

    private final boolean loop;
    private final String defAnim;
    private final SkeletonData data;
    private boolean flipX = false;
    private final DragonBoneAnimation renderedAnim;
    private final HashMap<String, DragonBoneAnimation> map = new HashMap<>();

    public AnimationLayers(SkeletonData data, String defAnim, boolean loop, AnimationState.AnimationStateListener listener)
    {
        this.data = data;
        this.defAnim = defAnim;
        this.loop = loop;

        for (int i = 0; i < data.getAnimations().size; i++)
        {
            String name = data.getAnimations().get(i).getName();
            if (name.equals(defAnim)) map.put(name, new DragonBoneAnimation(data, name, loop, listener));
            else map.put(name, new DragonBoneAnimation(data, name, loop, null));
        }
        renderedAnim = map.get(defAnim);
    }

    public void setFlipX(boolean flipX)
    {
        if (this.flipX == flipX) return;
        for (DragonBoneAnimation animation : map.values())
        {
            animation.setFlipX(flipX);
        }
        this.flipX = flipX;
    }

    public void setAngle(float angle)
    {
        for (DragonBoneAnimation animation : map.values())
        {
            animation.setAngle(angle);
        }
    }

    public void restartAnimation()
    {
        for (DragonBoneAnimation animation : map.values())
        {
            animation.restartAnimation();
        }
    }

    public AnimationLayers resetAnimationLayers(AnimationState.AnimationStateListener listener)
    {
        flipX = false;
        for (DragonBoneAnimation animation : map.values())
        {
            animation.resetAnimation(listener);
        }
        return this;
    }

    public void draw(Batch batch, float x, float y, float time)
    {
        transform(x, y, time);
        renderedAnim.draw(batch);
    }

    public void transform(float x, float y, float time)
    {
        for (DragonBoneAnimation animation : map.values())
        {
            animation.transform(x, y, time);
        }
    }

    public SkeletonBounds getSkeletonBounds(IntersectBoxeNames name)
    {
        if (map.containsKey(name.name())) return map.get(name.name()).getSkeletonBounds();
        return null;
    }

    public static AnimationLayers getFromPool(SkeletonData data, String name, boolean loop, AnimationState.AnimationStateListener listener)
    {
        AnimationLayers layers = MyPoolUtil.getFromPoolMap(data.getHash() + name + loop, pool);
        return layers == null ? new AnimationLayers(data, name, loop, listener) : layers.resetAnimationLayers(listener);
    }

    public void putToHash()
    {
        String key = data.getHash() + defAnim + loop;
        MyPoolUtil.putToPoolMap(key, this, pool);
    }
}
