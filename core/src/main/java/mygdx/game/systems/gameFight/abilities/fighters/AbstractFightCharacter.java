package mygdx.game.systems.gameFight.abilities.fighters;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.components.gameFight.AbilitiesListComponent;
import mygdx.game.components.gameFight.AbilityComponent;

public abstract class AbstractFightCharacter
{
    private final Entity entity = new Entity();
    private final AbilitiesListComponent list = new AbilitiesListComponent();
    private final AbilityComponent abilityComponent = new AbilityComponent();
    private AbstractAbility defAbility;

    public AbstractFightCharacter()
    {
        entity.add(list);
        entity.add(abilityComponent);
    }

//    public ICharacterLink reset()
//    {
//        abilityComponent.changeAbility(defAbility);
//        HitPointComponent pointComponent = entity.getComponent(HitPointComponent.class);
//        pointComponent.hp = pointComponent.maxHp;
//        return this;
//    }

    public void add(Component component)
    {
        entity.add(component);
    }

    public Entity getEntity()
    {
        return entity;
    }

    protected void addAbility(AbstractAbility ability)
    {
        list.abilities.add(ability);
    }

    public void setStartAbility(Class ability)
    {
        for (AbstractAbility a : list.abilities)
        {
            if (a.getClass() == ability)
            {
                abilityComponent.changeAbility(a);
                defAbility = a;
                return;
            }
        }
    }
}
