package mygdx.game.systems.gameFight.AbilitiesComponentSystems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.components.gameFight.selfComponents.RemovableComponent;
import mygdx.game.components.gameFight.selfComponents.SelfCharacterImpComponent;
import mygdx.game.components.gameFight.selfComponents.SelfOtherImpComponent;
import mygdx.game.components.gameFight.selfComponents.SelfRoundTrajectoryImpComponent;
import org.jetbrains.annotations.NotNull;

public class AbilitiesRemovableComponentRemoveSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> selfOtherImpComponent;
    private ImmutableArray<Entity> selfCharacterImpComponents;
    private ImmutableArray<Entity> selfRoundTrajectoryImpComponent;

    public AbilitiesRemovableComponentRemoveSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        selfOtherImpComponent = engine.getEntitiesFor(Family.all(SelfOtherImpComponent.class).get());
        selfCharacterImpComponents = engine.getEntitiesFor(Family.all(SelfCharacterImpComponent.class).get());
        selfRoundTrajectoryImpComponent = engine.getEntitiesFor(Family.all(SelfRoundTrajectoryImpComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        selfOtherImpComponent.forEach(entity -> updateTimer(entity, entity.getComponent(SelfOtherImpComponent.class)));
        selfCharacterImpComponents.forEach(entity -> updateTimer(entity, entity.getComponent(SelfCharacterImpComponent.class)));
        selfRoundTrajectoryImpComponent.forEach(entity -> updateTimer(entity, entity.getComponent(SelfRoundTrajectoryImpComponent.class)));
    }

    private void updateTimer(Entity entity, RemovableComponent component)
    {
        if (component.timer > 0)
        {
            component.timer--;
            if (component.timer == 0)
            {
                entity.remove(component.getClass());
            }
        }
    }
}
