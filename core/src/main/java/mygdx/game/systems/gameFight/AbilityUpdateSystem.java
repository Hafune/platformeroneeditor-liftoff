package mygdx.game.systems.gameFight;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import mygdx.game.audio.SoundPlayer;
import mygdx.game.systems.MyAbstractSystem;
import mygdx.game.systems.gameFight.abilities.AbstractAbility;
import mygdx.game.systems.gameFight.abilities.IntersectsSet;
import mygdx.game.systems.gameFight.abilities.effects.AnimationEffect;
import mygdx.game.systems.gameFight.abilities.effects.MyParticleEffect;
import mygdx.game.systems.gameFight.abilities.presetAbilities.SpellBuilder;
import mygdx.game.components.gameFight.AbilityComponent;
import mygdx.game.components.gameFight.IntersectTargetComponent;
import mygdx.game.components.gameFight.ImpactComponents.IImpact;
import mygdx.game.components.gameFight.ImpactComponents.ImpactHitFreezeComponent;
import mygdx.game.components.gameFight.IntersectsComponent;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;

public class AbilityUpdateSystem extends MyAbstractSystem
{
    private ImmutableArray<Entity> entities;

    public AbilityUpdateSystem(@NotNull PooledEngine engine) {
        super(engine);
    }

    @Override
    public void addedToEngine(Engine engine)
    {
        entities = engine.getEntitiesFor(Family.all(AbilityComponent.class).get());
    }

    @Override
    public void update(float deltaTime)
    {
        for (Entity entity : entities)
        {
            AbstractAbility ability = entity.getComponent(AbilityComponent.class).ability;
            ability.update();

            IntersectsComponent intersectsComponent = entity.getComponent(IntersectsComponent.class);
            if (intersectsComponent != null)
            {
                HashSet<IntersectsSet.EntityHit> hits = intersectsComponent.getCheckedEntities();
                IImpact[] impacts = ability.getImpacts();

                hits.forEach(h -> {
                    IntersectTargetComponent component = h.getEntity().getComponent(IntersectTargetComponent.class);
                    component.parents.add(entity);
                    component.impacts.add(impacts);

//                    SoundPlayer.INSTANCE.play(ability.getSoundHitEffect());
                });

                AnimationEffect.buildAnimationEffectsFlipFromPosition(ability.getHitEffect(), hits, entity, 0);
                if (ability.getParticleEffect() != null) MyParticleEffect.buildParticleEffects(ability.getParticleEffect(), hits);
                if (!hits.isEmpty())
                {
                    if (ability.getSelfImpacts() != null)
                        Arrays.stream(ability.getSelfImpacts()).forEach(selfImpact -> ability.addRemovableComponent((Component) selfImpact));
                    if (ability.getSpells() != null)
                    {
                        SpellBuilder.getInstance().cast(entity, ability.getSpells());
                        ability.stop();
                    }
                }
            }

            ability.duration++;
            if (ability.duration > ability.defDuration && ability.defDuration > 0 && entity.getComponent(ImpactHitFreezeComponent.class) == null)
            {
                ability.stop();
            }
        }
    }
}
