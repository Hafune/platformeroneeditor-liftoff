package mygdx.game.systems.gameFight.abilities.fighters.farah;

import com.badlogic.ashley.core.Entity;
import com.esotericsoftware.spine.SkeletonData;
import mygdx.game.systems.gameFight.abilities.AbilitiesPackages;
import mygdx.game.systems.gameFight.abilities.AnimationLayersBuilder;
import mygdx.game.systems.gameFight.abilities.presetAbilities.AbstractJumpUp;
import mygdx.game.systems.gameFight.DragonBoneClasses.DragonBoneAnimation;

public class FarahJumpUp extends AbstractJumpUp
{
    public static final SkeletonData data = DragonBoneAnimation.buildSkeletonData(
            AbilitiesPackages.Farah +
                    "Farah_stand");

    public FarahJumpUp(Entity entity)
    {
        super(entity);
        startAnimation = AnimationLayersBuilder.buildAnimationLayers(data, "hitBox", true, null);
    }
}
