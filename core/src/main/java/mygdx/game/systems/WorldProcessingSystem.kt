package mygdx.game.systems

import mygdx.game.systems.gameWorld.*
import mygdx.game.systems.gameWorld.abilities.*

class WorldProcessingSystem(myEngine: MyEngine) : MyAbstractSystem(myEngine.engine) {

    var time = 0f
        private set

    override fun update(deltaTime: Float) {
        time += deltaTime
        time = if (time > 518400) 0f.also { time = it } else time
        super.update(deltaTime)
    }

    val animationCharacterSystem = AnimationCharacterSystem(myEngine)
    private val imageAnimationSystem = ImageAnimationSystem(engine)
    private val platformSystem = PlatformSystem(engine)
    private val applyAttachOffsetPositionSystem = ApplyAttachOffsetPositionSystem(engine)
    private val applyBodyPositionSystem = ApplyBodyPositionSystem(engine)
    private val buttonCommandSystem = ButtonCommandSystem(myEngine)
    private val slowDownSystem = SlowDownSystem(engine)
    private val updateTargetPositionSystem = UpdateTargetPositionSystem(myEngine)
    private val checkRouteToPointSystem = CheckRouteToPointSystem(engine)
    private val abilityInitializeSystem = AbilityInitializeSystem(engine)
    private val standAbilitySystem = StandAbilitySystem(myEngine)
    private val movingAbilitySystem = MovingAbilitySystem(myEngine)
    private val dashAbilitySystem = DashAbilitySystem(myEngine)
    private val abilityTimeIncrementSystem = AbilityTimeIncrementSystem(engine)
    private val repulsiveForceSystem = RepulsiveForceSystem(engine)
    val clickFindPathSystem = ClickFindPathSystem(myEngine)
    val clickInteractiveSystem = ClickInteractiveSystem(engine)
    private val clickRemoveSystem = ClickRemoveSystem(engine)
    private val interactRadiusSystem = InteractRadiusSystem(myEngine)
    private val interactClickSystem = InteractClickSystem(myEngine)
    private val interactButtonSystem = InteractButtonSystem(myEngine)
    private val triggerPositionSystem = TriggerPositionSystem(myEngine)
    private val awaitUserVariableSystem = AwaitUserVariableSystem(myEngine)
    private val teleportSystem = TeleportSystem(myEngine)
    private val userControllerUpdateSystem = UserControllerUpdateSystem(myEngine)
    private val userActivitySystem = UserActivitySystem(myEngine.engine)


    init {
        //ниже могут задаваться новые абилки для следующего кадра, срабатывают скрипты
        //обработка активности игрока
        addSystem(buttonCommandSystem)// назначение новых абилок игроком, открытие меню, если есть управление
        addSystem(userActivitySystem)// удаляет авто движение к цели если игрок проявил активность

        addSystem(clickFindPathSystem)
        addSystem(clickInteractiveSystem)
        addSystem(clickRemoveSystem)

        addSystem(interactClickSystem)
        addSystem(interactRadiusSystem)
        addSystem(interactButtonSystem)

        addSystem(userControllerUpdateSystem)

        addSystem(updateTargetPositionSystem)
        addSystem(checkRouteToPointSystem)

        addSystem(repulsiveForceSystem)//задаёт отталкивание персонажей друг от друга

        //триггеры областей, телепорты
        addSystem(triggerPositionSystem)
        addSystem(awaitUserVariableSystem)
        addSystem(teleportSystem)

        //==================================================

        //установка новой абилки и калбек для старой если есть
        addSystem(abilityInitializeSystem)

        //Обновление/изменение позиций привязанных компонентов, восстановление дефолтных характеристик
        addSystem(applyAttachOffsetPositionSystem)
        addSystem(platformSystem)
        addSystem(applyBodyPositionSystem)

        //Изменение характеристик, например замедление перемещения --------- ? наверно стоит переместить, нужно больше таких систем, чтобы проверить
        addSystem(slowDownSystem)

        //Применение абилки, с учётом изменённых характеристик // оказывается применение сил к боди
        addSystem(standAbilitySystem)
        addSystem(movingAbilitySystem)
        addSystem(dashAbilitySystem)

        //Обновление времени абилок
        addSystem(abilityTimeIncrementSystem)

        // обновление значений для анимации
        addSystem(animationCharacterSystem)
        addSystem(imageAnimationSystem)//-- под вопросом, тут могут выполняться скрипты, может стоит перенести систему выше

        //В самом конце обновляется мир box2d
        //происходит отрисовка

    }
}