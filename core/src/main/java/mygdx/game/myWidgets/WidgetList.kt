package mygdx.game.myWidgets

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window

class WidgetList<T : Actor>(
    val scrollPane: ScrollPane,
    title: String,
    skin: Skin,
    style: String,
) :
    Window(title, skin, style) {

    val contentTable = Table()
    var focusItem: T? = null

    init {
        clearListeners()

        contentTable.top().left().pad(10f)
        scrollPane.fadeScrollBars = false
        scrollPane.actor = contentTable
        add(scrollPane).padTop(titleLabel.height / 2).grow()
    }

    fun showItem(actor: T) {
        val p = Vector2(actor.x, actor.y)
        actor.localToActorCoordinates(scrollPane, p)
        scrollPane.scrollTo(p.x, p.y, actor.width, actor.height)
    }
}
