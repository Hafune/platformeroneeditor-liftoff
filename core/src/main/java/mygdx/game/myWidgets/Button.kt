package mygdx.game.myWidgets

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import ktx.actors.onClick


class Button(region: TextureRegion, val onClick: () -> Unit) :
    Button(TextureRegionDrawable(region)) {

    init {
        this.onClick {
            onClick()
        }
    }
}