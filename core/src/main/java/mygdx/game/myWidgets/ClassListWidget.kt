package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.kotcrab.vis.ui.widget.VisWindow
import mygdx.game.editor.ComponentWin
import mygdx.game.editor.EditorRoot
import mygdx.game.editor.FormAnnotationHandler
import mygdx.game.editor.formItems.FormItem
import mygdx.game.lib.MyClassForName
import kotlin.reflect.KClass

class ClassListWidget<T : Any>(
    private val editorRoot: EditorRoot,
    private var onChangeValue: ((target: KClass<T>, valName: String, value: Any) -> Unit)? = null,
    private val onRemove: ((T) -> Unit)? = null
) : Table() {

    @Suppress("UNCHECKED_CAST")
    fun setupOnChangeValues(onChangeValue: ((target: KClass<T>, valName: String, value: Any) -> Unit)) {
        this.onChangeValue = onChangeValue
        children.forEach { win ->
            win as ComponentWin<T>
            win.setupOnChangeValues(onChangeValue)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun addComponent(component: T, attachFormToComponent: Boolean) {
        val win = ComponentWin(editorRoot, component, attachFormToComponent, onRemove)
        onChangeValue?.let { win.setupOnChangeValues(onChangeValue!!) }

        add(win).grow().row()
        sort()
    }

    private fun sort() {
        val c = children.sortedBy { win ->
            win as VisWindow
            win.titleLabel.text.toString().lowercase()
        }
        clear()
        c.forEach {
            add(it).grow().row()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun buildComponents(): ArrayList<T> {
        val array = ArrayList<T>()
        children.forEach { win ->
            win as VisWindow
            val forms = arrayListOf(*win.children.filter { it is FormItem<*> }.toTypedArray()) as ArrayList<FormItem<*>>

            val model = MyClassForName.get<Any>(win.userObject as String)

            FormAnnotationHandler().fillModelFromForms(model, forms)

            array.add(model as T)
        }
        return array
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> buildComponent(kClass: KClass<T>): T {
        children.forEach { win ->
            if (win.userObject as String != kClass.qualifiedName) return@forEach
            win as VisWindow
            val forms = arrayListOf(*win.children.filter { it is FormItem<*> }.toTypedArray()) as ArrayList<FormItem<*>>

            val model = MyClassForName.get<Any>(win.userObject as String)

            FormAnnotationHandler().fillModelFromForms(model, forms)

            return model as T
        }
        throw Error("component ${kClass.simpleName} not fount")
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> setComponentValue(target: KClass<T>, valName: String, value: Any) {
        val forms = (children.first { win ->
            win.userObject == target.qualifiedName
        } as VisWindow).children.filter { it is FormItem<*> } as List<FormItem<Any>>
        forms.first { it.valName == valName }.value = value
    }

    fun setComponents(attachFormToComponent: Boolean, components: List<T>) {
        clear()
        components.forEach { addComponent(it, attachFormToComponent) }
    }
}