package mygdx.game.myWidgets

import mygdx.game.lib.UIKeyController

interface ITabUIElement {

    var isActive: Boolean
    val keyController: UIKeyController
}