package mygdx.game.myWidgets;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;

public class ItemLabel extends HorizontalGroup
{
    private int id = 0;

//    private Label board = new Label("", ScreenContainer.Companion.getGameSkin(), "game_menu_selection")
//    {
//        @Override
//        public void draw(Batch batch, float parentAlpha)
//        {
//            if (focus) super.draw(batch, parentAlpha);
//        }
//    };
    private MyImage image = new MyImage();
//    private Label name = new Label("", ScreenContainer.Companion.getGameSkin(), Styles.NoBorder.name());
//    private Label description = new Label("", ScreenContainer.Companion.getGameSkin(), Styles.NoBorder.name());
    private boolean focus = false;

    public ItemLabel(int id)
    {
        this.id = id;
//        addActor(board);
//        addActor(image);
//        addActor(name);
//        addActor(description);
        setFocus(false);


        image.clearListeners();
        image.setTouchable(Touchable.disabled);

//        name.clearListeners();
//        name.setTouchable(Touchable.disabled);
//        name.setScale(FontScaleSizes.ItemName.getScale());
//
//        description.clearListeners();
//        description.setTouchable(Touchable.disabled);
//        description.setScale(FontScaleSizes.ItemName.getScale());
    }

//    @Override
//    public boolean addListener(EventListener listener)
//    {
//        return board.addListener(listener);
//    }

    public int getId()
    {
        return id;
    }

    public void setIcon(TextureRegion region)
    {
        image.setRegion(region);
        calculate();
    }

    public void setItemName(String text)
    {
//        name.setText(text);
//        name.setWidth(name.getMinWidth());
//        name.setHeight(name.getMinHeight());
        calculate();
    }

    public void setDescription(String text)
    {
//        description.setText(text);
//        description.setWidth(description.getMinWidth());
//        description.setHeight(description.getMinWidth());
        calculate();
    }

    public void calculate()
    {
        calcFoo(image);
//        calcFoo(name);
//        calcFoo(description);
        image.setX(10);
//        name.setX(10 + image.getRight());
//        description.setX(getWidth() - description.getWidth() - 10);
//        MyActorMath.INSTANCE.copySize(board, this);
    }

    private void calcFoo(Actor a)
    {
        if (a != null) a.setY((getHeight() - a.getHeight()) / 2);
    }

    public boolean hasFocus()
    {
        return focus;
    }

    public void setFocus(boolean focus)
    {
        this.focus = focus;
    }
}
