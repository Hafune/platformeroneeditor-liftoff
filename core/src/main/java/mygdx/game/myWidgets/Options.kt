package mygdx.game.myWidgets

import com.badlogic.gdx.utils.Array
import ktx.actors.setKeyboardFocus
import mygdx.game.language.GameText
import mygdx.game.language.KeyWord
import mygdx.game.lib.MyActorMath
import mygdx.game.systems.MyEngine
import mygdx.game.uiMenu.UIAbstractMenuPage

class Options(private val myEngine: MyEngine) : UIAbstractMenuPage(myEngine) {
    private val listWidget = myEngine.widgets.lists.bahnschrift_32_background()

    var selection = -1
        private set

    override fun open(callback: (() -> Unit)?) {
        super.open(callback)
        listWidget.actor.setKeyboardFocus()
        MyActorMath.toCenterOfParent(listWidget.actor)
        listWidget.audio.mute = false
    }

    fun setOptions(vararg textKeyWord: KeyWord) {
        val text = Array<String>()
        for (key in textKeyWord) {
            text.add(GameText[key.name])
        }
        listWidget.actor.setItems(text)
    }

    override fun close() {
        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = null
        listWidget.audio.mute = true
        super.close()
    }

    private fun cancel() {
        selection = -1
        close()
    }

    private fun confirm() {
        selection = listWidget.actor.selectedIndex
        close()
    }

    init {
        addActor(listWidget.actor)
        listWidget.controller.onConfirm = { confirm() }
        listWidget.controller.onCancel = { cancel() }
    }
}