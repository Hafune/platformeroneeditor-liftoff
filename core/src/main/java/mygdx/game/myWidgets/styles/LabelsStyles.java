package mygdx.game.myWidgets.styles;

public enum  LabelsStyles
{
    NoBorder,
    game_menu,
    game_menu_selection,
    game_menu_no_border,
    game_menu_background,
    game_menu_description,
    game_menu_health_bar,
    game_menu_health_bar_back
}
