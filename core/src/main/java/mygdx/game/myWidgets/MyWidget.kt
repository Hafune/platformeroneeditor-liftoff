package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import mygdx.game.lib.UIKeyController
import mygdx.game.myWidgets.propertyes.MyAudio
import mygdx.game.systems.MyEngine

class MyWidget<T : Actor>(val actor: T, val controller: UIKeyController, myEngine: MyEngine) {
    val audio = MyAudio(myEngine)
}