package mygdx.game.myWidgets

import mygdx.game.language.GameText
import mygdx.game.lib.Object
import mygdx.game.lib.Object.getPropertyKey
import kotlin.reflect.KClass

class Option<T>(var label: String, var value: T?) {

    companion object {
        fun <T> buildFromObject(o: Any): Array<Option<T>> {
            return Object.getValuesMap<T>(o)
                .map { Option(label = GameText[getPropertyKey(o, "${it.value}")], value = it.value) }.toTypedArray()
        }

        fun buildFromJavaEnum(o: Array<*>): Array<Option<String>> {
            return o.map { Option(label = GameText["${it!!::class.qualifiedName!!}.$it"], value = it.toString()) }
                .sortedBy { it.label }
                .toTypedArray()
        }

        fun buildFromKClasses(vararg o: KClass<*>): Array<Option<KClass<*>>> {
            return o.map { Option(label = GameText[it.qualifiedName!!], value = it) }.sortedBy { it.label }
                .toTypedArray()
        }

        fun <T> buildFromMap(map: Map<String, T>): Array<Option<T>> {
            return map.map { Option(label = GameText[it.key], value = it.value) }.toTypedArray()
        }

        fun <T> buildFrom(vararg values: T): Array<Option<T>> {
            return values.map { Option(label = GameText["$it"], value = it) }.toTypedArray()
        }
    }

    override fun toString(): String {
        return label
    }
}