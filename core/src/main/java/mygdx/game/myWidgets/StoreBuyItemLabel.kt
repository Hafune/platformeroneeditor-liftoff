package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.kotcrab.vis.ui.widget.VisTable
import mygdx.game.dataBase.services.itemsService.ItemModel
import mygdx.game.icons.IconStorage
import mygdx.game.systems.MyEngine

class StoreBuyItemLabel(myEngine: MyEngine, iconStorage: IconStorage, val itemModel: ItemModel, capacity: Int) : VisTable() {

    init {
        val image = Image(iconStorage.buildById(itemModel.icon_id).region)
        add(image).left().padLeft(10f)
        add(myEngine.widgets.labels.default())
        add(myEngine.widgets.labels.bahnschrift_32(itemModel.price.toString())).expandX()
        add(myEngine.widgets.labels.bahnschrift_32(capacity.toString())).right().padLeft(10f)
    }
}