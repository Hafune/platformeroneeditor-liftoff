package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.kotcrab.vis.ui.widget.VisSlider

class SliderHorizontal(min: Float, max: Float, stepSize: Float, onChange: (value: Float) -> Unit) :
    VisSlider(min, max, stepSize, false) {

    init {
        addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                onChange(value)
            }
        })
    }
}
