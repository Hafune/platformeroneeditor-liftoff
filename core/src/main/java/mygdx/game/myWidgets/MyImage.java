package mygdx.game.myWidgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MyImage extends Image
{
    private final TextureRegionDrawable drawable = new TextureRegionDrawable();

    public MyImage()
    {

    }

    public MyImage(String path)
    {
        setRegion(new TextureRegion(new Texture(path)));
    }

    public MyImage(TextureRegion region)
    {
        setRegion(region);
    }

    public void setRegion(TextureRegion region)
    {
        drawable.setRegion(region);
        if (region != null) {
            setDrawable(drawable);
            setSize(region.getRegionWidth(), region.getRegionHeight());
        }
        else setDrawable(null);
    }

    public void setAlpha(float a)
    {
        Color c = getColor();
        c.a = a;
        setColor(c);
    }

    public TextureRegion getRegion()
    {
        return drawable.getRegion();
    }
}
