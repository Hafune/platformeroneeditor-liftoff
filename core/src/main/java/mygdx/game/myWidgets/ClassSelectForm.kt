package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.kotcrab.vis.ui.widget.VisTextButton
import ktx.actors.onClick
import ktx.collections.gdxArrayOf
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visSelectBox
import mygdx.game.lib.MyClassForName
import kotlin.reflect.KClass

class ClassSelectForm<T : Any>(
    private val container: ClassListWidget<T>,
    list: Array<KClass<T>>,
    private var onAdd: ((T) -> Unit)? = null
) :
    Table() {

    private val selector =
        scene2d.visSelectBox<Option<T>> {
            items = gdxArrayOf(*list.map { Option(it.simpleName!!, MyClassForName.get<T>(it)) }
                .toTypedArray())
        }

    private val addButton = VisTextButton("Add").apply {
        onClick {
            container.addComponent(selector.selected.value!!, false)
            onAdd?.invoke(selector.selected.value!!)
        }
    }

    init {
        val group = HorizontalGroup().apply {
            addActor(selector)
            addActor(addButton)
        }
        add(group).growX()
    }
}