package mygdx.game.myWidgets.propertyes

import mygdx.game.audio.SoundPath
import mygdx.game.systems.MyEngine

class MyAudio(private val myEngine: MyEngine) {
    var soundChange = defaultSoundChange
    var soundConfirm: SoundPath? = defaultSoundConfirm
    var soundCancel = defaultSoundCancel
    var mute = false
    val defaultSoundChange: SoundPath
        get() = SoundPath.Decision2
    val defaultSoundConfirm: SoundPath
        get() = SoundPath.Decision3
    val defaultSoundCancel: SoundPath?
        get() = null

    fun playChange() {
        if (!mute) myEngine.soundPlayer.play(soundChange)
    }

    fun playConfirm() {
        if (!mute) myEngine.soundPlayer.play(soundConfirm)
    }

    fun playCancel() {
        if (!mute) myEngine.soundPlayer.play(soundCancel)
    }
}