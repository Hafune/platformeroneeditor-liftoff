package mygdx.game.myWidgets

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.kotcrab.vis.ui.widget.VisImageButton

class VisImageButtonWrap(region: TextureRegion) : VisImageButton(TextureRegionDrawable(region))