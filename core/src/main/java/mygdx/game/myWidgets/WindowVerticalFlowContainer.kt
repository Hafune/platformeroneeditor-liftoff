package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import mygdx.game.editor.EditorWindow


open class WindowVerticalFlowContainer(
    winName: String,
    vararg actors: Actor,
) : EditorWindow(winName) {

    init {
        add(VerticalFlowContainer(*actors))
    }
}