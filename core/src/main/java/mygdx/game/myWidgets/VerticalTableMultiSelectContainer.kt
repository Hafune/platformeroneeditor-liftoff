package mygdx.game.myWidgets

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.ui.Container
import ktx.actors.onTouchEvent
import ktx.scene2d.KTableWidget
import ktx.scene2d.scene2d
import ktx.scene2d.table
import kotlin.math.max
import kotlin.math.min

class VerticalTableMultiSelectContainer(
    columns: Int,
    vararg actors: Actor,
    val onMultiSelect: (ArrayList<Actor>) -> Unit
) :
    Container<ScrollPaneVertical>() {

    private val downPoint = Vector2()
    private val upPoint = Vector2()
    var selectedElements = arrayListOf<Actor>()

    init {
        val table = scene2d.table {
            padLeft(10f).padRight(10f)
        }

        actors.forEachIndexed { index, it ->
            table.add(it)
            if ((index + 1) % columns == 0) {
                table.row()
            }
            it.addListener(object : InputListener() {
                override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                    downPoint.set(it.localToActorCoordinates((actor).children.first(), Vector2(x, y)))
                    return true
                }
            })
            it.onTouchEvent { event, x, y ->
                if (event.type == InputEvent.Type.touchUp) {
                    upPoint.set(it.localToActorCoordinates((actor).children.first(), Vector2(x, y)))
                    selectedElements = arrayListOf()
                    ((actor).children.first() as KTableWidget).cells.forEach { a ->
                        val x0 = min(downPoint.x, upPoint.x)
                        val x1 = max(downPoint.x, upPoint.x)
                        val y0 = min(downPoint.y, upPoint.y)
                        val y1 = max(downPoint.y, upPoint.y)
                        if (
                            x0 <= a.actorX + a.actorWidth &&
                            x1 >= a.actorX &&
                            y0 <= a.actorY + a.actorHeight &&
                            y1 >= a.actorY
                        ) {
                            selectedElements.add(a.actor)
                        }
                    }
                    if (selectedElements.size > 1) {
                        onMultiSelect(selectedElements)
                    }
                }
            }
        }

        actor = ScrollPaneVertical(table).also {
            it.fadeScrollBars = false
        }
        actor.listeners.removeIndex(0)
    }
}