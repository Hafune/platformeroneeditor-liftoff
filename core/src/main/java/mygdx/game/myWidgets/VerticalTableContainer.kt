package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Container
import ktx.scene2d.scene2d
import ktx.scene2d.table

class VerticalTableContainer(columns: Int, vararg actors: Actor) : Container<Actor>() {

    init {
        val table = scene2d.table{
            padLeft(10f).padRight(10f)
        }

        actors.forEachIndexed { index, it ->
            table.add(it)
            if ((index + 1) % columns == 0) {
                table.row()
            }
        }

        actor = ScrollPaneVertical(table)
    }
}