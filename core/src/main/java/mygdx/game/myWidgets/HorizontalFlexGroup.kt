package mygdx.game.myWidgets

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group

class HorizontalFlexGroup : Group() {

    override fun draw(batch: Batch?, parentAlpha: Float) {
        children.forEachIndexed { index, actor ->
            val percent = index.toFloat() / (children.size - 1)
            actor.x = width * percent - (actor.width * percent)
            actor.y = 0 - actor.height / 2
        }
        super.draw(batch, parentAlpha)
    }
}