package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.kotcrab.vis.ui.layout.FlowGroup

open class VerticalFlowContainer(vararg actors: Actor) : Container<Actor>() {

    init {
        val flowGroup = FlowGroup(false)

        actors.forEach { flowGroup.addActor(it) }

        actor = ScrollPaneVertical(flowGroup)
    }
}