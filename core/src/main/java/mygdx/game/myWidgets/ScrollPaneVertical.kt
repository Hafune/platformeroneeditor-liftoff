package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.kotcrab.vis.ui.widget.VisScrollPane

class ScrollPaneVertical(actor: Actor, height: Float? = null) : VisScrollPane(actor) {

    init {
        fadeScrollBars = false
        setOverscroll(false, false)
        setScrollingDisabled(true, false)

        height?.run { setSize(prefWidth, height) }
        addListener(object : InputListener() {
            override fun mouseMoved(event: InputEvent?, x: Float, y: Float): Boolean {
                stage.scrollFocus = this@ScrollPaneVertical
                return true
            }
        })
    }
}