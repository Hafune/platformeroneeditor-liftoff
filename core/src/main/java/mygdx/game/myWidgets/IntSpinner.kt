package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner

class IntSpinner(
    default: Int,
    min: Int,
    max: Int,
    stepSize: Int,
    label: String = "int spinner",
    onChange: ((value: Int) -> Unit)? = null
) :
    Spinner(label, IntSpinnerModel(default, min, max, stepSize)) {

    var value: Int
        set(value) {
            intSpinnerModel.value = value
        }
        get() = intSpinnerModel.value

    private val intSpinnerModel = this.model as IntSpinnerModel

    init {
        addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                onChange?.invoke(intSpinnerModel.value)
            }
        })
    }
}
