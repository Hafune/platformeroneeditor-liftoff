package mygdx.game.myWidgets

import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.utils.TimeUtils
import com.kotcrab.vis.ui.widget.VisSelectBox
import com.kotcrab.vis.ui.widget.VisTable
import ktx.actors.onChange
import ktx.actors.onClick
import ktx.actors.onKeyDownEvent
import ktx.actors.onKeyboardFocusEvent
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import ktx.scene2d.scene2d
import ktx.scene2d.vis.visSelectBox
import ktx.scene2d.vis.visTextField
import kotlin.math.max
import kotlin.math.min

open class SearchableListWithSelectBox<T>(list: Array<Option<T>>, onSelect: (value: T?) -> Unit) : VisTable() {

    //    private lateinit var kVisList: KVisList<Option<T>>
    private lateinit var selectBox: VisSelectBox<Option<T>>

    init {

        val textField = scene2d.visTextField {
            onKeyboardFocusEvent { e ->
                if (e.isFocused) {
                    selectBox
                }
            }
            onChange {
                if (text == "") {
                    selectBox.items = gdxArrayOf(*list)
                } else {
                    val arr = GdxArray<Option<T>>()
                    list.forEach { if (it.toString().contains(text, true)) arr.add(it) }
                    selectBox.items = arr
                }
            }
            onKeyDownEvent { event: InputEvent, keyCode: Int ->
                event.stop()
                if (selectBox.items.size == 0) return@onKeyDownEvent
                when (keyCode) {
                    Input.Keys.DOWN -> {
                        selectBox.selectedIndex = min(selectBox.selectedIndex + 1, selectBox.items.size - 1)
                    }
                    Input.Keys.UP -> {
                        selectBox.selectedIndex = max(selectBox.selectedIndex - 1, 0)
                    }
                    Input.Keys.ENTER -> {
                        onSelect(selectBox.selected.value)
                    }
                    Input.Keys.NUMPAD_ENTER -> {
                        onSelect(selectBox.selected.value)
                    }
                }
            }
        }

        add(textField).top().left()
        row()

        //DOUBLE CLICK
        selectBox = scene2d.visSelectBox {
            maxListCount = 8
            items = gdxArrayOf(*list)
            var clickTime: Long = 0
            onClick {
                if (TimeUtils.timeSinceMillis(clickTime) < 300 && selectBox.items.size > 0) {
                    onSelect(selectBox.selected.value)
                }
                clickTime = TimeUtils.millis()
                textField.focusField()
            }
        }
        add(selectBox).top().left().expand()
    }
}