@file:Suppress("SpellCheckingInspection", "FunctionName", "unused", "SameParameterValue")

package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.List
import com.rafaskoberg.gdx.typinglabel.TypingLabel
import com.ray3k.stripe.scenecomposer.SceneComposerStageBuilder
import mygdx.game.lib.*
import mygdx.game.systems.MyEngine

class InteractiveWidgetFactory(myEngine: MyEngine, val skin: Skin) {
    class ImageButtons(private val skin: Skin, private val myEngine: MyEngine) {
        fun arrow_prev() = create("arrow_prev")
        fun arrow_next() = create("arrow_next")
        fun volume() = create("volume")
        fun help() = create("help")
        fun config() = create("config")
        fun auto() = create("auto")
        fun close() = create("close")
        fun load() = create("load")
        fun log() = create("log")
        fun menu() = create("menu")
        fun qload() = create("qload")
        fun qsave() = create("qsave")
        fun save() = create("save")
        fun screen() = create("screen")
        fun share() = create("share")
        fun skip() = create("skip")
        fun tips() = create("tips")
        fun title() = create("title")
        fun voice() = create("voice")
        fun button_back() = create("button_back")

        private fun create(style: String): MyWidget<ImageButton> {
            val actor = ImageButton(skin, style)
            actor.addListener(MyMouseMoveFocusListener())
            return MyWidget(actor, UIKeyController(actor), myEngine)
        }
    }

    class Lists(private val skin: Skin, private val myEngine: MyEngine) {
        fun bahnschrift_32_background() = create("bahnschrift_32_background")
        fun bahnschrift_32() = create("bahnschrift_32")

        private fun create(style: String): MyWidget<List<String>> {
            val actor = List<String>(skin, style)
            actor.addListener(MyMouseMoveFocusListener())
            return MyWidget(actor, UIListController(actor), myEngine)
        }
    }

    class Labels(private val skin: Skin) {
        fun default(text: String = "") = create(text, "default")
        fun bahnschrift_32(text: String = "") = create(text, "bahnschrift_32")
        fun bahnschrift_28(text: String = "") = create(text, "bahnschrift_28")
        fun bahnschrift_28_character_card_name(text: String = "") = create(text, "bahnschrift_28_character_card_name")
        fun bahnschrift_22(text: String = "") = create(text, "bahnschrift_22")
        fun bahnschrift_22_bold(text: String = "") = create(text, "bahnschrift_22_bold")
        fun bahnschrift_28_hitpoint(text: String = "") = create(text, "bahnschrift_28_hitpoint")
        fun bahnschrift_22_max_hitpoint(text: String = "") = create(text, "bahnschrift_22_max_hitpoint")
        fun touch_region_background(text: String = "") = create(text, "touch_region_background")

        private fun create(text: String = "", style: String): Label {
            return Label(text, skin, style)
        }
    }

    class TypingLabels(private val skin: Skin) {
        fun default(text: String = "") = create(text, "default")
        fun bahnschrift_32(text: String = "") = create(text, "bahnschrift_32")
        fun touch_region_background(text: String = "") = create(text, "touch_region_background")

        private fun create(text: String = "", style: String): TypingLabel {
            return TypingLabel(text, skin, style)
        }
    }

    class ScrollPanes(private val skin: Skin) {
        fun default(actor: Actor? = null) = create(actor, "default")
        fun n(actor: Actor? = null) = create(actor, "defaulta")

        private fun create(actor: Actor?, style: String): ScrollPane {
            return ScrollPane(actor, skin, style)
        }
    }

    class MyItemScrollPanes(private val skin: Skin, private val myEngine: MyEngine) {
        fun <T : Actor> default() = create<T>("default")
        fun <T : Actor> n() = create<T>("defaulta")

        private fun <T : Actor> create(style: String): MyWidget<MyItemScrollPane<T>> {
            val widget = MyItemScrollPane<T>(myEngine, skin, style)
            return MyWidget(widget, UIKeyController(widget), myEngine)
        }
    }

    class MyWidgetLists(private val skin: Skin, private val myEngine: MyEngine) {
        fun <T : Actor> default(title: String) = create<T>(title, "default", "default")
        fun <T : Actor> n(title: String) = create<T>(title, "defaulta", "defaulta")

        private fun <T : Actor> create(
            title: String,
            style: String,
            scrollStyle: String
        ): MyWidget<WidgetList<T>> {
            val widget = WidgetList<T>(ScrollPane(null, skin, scrollStyle), title, skin, style)
            return MyWidget(widget, UIWidgetListController(widget), myEngine)
        }
    }

    class MyWidgetListVertical(private val myWidgetLists: MyWidgetLists) {
        fun <T : Actor> default(title: String) = create<T>(myWidgetLists.default(title))
        fun <T : Actor> n(title: String) = create<T>(myWidgetLists.n(title))

        private fun <T : Actor> create(
            widgetList: MyWidget<WidgetList<T>>
        ): MyWidget<WidgetList<T>> {
            val s = widgetList.actor.scrollPane
            s.setScrollingDisabled(true, false)
            s.setOverscroll(false, false)

            return widgetList
        }
    }

    class TextButtons(private val skin: Skin, private val myEngine: MyEngine) {
        fun default(text: String = "") = create(text, "default")
        fun n(text: String = "") = create(text, "defaulta")

        private fun create(text: String = "", style: String): MyWidget<TextButton> {
            val button = TextButton(text, skin, style)
            return MyWidget(button, UIKeyController(button), myEngine)
        }
    }

    class Sliders(private val skin: Skin, private val myEngine: MyEngine) {
        fun default_horizontal(min: Float, max: Float, stepSize: Float) =
            create(min, max, stepSize, false, "default-horizontal")

        fun default_vertical(min: Float, max: Float, stepSize: Float) =
            create(min, max, stepSize, true, "default-vertical")

        private fun create(
            min: Float,
            max: Float,
            stepSize: Float,
            vertical: Boolean,
            style: String
        ): MyWidget<Slider> {
            val actor = Slider(min, max, stepSize, vertical, skin, style)
            actor.addListener(MyMouseMoveFocusListener())
            return MyWidget(actor, UISliderController(actor), myEngine)
        }
    }

    class ProgressBars(private val skin: Skin) {
        fun default_horizontal(min: Float, max: Float, stepSize: Float) =
            create(min, max, stepSize, false, "default-horizontal")

        fun default_vertical(min: Float, max: Float, stepSize: Float) =
            create(min, max, stepSize, true, "default-vertical")

        fun gauge_horizontal(min: Float, max: Float, stepSize: Float) =
            create(min, max, stepSize, false, "gauge_horizontal")

        private fun create(min: Float, max: Float, stepSize: Float, vertical: Boolean, style: String): ProgressBar {
            return ProgressBar(min, max, stepSize, vertical, skin, style)
        }
    }

    class CharacterCards(private val myEngine: MyEngine) {
        fun default(model: CharacterCard.Model, height: Float) = create(model, height)

        private fun create(model: CharacterCard.Model, height: Float): MyWidget<CharacterCard> {
            val actor = CharacterCard(model, height, myEngine)
            actor.addListener(MyMouseMoveFocusListener())
            return MyWidget(actor, UIKeyController(actor), myEngine)
        }
    }

    class Tables(private val skin: Skin) {
        fun character_card_info() = create("sceneComposer/character_card_info.json")

        private fun create(filename: String): Table {
            return SceneComposerStageBuilder().build(skin, FileStorage[filename]).first()
        }
    }

    val imageButtons = ImageButtons(skin, myEngine)

    val lists = Lists(skin, myEngine)

    val labels = Labels(skin)

    val typingLabels = TypingLabels(skin)

    val scrollPanes = ScrollPanes(skin)

    val myItemScrollPanes = MyItemScrollPanes(skin, myEngine)

    val myWidgetLists = MyWidgetLists(skin, myEngine)

    val myWidgetListsVertical = MyWidgetListVertical(myWidgetLists)

    val textButtons = TextButtons(skin, myEngine)

    val sliders = Sliders(skin, myEngine)

    val progressBars = ProgressBars(skin)

    val characterCards = CharacterCards(myEngine)

    val tables = Tables(skin)
}