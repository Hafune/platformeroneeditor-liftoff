package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import mygdx.game.lib.UIWidgetListController
import mygdx.game.myWidgets.propertyes.MyAudio
import mygdx.game.systems.MyEngine

class MyWidgetContainer<T : Actor>(val actor: T, val controller: UIWidgetListController<T>, myEngine: MyEngine) {
    val audio = MyAudio(myEngine)
}