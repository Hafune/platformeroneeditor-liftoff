package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import ktx.actors.txt
import mygdx.game.language.GameText
import mygdx.game.lib.MyBackgroundDrawable
import mygdx.game.systems.MyEngine

class CharacterCard(model: Model, height: Float, myEngine: MyEngine) : Table() {

    private val cardWidth = 300f

    val table: Table = myEngine.widgets.tables.character_card_info()


    init {

        table.width = cardWidth
        table.y = height * .25f

        table.findActor<Label>("level").txt = GameText["level"]
        table.findActor<Label>("level_value").setText(model.level)
        table.findActor<Label>("name").txt = GameText[model.name]
        table.findActor<Label>("hit_point").setText(model.hitPoint)
        table.findActor<Label>("max_hit_point").setText(model.maxHitPoint)
        table.findActor<Label>("oz").txt = GameText["mygdx.game.myWidgets.CharacterCard.oz"]
        table.findActor<ProgressBar>("progress_bar").also {
            it.value = (it.maxValue / model.progressBarMax) * model.progressBar
        }
        table.findActor<Label>("i1").txt = model.playerMark
        table.findActor<Label>("handle").txt = model.control
        table.findActor<Label>("osv").txt = GameText["mygdx.game.myWidgets.CharacterCard.osv"]
        table.findActor<Label>("osv_value").setText(model.osvValue)

        val back = MyBackgroundDrawable(
            model.background,
            align = Align.bottom,
            parent = this,
            hoverImage = myEngine.guiImages.backgrounds.background_hover()
        )
        background = back

        val group = Group()
        group.width = cardWidth
        group.height = height
        add(group)

        group.addActor(table)
    }

    data class Model(
        val level: Int,
        val name: String,
        val hitPoint: Int,
        val maxHitPoint: Int,
        val progressBar: Float,
        val progressBarMax: Float,
        val playerMark: String,
        val control: String,
        val osvValue: Int,
        val background: String
    )
}