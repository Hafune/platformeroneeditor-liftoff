package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import mygdx.game.dataBase.services.itemsService.ItemModel
import mygdx.game.systems.MyEngine

class StoreSellItemLabel(myEngine: MyEngine, val itemModel: ItemModel) : Table() {

    val hoverBackground = myEngine.drawables.button_select_02_ten()

    init {
        val image = Image(myEngine.iconStorage.buildById(itemModel.icon_id).region)
        add(image).left()
        add(myEngine.widgets.labels.bahnschrift_32(itemModel.name)).left().padLeft(10f)
        add(myEngine.widgets.labels.bahnschrift_32("x")).right().padRight(10f).expandX()
        add(myEngine.widgets.labels.bahnschrift_32(itemModel.capacity.toString()).also {
            it.setAlignment(Align.right)
        }).width(50f).fillX().right()
    }
}