package mygdx.game.myWidgets

import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.utils.TimeUtils
import com.kotcrab.vis.ui.widget.VisTable
import ktx.actors.onChange
import ktx.actors.onClick
import ktx.actors.onKeyDownEvent
import ktx.scene2d.scene2d
import ktx.scene2d.vis.KVisList
import ktx.scene2d.vis.visList
import ktx.scene2d.vis.visScrollPane
import ktx.scene2d.vis.visTextField
import kotlin.math.max
import kotlin.math.min

class SearchableList<T>(list: Array<Option<T>>, var onSelect: ((value: T) -> Unit)? = null) :
    VisTable() {

    private lateinit var kVisList: KVisList<Option<T>>

    init {

        val textField = scene2d.visTextField {
            onChange {
                if (text == "") {
                    kVisList.setItems(*list)
                } else {
                    kVisList.setItems(*list.filter {
                        it.label.contains(
                            text,
                            true
                        ) || it.value.toString().contains(text, true)
                    }.toTypedArray())
                }
            }
            onKeyDownEvent { event: InputEvent, keyCode: Int ->
                event.stop()
                if (kVisList.items.size == 0) return@onKeyDownEvent
                when (keyCode) {
                    Input.Keys.DOWN -> {
                        kVisList.selectedIndex = min(kVisList.selectedIndex + 1, kVisList.items.size - 1)
                    }
                    Input.Keys.UP -> {
                        kVisList.selectedIndex = max(kVisList.selectedIndex - 1, 0)
                    }
                    Input.Keys.ENTER -> {
                        if (kVisList.selected != null) onSelect?.invoke(kVisList.selected.value!!)
                    }
                    Input.Keys.NUMPAD_ENTER -> {
                        if (kVisList.selected != null) onSelect?.invoke(kVisList.selected.value!!)
                    }
                }
            }
        }

        add(textField).top().left()
        row()

        //DOUBLE CLICK
        val pane = scene2d.visScrollPane {
            setOverscroll(false, false)
            setScrollingDisabled(true, false)
            visList<Option<T>> {
                kVisList = this
                setItems(*list)
                var clickTime: Long = 0
                onClick {
                    if (TimeUtils.timeSinceMillis(clickTime) < 300 && kVisList.items.size > 0) {
                        if (kVisList.selected != null) onSelect?.invoke(kVisList.selected.value!!)
                    }
                    clickTime = TimeUtils.millis()
                    textField.focusField()
                }
            }
        }
        add(pane).top().left().expand()
    }

    fun selected(): T? {
        return kVisList.selected?.value
    }
}