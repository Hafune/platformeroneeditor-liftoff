package mygdx.game.myWidgets

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.kotcrab.vis.ui.widget.VisTable
import ktx.actors.setKeyboardFocus
import mygdx.game.inputs.IMyButtonTarget
import mygdx.game.inputs.MyPadButtons
import mygdx.game.lib.UIKeyController
import mygdx.game.myWidgets.propertyes.MyAudio
import mygdx.game.systems.MyEngine

class MyItemScrollPane<T : Actor>(
    myEngine: MyEngine,
    skin: Skin,
    style: String,
    var onSelect: ((i: T) -> Unit)? = null,
    var onCancel: (() -> Unit)? = null
) :
    VisTable(), ITabUIElement, IMyButtonTarget {

    private val itemsTable = Table()
    private val contentContainer = Container(itemsTable)
    private val scroll = ScrollPane(contentContainer, skin, style)
    private val scrollContainer = Container(scroll)

    val items = ArrayList<T>()

    var currentItemLabel: T? = null
        private set

    val audio = MyAudio(myEngine)
    override val keyController = UIKeyController(this)
    var onDownLeft: (() -> Unit)? = { keyController.buttonDown(MyPadButtons.LEFT) }
    var onDownRight: (() -> Unit)? = { keyController.buttonDown(MyPadButtons.RIGHT) }

    override var isActive = false

    val backGround = myEngine.widgets.labels.default()

    init {
        scroll.fadeScrollBars = false
        scroll.setScrollingDisabled(true, false)
        scroll.setOverscroll(false, false)

        scroll.addListener(object : InputListener() {
            override fun mouseMoved(event: InputEvent, x: Float, y: Float): Boolean {
                stage.scrollFocus = scroll
                return true
            }
        })

        addActor(backGround)
        add(scrollContainer).pad(10f).top().left().expand()

        backGround.setFillParent(true)
        scroll.setFillParent(true)

        scroll.addListener(object : ClickListener() {
            override fun mouseMoved(event: InputEvent?, x: Float, y: Float): Boolean {
                if (itemsTable.children.size == 0) return false
                val index = itemsTable.getRow(y + scroll.maxY - scroll.scrollY)
                if (index in 0 until items.size) {
                    currentItemLabel = items[index]
                }
                return true
            }
        })
    }

    fun clearItems() {
        items.clear()
        refreshItems()
        setCurrent(null)
    }

    fun addItem(item: T) {
        items.add(item)
        if (currentItemLabel == null) currentItemLabel = item
        refreshItems()
    }

    fun removeItem(item: T) {
        items.remove(item)
        currentItemLabel = if (items.size > 0) items[0] else null
        refreshItems()
        currentItemLabel ?: keyController.changeAny()
    }

    private fun refreshItems() {
        itemsTable.clear()
        items.forEach {
            itemsTable.add(it).width(width - 20)
            itemsTable.row()
        }
        isActive = items.isNotEmpty()
    }

    private fun setCurrent(label: T?) {
        if (currentItemLabel === label) return
        currentItemLabel = label
        if (currentItemLabel != null) {
            val index = items.indexOf(currentItemLabel)
            audio.playChange()
            scroll.scrollTo(
                currentItemLabel!!.x,
                currentItemLabel!!.y - index * currentItemLabel!!.height,
                currentItemLabel!!.width,
                currentItemLabel!!.height
            )
        }
    }

    override fun buttonDown(button: MyPadButtons) {
        if (currentItemLabel != null) {
            val currentIndex = items.indexOf(currentItemLabel)
            when (button) {
                MyPadButtons.UP -> {
                    val index = if (currentIndex == 0) items.size - 1 else currentIndex - 1
                    setCurrent(items[index])
                }
                MyPadButtons.DOWN -> {
                    val index = if (currentIndex == items.size - 1) 0 else currentIndex + 1
                    setCurrent(items[index])
                }
                MyPadButtons.CROSS -> {
                    audio.playConfirm()
                    onSelect?.invoke(currentItemLabel!!)
                }
                else -> {
                }
            }
        }
        when (button) {
            MyPadButtons.CIRCLE -> {
                audio.playChange()
                onCancel?.invoke()
            }
            MyPadButtons.LEFT -> {
                onDownLeft?.invoke()
            }
            MyPadButtons.RIGHT -> {
                onDownRight?.invoke()
            }
            else -> {
            }
        }
    }

    fun setKeyBoardFocus() {
        setKeyboardFocus()
//        myEngine.myInputHandler.keyboardHandler0.iMyButtonTarget = this
        audio.playChange()
    }


}